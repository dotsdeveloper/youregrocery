package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Pwb
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardWhybuyBinding
import com.yes.youregrocery.session.SharedHelper
import kotlin.collections.ArrayList

class WhybuyAdapter(
    var fragment: ProductViewFragment,
    var context: Context,
    var list: ArrayList<Pwb>
) :
    RecyclerView.Adapter<WhybuyAdapter.ViewHolder>() {
    lateinit var productViewFragment: ProductViewFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardWhybuyBinding = CardWhybuyBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        productViewFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_whybuy,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.cardViewBgColor(holder.binding.card, SharedHelper(context).primaryColor, null)
        holder.binding.head.text = list[position].heading.toString()
        holder.binding.body.text = list[position].description.toString()

        if(list[position].file != null && !list[position].file.equals("")){
            UiUtils.loadImage(holder.binding.img,list[position].file)
        }
    }
}