package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.MyWalletActivity
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Criditaudit
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.databinding.CardWalletBinding
import kotlin.collections.ArrayList

class WalletAdapter(
    var activity: MyWalletActivity,
    var context: Context,
    var list: ArrayList<Criditaudit>
) :
    RecyclerView.Adapter<WalletAdapter.ViewHolder>() {
    private lateinit var myWalletActivity: MyWalletActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardWalletBinding = CardWalletBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        myWalletActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_wallet,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val amount = "${context.getString(R.string.amount)} ${":"} ${Constants.Common.CURRENCY}${list[position].amount.toString()}"
        val balance = "${context.getString(R.string.balance)} ${":"} ${Constants.Common.CURRENCY}${list[position].balance.toString()}"
        holder.binding.amount.text = amount
        holder.binding.balance.text = balance

        if(list[position].type.toString() == "1"){
            holder.binding.credit.text = context.getString(R.string.credit)
        }
        else {
            holder.binding.credit.text = context.getString(R.string.debit)
        }

        holder.binding.date.text = BaseUtils.getFormattedDateUtc(list[position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.DD)
        holder.binding.year.text = BaseUtils.getFormattedDateUtc(list[position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.YYYY)
        holder.binding.month.text = BaseUtils.getFormattedDateUtc(list[position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.MMMM)
        holder.binding.time.text = BaseUtils.getFormattedDateUtc(list[position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.HH_MM_AAA)
    }
}