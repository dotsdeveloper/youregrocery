package com.yes.youregrocery.adapter

import android.content.Context
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.responses.Quantities
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardQuantityProductsBinding
import kotlin.collections.ArrayList
import com.google.android.material.button.MaterialButton
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.network.ViewModel

class ProductQuantityAdapter(
    var activity: DashBoardActivity,
    var fragment: ProductViewFragment,
    var context: Context,
    var list: ArrayList<Quantities>,
    var product: Products
) :
    RecyclerView.Adapter<ProductQuantityAdapter.HomeHeaderViewHolder>() {
    var sharedHelper: SharedHelper? = null
    var viewModel: ViewModel? = null
    lateinit var dashBoardActivity:DashBoardActivity
    lateinit var productViewFragment: ProductViewFragment
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardQuantityProductsBinding = CardQuantityProductsBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        sharedHelper = SharedHelper(context)
        dashBoardActivity  = activity
        productViewFragment = fragment
        viewModel = ViewModelProvider(dashBoardActivity).get(ViewModel::class.java)
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_quantity_products,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        UiUtils.plusMinus(holder.binding.plus)
        UiUtils.plusMinus(holder.binding.minus)
        UiUtils.textViewTextColor(holder.binding.qty, SharedHelper(context).primaryColor, null)
        UiUtils.textViewBgTint(holder.binding.offer, SharedHelper(context).primaryColor,null)
        UiUtils.textViewBgTint(holder.binding.add, SharedHelper(context).primaryColor, null)

        if(position == 0){
            if(UiUtils.formattedValues(list[position].offer_price) == Constants.IntentKeys.AMOUNT_DUMMY){
                 productViewFragment.binding!!.price1.visibility = View.VISIBLE
                 productViewFragment.binding!!.price2.visibility = View.GONE
                 val price1 = "${Constants.Common.CURRENCY}${list[position].amount}"
                 productViewFragment.binding!!.price1.text = price1
                 UiUtils.textViewTextColor(productViewFragment.binding!!.price1,null,R.color.black)
             }
             else {
                 productViewFragment.binding!!.price1.visibility = View.VISIBLE
                 productViewFragment.binding!!.price2.visibility = View.VISIBLE
                 val price1 = "${Constants.Common.CURRENCY}${list[position].offer_price}"
                 val price2 = "${Constants.Common.CURRENCY}${list[position].amount}"
                 productViewFragment.binding!!.price1.text = price1
                 UiUtils.textViewTextColor(productViewFragment.binding!!.price1,null,R.color.black)
                 productViewFragment.binding!!.price2.text = price2
                 UiUtils.textViewTextColor(productViewFragment.binding!!.price2,null,R.color.red)
                 UiUtils.textviewCustomDrawable(productViewFragment.binding!!.price2,R.drawable.line_strike)
             }
        }


        val kg = "${list[position].quantity} ${list[position].product_unit!!.name}"
        holder.binding.kg.text = kg

        if(UiUtils.formattedValues(list[position].offer_price) == Constants.IntentKeys.AMOUNT_DUMMY){
            holder.binding.price1.visibility = View.VISIBLE
            val price1 = "${Constants.Common.CURRENCY}${list[position].amount}"
            holder.binding.price1.text = price1
            UiUtils.textViewTextColor(holder.binding.price1,null,R.color.black)
            holder.binding.price2.visibility = View.GONE
        }
        else {
            holder.binding.price1.visibility = View.VISIBLE
            holder.binding.price2.visibility = View.VISIBLE
            val price1 = "${Constants.Common.CURRENCY}${list[position].offer_price}"
            val price2 = "${Constants.Common.CURRENCY}${list[position].amount}"
            holder.binding.price1.text = price1
            UiUtils.textViewTextColor(holder.binding.price1,null,R.color.black)
            holder.binding.price2.text = price2
            UiUtils.textViewTextColor(holder.binding.price2,null,R.color.red)
            UiUtils.textviewCustomDrawable(holder.binding.price2,R.drawable.line_strike)
        }

        if(UiUtils.formattedValues(list[position].savedpercentage) != Constants.IntentKeys.AMOUNT_DUMMY){
            holder.binding.offer.visibility = View.VISIBLE
            val offer = "${list[position].savedpercentage.toString()}${"%"} ${context.getString(R.string.off)}"
            holder.binding.offer.text = offer
            if(position == 0){
                productViewFragment.binding!!.offerTxt.visibility = View.VISIBLE
                UiUtils.textViewBgTint(productViewFragment.binding!!.offerTxt, SharedHelper(context).primaryColor, null)
                productViewFragment.binding!!.offerTxt.text = offer
            }
        }
        else{
            holder.binding.offer.visibility = View.GONE
            if(position == 0){
                productViewFragment.binding!!.offerTxt.visibility = View.GONE
            }
        }

        if(BaseUtils.nullCheckerInt(product.display) == 1){
            holder.binding.addRemoveLinear.visibility = View.GONE
            holder.binding.availableLinear.visibility = View.VISIBLE
            val time = "${BaseUtils.getFormattedDate(product.available_from_time.toString(),"HH:MM:SS","hh:mm aa")} ${"-"} ${BaseUtils.getFormattedDate(product.available_to_time.toString(),"HH:MM:SS","hh:mm aa")}"
            holder.binding.time.text = time
        }
        else if(BaseUtils.nullCheckerInt(product.display) == 0){
            if(BaseUtils.nullCheckerInt(product.enquiry) == 1){
                holder.binding.addRemoveLinear.visibility = View.GONE
                holder.binding.availableLinear.visibility = View.GONE
                holder.binding.add.visibility = View.VISIBLE
                holder.binding.add.text = context.getString(R.string.enquiry)

                holder.binding.add.setOnClickListener{
                    DialogUtils.showEnquiryDialog(product.id!!,dashBoardActivity,context, object :
                        DialogCallBack {
                        override fun onPositiveClick(value: String) {
                            UiUtils.showSnack(holder.binding.root,value)
                        }
                        override fun onNegativeClick() {

                        } })
                }
            }
            else if(BaseUtils.nullCheckerInt(product.enquiry) == 0){
                holder.binding.addRemoveLinear.visibility = View.VISIBLE
                holder.binding.availableLinear.visibility = View.GONE
                holder.binding.add.visibility = View.GONE
                holder.binding.add.text = context.getString(R.string.add)
                val stockOption = BaseUtils.nullCheckerInt(sharedHelper!!.shopWebData.profile.stock_option)
                val maxQuantity = BaseUtils.nullCheckerInt(sharedHelper!!.shopWebData.profile.max_quantity)
                holder.binding.plus.setOnClickListener {
                    if(stockOption == 1){
                        if(holder.binding.qty.text.toString().toInt() > maxQuantity && maxQuantity != 0) {
                            UiUtils.showSnack(holder.binding.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                        }
                        else{
                            if((maxQuantity == 0 || maxQuantity > holder.binding.qty.text.toString().toInt()) && holder.binding.qty.text.toString().toInt() < list[position].stock.toString().toInt()){
                                plusClickStart(holder.binding,position)
                            }
                            else{
                                UiUtils.showSnack(holder.binding.root,context.getString(R.string.out_of_stock))
                            }
                        }
                    }
                    else if(stockOption == 0){
                        if(holder.binding.qty.text.toString().toInt() > maxQuantity && maxQuantity != 0){
                            UiUtils.showSnack(holder.binding.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                        }
                        else{
                            plusClickStart(holder.binding,position)
                        }
                    }
                }

                holder.binding.minus.setOnClickListener {
                    if(holder.binding.qty.text.toString().toInt() > 0 && holder.binding.qty.text.toString().toInt() != 1){
                        minusClickStart(holder.binding,position)
                    }
                    else if(holder.binding.qty.text.toString().toInt() == 1){
                        minusClickStart(holder.binding,position)
                    }
                }
            }
        }


        if(list[position].iscart!!){
            holder.binding.qty.text = list[position].qty.toString()
        }

        if(productViewFragment.isoutofstock){
            holder.binding.plus.isClickable = false
            holder.binding.minus.isClickable = false
            holder.binding.root.isClickable = false
            holder.binding.root.alpha = .5f
        }

    }

    private fun plusClickStart(binding: CardQuantityProductsBinding, position: Int){
        UiUtils.imgBtnLoadingEnd(binding.plus)
        viewModel?.addToCart(context,binding.qty.text.toString().toInt()+1,list[position].id,productViewFragment.productid)
            ?.observe(dashBoardActivity) {
                plusClickEnd(binding.plus)
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            if (binding.qty.text.toString()
                                    .toInt() == 0 && BaseUtils.nullCheckerStr(sharedHelper!!.shopWebData.afteraddcart)
                                    .isNotEmpty()
                            ) {
                                UiUtils.showSnack(binding.root,
                                    sharedHelper!!.shopWebData.afteraddcart!!)
                            }
                            if (!sharedHelper!!.loggedIn) {
                                sharedHelper!!.cartIdTemp = it.addcart_data!!.cart_id.toString()
                            }
                            list[position].iscart = true
                            list[position].cartid = list[position].id
                            list[position].qty = binding.qty.text.toString().toInt() + 1
                            val qty = "${(binding.qty.text.toString().toInt() + 1)}"
                            binding.qty.text = qty
                            notifyItemChanged(position)
                            dashBoardActivity.getCart()
                        }
                    }
                }
            }
    }

    private fun plusClickEnd(button: MaterialButton){
        UiUtils.imgBtnLoadingEnd(button, R.drawable.ic_baseline_add_24)
        productViewFragment.iscartadd = true
    }

    private fun minusClickStart(binding: CardQuantityProductsBinding, position: Int){
        UiUtils.imgBtnLoadingEnd(binding.minus)
        viewModel?.addToCart(context,binding.qty.text.toString().toInt()-1,list[position].id,productViewFragment.productid)
            ?.observe(dashBoardActivity) {
                minusClickEnd(binding.minus)
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            if (!sharedHelper!!.loggedIn && (binding.qty.text.toString()
                                    .toInt() - 1) != 0
                            ) {
                                sharedHelper!!.cartIdTemp = it.addcart_data!!.cart_id.toString()
                            }
                            binding.qty.text = (binding.qty.text.toString().toInt() - 1).toString()
                            dashBoardActivity.getCart()
                        }
                    }
                }
            }
    }

    private fun minusClickEnd(button: MaterialButton){
        UiUtils.imgBtnLoadingEnd(button, R.drawable.ic_baseline_remove_24)
        productViewFragment.iscartadd = true
    }
}