package com.yes.youregrocery.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.HomeFragment
import com.yes.youregrocery.fragment.CategoryListFragment
import com.yes.youregrocery.fragment.ProductListFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.CategoryList
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardPopularCategory1Binding
import com.yes.youregrocery.databinding.CardPopularCategory2Binding
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.databinding.CardPopularCategory3Binding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.BaseUtils

class CategoryListAdapter(
    var activity: DashBoardActivity,
    var view: Int,
    private var fragment1: HomeFragment?,
    private var fragment2: CategoryListFragment?,
    var context: Context,
    var list: ArrayList<CategoryList>
) :
    RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {
    lateinit var dashBoardActivity: DashBoardActivity
    private lateinit var homeFragment: HomeFragment
    private lateinit var categoryListFragment: CategoryListFragment
    inner class ViewHolder(view: View, type: Int) : RecyclerView.ViewHolder(view) {
        var binding1: CardPopularCategory1Binding? = null
        var binding2: CardPopularCategory2Binding? = null
        var binding3: CardPopularCategory3Binding? = null

        init {
            when (type) {
                1 -> {
                    binding1 = CardPopularCategory1Binding.bind(view)
                }
                2 -> {
                    binding2 = CardPopularCategory2Binding.bind(view)
                }
                3 -> {
                    binding3 = CardPopularCategory3Binding.bind(view)
                }
                4 -> {
                    binding2 = CardPopularCategory2Binding.bind(view)
                }
                5 -> {
                    binding2 = CardPopularCategory2Binding.bind(view)
                }
                6 -> {
                    binding2 = CardPopularCategory2Binding.bind(view)
                }
                else -> {
                    binding2 = CardPopularCategory2Binding.bind(view)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        dashBoardActivity = activity
        if(fragment1 != null){
            homeFragment = fragment1!!
        }
        if(fragment2 != null){
            categoryListFragment = fragment2!!
        }
        when (viewType) {
            1 -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_popular_category1,
                        parent,
                        false
                    ),1
                )
            }
            2 -> {
                val itemView: View = LayoutInflater.from(context).inflate(R.layout.card_popular_category2, parent, false)
                val height: Int = parent.measuredHeight / 3
                itemView.minimumHeight = height
                return ViewHolder(itemView,2)
            }
            3 -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_popular_category3,
                        parent,
                        false
                    ),3
                )
            }
            4 -> {
                val itemView: View = LayoutInflater.from(context).inflate(R.layout.card_popular_category2, parent, false)
                val height: Int = parent.measuredHeight / 2
                itemView.minimumHeight = height
                return ViewHolder(itemView,4)
            }
            5 -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_popular_category2,
                        parent,
                        false
                    ),5
                )
            }
            6 -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_popular_category2,
                        parent,
                        false
                    ),6
                )
            }
            else -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_popular_category2,
                        parent,
                        false
                    ),0
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (view) {
            1 -> {
                1
            }
            2 -> {
                2
            }
            3 -> {
                3
            }
            4 -> {
                4
            }
            5 -> {
                5
            }
            6 -> {
                6
            }
            else -> {
                0
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(view == 1){
            holder.binding1!!.text.text = list[position].name
            
            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding1!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding1!!.imageViewbanner)
            }

            holder.binding1!!.root.setOnClickListener {
                rootClick(position,false)
            }
        }
        else if(view == 2){
            holder.binding2!!.text.text = list[position].name
            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding2!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding2!!.imageViewbanner)
            }

            if(BaseUtils.nullCheckerStr(list[position].offer).isEmpty() || list[position].offer.equals("NA")){
                holder.binding2!!.offerLinear.visibility = View.GONE
            }
            else{
                holder.binding2!!.offerLinear.visibility = View.VISIBLE
                holder.binding2!!.offer.text = list[position].offer
            }

            holder.binding2!!.root.setOnClickListener {
                rootClick(position,false)
            }

            when {
                position == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 2 -> {
                    //end
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 2 -> {
                    //end
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
            }

            if(position == (list.size-1)  && list.size > 3){
                val observer: ViewTreeObserver = holder.binding2!!.mainRelative.viewTreeObserver
                observer.addOnGlobalLayoutListener {
                    val params: ViewGroup.LayoutParams = homeFragment.binding.popular.layoutParams
                    params.height = holder.binding2!!.mainRelative.height *2
                    homeFragment.binding.popular.layoutParams = params
                }
            }
        }
        else if(view == 3){
            holder.binding3!!.text.text = list[position].name

            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding3!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding3!!.imageViewbanner)
            }
            holder.binding3!!.root.setOnClickListener {
                rootClick(position,false)
            }
        }
        else if(view == 4){
            holder.binding2!!.text.text = list[position].name

            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding2!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding2!!.imageViewbanner)
            }

            if(BaseUtils.nullCheckerStr(list[position].offer).isEmpty() || list[position].offer.equals("NA")){
                holder.binding2!!.offerLinear.visibility = View.GONE
            }
            else{
                holder.binding2!!.offerLinear.visibility = View.VISIBLE
                holder.binding2!!.offer.text = list[position].offer
            }

            holder.binding2!!.root.setOnClickListener {
                rootClick(position,false)
            }

            when {
                position == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%2 == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%2 == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
            }
        }
        else if(view == 5){
            holder.binding2!!.text.text = list[position].name

            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding2!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding2!!.imageViewbanner)
            }

            if(BaseUtils.nullCheckerStr(list[position].offer).isEmpty() || list[position].offer.equals("NA")){
                holder.binding2!!.offerLinear.visibility = View.GONE
            }
            else{
                holder.binding2!!.offerLinear.visibility = View.VISIBLE
                holder.binding2!!.offer.text = list[position].offer
            }

            holder.binding2!!.root.setOnClickListener {
                rootClick(position,true)
            }

            when {
                position == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 2 -> {
                    //end
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 2 -> {
                    //end
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%3 == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
            }

            if(position == (list.size-1) && list.size > 3){
                val observer: ViewTreeObserver = holder.binding2!!.mainRelative.viewTreeObserver
                observer.addOnGlobalLayoutListener {
                    val params: ViewGroup.LayoutParams = homeFragment.binding.brandRecycler.layoutParams
                    params.height = holder.binding2!!.mainRelative.height *2
                    homeFragment.binding.brandRecycler.layoutParams = params
                }
            }
        }
        else if(view == 6){
            holder.binding2!!.text.text = list[position].name
           
            if(BaseUtils.nullCheckerStr(list[position].file).isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding2!!.imageViewbanner, list[position].file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding2!!.imageViewbanner)
            }
            if(BaseUtils.nullCheckerStr(list[position].offer).isEmpty() || list[position].offer.equals("NA")){
                holder.binding2!!.offerLinear.visibility = View.GONE
            }
            else{
                holder.binding2!!.offerLinear.visibility = View.VISIBLE
                holder.binding2!!.offer.text = list[position].offer
            }

            holder.binding2!!.root.setOnClickListener {
                rootClick(position,true)
            }

            when {
                position == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 1,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%2 == 0 -> {
                    //start
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(1, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
                position%2 == 1 -> {
                    //center
                    // val marginInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, context.getResources().getDisplayMetrics()).toInt()
                    val params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    params.setMargins(0, 0,1, 1)
                    holder.binding2!!.mainRelative.layoutParams = params
                }
            }
        }
    }

    private fun rootClick(position: Int,isBrand:Boolean) {
        if(isBrand){
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,list[position].name)
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,Constants.IntentKeys.LOAD_BRAND_P)
            args.putInt(Constants.IntentKeys.CID,list[position].id!!)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        else {
            if (list[position].subcategory_count == 0) {
                val args = Bundle()
                args.putString(Constants.IntentKeys.CNAME, list[position].name)
                args.putBoolean(Constants.IntentKeys.IS_COME, false)
                args.putString(Constants.IntentKeys.PAGE, Constants.IntentKeys.LOAD_CATEGORY_P)
                args.putInt(Constants.IntentKeys.CID, list[position].id!!)
                args.putInt(Constants.IntentKeys.S_C_ID, 0)
                dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
            }
            else {
                val args = Bundle()
                args.putString(Constants.IntentKeys.CNAME, list[position].name)
                args.putBoolean(Constants.IntentKeys.IS_COME, false)
                args.putString(Constants.IntentKeys.PAGE, Constants.IntentKeys.LOAD_SUBCATEGORY_LIST)
                args.putInt(Constants.IntentKeys.CID, list[position].id!!)
                args.putInt(Constants.IntentKeys.S_C_ID, 0)
                dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
            }
        }
    }
}