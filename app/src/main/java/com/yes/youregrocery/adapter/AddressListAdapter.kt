package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.CheckoutActivity
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DeliveryAddressActivity
import com.yes.youregrocery.responses.AddressData
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardListaddressBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import kotlin.collections.ArrayList

class AddressListAdapter(
    var view: Int,
    private var activity1: CheckoutActivity?,
    private var activity2: DeliveryAddressActivity?,
    var context: Context,
    var list: ArrayList<AddressData>?
) :
    RecyclerView.Adapter<AddressListAdapter.ViewHolder>() {
    private lateinit var checkoutActivity: CheckoutActivity
    private lateinit var deliveryAddressActivity: DeliveryAddressActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardListaddressBinding = CardListaddressBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(activity1 != null){
            checkoutActivity = activity1!!
        }
        if(activity2 != null){
            deliveryAddressActivity = activity2!!
        }
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_listaddress,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(view == 1){
            if(checkoutActivity.selectedDeliveryPosition == position){
                load(holder,position)
                selected(holder,position)
            }
            else {
                load(holder,position)
                unselected(holder,position)
            }
            if(list!!.size>1){
                val observer: ViewTreeObserver = holder.binding.card.viewTreeObserver
                observer.addOnGlobalLayoutListener {
                    val params: ViewGroup.LayoutParams = checkoutActivity.binding.swiperefresh.layoutParams
                    params.height = holder.binding.card.height *2
                    checkoutActivity.binding.swiperefresh.layoutParams = params
                }
            }
            else{
                val observer: ViewTreeObserver = holder.binding.card.viewTreeObserver
                observer.addOnGlobalLayoutListener {
                    val params: ViewGroup.LayoutParams = checkoutActivity.binding.swiperefresh.layoutParams
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    params.width = ViewGroup.LayoutParams.MATCH_PARENT
                    checkoutActivity.binding.swiperefresh.layoutParams = params
                }
            }
        }
        else if(view == 0){
            load(holder,position)
        }

    }

    fun load(holder: ViewHolder,position: Int) {
        holder.binding.name.text = list!![position].location_name
        holder.binding.location.text = list!![position].address
        holder.binding.pincode.text = list!![position].pincode
        holder.binding.deliveryhere.visibility = View.GONE
        if(list!![position].land_mark != null){
            holder.binding.landmark.visibility = View.VISIBLE
            holder.binding.landmark.text = list!![position].land_mark
        }
        else{
            holder.binding.landmark.visibility = View.GONE
        }
    }

    private fun selected(holder: ViewHolder, position: Int){
        UiUtils.textViewBgTint(holder.binding.deliveryhere, SharedHelper(context).primaryColor, null)
        holder.binding.deliveryhere.setTextColor(Color.WHITE)
        holder.binding.selected.visibility = View.VISIBLE
        UiUtils.setTextViewDrawableColor(holder.binding.pincode,SharedHelper(context).primaryColor,null)
        UiUtils.setTextViewDrawableColor(holder.binding.landmark,SharedHelper(context).primaryColor,null)
        UiUtils.textViewTextColor(holder.binding.name, SharedHelper(context).primaryColor, null)
        UiUtils.imageViewTint(holder.binding.selected, SharedHelper(context).primaryColor, null)
        holder.binding.card.background = UiUtils.getAngleDrawable(
            context,
            R.color.bg_color,
            floatArrayOf(5F,5F,5F,5F,5F,5F,5F,5F),
            1,
            SharedHelper(context).primaryColor
        )
        checkoutActivity.deliveryid = list!![position].id.toString()
        checkoutActivity.locationname = list!![position].location_name.toString()
        checkoutActivity.spincode = list!![position].pincode.toString()
        checkoutActivity.address = list!![position].address.toString()
        checkoutActivity.landmark = list!![position].land_mark.toString()
        checkoutActivity.isaddaddress = false
        checkoutActivity.isselectedaddress = true
        if(!list!![position].location_lat.equals(Constants.IntentKeys.UNDEFINED)){
            checkoutActivity.flat = list!![position].location_lat!!.toDouble()
            checkoutActivity.flng = list!![position].location_long!!.toDouble()
        }
        else{
            checkoutActivity.flat = 0.0
            checkoutActivity.flng = 0.0
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun unselected(holder: ViewHolder, position: Int){
        holder.binding.deliveryhere.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.WHITE, BlendModeCompat.SRC_ATOP)
        UiUtils.textViewTextColor(holder.binding.deliveryhere, SharedHelper(context).primaryColor, null)
        holder.binding.selected.visibility = View.INVISIBLE
        UiUtils.textViewTextColor(holder.binding.name, null, R.color.black)
        UiUtils.setTextViewDrawableColor(holder.binding.pincode,null,R.color.grey)
        UiUtils.setTextViewDrawableColor(holder.binding.landmark,null,R.color.grey)
        holder.binding.card.background = AppCompatResources.getDrawable(context,R.drawable.border_address)
        holder.binding.card.setOnClickListener {
            checkoutActivity.selectedDeliveryPosition = position
            notifyDataSetChanged()
        }
    }
}