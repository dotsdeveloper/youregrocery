package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.CouponsActivity
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.ListCouponData
import com.yes.youregrocery.databinding.CardCouponlistBinding
import android.content.Context.CLIPBOARD_SERVICE

import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.UiUtils

class CouponListAdapter(
    var activity: CouponsActivity,
    var context: Context,
    var list: ArrayList<ListCouponData>?
) :
    RecyclerView.Adapter<CouponListAdapter.ViewHolder>() {
    private var selectedPosition = -1
    private lateinit var couponsActivity: CouponsActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCouponlistBinding = CardCouponlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        couponsActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_couponlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @SuppressLint("ResourceType", "NotifyDataSetChanged","RecyclerView")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.textViewBgTint(holder.binding.discount, SharedHelper(context).primaryColor, null)
        UiUtils.buttonBgColor(holder.binding.copy, SharedHelper(context).primaryColor, null)
        holder.binding.expdate.text = BaseUtils.getFormattedDate(list!![position].expires_on!!, "yyyy-MM-dd","MMM dd,yyyy")

        if(list!![position].image != null && list!![position].image!!.isNotEmpty()){
            UiUtils.loadImage(holder.binding.img,list!![position].image)
        }

        if(list!![position].type == 1){
            val text = "${list!![position].name} ${list!![position].description} ${Constants.Common.CURRENCY}${list!![position].value}"
            holder.binding.text.text = text
        }
        else if(list!![position].type == 2){
            val text = "${list!![position].name} ${list!![position].description} ${list!![position].value}${"%"} ${context.getString(R.string.off)}"
            holder.binding.text.text = text
        }


        if(selectedPosition == position){
            holder.binding.copy.text = context.getString(R.string.copied)
            holder.binding.copy.isEnabled = false
        }
        else{
            holder.binding.copy.text = context.getString(R.string.copy)
            holder.binding.copy.isEnabled = true
            holder.binding.copy.setOnClickListener {
                val clipboard: ClipboardManager = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText(Constants.IntentKeys.LABEL, list!![position].name.toString())
                clipboard.setPrimaryClip(clip)
                Toast.makeText(context,context.getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show()
                selectedPosition = position
                notifyDataSetChanged()
            }
        }
    }
}