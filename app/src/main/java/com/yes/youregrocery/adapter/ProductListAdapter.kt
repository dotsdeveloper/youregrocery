package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.HomeFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.utils.UiUtils
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.fragment.ProductListFragment
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.`interface`.OnClickListener
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.*
import com.yes.youregrocery.fragment.SearchFragment

class ProductListAdapter(
    var activity: DashBoardActivity,
    var view: Int,
    private var fragment1: HomeFragment?,
    private var fragment2: ProductListFragment?,
    private var fragment3: SearchFragment?,
    var context: Context,
    var list: ArrayList<Products>,
    private var issearch: Boolean
) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>()
{
    lateinit var sharedHelper: SharedHelper
    var viewModel: ViewModel? = null
    lateinit var dashBoardActivity: DashBoardActivity
    private lateinit var homeFragment: HomeFragment
    lateinit var productListFragment: ProductListFragment
    private lateinit var searchFragment: SearchFragment
    var isscroolcomplete = false
    inner class ViewHolder(view: View,type: Int) : RecyclerView.ViewHolder(view) {
        var binding1: CardProducts1Binding? = null
        var binding2: CardListproductsBinding? = null
        init {
            when (type) {
                1 -> {
                    binding1 = CardProducts1Binding.bind(view)
                }
                2 -> {
                    binding2 = CardListproductsBinding.bind(view)
                }
                else -> {
                    binding1 = CardProducts1Binding.bind(view)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        sharedHelper = SharedHelper(context)
        dashBoardActivity = activity
        if(fragment1 != null){
            homeFragment = fragment1!!
        }
        if(fragment2 != null){
            productListFragment = fragment2!!
        }
        if(fragment3 != null){
            searchFragment = fragment3!!
        }
        viewModel = ViewModelProvider(dashBoardActivity).get(ViewModel::class.java)
        when (viewType) {
            1 -> {
                val itemView: View = LayoutInflater.from(context).inflate(R.layout.card_products1, parent, false)
                val height: Int = parent.measuredHeight / 3
                itemView.minimumHeight = height
                return ViewHolder(itemView,1)
            }
            2 -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_listproducts,
                        parent,
                        false
                    ),2
                )
            }
            else -> {
                return ViewHolder(
                    LayoutInflater.from(context).inflate(
                        R.layout.card_products1,
                        parent,
                        false
                    ),0
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if(view == 1){
            val params: ViewGroup.LayoutParams = holder.binding1!!.lin.layoutParams
            val params1: ViewGroup.LayoutParams = holder.binding1!!.imageViewbanner.layoutParams
            if(homeFragment.binding.newarrivalLinear.visibility == View.VISIBLE){
                params.width = homeFragment.binding.newarrivals.width /(3)
                params1.height = homeFragment.binding.newarrivals.width /(3)
            }
            else if(homeFragment.binding.bestsellerLinear.visibility == View.VISIBLE){
                params.width = homeFragment.binding.bestseller.width /(3)
                params1.height = homeFragment.binding.bestseller.width /(3)
            }

            holder.binding1!!.lin.layoutParams = params
            holder.binding1!!.imageViewbanner.layoutParams = params1

            holder.binding1!!.text.text = list[position].name
            UiUtils.textViewBgTint(holder.binding1!!.offer, SharedHelper(context).primaryColor, null)

            if(list[position].files!!.isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding1!!.imageViewbanner, list[position].files!![0])
            }
            else{
                UiUtils.loadDefaultImage(holder.binding1!!.imageViewbanner)
            }

            if(UiUtils.formattedValues(list[position].savedpercentage) != Constants.IntentKeys.AMOUNT_DUMMY){
                holder.binding1!!.offer.visibility = View.VISIBLE
                val offer = "${list[position].savedpercentage.toString()}${"%"} ${context.getString(R.string.off)}"
                holder.binding1!!.offer.text = offer
            }
            else{
                holder.binding1!!.offer.visibility = View.GONE
            }

            holder.binding1!!.root.setOnClickListener {
                val args = Bundle()
                args.putSerializable(Constants.IntentKeys.KEY, list[position])
                dashBoardActivity.addFragment(ProductViewFragment(), args, -1, -1)
            }

            if(list[position].quantities!!.size == 0){
                outOffStock(holder.binding1!!,null, value = true, isqty = false)
            }
            else{
                if(BaseUtils.nullCheckerInt(list[position].stock_available) == 0){
                    outOffStock(holder.binding1!!,null, value = true, isqty = true)
                }
                else{
                    outOffStock(holder.binding1!!,null, value = false, isqty = true)
                }
            }
        }
        else if(view == 2){
            UiUtils.plusMinus(holder.binding2!!.plus)
            UiUtils.plusMinus(holder.binding2!!.minus)
            UiUtils.textViewTextColor(holder.binding2!!.qtyPm, SharedHelper(context).primaryColor, null)
            UiUtils.buttonBgColor(holder.binding2!!.add, SharedHelper(context).primaryColor, null)
            UiUtils.textViewBgTint(holder.binding2!!.offer, SharedHelper(context).primaryColor, null)

            if(!issearch){
                loadMidBanner(holder.binding2!!,position)
            }

            holder.binding2!!.text.text = list[position].name
            if(BaseUtils.nullCheckerInt(list[position].brand_id) != 0 && list[position].brand.name.isNotEmpty()){
                holder.binding2!!.category.visibility = View.VISIBLE
                holder.binding2!!.category.text = list[position].brand.name
                // holder.binding2!!.category.text = list[position].category!!.name
            }
            else{
                holder.binding2!!.category.visibility = View.GONE
            }

            if(list[position].files!!.isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding2!!.imageViewbanner, list[position].files!![0])
            }
            else{
                UiUtils.loadDefaultImage(holder.binding2!!.imageViewbanner)
            }

            if(UiUtils.formattedValues(list[position].savedpercentage) != Constants.IntentKeys.AMOUNT_DUMMY){
                holder.binding2!!.offer.visibility = View.VISIBLE
                val offer = "${list[position].savedpercentage.toString()}${"%"} ${context.getString(R.string.off)}"
                holder.binding2!!.offer.text = offer
            }
            else{
                holder.binding2!!.offer.visibility = View.GONE
            }

            holder.binding2!!.imageViewbanner.setOnClickListener {
                movePage(position)
            }

            holder.binding2!!.text.setOnClickListener {
                movePage(position)
            }

            for(items in list[position].quantities!!){
                if(items.id == list[position].sqtyid){
                    val qty = "${items.quantity} ${items.product_unit!!.name}"
                    holder.binding2!!.qty.text = qty
                    if(UiUtils.formattedValues(items.offer_price) == Constants.IntentKeys.AMOUNT_DUMMY){
                        holder.binding2!!.price1.visibility = View.VISIBLE
                        holder.binding2!!.price2.visibility = View.GONE
                        val price1 = "${Constants.Common.CURRENCY}${items.amount}"
                        holder.binding2!!.price1.text = price1
                        holder.binding2!!.price1.setTextColor(context.resources.getColor(R.color.black,null))
                    }
                    else {
                        holder.binding2!!.price1.visibility = View.VISIBLE
                        holder.binding2!!.price2.visibility = View.VISIBLE
                        val price1 = "${Constants.Common.CURRENCY}${items.offer_price}"
                        holder.binding2!!.price1.text = price1
                        holder.binding2!!.price1.setTextColor(context.resources.getColor(R.color.black,null))
                        val price2 = "${Constants.Common.CURRENCY}${items.amount}"
                        holder.binding2!!.price2.text = price2
                        holder.binding2!!.price2.setTextColor(context.resources.getColor(R.color.red,null))
                        holder.binding2!!.price2.background = AppCompatResources.getDrawable(context,R.drawable.line_strike)
                    }

                    if(items.iscart == true){
                        holder.binding2!!.availableLinear.visibility = View.GONE
                        holder.binding2!!.add.visibility = View.GONE
                        holder.binding2!!.addRemoveLinear.visibility = View.VISIBLE
                        holder.binding2!!.qtyPm.text = items.qty.toString()
                        if(BaseUtils.nullCheckerInt(list[position].display) == 1){
                            holder.binding2!!.addRemoveLinear.visibility = View.GONE
                            holder.binding2!!.add.visibility = View.GONE
                            holder.binding2!!.availableLinear.visibility = View.VISIBLE
                            val time = "${BaseUtils.getFormattedDate(list[position].available_from_time.toString(),"HH:MM:SS","hh:mm aa")} ${"-"} ${BaseUtils.getFormattedDate(list[position].available_to_time.toString(),"HH:MM:SS","hh:mm aa")}"
                            holder.binding2!!.time.text = time
                        }
                        else if(BaseUtils.nullCheckerInt(list[position].display) == 0){
                            if(BaseUtils.nullCheckerInt(list[position].enquiry) == 1){
                                holder.binding2!!.addRemoveLinear.visibility = View.GONE
                                holder.binding2!!.availableLinear.visibility = View.GONE
                                holder.binding2!!.add.visibility = View.VISIBLE
                                holder.binding2!!.add.text = context.getString(R.string.enquiry)

                                holder.binding2!!.add.setOnClickListener{
                                    DialogUtils.showEnquiryDialog(list[position].id!!,dashBoardActivity,context, object :
                                        DialogCallBack {
                                        override fun onPositiveClick(value: String) {
                                            UiUtils.showSnack(holder.binding2!!.root,value)
                                        }
                                        override fun onNegativeClick() {

                                        } })
                                }
                            }
                            else if(BaseUtils.nullCheckerInt(list[position].enquiry) == 0){
                                val stockOption = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.stock_option)
                                val maxQuantity = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.max_quantity)
                                holder.binding2!!.plus.setOnClickListener {
                                    if(stockOption == 1){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0) {
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            if((maxQuantity == 0 || maxQuantity > holder.binding2!!.qtyPm.text.toString().toInt()) && holder.binding2!!.qtyPm.text.toString().toInt() < list[position].quantities!![list[position].sqtyindex!!].stock.toString().toInt()){
                                                //DialogUtils.showCartLoader(context)
                                                plusClickStart(holder.binding2!!,position)
                                            }
                                            else{
                                                UiUtils.showSnack(holder.binding2!!.root,sharedHelper.shopWebData.outofstock_message!!)
                                            }
                                        }
                                    }
                                    else if(stockOption == 0){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0){
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            plusClickStart(holder.binding2!!,position)
                                        }
                                    }
                                }
                                holder.binding2!!.minus.setOnClickListener {
                                    if(holder.binding2!!.qtyPm.text.toString().toInt() == 1){
                                        minusClickStart(holder.binding2!!,position,true)
                                    }
                                    else if(holder.binding2!!.qtyPm.text.toString().toInt() > 0){
                                        minusClickStart(holder.binding2!!,position,false)
                                    }

                                }
                            }
                        }
                    }
                    else{
                        holder.binding2!!.availableLinear.visibility = View.GONE
                        holder.binding2!!.addRemoveLinear.visibility = View.GONE
                        holder.binding2!!.qtyPm.text = "0"
                        holder.binding2!!.add.visibility = View.VISIBLE
                        if(BaseUtils.nullCheckerInt(list[position].display) == 1){
                            holder.binding2!!.addRemoveLinear.visibility = View.GONE
                            holder.binding2!!.add.visibility = View.GONE
                            holder.binding2!!.availableLinear.visibility = View.VISIBLE
                            val time = "${BaseUtils.getFormattedDate(list[position].available_from_time.toString(),"HH:MM:SS","hh:mm aa")} ${"-"} ${BaseUtils.getFormattedDate(list[position].available_to_time.toString(),"HH:MM:SS","hh:mm aa")}"
                            holder.binding2!!.time.text = time
                        }
                        else if(BaseUtils.nullCheckerInt(list[position].display) == 0){
                            if(BaseUtils.nullCheckerInt(list[position].enquiry) == 1){
                                holder.binding2!!.addRemoveLinear.visibility = View.GONE
                                holder.binding2!!.availableLinear.visibility = View.GONE
                                holder.binding2!!.add.visibility = View.VISIBLE
                                holder.binding2!!.add.text = context.getString(R.string.enquiry)

                                holder.binding2!!.add.setOnClickListener{
                                    DialogUtils.showEnquiryDialog(list[position].id!!,dashBoardActivity,context, object :
                                        DialogCallBack {
                                        override fun onPositiveClick(value: String) {
                                            UiUtils.showSnack(holder.binding2!!.root,value)
                                        }
                                        override fun onNegativeClick() {

                                        } })
                                }
                            }
                            else if(BaseUtils.nullCheckerInt(list[position].enquiry) == 0){
                                holder.binding2!!.addRemoveLinear.visibility = View.GONE
                                holder.binding2!!.availableLinear.visibility = View.GONE
                                holder.binding2!!.add.visibility = View.VISIBLE
                                holder.binding2!!.add.text = context.getString(R.string.add)
                                val stockOption = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.stock_option)
                                val maxQuantity = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.max_quantity)
                                holder.binding2!!.add.setOnClickListener {
                                    if(stockOption == 1){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0) {
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            if((maxQuantity == 0 || maxQuantity > holder.binding2!!.qtyPm.text.toString().toInt()) && holder.binding2!!.qtyPm.text.toString().toInt() < list[position].quantities!![list[position].sqtyindex!!].stock.toString().toInt()){
                                                addClickStart(holder.binding2!!,position)
                                            }
                                            else{
                                                UiUtils.showSnack(holder.binding2!!.root,sharedHelper.shopWebData.outofstock_message!!)
                                            }
                                        }
                                    }
                                    else if(stockOption == 0){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0){
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            addClickStart(holder.binding2!!,position)
                                        }
                                    }
                                }

                                holder.binding2!!.plus.setOnClickListener {
                                    if(stockOption == 1){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0) {
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            if((maxQuantity == 0 || maxQuantity > holder.binding2!!.qtyPm.text.toString().toInt()) && holder.binding2!!.qtyPm.text.toString().toInt() < list[position].quantities!![list[position].sqtyindex!!].stock.toString().toInt()){
                                                //DialogUtils.showCartLoader(context)
                                                plusClickStart(holder.binding2!!,position)
                                            }
                                            else{
                                                UiUtils.showSnack(holder.binding2!!.root,sharedHelper.shopWebData.outofstock_message!!)
                                            }
                                        }
                                    }
                                    else if(stockOption == 0){
                                        if(holder.binding2!!.qtyPm.text.toString().toInt() > maxQuantity && maxQuantity != 0){
                                            UiUtils.showSnack(holder.binding2!!.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                                        }
                                        else{
                                            plusClickStart(holder.binding2!!,position)
                                        }
                                    }
                                }

                                holder.binding2!!.minus.setOnClickListener {
                                    if(holder.binding2!!.qtyPm.text.toString().toInt() == 1){
                                        minusClickStart(holder.binding2!!,position,true)
                                    }
                                    else if(holder.binding2!!.qtyPm.text.toString().toInt() > 0){
                                        minusClickStart(holder.binding2!!,position,false)
                                    }

                                }
                            }
                        }
                    }
                }
            }

            qtyLoad(holder.binding2!!,position)

            if(list.size == position+1 && list.size > 5 && isscroolcomplete){
                holder.binding2!!.thatsAll.visibility =View.VISIBLE
            }
            else{
                holder.binding2!!.thatsAll.visibility =View.GONE
            }
        }
        else{
           /* val params: ViewGroup.LayoutParams = holder.binding1!!.lin.layoutParams
            val params1: ViewGroup.LayoutParams = holder.binding1!!.imageViewbanner.layoutParams
            if(homeFragment.binding.hotsaleLinear.visibility == View.VISIBLE){
                params.width = homeFragment.binding.hotsale.width /(2)
                params1.height = homeFragment.binding.hotsale.width /(2)
            }
            holder.binding1!!.lin.layoutParams = params
            holder.binding1!!.imageViewbanner.layoutParams = params1*/

            holder.binding1!!.text.text = list[position].name
            UiUtils.textViewBgTint(holder.binding1!!.offer, SharedHelper(context).primaryColor, null)

            if(list[position].files!!.isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding1!!.imageViewbanner, list[position].files!![0])
            }
            else{
                UiUtils.loadDefaultImage(holder.binding1!!.imageViewbanner)
            }

            if(UiUtils.formattedValues(list[position].savedpercentage) != Constants.IntentKeys.AMOUNT_DUMMY){
                holder.binding1!!.offer.visibility = View.VISIBLE
                val offer = "${list[position].savedpercentage.toString()}${"%"} ${context.getString(R.string.off)}"
                holder.binding1!!.offer.text = offer
            }
            else{
                holder.binding1!!.offer.visibility = View.GONE
            }

            holder.binding1!!.root.setOnClickListener {
                val args = Bundle()
                args.putSerializable(Constants.IntentKeys.KEY, list[position])
                dashBoardActivity.addFragment(ProductViewFragment(), args, -1, -1)
            }

            if(list[position].quantities!!.size == 0){
                outOffStock(holder.binding1!!,null, value = true, isqty = false)
            }
            else{
                if(BaseUtils.nullCheckerInt(list[position].stock_available) == 0){
                    outOffStock(holder.binding1!!,null, value = true, isqty = true)
                }
                else{
                    outOffStock(holder.binding1!!,null, value = false, isqty = true)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (view) {
            1 -> {
                1
            }
            2 -> {
                2
            }
            else -> {
                0
            }
        }
    }

    private fun movePage(position: Int){
        if(issearch){
            searchFragment.lpos = position
        }
        else{
            productListFragment.lpos = position
        }
        val args = Bundle()
        args.putSerializable(Constants.IntentKeys.KEY, list[position])
        dashBoardActivity.addFragment(ProductViewFragment(), args, -1, -1)
    }

    private fun loadMidBanner(binding: CardListproductsBinding, position: Int){
        if(position == 0){
            if(productListFragment.issubcategory){
                for(items in sharedHelper.banners){
                    if(items.title.equals(Constants.IntentKeys.SUBCATEGORY)){
                        if(items.link_id == productListFragment.subcategoryid){
                            if(items.file.isNotEmpty()){
                                binding.card.visibility = View.VISIBLE
                                UiUtils.loadImageWithCenterCrop(binding.banner, items.file)
                            }
                            else{
                                binding.card.visibility = View.GONE
                            }
                        }
                    }
                }
            }
            else if(productListFragment.iscategory){
                for(items in sharedHelper.banners){
                    if(items.title.equals(Constants.IntentKeys.CATEGORY)){
                        if(items.category_id == productListFragment.subcategoryid){
                            if(items.file.isNotEmpty()){
                                binding.card.visibility = View.VISIBLE
                                UiUtils.loadImageWithCenterCrop(binding.banner, items.file)
                            }
                            else{
                                binding.card.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        }
        else if(((position+BaseUtils.nullCheckerInt(sharedHelper.shopWebData.newarrival_count)) % (BaseUtils.nullCheckerInt(sharedHelper.shopWebData.newarrival_count))) == 0){
            if(productListFragment.issubcategory){
                for(items in sharedHelper.banners){
                    if(items.title.equals(Constants.ApiKeys.PRODUCT_MIDDLE)){
                        if(items.link_id == productListFragment.subcategoryid){
                            if(items.file.isNotEmpty()){
                                binding.card.visibility = View.VISIBLE
                                UiUtils.loadImageWithCenterCrop(binding.banner, items.file)
                            }
                            else{
                                binding.card.visibility = View.GONE
                            }
                        }
                    }
                }
            }
            else if(productListFragment.iscategory){
                for(items in sharedHelper.banners){
                    if(items.title.equals(Constants.ApiKeys.PRODUCT_MIDDLE)){
                        if(items.category_id == productListFragment.categoryid){
                            if(items.file.isNotEmpty()){
                                binding.card.visibility = View.VISIBLE
                                UiUtils.loadImageWithCenterCrop(binding.banner, items.file)
                            }
                            else{
                                binding.card.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        }
        else{
            binding.card.visibility = View.GONE
        }
    }

    private fun qtyLoad(binding: CardListproductsBinding, position: Int){
        if(list[position].quantities!!.size  > 1){
            binding.qty.isEnabled = true
            UiUtils.textviewImgDrawable(binding.qty,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
            // holder.binding2!!.qty.compoundDrawableTintList = ColorStateList.valueOf(context.getColor(R.color.grey))
            binding.qty.setOnClickListener {
                DialogUtils.showQuantityDialog(context, list[position].name.toString(), list[position].quantities,
                    object :
                        OnClickListener {
                        override fun onClickItem(position1: Int,position2: Int) {
                            list[position].sqtyid = position1
                            list[position].sqtyindex = position2
                            notifyItemChanged(position)
                        }
                    })
            }
            if(BaseUtils.nullCheckerInt(list[position].stock_available) == 0){
                outOffStock(null,binding, value = true, isqty = true)
            }
            else{
                outOffStock(null,binding, value = false, isqty = true)
            }
        }
        else if(list[position].quantities!!.size == 1){
            binding.qty.isEnabled = false
            UiUtils.textviewImgDrawable(binding.qty,null,"")
            if(BaseUtils.nullCheckerInt(list[position].stock_available) == 0){
                outOffStock(null,binding, value = true, isqty = true)
            }
            else{
                outOffStock(null,binding, value = false, isqty = true)
            }
        }
        else if(list[position].quantities!!.size == 0){
            binding.price1.visibility = View.INVISIBLE
            binding.price2.visibility = View.INVISIBLE
            binding.qty.text = context.getText(R.string.empty)
            binding.qty.isEnabled = false
            binding.qty.setCompoundDrawables(null,null,null,null)
            outOffStock(null,binding, value = true, isqty = false)
        }
    }

    private fun outOffStock(binding1: CardProducts1Binding?, binding2: CardListproductsBinding?, value:Boolean, isqty:Boolean){
        if(value) {
            if(binding1 != null){
                binding1.root.isClickable = isqty
                binding1.imageViewbanner.alpha = .5f
                binding1.offer.alpha = .5f
                UiUtils.textViewTextColor(binding1.oos, SharedHelper(context).primaryColor, null)
                binding1.oos.text = sharedHelper.shopWebData.outofstock_message
                binding1.oos.visibility = View.VISIBLE
            }
            else if(binding2 != null){
                binding2.root.isClickable = isqty
                binding2.imageViewbanner.isClickable = isqty
                binding2.text.isClickable = isqty
                binding2.add.isClickable = false
                binding2.plus.isClickable = false
                binding2.minus.isClickable = false
                binding2.qty.isClickable = false
                binding2.linear2.alpha = .5f
                binding2.imageViewbanner.alpha = .5f
                binding2.offer.alpha = .5f
                UiUtils.textViewTextColor(binding2.oos, SharedHelper(context).primaryColor, null)
                binding2.oos.text = sharedHelper.shopWebData.outofstock_message
                binding2.oos.visibility = View.VISIBLE
            }
        }
        else{
            if(binding1 != null){
                binding1.root.isClickable = isqty
                binding1.imageViewbanner.alpha = 1f
                binding1.offer.alpha = 1f
                UiUtils.textViewTextColor(binding1.oos, SharedHelper(context).primaryColor, null)
                binding1.oos.text = sharedHelper.shopWebData.outofstock_message
                binding1.oos.visibility = View.GONE
            }
            else if(binding2 != null){
                binding2.root.isClickable = isqty
                binding2.imageViewbanner.isClickable = isqty
                binding2.text.isClickable = isqty
                binding2.add.isClickable = true
                binding2.plus.isClickable = true
                binding2.minus.isClickable = true
                binding2.qty.isClickable = true
                binding2.linear2.alpha = 1f
                binding2.imageViewbanner.alpha = 1f
                binding2.offer.alpha = 1f
                UiUtils.textViewTextColor(binding2.oos, SharedHelper(context).primaryColor, null)
                binding2.oos.text = sharedHelper.shopWebData.outofstock_message
                binding2.oos.visibility = View.GONE
            }
        }
    }

    private fun addClickStart(binding: CardListproductsBinding, position: Int){
        UiUtils.txtBtnLoadingStart(binding.add)
        viewModel?.addToCart(context,binding.qtyPm.text.toString().toInt()+1, list[position].sqtyid,list[position].id)?.observe(dashBoardActivity) {
            UiUtils.txtBtnLoadingEnd(binding.add, context.getString(R.string.add))
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        if (BaseUtils.nullCheckerStr(sharedHelper.shopWebData.afteraddcart)
                                .isNotEmpty()
                        ) {
                            UiUtils.showSnack(binding.root, sharedHelper.shopWebData.afteraddcart!!)
                        }
                        if (!sharedHelper.loggedIn) {
                            sharedHelper.cartIdTemp = it.addcart_data!!.cart_id.toString()
                        }
                        for ((index, value) in list[position].quantities!!.withIndex()) {
                            if (value.id == list[position].sqtyid) {
                                list[position].quantities!![index].iscart = true
                                list[position].quantities!![index].qty =
                                    binding.qtyPm.text.toString().toInt() + 1
                                list[position].quantities!![index].cartid = 0
                            }
                        }
                        notifyItemChanged(position)
                        dashBoardActivity.getCart()
                    }
                }
            }
        }
    }

    private fun plusClickStart(binding: CardListproductsBinding, position: Int){
        UiUtils.imgBtnLoadingEnd(binding.plus)
        viewModel?.addToCart(context,binding.qtyPm.text.toString().toInt()+1, list[position].sqtyid,list[position].id)?.observe(dashBoardActivity) {
            UiUtils.imgBtnLoadingEnd(binding.plus, R.drawable.ic_baseline_add_24)
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        if (!sharedHelper.loggedIn) {
                            sharedHelper.cartIdTemp = it.addcart_data!!.cart_id.toString()
                        }

                        for ((index, value) in list[position].quantities!!.withIndex()) {
                            if (value.id == list[position].sqtyid) {
                                list[position].quantities!![index].iscart = true
                                list[position].quantities!![index].qty =
                                    binding.qtyPm.text.toString().toInt() + 1
                                list[position].quantities!![index].cartid =
                                    list[position].id!!.toInt()
                            }
                        }
                        notifyItemChanged(position)
                        dashBoardActivity.getCart()
                    }
                }
            }
        }

    }

    private fun minusClickStart(binding: CardListproductsBinding, position: Int, isdelete:Boolean){
        UiUtils.imgBtnLoadingEnd(binding.minus)
        viewModel?.addToCart(context,binding.qtyPm.text.toString().toInt()-1,list[position].sqtyid,list[position].id)
            ?.observe(dashBoardActivity) {
                //DialogUtils.dismissLoader()
                UiUtils.imgBtnLoadingEnd(binding.minus, R.drawable.ic_baseline_remove_24)
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            if (!sharedHelper.loggedIn && (binding.qtyPm.text.toString()
                                    .toInt() - 1) != 0
                            ) {
                                sharedHelper.cartIdTemp = it.addcart_data!!.cart_id.toString()
                            }

                            if (isdelete) {
                                for ((index, value) in list[position].quantities!!.withIndex()) {
                                    if (value.id == list[position].sqtyid) {
                                        list[position].quantities!![index].iscart = false
                                        list[position].quantities!![index].qty =
                                            binding.qtyPm.text.toString().toInt() - 1
                                        list[position].quantities!![index].cartid = 0
                                    }
                                }
                                notifyItemChanged(position)
                                dashBoardActivity.getCart()
                            } else {
                                for ((index, value) in list[position].quantities!!.withIndex()) {
                                    if (value.id == list[position].sqtyid) {
                                        list[position].quantities!![index].iscart = true
                                        list[position].quantities!![index].qty =
                                            binding.qtyPm.text.toString().toInt() - 1
                                        list[position].quantities!![index].cartid =
                                            list[position].id!!.toInt()
                                    }
                                }
                                notifyItemChanged(position)
                                dashBoardActivity.getCart()
                            }
                        }
                    }
                }
            }
    }
}