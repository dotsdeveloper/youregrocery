package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.MyReferralsActivity
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.ReferData
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.databinding.CardReferalBinding
import kotlin.collections.ArrayList

class ReferralAdapter(
    var activity: MyReferralsActivity,
    var context: Context,
    var list: ArrayList<ReferData>
) :
    RecyclerView.Adapter<ReferralAdapter.ViewHolder>() {
    private lateinit var myReferralsActivity: MyReferralsActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardReferalBinding = CardReferalBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        myReferralsActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_referal,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.name.text = list[position].user!!.name.toString()
        holder.binding.mobile.text = list[position].user!!.mobile.toString()

        if(list[position].amount != null && list[position].amount!!.isNotEmpty()){
            holder.binding.amount.visibility = View.VISIBLE
            val amount = "${Constants.Common.CURRENCY}${list[position].amount}"
            holder.binding.amount.text = amount
        }
        else{
            holder.binding.amount.visibility = View.GONE
        }

        holder.binding.date.text = BaseUtils.getFormattedDateUtc(list[position].updated_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.DD)
        holder.binding.year.text = BaseUtils.getFormattedDateUtc(list[position].updated_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.YYYY)
        holder.binding.month.text = BaseUtils.getFormattedDateUtc(list[position].updated_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.MMMM)
        holder.binding.time.text = BaseUtils.getFormattedDateUtc(list[position].updated_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.HH_MM_AAA)

        val since = "${context.getString(R.string.since)} ${":"} ${BaseUtils.getFormattedDateUtc(list[position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,"dd - MMMM - yyyy")}"
        holder.binding.since.text = since
    }
}