package com.yes.youregrocery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;
import com.yes.youregrocery.R;

public class WelcomeAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public WelcomeAdapter(Context context) {
        this.context = context;
    }
    int[] images = {R.drawable.welcome_one, R.drawable.welcome_two, R.drawable.welcome_three,R.drawable.welcome_four,R.drawable.welcome_five};
    String[] headings = {"Order Delicious Food", "Order Milk,Panner & other Dairy Products", "Order Fresh Vegetables","Order All Kirana Items","Delivered Quickly Your Doorsteps"};
    int[] descriptions = {R.string.description, R.string.description, R.string.description, R.string.description,R.string.description};
    @Override
    public int getCount() {
        return headings.length;
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.card_welcome,container, false);
        ImageView imageView = view.findViewById(R.id.slider_image);
        TextView heading = view.findViewById(R.id.slider_header);
        TextView desc = view.findViewById(R.id.slider_desc);
        imageView.setImageResource(images[position]);
        heading.setText(headings[position]);
        desc.setText(descriptions[position]);
        container.addView(view);
        return view;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout) object);
    }
}
