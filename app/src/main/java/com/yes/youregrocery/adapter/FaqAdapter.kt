package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Faq
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils

class FaqAdapter(
    var fragment: ProductViewFragment,
    var context: Context,
    var list: ArrayList<Faq>?
):
    RecyclerView.Adapter<FaqAdapter.ViewHolder>(){
    lateinit var productViewFragment: ProductViewFragment

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView: CardView = view.findViewById(R.id.faq_cardview)
        var question: TextView = view.findViewById(R.id.question)
        var answer: TextView = view.findViewById(R.id.answer)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        productViewFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_faq,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.cardViewBgColor(holder.cardView, SharedHelper(context).primaryColor, null)
        holder.question.text= list!![position].question
        holder.answer.text= list!![position].answer
        holder.question.setOnClickListener{
            if (holder.answer.visibility == View.GONE) {
                UiUtils.textviewImgDrawable(holder.question,R.drawable.ic_baseline_arrow_drop_up_24,Constants.IntentKeys.END)
                holder.answer.visibility = View.VISIBLE
            }
            else {
                UiUtils.textviewImgDrawable(holder.question,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
                holder.answer.visibility = View.GONE
            }
        }
    }
}
