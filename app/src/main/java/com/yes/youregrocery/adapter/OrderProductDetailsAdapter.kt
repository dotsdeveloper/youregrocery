package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.fragment.OrderDetailFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Order
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardProductdetailsBinding
import com.yes.youregrocery.session.SharedHelper

class OrderProductDetailsAdapter(
    var fragment: OrderDetailFragment,
    var context: Context,
    var list: ArrayList<Order>?
) :
    RecyclerView.Adapter<OrderProductDetailsAdapter.HomeHeaderViewHolder>() {
    private lateinit var  orderDetailFragment: OrderDetailFragment
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardProductdetailsBinding = CardProductdetailsBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        orderDetailFragment = fragment
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_productdetails,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        val qty = "${list!![position].quantity.toString()} ${context.getString(R.string.quantity)}"
        val details = "${list!![position].quantities!!.quantity.toString()} ${list!![position].quantities!!.product_unit!!.name}"
        holder.binding.qty.text = qty
        holder.binding.details.text = details
        holder.binding.category.text = list!![position].product!!.category!!.name

        if(list!![position].product!!.brand_id != 0 && list!![position].product!!.brand.name.isNotEmpty()){
            val text = "${list!![position].product!!.brand.name} ${"-"} ${list!![position].product!!.name.toString()}"
            holder.binding.text.text = text
        }
        else{
            holder.binding.text.text = list!![position].product!!.name.toString()
        }

        if(list!![position].product!!.files!!.isNotEmpty()){
            UiUtils.loadImageWithCenterCrop(holder.binding.imageViewbanner,list!![position].product!!.files!![0])
        }
        else{
            UiUtils.loadDefaultImage(holder.binding.imageViewbanner)
        }

        if(UiUtils.formattedValues(list!![position].save_value) != Constants.IntentKeys.AMOUNT_DUMMY){
            holder.binding.offer.visibility = View.VISIBLE
            UiUtils.textViewBgTint(holder.binding.offer, SharedHelper(context).primaryColor,null)
            val offer = "${Constants.Common.CURRENCY}${list!![position].save_value.toString()} ${context.getString(R.string.saved)}"
            holder.binding.offer.text = offer
            val price = "${Constants.Common.CURRENCY}${list!![position].quantities!!.offer_price}"
            holder.binding.price.text = price
        }
        else{
            holder.binding.offer.visibility = View.GONE
            val price = "${Constants.Common.CURRENCY}${list!![position].quantities!!.amount}"
            holder.binding.price.text = price
        }

        if(position != 0){
            holder.binding.category.text = list!![position].product!!.category!!.name
            if(list!![position].product!!.category!!.ccount == 1){
                val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.item)}"
                holder.binding.count.text = count
            }
            else{
                val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.items)}"
                holder.binding.count.text = count
            }
            val old = list!![position-1].product!!.category!!.name
            val new = list!![position].product!!.category!!.name
            if(old == new){
                holder.binding.linearCat.visibility = View.GONE
            }
            else{
                holder.binding.linearCat.visibility = View.VISIBLE
            }
        }
        else{
            holder.binding.category.text = list!![position].product!!.category!!.name
            if(list!![position].product!!.category!!.ccount == 1){
                val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.item)}"
                holder.binding.count.text = count
            }
            else{
                val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.items)}"
                holder.binding.count.text = count
            }
        }

    }
}