package com.yes.youregrocery.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.NotificationFragment
import com.yes.youregrocery.fragment.OrderDetailFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.NotificationData
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardNotificationBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper

class NotificationListAdapter(
    var fragment: NotificationFragment,
    var context: Context,
    var list: ArrayList<NotificationData>?
) :
    RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {
    private lateinit var notificationFragment: NotificationFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardNotificationBinding = CardNotificationBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        notificationFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_notification,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text = list!![position].title
        holder.binding.body.text = list!![position].body

        holder.binding.day.text = BaseUtils.getTimeAgo(list!![position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT)

        holder.binding.root.setOnClickListener {
            if(list!![position].order_id!!.toInt() != 0){
                if(list!![position].status == 1){
                    notificationFragment.readNotify(list!![position].id!!)
                }
                DialogUtils.showLoader(context)
                notificationFragment.viewModel?.getOrder(context,list!![position].order_id!!)?.observe(notificationFragment) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(holder.binding.root, it.message!!)
                            } else {
                                val args = Bundle()
                                args.putSerializable(Constants.IntentKeys.KEY, it.data!![0])
                                notificationFragment.dashBoardActivity.addFragment(
                                    OrderDetailFragment(),
                                    args,
                                    -1,
                                    -1
                                )
                            }
                        }
                    }
                }
            }
        }

        if(list!![position].status == 1){
            UiUtils.textViewTextColor(holder.binding.title, SharedHelper(context).primaryColor, null)
        }
        else{
            UiUtils.textViewTextColor(holder.binding.title, null, R.color.black)
        }
    }
}