package com.yes.youregrocery.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.R
import com.yes.youregrocery.`interface`.OnClickListener
import com.yes.youregrocery.databinding.CardQuantityProducts2Binding
import com.yes.youregrocery.responses.Quantities
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils

class DialogProductQuantityAdapter(
    private var dialog: Dialog,
    private var onClickListener: OnClickListener,
    var context: Context,
    var list: ArrayList<Quantities>?
) :
    RecyclerView.Adapter<DialogProductQuantityAdapter.ViewHolder>() {
    lateinit var sharedHelper: SharedHelper
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardQuantityProducts2Binding = CardQuantityProducts2Binding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        sharedHelper = SharedHelper(context)
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_quantity_products2,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val unit = "${list!![position].quantity} ${list!![position].product_unit!!.name}"
        val qty = "${Constants.Common.CURRENCY}${list!![position].amount}"
        if(UiUtils.formattedValues(list!![position].offer_price) == Constants.IntentKeys.AMOUNT_DUMMY){
            holder.binding.qty1.visibility = View.GONE
            holder.binding.unit.text = unit
            holder.binding.qty.text = qty
        }
        else{
            val qty1 = "${Constants.Common.CURRENCY}${list!![position].offer_price}"
            holder.binding.qty1.visibility = View.VISIBLE
            holder.binding.qty.setTextColor(context.getColor(R.color.red))
            holder.binding.qty.paintFlags = holder.binding.qty.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            holder.binding.unit.text = unit
            holder.binding.qty1.text = qty1
            holder.binding.qty.text = qty
        }

        holder.binding.root.setOnClickListener {
            onClickListener.onClickItem(list!![position].id!!,position)
            dialog.dismiss()
        }
    }
}