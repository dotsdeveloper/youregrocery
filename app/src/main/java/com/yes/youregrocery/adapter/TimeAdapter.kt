package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.CheckoutActivity
import com.yes.youregrocery.R
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardTimeBinding
import com.yes.youregrocery.session.SharedHelper

class TimeAdapter(
    var activity: CheckoutActivity,
    var context: Context,
    var list: Array<String>?
) :
    RecyclerView.Adapter<TimeAdapter.HomeHeaderViewHolder>() {
    private var selectedPosition = -1
    private lateinit var checkoutActivity: CheckoutActivity
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardTimeBinding = CardTimeBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        checkoutActivity = activity
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_time,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if(selectedPosition == position){
            UiUtils.textViewBgTint(holder.binding.time, SharedHelper(context).primaryColor, null)
            UiUtils.textViewTextColor(holder.binding.time,null,R.color.white)
            holder.binding.root.setOnClickListener {
                checkoutActivity.deliveryslot = ""
                selectedPosition = -1
                notifyDataSetChanged()
            }
        }
        else{
            if(list!![position].isNotEmpty()){
                UiUtils.textViewTextColor(holder.binding.time,null,R.color.black)
                UiUtils.textViewBgTint(holder.binding.time, null, R.color.white)
                holder.binding.time.text = BaseUtils.getFormattedDate(list!![position],"HH:MM:SS","hh:mm aa")
                holder.binding.root.setOnClickListener {
                    checkoutActivity.deliveryslot = holder.binding.time.text.toString()
                    selectedPosition = position
                    notifyDataSetChanged()
                }
            }
        }
    }
}