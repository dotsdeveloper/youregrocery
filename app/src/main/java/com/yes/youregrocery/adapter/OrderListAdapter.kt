package com.yes.youregrocery.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.fragment.BasketFragment
import com.yes.youregrocery.fragment.OrderDetailFragment
import com.yes.youregrocery.fragment.OrdersFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.OrderData
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardOrderlistBinding
import com.yes.youregrocery.session.SharedHelper

class OrderListAdapter(
    var fragment: OrdersFragment,
    var context: Context,
    var list: ArrayList<OrderData>?
) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {
    private lateinit var ordersFragment: OrdersFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrderlistBinding = CardOrderlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        ordersFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_orderlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.textViewTextColor(holder.binding.reorder, SharedHelper(context).primaryColor, null)
        UiUtils.textViewTextColor(holder.binding.details, SharedHelper(context).primaryColor, null)
        holder.binding.orderid.text = list!![position].invoice.toString()
        val orderAmount = "${context.getString(R.string.amount)} ${":"} ${Constants.Common.CURRENCY}${list!![position].grand_total.toString()}"
        holder.binding.orderamount.text = orderAmount
        holder.binding.date.text = BaseUtils.getFormattedDateUtc(list!![position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.DD)
        holder.binding.year.text = BaseUtils.getFormattedDateUtc(list!![position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.YYYY)
        holder.binding.month.text = BaseUtils.getFormattedDateUtc(list!![position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.MMMM)
        holder.binding.time.text = BaseUtils.getFormattedDateUtc(list!![position].created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,Constants.ApiKeys.HH_MM_AAA)

        if(list!![position].items!!.size == 1){
            val text = "${list!![position].items!!.size} ${context.getString(R.string.item)}"
            holder.binding.items.text = text
        }
        else{
            val text = "${list!![position].items!!.size} ${context.getString(R.string.items)}"
            holder.binding.items.text = text
        }

        holder.binding.root.setOnClickListener {
            moveOrderDetailFragment(position)
        }
        holder.binding.details.setOnClickListener {
            moveOrderDetailFragment(position)
        }

        holder.binding.reorder.setOnClickListener {
            DialogUtils.showLoader(context)
            ordersFragment.viewModel?.reOrder(context,list!![position].id!!)?.observe(ordersFragment) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(holder.binding.root, it.message!!)
                        } else {
                            moveBasketFragment()
                        }
                    }
                }
            }
        }
    }

    private fun moveBasketFragment() {
        ordersFragment.dashBoardActivity!!.addFragment(
            BasketFragment(),
            null,
            -1,
            -1
        )
    }

    private fun moveOrderDetailFragment(position: Int) {
        val args = Bundle()
        args.putSerializable(Constants.IntentKeys.KEY, list!![position])
        ordersFragment.dashBoardActivity!!.addFragment(OrderDetailFragment(), args, -1, -1)
    }
}