package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.CheckoutActivity
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.PinData
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardPincodeBinding
import com.yes.youregrocery.session.SharedHelper

class PincodeAdapter(
    var activity: CheckoutActivity,
    var context: Context,
    var list: ArrayList<PinData>
) :
    RecyclerView.Adapter<PincodeAdapter.HomeHeaderViewHolder>() {
    private var selectedPosition = -1
    private lateinit var checkoutActivity: CheckoutActivity
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardPincodeBinding = CardPincodeBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        checkoutActivity = activity
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_pincode,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, @SuppressLint("RecyclerView") position: Int) {
        UiUtils.radioButtonTint(holder.binding.pincode,SharedHelper(context).primaryColor,null)
        if(selectedPosition == position){
            holder.binding.pincode.isChecked = true
            holder.binding.pincode.text = list[position].name.toString()
        }
        else{
            holder.binding.pincode.isChecked = false
            holder.binding.pincode.text = list[position].name.toString()
            holder.binding.pincode.setOnClickListener {
                checkoutActivity.isPinCode(list[position].name.toString())
                selectedPosition = position
                notifyDataSetChanged()
            }
        }

    }
}