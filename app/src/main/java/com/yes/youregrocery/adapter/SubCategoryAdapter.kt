package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.ProductListFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.CategoryList
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.CardSubcategoryBinding

class SubCategoryAdapter(
    var fragment: ProductListFragment,
    var context: Context,
    var list: ArrayList<CategoryList>
) :
    RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {
    var sharedHelper: SharedHelper? = null
    var viewModel: ViewModel? = null
    private var selectedPosition = 0
    lateinit var productListFragment: ProductListFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardSubcategoryBinding = CardSubcategoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        productListFragment = fragment
        sharedHelper = SharedHelper(context)
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_subcategory,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if(selectedPosition == position){
            holder.binding.root.isClickable = false
            holder.binding.name.text = list[position].name
            UiUtils.cardViewBgTint(holder.binding.card, SharedHelper(context).primaryColor, null)
            holder.binding.name.setTextColor(Color.WHITE)
        }
        else{
            holder.binding.card.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.WHITE, BlendModeCompat.SRC_ATOP)
            holder.binding.name.text = list[position].name
            holder.binding.name.setTextColor(Color.BLACK)
            holder.binding.root.isClickable = true
            holder.binding.root.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                productListFragment.loadSubCategoryProducts(productListFragment.categoryid, list[position].id!!)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}