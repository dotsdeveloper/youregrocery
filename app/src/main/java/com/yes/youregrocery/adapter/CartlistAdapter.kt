package com.yes.youregrocery.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.fragment.BasketFragment
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.fragment.QuickOrderFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.responses.Order
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardCartlistBinding
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.utils.BaseUtils
import kotlin.collections.ArrayList

class CartlistAdapter(
    var activity: DashBoardActivity,
    private var fragment1: BasketFragment?,
    private var fragment2: QuickOrderFragment?,
    var context: Context,
    var list: ArrayList<Order>?
) : RecyclerView.Adapter<CartlistAdapter.ViewHolder>()
{
    lateinit var sharedHelper: SharedHelper
    lateinit var viewModel: ViewModel
    private lateinit var basketFragment: BasketFragment
    private lateinit var quickOrderFragment: QuickOrderFragment
    lateinit var dashBoardActivity: DashBoardActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCartlistBinding = CardCartlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        sharedHelper = SharedHelper(context)
        basketFragment = if(fragment1 != null){
            fragment1!!
        } else{
            BasketFragment()
        }
        quickOrderFragment = if(fragment2 != null){
            fragment2!!
        } else{
            QuickOrderFragment()
        }
        dashBoardActivity = activity
        viewModel = ViewModelProvider(dashBoardActivity).get(ViewModel::class.java)
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_cartlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            UiUtils.plusMinus(holder.binding.plus)
            UiUtils.plusMinus(holder.binding.minus)
            UiUtils.textViewTextColor(holder.binding.qty, SharedHelper(context).primaryColor, null)
            if(list!![position].product!!.brand_id != 0  && list!![position].product!!.brand.name.isNotEmpty()){
                val text = "${list!![position].product!!.brand.name} ${"-"} ${list!![position].product!!.name.toString()}"
                holder.binding.text.text = text
            }
            else{
                holder.binding.text.text = list!![position].product!!.name.toString()
            }

            if(list!![position].product!!.files!!.isNotEmpty()){
                UiUtils.loadImageWithCenterCrop(holder.binding.imageViewbanner,list!![position].product!!.files!![0])
            }
            else{
                UiUtils.loadDefaultImage(holder.binding.imageViewbanner)
            }

            holder.binding.qty.text = list!![position].quantity.toString()
            val details1 = "${"("}${list!![position].quantities!!.quantity.toString()} ${list!![position].quantities!!.product_unit!!.name.toString()}${")"}"
            val details2 = "${list!![position].quantity.toString()}${"X"} ${Constants.Common.CURRENCY}${UiUtils.formattedValues(list!![position].amount)}"
            val details3 = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(list!![position].total)}"
            holder.binding.details1.text = details1
            holder.binding.details2.text = details2
            holder.binding.details3.text = details3

            if(UiUtils.formattedValues(list!![position].save_value) != Constants.IntentKeys.AMOUNT_DUMMY){
                holder.binding.offer.visibility = View.VISIBLE
                UiUtils.textViewBgTint(holder.binding.offer, SharedHelper(context).primaryColor, null)
                val offer = "${Constants.Common.CURRENCY}${list!![position].save_value.toString()} ${context.getString(R.string.saved)}"
                holder.binding.offer.text = offer
            }
            else{
                holder.binding.offer.visibility = View.GONE
            }

            if(position != 0){
                holder.binding.category.text = list!![position].product!!.category!!.name
                if(list!![position].product!!.category!!.ccount == 1){
                    val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.item)}"
                    holder.binding.count.text = count
                }
                else{
                    val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.items)}"
                    holder.binding.count.text = count
                }

                val old = list!![position-1].product!!.category!!.name
                val new = list!![position].product!!.category!!.name
                if(old == new){
                    holder.binding.linearCat.visibility = View.GONE
                }
                else{
                    holder.binding.linearCat.visibility = View.VISIBLE
                }
            }
            else{
                holder.binding.category.text = list!![position].product!!.category!!.name
                if(list!![position].product!!.category!!.ccount == 1){
                    val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.item)}"
                    holder.binding.count.text = count
                }
                else{
                    val count = "${list!![position].product!!.category!!.ccount.toString()} ${context.getString(R.string.items)}"
                    holder.binding.count.text = count
                }
                holder.binding.linearCat.visibility = View.VISIBLE
            }

            holder.binding.plus.setOnClickListener {
                val stockOption = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.stock_option)
                val maxQuantity = BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.max_quantity)
                if(stockOption == 1){
                    if(holder.binding.qty.text.toString().toInt() > maxQuantity && maxQuantity != 0) {
                        UiUtils.showSnack(holder.binding.root, context.getString(R.string.sorry_you_cannot_add_more_quantity))
                    }
                    else{
                        if((maxQuantity == 0 || maxQuantity > holder.binding.qty.text.toString().toInt()) && holder.binding.qty.text.toString().toInt() < list!![position].quantities!!.stock.toString().toInt()){
                            plusClickStart(holder.binding,position)
                        }
                        else{
                            UiUtils.showSnack(holder.binding.root,context.getString(R.string.out_of_stock))
                        }
                    }
                }
                else if(stockOption == 0){
                    if(holder.binding.qty.text.toString().toInt() > maxQuantity && maxQuantity != 0){
                        UiUtils.showSnack(holder.binding.root,context.getString(R.string.sorry_you_cannot_add_more_quantity))
                    }
                    else{
                        plusClickStart(holder.binding,position)
                    }
                }
            }
            holder.binding.minus.setOnClickListener {
                if(holder.binding.qty.text.toString().toInt() > 0 && holder.binding.qty.text.toString().toInt() != 1){
                    minusClickStart(holder.binding,position,false)
                }
                else if(holder.binding.qty.text.toString().toInt() == 1){
                    minusClickStart(holder.binding,position,true)
                }
            }
            holder.binding.delete.setOnClickListener {
                deleteClick(holder.binding,position)
            }

            holder.binding.imageViewbanner.setOnClickListener {
                rootClick(holder.binding,position)
            }
            holder.binding.text.setOnClickListener {
                rootClick(holder.binding,position)
            }
    }

    private fun plusClickStart(binding: CardCartlistBinding, position: Int){
        UiUtils.imgBtnLoadingEnd(binding.plus)
        viewModel.addToCart(context,binding.qty.text.toString().toInt()+1,list!![position].quantities!!.id,list!![position].product_id!!.toInt()).observe(dashBoardActivity) {
            //DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        plusClickEnd(binding.plus)
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        plusClickEnd(binding.plus)
                        if (!sharedHelper.loggedIn) {
                            sharedHelper.cartIdTemp = it.addcart_data!!.cart_id.toString()
                        }
                        if (basketFragment.isAdded) {
                            basketFragment.getCart(
                                isGet = false,
                                isAdd = false,
                                isModify = true,
                                isDelete = false
                            )
                        } else if (quickOrderFragment.isAdded) {
                            quickOrderFragment.getCart(
                                isGet = false,
                                isAdd = false,
                                isModify = true,
                                isDelete = false
                            )
                        }
                        list!![position].quantity =
                            (binding.qty.text.toString().toInt() + 1).toString()
                        list!![position].total =
                            UiUtils.formattedValues(((binding.qty.text.toString()
                                .toInt() + 1) * (list!![position].amount!!.toDouble())).toString())
                        notifyItemChanged(position)
                    }
                }
            }
        }
    }

    private fun plusClickEnd(button: MaterialButton){
        UiUtils.imgBtnLoadingEnd(button, R.drawable.ic_baseline_add_24)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun minusClickStart(binding: CardCartlistBinding, position: Int, isdelete:Boolean){
        UiUtils.imgBtnLoadingEnd(binding.minus)
        viewModel.addToCart(context,binding.qty.text.toString().toInt()-1,list!![position].quantities!!.id,list!![position].product_id!!.toInt())
            .observe(dashBoardActivity) {
                //DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            minusClickEnd(binding.minus)
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            minusClickEnd(binding.minus)
                            if (!sharedHelper.loggedIn && (binding.qty.text.toString()
                                    .toInt() - 1) != 0
                            ) {
                                sharedHelper.cartIdTemp = it.addcart_data!!.cart_id.toString()
                            }

                            if (isdelete) {
                                val scatname = list!![position].product!!.category!!.name
                                val scatcount =
                                    list!![position].product!!.category!!.ccount!!.toInt()
                                for (items in list!!) {
                                    if (items.product!!.category!!.name == scatname) {
                                        items.product!!.category!!.ccount = scatcount - 1
                                    }
                                }
                                list!!.removeAt(position)
                                notifyDataSetChanged()
                                if (basketFragment.isAdded) {
                                    basketFragment.getCart(
                                        isGet = false,
                                        isAdd = false,
                                        isModify = false,
                                        isDelete = true
                                    )
                                } else if (quickOrderFragment.isAdded) {
                                    quickOrderFragment.getCart(
                                        isGet = false,
                                        isAdd = false,
                                        isModify = false,
                                        isDelete = true
                                    )
                                }
                            } else {
                                list!![position].quantity =
                                    (binding.qty.text.toString().toInt() - 1).toString()
                                list!![position].total =
                                    UiUtils.formattedValues(((binding.qty.text.toString()
                                        .toInt() - 1) * (list!![position].amount!!.toDouble())).toString())
                                notifyItemChanged(position)
                                if (basketFragment.isAdded) {
                                    basketFragment.getCart(
                                        isGet = false,
                                        isAdd = false,
                                        isModify = true,
                                        isDelete = false
                                    )
                                } else if (quickOrderFragment.isAdded) {
                                    quickOrderFragment.getCart(
                                        isGet = false,
                                        isAdd = false,
                                        isModify = true,
                                        isDelete = false
                                    )
                                }
                            }
                        }
                    }
                }
            }
    }

    private fun minusClickEnd(button: MaterialButton){
        UiUtils.imgBtnLoadingEnd(button, R.drawable.ic_baseline_remove_24)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun deleteClick(binding: CardCartlistBinding, position: Int){
        DialogUtils.showCartLoader(context)
        viewModel.deleteCart(context,list!![position].id!!.toInt())
            .observe(dashBoardActivity) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            val scatname = list!![position].product!!.category!!.name
                            val scatcount = list!![position].product!!.category!!.ccount!!.toInt()
                            for (items in list!!) {
                                if (items.product!!.category!!.name == scatname) {
                                    items.product!!.category!!.ccount = scatcount - 1
                                }
                            }
                            list!!.removeAt(position)
                            notifyDataSetChanged()
                            if (basketFragment.isAdded) {
                                basketFragment.getCart(
                                    isGet = false,
                                    isAdd = false,
                                    isModify = false,
                                    isDelete = true
                                )
                            } else if (quickOrderFragment.isAdded) {
                                quickOrderFragment.getCart(
                                    isGet = false,
                                    isAdd = false,
                                    isModify = false,
                                    isDelete = true
                                )
                            }
                        }
                    }
                }
            }
    }

    private fun rootClick(binding: CardCartlistBinding, position: Int){
        DialogUtils.showLoader(context)
        viewModel.getProduct(context,list!![position].product_id).observe(dashBoardActivity) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        it.product_data?.let { data ->
                            if (quickOrderFragment.isAdded) {
                                quickOrderFragment.lpos = position
                                val args = Bundle()
                                args.putSerializable("key", data[0])
                                dashBoardActivity.addFragment(
                                    ProductViewFragment(), args, -1, -1
                                )
                            } else if (basketFragment.isAdded) {
                                basketFragment.lpos = position
                                val args = Bundle()
                                args.putSerializable("key", data[0])
                                dashBoardActivity.addFragment(
                                    ProductViewFragment(), args, -1, -1
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}