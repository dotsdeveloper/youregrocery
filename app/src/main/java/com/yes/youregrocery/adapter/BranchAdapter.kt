package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.BranchActivity
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Branches
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.databinding.CardBranchBinding
import com.yes.youregrocery.utils.UiUtils
import kotlin.collections.ArrayList

class BranchAdapter(
    var activity: BranchActivity,
    var context: Context,
    var list: ArrayList<Branches>
) :
    RecyclerView.Adapter<BranchAdapter.HomeHeaderViewHolder>() {
    private lateinit var branchActivity: BranchActivity
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardBranchBinding = CardBranchBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        branchActivity = activity
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_branch,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        holder.binding.name1.text = list[position].branchShop!!.name
        holder.binding.name2.text = list[position].branchShop!!.location

        holder.binding.root.setOnClickListener {
            if(!branchActivity.sharedHelper.showBranch){
                val list1: ArrayList<Branches> = ArrayList()
                list1.add(list[position])
                SharedHelper(branchActivity).shopList = list1
            }
            SharedHelper(context).shopId = list[position].branchShop!!.id!!
            BaseUtils.startDashboardActivity(branchActivity,true,"")
        }

        if(list[position].logo != null && !list[position].logo.equals("")){
            UiUtils.loadImage(holder.binding.branchImg,list[position].logo)
        }
    }
}