package com.yes.youregrocery.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.WishlistFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Wishlist
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardWishlistBinding
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.DialogUtils
import kotlin.collections.ArrayList

class WishlistAdapter(
    var fragment: WishlistFragment,
    var context: Context,
    var list: ArrayList<Wishlist>?
) :
    RecyclerView.Adapter<WishlistAdapter.ViewHolder>() {
    private lateinit var wishlistFragment: WishlistFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardWishlistBinding = CardWishlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        wishlistFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_wishlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.text.text = list!![position].product!!.name.toString()
        if(list!![position].product!!.files!!.isNotEmpty()){
            UiUtils.loadImage(holder.binding.imageViewbanner,list!![position].product!!.files!![0])
        }
        else{
            UiUtils.loadDefaultImage(holder.binding.imageViewbanner)
        }

        holder.binding.delete.setOnClickListener {
            wishlistFragment.deleteWishlist(position)
        }

        holder.binding.root.setOnClickListener {
            DialogUtils.showLoader(context)
            wishlistFragment.viewModel?.getProduct(context,list!![position].product_id)?.observe(wishlistFragment) {
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            DialogUtils.dismissLoader()
                            UiUtils.showSnack(holder.binding.root, it.message!!)
                        } else {
                            it.product_data?.let { data ->
                                val args = Bundle()
                                args.putSerializable(Constants.IntentKeys.KEY, data[0])
                                wishlistFragment.dashBoardActivity!!.addFragment(
                                    ProductViewFragment(), args, -1, -1
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}