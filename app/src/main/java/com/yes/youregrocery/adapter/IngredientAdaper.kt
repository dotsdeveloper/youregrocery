package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Ingredient
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardIngredientdBinding
import com.yes.youregrocery.session.SharedHelper
import kotlin.collections.ArrayList

class IngredientAdaper(
    var fragment: ProductViewFragment,
    var context: Context,
    var list: ArrayList<Ingredient>
) :
    RecyclerView.Adapter<IngredientAdaper.ViewHolder>() {
    lateinit var productViewFragment: ProductViewFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardIngredientdBinding = CardIngredientdBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        productViewFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_ingredientd,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.cardViewBgColor(holder.binding.card, SharedHelper(context).primaryColor, null)
        holder.binding.head.text = list[position].heading.toString()
        holder.binding.body.text = list[position].description.toString()
    }
}