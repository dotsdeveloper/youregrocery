package com.yes.youregrocery.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.HomeFragment
import com.yes.youregrocery.fragment.ProductListFragment
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.databinding.CardBanners1Binding
import com.yes.youregrocery.responses.Banners
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import java.util.ArrayList

class HomeSmallBannerAdaper(
    var fragment: HomeFragment,
    var context: Context,
    var list: ArrayList<Banners>
) :
    RecyclerView.Adapter<HomeSmallBannerAdaper.HomeHeaderViewHolder>() {
    private lateinit var homeFragment:HomeFragment
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardBanners1Binding = CardBanners1Binding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        homeFragment = fragment
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_banners1,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        UiUtils.loadImageWithCenterCrop(holder.binding.imageViewbanner, list[position].file)
        holder.binding.imageViewbanner.setOnClickListener {
            when (list[position].link_type) {
                1 -> {
                    DialogUtils.showLoader(context)
                    homeFragment.viewModel?.getSubCategory(context,list[position].link_id!!)?.observe(homeFragment) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    moveFragment(position, Constants.IntentKeys.LOAD_CATEGORY_P)
                                } else {
                                    moveFragment(position,
                                        Constants.IntentKeys.LOAD_SUBCATEGORY_LIST)
                                }
                            }
                        }
                    }
                }
                2 -> {
                    moveFragment(position, Constants.IntentKeys.LOAD_SUBCATEGORY_P)
                }
                3 -> {
                    DialogUtils.showLoader(context)
                    homeFragment.viewModel?.getProduct(context,list[position].link_id)?.observe(homeFragment) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(holder.binding.imageViewbanner, it.message!!)
                                } else {
                                    it.product_data?.let { data ->
                                        val args = Bundle()
                                        args.putSerializable(Constants.IntentKeys.KEY, data[0])
                                        homeFragment.dashBoardActivity.addFragment(
                                            ProductViewFragment(), args, -1, -1
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

       /* val observer: ViewTreeObserver = holder.binding.imageViewbanner.viewTreeObserver
        observer.addOnGlobalLayoutListener {
            if(position == 0 && isfirst) {
                isfirst = false
                val params: ViewGroup.LayoutParams = homeFragment.binding.banners2.layoutParams
                val bparams: ViewGroup.LayoutParams = homeFragment.binding.banners2.layoutParams
                val iparams: ViewGroup.LayoutParams = holder.binding.imageViewbanner.layoutParams
                Log.d("nxvbn",""+bparams.height+"==="+iparams.height)
                if(bparams.height > iparams.height){
                    Log.d("mxcv","sucess")
                    params.height = iparams.height
                    homeFragment.binding.banners2.layoutParams = params
                }
            }
        }*/
    }

    private fun moveFragment(position: Int, page: String) {
        if(page == Constants.IntentKeys.LOAD_SUBCATEGORY_P){
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,context.getString(R.string.products))
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,page)
            args.putInt(Constants.IntentKeys.CID,list[position].category_id!!)
            args.putInt(Constants.IntentKeys.S_C_ID,list[position].link_id!!)
            homeFragment.dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        else{
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,context.getString(R.string.products))
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,page)
            args.putInt(Constants.IntentKeys.CID,list[position].link_id!!)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            homeFragment.dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
    }
}