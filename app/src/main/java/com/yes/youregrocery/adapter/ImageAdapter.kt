package com.yes.youregrocery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.fragment.ProductViewFragment
import com.yes.youregrocery.R
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.CardImageBinding

class ImageAdapter(
    var fragment: ProductViewFragment,
    var context: Context,
    var list: Array<String>
) :
    RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    lateinit var productViewFragment: ProductViewFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardImageBinding = CardImageBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        productViewFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_image,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UiUtils.loadImage(holder.binding.image, list[position])
        holder.binding.image.setOnClickListener {
            UiUtils.loadImage(productViewFragment.binding!!.productImage,  list[position])
        }
    }
}