package com.yes.youregrocery.activity

import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.yes.youregrocery.R
import com.yes.youregrocery.adapter.WelcomeAdapter
import com.yes.youregrocery.databinding.ActivityWelcomeBinding
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.UiUtils

@Suppress("UNUSED_PARAMETER")
class WelcomeActivity : BaseActivity() {
    lateinit var binding: ActivityWelcomeBinding
    private var viewPager: ViewPager? = null
    private var dotsLayout: LinearLayout? = null
    private var sliderAdapter: WelcomeAdapter? = null
    private var dots = arrayOfNulls<TextView>(5)
    var animation: Animation? = null
    var currentPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewPager = binding.slider
        dotsLayout = binding.dots
        sliderAdapter = WelcomeAdapter(this)
        viewPager!!.adapter = sliderAdapter
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.buttonBgTint(binding.getStartedBtn,sharedHelper.primaryColor,null)
        UiUtils.buttonBgTint(binding.skipBtn,sharedHelper.primaryColor,null)
        addDots(0)
        viewPager!!.addOnPageChangeListener(changeListener)
    }

    fun getStarted(view: View){
        BaseUtils.startActivity(this,BranchActivity(),null,true)
    }

    fun skip(view: View){
       BaseUtils.startActivity(this,BranchActivity(),null,true)
   }

    fun next(view: View){
        viewPager!!.currentItem = currentPosition + 1
    }

    @Suppress("DEPRECATION")
    private fun addDots(position: Int) {
        dots =  arrayOfNulls(5)
        dotsLayout!!.removeAllViews()
        for ((index, _) in dots.withIndex()) {
            dots[index] = TextView(this)
            dots[index]!!.text = Html.fromHtml("&#8226;")
            dots[index]!!.textSize = 35F
            dots[index]!!.setTextColor(resources.getColor(R.color.grey,null))
            dotsLayout!!.addView(dots[index])
        }

        if (dots.isNotEmpty()) {
            dots[position]!!.setTextColor(Color.parseColor(sharedHelper.primaryColor))
        }
    }

    private var changeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int,
        ) {

        }

        override fun onPageSelected(position: Int) {
            addDots(position)
            currentPosition = position
           /* when (position) {
                0 -> {
                    binding.nextBtn.visibility = View.VISIBLE
                    binding.getStartedBtn.visibility = View.GONE
                }
                1 -> {
                    binding.nextBtn.visibility = View.VISIBLE
                    binding.getStartedBtn.visibility = View.GONE
                }
                2 -> {
                    binding.nextBtn.visibility = View.VISIBLE
                    binding.getStartedBtn.visibility = View.GONE
                }
                3 -> {
                    binding.nextBtn.visibility = View.VISIBLE
                    binding.getStartedBtn.visibility = View.GONE
                }
                else -> {
                    animation = AnimationUtils.loadAnimation(this@WelcomeActivity, R.anim.item_animation_fall_down)
                    binding.getStartedBtn.animation = animation
                    binding.getStartedBtn.visibility = View.VISIBLE
                    binding.nextBtn.visibility = View.GONE
                }
            }*/
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
}