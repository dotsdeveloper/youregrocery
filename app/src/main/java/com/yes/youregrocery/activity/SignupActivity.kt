package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivitySignupBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.session.TempSingleton

class SignupActivity : BaseActivity() {
    lateinit var binding: ActivitySignupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_signup)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.back, sharedHelper.primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding.back,sharedHelper.primaryColor, null)
        UiUtils.textViewTextColor(binding.signin, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.signup, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.editTextColor(binding.nameTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.mobileTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.emailTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.passwordTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.cpasswordTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.rcodeTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.locationTextField,sharedHelper.primaryColor,null)

        binding.back.setOnClickListener {
            finish()
        }

        binding.signin.setOnClickListener {
            finish()
        }

        binding.signup.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                viewModel.register(this,
                    binding.name.text.toString(),
                    binding.mail.text.toString(),
                    binding.mobile.text.toString(),
                    binding.password.text.toString(),
                    binding.confpassword.text.toString(),
                    binding.location.text.toString(),
                    binding.refercode.text.toString()
                ).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                //UiUtils.showSnack(binding.root, it.message!!)
                                TempSingleton.getInstance().password =
                                    binding.password.text.toString()
                                TempSingleton.getInstance().mobile = binding.mobile.text.toString()
                                //finish()
                                startActivity(Intent(this, OtpActivity::class.java)
                                    .putExtra(Constants.IntentKeys.PAGE,
                                        Constants.IntentKeys.SIGNUP)
                                    .putExtra(Constants.IntentKeys.MOBILE,
                                        binding.mobile.text.toString()))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.name.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_name))
                return false
            }
            /* binding.mail.text!!.isEmpty() -> {
                 UiUtils.showSnack(binding.root,getString(R.string.please_enter_mail))
                 return false
             }*/
            binding.password.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_password))
                return false
            }
            binding.confpassword.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_confirm_password))
                return false
            }
            binding.location.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_location))
                return false
            }
            binding.mobile.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_mobile_no))
                return false
            }
            binding.mobile.length() != 10 -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_valid_mobile_no))
                return false
            }
            binding.password.text.toString().trim() != binding.confpassword.text.toString().trim() -> {
                UiUtils.showSnack(binding.root,getString(R.string.password_and_confirm_password_does_not_match))
                return false
            }
        }
        return true
    }
}