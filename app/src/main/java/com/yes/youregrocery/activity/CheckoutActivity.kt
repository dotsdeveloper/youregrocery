package com.yes.youregrocery.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.text.*
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import com.yes.youregrocery.R
import com.yes.youregrocery.adapter.AddressListAdapter
import com.yes.youregrocery.adapter.PincodeAdapter
import com.yes.youregrocery.adapter.TimeAdapter
import com.yes.youregrocery.databinding.ActivityCheckoutNewBinding
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.others.VolleyMultipartRequest.DataPart
import com.yes.youregrocery.responses.*
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt
import android.content.ClipboardManager

class CheckoutActivity : BaseActivity(), PaymentResultWithDataListener {
    lateinit var binding: ActivityCheckoutNewBinding
    private var timeData: ArrayList<TimeData>? = ArrayList()
    private val nAddressData: ArrayList<AddressData> = ArrayList()
    var flat: Double? = 0.0
    var flng: Double? = 0.0
    private var ordertype: Int? = 0
    private var couponid: String? = ""
    var selectedDeliveryPosition = 0
    private var odeliverycount = 0
    var deliveryid: String? = ""
    private var deliverytype: Int? = 0
    var spincode: String? = ""
    var locationname: String? = ""
    var address: String? = ""
    var landmark: String? = ""
    var description: String? = ""
    private var deliverydate: String? = ""
    var deliveryslot: String? = ""
    private var paymentid: String? = ""
    private var walletamount: String? = ""
    private var balanceamount: String? = ""
    private var orderamount: Double? = null
    private var payorderamount: Double? = null
    private var iswalletapproved: Boolean? = false
    private var iswalletpartiallyapproved: Boolean? = false
    private var uploadFile: File? = null
    private var imageData1: DataPart? = null
    private var imageData2: DataPart? = null
    private var isAttach1: Boolean = false
    private var isAttach2: Boolean = false
    var isaddaddress: Boolean = false
    var isselectedaddress: Boolean = false
    private var backcount = 0
    var requestCode = 0
    private var orderid = ""
    private var isorderplaced = false
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                Log.d("ncv", "" + result.resultCode + "cchjbv" + requestCode)
                when (requestCode) {
                    Constants.RequestCode.GALLERY_INTENT,//gallery
                    -> {
                        DialogUtils.showLoader(this)
                        handleGallery(data)
                    }
                    Constants.RequestCode.CAMERA_INTENT,//camera
                    -> {
                        DialogUtils.showLoader(this)
                        handleCamera()
                    }
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutNewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
        if(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.nonlisted_text).isNotEmpty()){
            binding.nonlistTxt.text = sharedHelper.shopWebData.nonlisted_text
        }
        getCart()
        getTimeSlot()
        getPayOption()
        getDeliveryOption()
        loadPage()
    }

    override fun onResume() {
        super.onResume()
        if (isaddaddress) {
            isaddaddress = false
            getAddress()
        }
    }

    override fun onBackPressed() {
        if (backcount > 0) {
            if (backcount == 3) {
                BaseUtils.startDashboardActivity(this, true,"")
            } else {
                backcount--
                loadPage()
            }
        } else {
            super.onBackPressed()
        }
    }

    /*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = data
            Log.d("ncv",""+resultCode+"cchjbv"+requestCode)
            when (requestCode) {
                Constants.RequestCode.GALLERY_INTENT//gallery
                ->  {
                    DialogUtils.showLoader(this)
                    handleGallery(data)
                }
                Constants.RequestCode.CAMERA_INTENT//camera
                ->  {
                    DialogUtils.showLoader(this)
                    handleCamera()
                }
            }
        }
   }
*/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constants.RequestCode.CAMERA_REQUEST ->
                if (grantResults.isNotEmpty()) {
                    if (BaseUtils.grantResults(grantResults)) {
                        BaseUtils.openCamera(this)
                    } else {
                        if (BaseUtils.isDeniedPermission(this, Constants.IntentKeys.CAMERA)) {
                            //BaseUtils.permissionsEnableRequest(this,"camera")
                            return
                        } else {
                            BaseUtils.displayManuallyEnablePermissionsDialog(
                                this,
                                Constants.IntentKeys.CAMERA,
                                null
                            )
                        }
                    }
                }
            Constants.RequestCode.GALLERY_REQUEST ->
                if (grantResults.isNotEmpty()) {
                    if (BaseUtils.grantResults(grantResults)) {
                        BaseUtils.openGallery(this)
                    } else {
                        if (BaseUtils.isDeniedPermission(this, Constants.IntentKeys.GALLERY)) {
                            //BaseUtils.permissionsEnableRequest(this,"gallery")
                            return
                        } else {
                            BaseUtils.displayManuallyEnablePermissionsDialog(
                                this,
                                Constants.IntentKeys.GALLERY,
                                null
                            )
                        }
                    }
                }
        }
    }

    fun listener() {
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.linearLayoutBgTint(binding.linearConfirm, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.attach, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.btnChangepincode, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.shopmore, sharedHelper.primaryColor, null)
        UiUtils.imageViewTint(binding.imgCoupon, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.btnCheckpincode, sharedHelper.primaryColor, null)
        UiUtils.radioButtonTint(binding.radioHomedelivery, sharedHelper.primaryColor, null)
        UiUtils.radioButtonTint(binding.radioPickup, sharedHelper.primaryColor, null)
        UiUtils.imageViewTint(binding.selectedCard, sharedHelper.primaryColor, null)
        UiUtils.imageViewTint(binding.selectedCod, sharedHelper.primaryColor, null)
        UiUtils.imageViewTint(binding.selectedUpi, sharedHelper.primaryColor, null)
        UiUtils.checkBoxTint(binding.walletCheckbox, sharedHelper.primaryColor, null)
        UiUtils.textViewTextColor(binding.msgSuccess, sharedHelper.primaryColor, null)
        UiUtils.imageViewTint(binding.cpy, sharedHelper.primaryColor, null)

        binding.cpy.setOnClickListener {
            val myClipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val myClip: ClipData = ClipData.newPlainText("Label", sharedHelper.shopWebData.profile.payment_mode)
            myClipboard.setPrimaryClip(myClip)
            Toast.makeText(this, "UPI ID copied to clipboard", Toast.LENGTH_LONG).show()
        }
        binding.edtPincode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 6) {
                    checkPinCode()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int,
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int,
            ) {
            }
        })
        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.attach.setOnClickListener {
            isAttach1 = false
            isAttach2 = true
            DialogUtils.showPictureDialog(this)
        }
        binding.linearConfirm.setOnClickListener {
            proceedClick()
        }
        binding.btnCheckpincode.setOnClickListener {
            checkPinCode()
        }
        binding.availablepincode.setOnClickListener {
            binding.txtAvailablepincode.visibility = View.VISIBLE
            binding.recyclerPincode.visibility = View.VISIBLE
            getAvailablePinCode()
        }
        binding.btnChangepincode.setOnClickListener {
            pinCodeUnSeleted()
        }
        binding.addrRefresh.setOnClickListener {
            odeliverycount = nAddressData.size
            getAddress()
        }
        binding.imgCoupon.setOnClickListener {
            if (binding.coupon.text.isNotEmpty()) {
                checkCoupon(spincode!!, binding.coupon.text.toString())
            } else {
                UiUtils.showSnack(binding.root, getString(R.string.please_enter_coupon))
            }
        }
        binding.coupon.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                BaseUtils.closeKeyBoard(binding.coupon, this)
                if (binding.coupon.text.isNotEmpty()) {
                    checkCoupon(spincode!!, binding.coupon.text.toString())
                } else {
                    UiUtils.showSnack(binding.root, getString(R.string.please_enter_coupon))
                }
                return@OnEditorActionListener true
            }
            false
        })
        binding.linearDeliverydate.setOnClickListener {
            val calendar = Calendar.getInstance()
            val year1 = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this, { _, year, monthOfYear, dayOfMonth ->
                val date = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                val sday = BaseUtils.getFormattedDate(date, "dd-MM-yyyy", "EEEE")
                Log.d("fggfcg1", "" + date)
                Log.d(
                    "fggfcg2",
                    "" + BaseUtils.getFormattedDate(date, "dd-MM-yyyy", "EEEE dd-MMM-yyyy")
                )
                binding.ddate.text =
                    BaseUtils.getFormattedDate(date, "dd-MM-yyyy", "EEEE dd-MMM-yyyy")
                deliverydate = binding.ddate.text.toString()

                if (timeData!!.isNotEmpty()) {
                    for (items in timeData!!) {
                        if (items.day.equals(sday)) {
                            binding.cardTimeslot.visibility = View.VISIBLE
                            binding.recyclerTime.layoutManager = LinearLayoutManager(
                                this,
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                            binding.recyclerTime.adapter = TimeAdapter(
                                this,
                                this,
                                items.timing
                            )
                            break
                        } else {
                            binding.cardTimeslot.visibility = View.GONE
                            deliveryslot = ""
                        }
                    }
                } else {
                    binding.cardTimeslot.visibility = View.GONE
                    deliveryslot = ""
                }

            }, year1, month, day)

            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
            datePickerDialog.show()
        }
        binding.cardUploadproduct.setOnClickListener {
            isAttach1 = true
            isAttach2 = false
            DialogUtils.showPictureDialog(this)
        }
        binding.linearHomedelivery.setOnClickListener {
            homeDeliveryClick()
        }
        binding.radioHomedelivery.setOnClickListener {
            homeDeliveryClick()
        }
        binding.linearPickup.setOnClickListener {
            pickupDeliveryClick()
        }
        binding.radioPickup.setOnClickListener {
            pickupDeliveryClick()
        }
        binding.linearAddaddrs.setOnClickListener {
            odeliverycount = nAddressData.size
            isaddaddress = true
            val args = Bundle()
            args.putBoolean(Constants.IntentKeys.IS_CHECKOUT, true)
            args.putString(Constants.IntentKeys.PIN_CODE, spincode)
            BaseUtils.startActivity(this, AddAddressActivity(), args, false)
        }
        binding.linearPayCard.setOnClickListener {
            cardPayClick()
        }
        binding.linearPayGpay.setOnClickListener {
            upiPayClick()
        }
        binding.linearPayCod.setOnClickListener {
            codPayClick()
        }
        binding.walletCheckbox.setOnClickListener {
            if (iswalletapproved == true) {
                cardPayUnselected()
                upiPayUnselected()
                codPayUnselected()
            }
            /* else if(iswalletpartiallyapproved == true) {

            }*/
        }
        binding.close1.setOnClickListener {
            binding.relativeImg1.visibility = View.GONE
            imageData1 = null
        }
        binding.close2.setOnClickListener {
            binding.relativeImg2.visibility = View.GONE
            imageData2 = null
        }
        binding.shopmore.setOnClickListener {
            BaseUtils.startDashboardActivity(this, true,"")
        }
        binding.trackorder.setOnClickListener {
            BaseUtils.startDashboardActivity(this, true,Constants.IntentKeys.IS_ORDER)
            /* DialogUtils.showLoader(this)
                 mapviewModel.getorder(this,0)?.observe(this, Observer {
                     DialogUtils.dismissLoader()
                     it?.let {
                         it.error?.let { error ->
                             if (error) {
                                 UiUtils.showSnack(binding.root, it.message!!)
                             }
                             else {
                                 finish()
                                 val intent = Intent(this, DashBoardActivity::class.java)
                                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                 intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                 intent.putExtra("isorder",true)
                                 intent.putExtra("iscart",false)
                                 intent.putExtra("key",it.data!![0])
                                 startActivity(intent)
                             }
                         }
                     }
                 })*/
        }

        /*
        binding.closeAddress.setOnClickListener {
            binding.addaddress.visibility = View.VISIBLE
            binding.linearListadddress.visibility = View.VISIBLE
            binding.linearAddadddress.visibility = View.GONE
            binding.edtAddressfull.setText("")
            binding.edtAddresslandmark.setText("")
            binding.edtAddressname.setText("")
            flat = 0.0
            flng = 0.0
            deliveryid = "0"
            isaddaddress = false
            isselectedaddress = true
            getaddress()
        }
*/
    }

    private fun homeDeliveryClick() {
        binding.radioHomedelivery.isChecked = true
        binding.radioPickup.isChecked = false
        deliverytype = 2
        binding.cardAddcoupon.visibility = View.GONE
        flat = 0.0
        couponReload()
    }

    private fun pickupDeliveryClick() {
        binding.radioPickup.isChecked = true
        binding.radioHomedelivery.isChecked = false
        deliverytype = 1
        binding.cardAddcoupon.visibility = View.VISIBLE
        flat = 0.0
        val text1 =
            "${getString(R.string.shop_name)} ${":"} ${BaseUtils.nullCheckerStr(sharedHelper.shopWebData.shopper.name)}"
        val text2 =
            "${getString(R.string.shop_location)} ${":"} ${BaseUtils.nullCheckerStr(sharedHelper.shopWebData.shopper.location)}"
        binding.shopname.text = text1
        binding.shoplocation.text = text2
        couponReload()
    }

    private fun cardPayClick() {
        if (binding.walletCheckbox.isChecked) {
            if (iswalletapproved == true) {
                cardPaySelected()
                binding.walletCheckbox.isChecked = false
                payorderamount = orderamount!!.toDouble()
            } else if (iswalletpartiallyapproved == true) {
                cardPaySelected()
                payorderamount = orderamount!!.toDouble() - walletamount!!.toDouble()
            }
        } else {
            cardPaySelected()
            payorderamount = orderamount!!.toDouble()
        }
    }

    private fun codPayClick() {
        if (binding.walletCheckbox.isChecked) {
            if (iswalletapproved == true) {
                codPaySelected()
                binding.walletCheckbox.isChecked = false
            } else if (iswalletpartiallyapproved == true) {
                codPaySelected()
            }
        } else {
            codPaySelected()
        }
    }

    private fun upiPayClick() {
        if (binding.walletCheckbox.isChecked) {
            if (iswalletapproved == true) {
                upiPaySelected()
                binding.walletCheckbox.isChecked = false
            } else if (iswalletpartiallyapproved == true) {
                upiPaySelected()
            }
        } else {
            upiPaySelected()
        }
    }

    private fun cardPaySelected() {
        codPayUnselected()
        upiPayUnselected()
        binding.selectedCard.visibility = View.VISIBLE
        binding.linearPayCard.background = UiUtils.getAngleDrawable(
            this,
            R.color.bg_color,
            floatArrayOf(5F, 5F, 5F, 5F, 5F, 5F, 5F, 5F),
            1,
            sharedHelper.primaryColor
        )
    }

    private fun cardPayUnselected() {
        binding.selectedCard.visibility = View.INVISIBLE
        binding.linearPayCard.background =
            AppCompatResources.getDrawable(this, R.drawable.border_address)
    }

    private fun upiPaySelected() {
        binding.attach.visibility = View.VISIBLE
        codPayUnselected()
        cardPayUnselected()
        binding.selectedUpi.visibility = View.VISIBLE
        binding.linearPayGpay.background = UiUtils.getAngleDrawable(
            this,
            R.color.bg_color,
            floatArrayOf(5F, 5F, 5F, 5F, 5F, 5F, 5F, 5F),
            1,
            sharedHelper.primaryColor
        )
        upiLoad(true)
    }

    private fun upiPayUnselected() {
        binding.attach.visibility = View.GONE
        binding.selectedUpi.visibility = View.INVISIBLE
        binding.linearPayGpay.background =
            AppCompatResources.getDrawable(this, R.drawable.border_address)
        upiLoad(false)
    }

    private fun codPaySelected() {
        cardPayUnselected()
        upiPayUnselected()
        binding.selectedCod.visibility = View.VISIBLE
        binding.linearPayCod.background = UiUtils.getAngleDrawable(
            this,
            R.color.bg_color,
            floatArrayOf(5F, 5F, 5F, 5F, 5F, 5F, 5F, 5F),
            1,
            sharedHelper.primaryColor
        )
    }

    private fun codPayUnselected() {
        binding.selectedCod.visibility = View.INVISIBLE
        binding.linearPayCod.background =
            AppCompatResources.getDrawable(this, R.drawable.border_address)
    }

    private fun pinCodeSelected(pincode: String) {
        binding.txtAvailablepincode.visibility = View.GONE
        binding.availablepincode.visibility = View.GONE
        binding.recyclerPincode.visibility = View.GONE
        binding.linearCheckpincode.visibility = View.GONE
        binding.linearChangepincode.visibility = View.VISIBLE
        val text = "${getString(R.string.delivery_to)} $pincode"
        binding.fpincode.text = text
        binding.edtPinAdd.setText(pincode)
        // binding.edtPincode.setText(pincode)
        binding.cardAddcoupon.visibility = View.VISIBLE
        couponReload()
        binding.cardAddress.visibility = View.VISIBLE
        spincode = pincode
        getAddress()
    }

    private fun pinCodeUnSeleted() {
        binding.txtAvailablepincode.visibility = View.GONE
        binding.availablepincode.visibility = View.GONE
        binding.recyclerPincode.visibility = View.GONE
        binding.linearCheckpincode.visibility = View.VISIBLE
        binding.linearChangepincode.visibility = View.GONE
        binding.cardAddcoupon.visibility = View.GONE
        couponReload()
        binding.cardAddress.visibility = View.GONE
        spincode = ""
    }

    private fun loadPage() {
        if (backcount == 0) {
            binding.linearShop.visibility = View.GONE
            binding.cardSummary.visibility = View.GONE
            binding.cardDeliveryoption.visibility = View.VISIBLE
            binding.cardPincode.visibility = View.GONE
            binding.cardAddcoupon.visibility = View.GONE
            binding.cardDeliverydate.visibility = View.GONE
            binding.cardTimeslot.visibility = View.GONE
            binding.cardUploadproduct.visibility = View.VISIBLE
            binding.relativeImg1.visibility = View.GONE
            binding.cardDeliverynotes.visibility = View.VISIBLE
            binding.cardAddress.visibility = View.GONE
            binding.cardPaymentmode.visibility = View.GONE
            binding.attach.visibility = View.GONE
            binding.relativeImg2.visibility = View.GONE
            binding.confirmTxt.visibility = View.VISIBLE
            binding.totalamount.visibility = View.GONE
            binding.confirmTxt.gravity = Gravity.CENTER
            binding.confirmTxt.text = getString(R.string.proceed)
            val text = "${getString(R.string.checkout)}${"(1/3)"}"
            binding.checkout.text = text

            UiUtils.imageviewDrawable(binding.imgTrack1, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack1, sharedHelper.primaryColor, null)
            UiUtils.textViewTextColor(binding.txtTrack1, sharedHelper.primaryColor, null)

            UiUtils.imageviewDrawable(binding.imgTrack2, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack2, null, R.color.grey)
            UiUtils.textViewTextColor(binding.txtTrack2, null, R.color.grey)

            UiUtils.imageviewDrawable(binding.imgTrack3, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack3, null, R.color.grey)
            UiUtils.textViewTextColor(binding.txtTrack3, null, R.color.grey)
        } else if (backcount == 1) {
            if (deliverytype == 1) {
                binding.cardSummary.visibility = View.VISIBLE
                binding.cardDeliveryoption.visibility = View.GONE
                binding.cardPincode.visibility = View.GONE
                binding.cardAddcoupon.visibility = View.GONE
                binding.cardDeliverydate.visibility = View.GONE
                binding.cardTimeslot.visibility = View.GONE
                binding.cardUploadproduct.visibility = View.GONE
                binding.relativeImg1.visibility = View.GONE
                binding.cardDeliverynotes.visibility = View.GONE
                binding.cardAddress.visibility = View.GONE
                binding.cardPaymentmode.visibility = View.VISIBLE
                binding.attach.visibility = View.GONE
                binding.relativeImg2.visibility = View.GONE
                binding.confirmTxt.visibility = View.VISIBLE
                binding.linearShop.visibility = View.VISIBLE
                binding.confirmTxt.text = getString(R.string.place_order)
                binding.totalamount.visibility = View.VISIBLE
                binding.confirmTxt.gravity = Gravity.START
                binding.totalamount.text = binding.total2.text
                val text = "${getString(R.string.checkout)}${"(2/3)"}"
                binding.checkout.text = text

                UiUtils.imageviewDrawable(binding.imgTrack1, R.drawable.ic_done_overlay)
                UiUtils.imageViewTint(binding.imgTrack1, null, R.color.black)
                UiUtils.textViewTextColor(binding.txtTrack1, null, R.color.black)

                UiUtils.imageviewDrawable(binding.imgTrack2, R.drawable.ic_point)
                UiUtils.imageViewTint(binding.imgTrack2, sharedHelper.primaryColor, null)
                UiUtils.textViewTextColor(binding.txtTrack2, sharedHelper.primaryColor, null)

                UiUtils.imageviewDrawable(binding.imgTrack3, R.drawable.ic_point)
                UiUtils.imageViewTint(binding.imgTrack3, null, R.color.grey)
                UiUtils.textViewTextColor(binding.txtTrack3, null, R.color.grey)
            } else if (deliverytype == 2) {
                binding.linearShop.visibility = View.GONE
                binding.cardSummary.visibility = View.GONE
                binding.cardDeliveryoption.visibility = View.GONE
                binding.cardPincode.visibility = View.VISIBLE
                binding.cardAddcoupon.visibility = View.GONE
                binding.cardDeliverydate.visibility = View.VISIBLE
                binding.cardTimeslot.visibility = View.VISIBLE
                binding.cardUploadproduct.visibility = View.GONE
                binding.relativeImg1.visibility = View.GONE
                binding.cardDeliverynotes.visibility = View.GONE
                if (spincode.equals("")) {
                    binding.cardAddress.visibility = View.GONE
                    binding.cardAddcoupon.visibility = View.GONE
                } else {
                    binding.cardAddress.visibility = View.VISIBLE
                    binding.cardAddcoupon.visibility = View.VISIBLE
                }
                binding.cardPaymentmode.visibility = View.GONE
                binding.attach.visibility = View.GONE
                binding.relativeImg2.visibility = View.GONE
                binding.confirmTxt.visibility = View.VISIBLE
                binding.totalamount.visibility = View.GONE
                binding.confirmTxt.gravity = Gravity.CENTER
                binding.confirmTxt.text = getString(R.string.proceed_to_payment)
                val text = "${getString(R.string.checkout)}${"(1/3)"}"
                binding.checkout.text = text

                UiUtils.imageviewDrawable(binding.imgTrack1, R.drawable.ic_point)
                UiUtils.imageViewTint(binding.imgTrack1, sharedHelper.primaryColor, null)
                UiUtils.textViewTextColor(binding.txtTrack1, sharedHelper.primaryColor, null)

                UiUtils.imageviewDrawable(binding.imgTrack2, R.drawable.ic_point)
                UiUtils.imageViewTint(binding.imgTrack2, null, R.color.grey)
                UiUtils.textViewTextColor(binding.txtTrack2, null, R.color.grey)

                UiUtils.imageviewDrawable(binding.imgTrack3, R.drawable.ic_point)
                UiUtils.imageViewTint(binding.imgTrack3, null, R.color.grey)
                UiUtils.textViewTextColor(binding.txtTrack3, null, R.color.grey)
                showDeliveryDate()
            }
        } else if (backcount == 2) {
            binding.linearShop.visibility = View.GONE
            binding.cardSummary.visibility = View.VISIBLE
            binding.cardDeliveryoption.visibility = View.GONE
            binding.cardPincode.visibility = View.GONE
            binding.cardAddcoupon.visibility = View.GONE
            binding.cardDeliverydate.visibility = View.GONE
            binding.cardTimeslot.visibility = View.GONE
            binding.cardUploadproduct.visibility = View.GONE
            binding.relativeImg1.visibility = View.GONE
            binding.cardDeliverynotes.visibility = View.GONE
            binding.cardAddress.visibility = View.GONE
            binding.cardPaymentmode.visibility = View.VISIBLE
            binding.attach.visibility = View.GONE
            binding.relativeImg2.visibility = View.GONE
            binding.confirmTxt.visibility = View.VISIBLE
            binding.confirmTxt.text = getString(R.string.place_order)
            binding.totalamount.visibility = View.VISIBLE
            binding.confirmTxt.gravity = Gravity.START
            binding.totalamount.text = binding.total2.text
            val text = "${getString(R.string.checkout)}${"(2/3)"}"
            binding.checkout.text = text

            UiUtils.imageviewDrawable(binding.imgTrack1, R.drawable.ic_done_overlay)
            UiUtils.imageViewTint(binding.imgTrack1, null, R.color.black)
            UiUtils.textViewTextColor(binding.txtTrack1, null, R.color.black)

            UiUtils.imageviewDrawable(binding.imgTrack2, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack2, sharedHelper.primaryColor, null)
            UiUtils.textViewTextColor(binding.txtTrack2, sharedHelper.primaryColor, null)

            UiUtils.imageviewDrawable(binding.imgTrack3, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack3, null, R.color.grey)
            UiUtils.textViewTextColor(binding.txtTrack3, null, R.color.grey)
        } else if (backcount == 3) {
            val text = "${getString(R.string.checkout)}${"(3/3)"}"
            binding.checkout.text = text
            binding.linearConfirm.visibility = View.GONE
            binding.midScrool.visibility = View.GONE
            binding.midScroolSucess.visibility = View.VISIBLE

            UiUtils.imageviewDrawable(binding.imgTrack1, R.drawable.ic_done_overlay)
            UiUtils.imageViewTint(binding.imgTrack1, null, R.color.black)
            UiUtils.textViewTextColor(binding.txtTrack1, null, R.color.black)

            UiUtils.imageviewDrawable(binding.imgTrack2, R.drawable.ic_done_overlay)
            UiUtils.imageViewTint(binding.imgTrack2, null, R.color.black)
            UiUtils.textViewTextColor(binding.txtTrack2, null, R.color.black)

            UiUtils.imageviewDrawable(binding.imgTrack3, R.drawable.ic_point)
            UiUtils.imageViewTint(binding.imgTrack3, sharedHelper.primaryColor, null)
            UiUtils.textViewTextColor(binding.txtTrack3, sharedHelper.primaryColor, null)

            if (isorderplaced) {
                Glide.with(this)
                    .load(AppCompatResources.getDrawable(this, R.drawable.order_success)).apply(
                        RequestOptions()
                            .placeholder(R.drawable.no_image)
                            .error(R.mipmap.ic_launcher)
                    ).into(binding.orderImg)
                val texto = "${getString(R.string.your_order_number_is)}$orderid"
                binding.orderid.text = texto
                DialogUtils.showNotify(Constants.NotificationKeys.ORDER_PLACED, this)

                var dcontent = getString(R.string.thank_you_for_your_order)
                if (sharedHelper.shopWebData.ordersuccess != null && sharedHelper.shopWebData.ordersuccess!!.isNotEmpty()) {
                    val spanned = HtmlCompat.fromHtml(
                        sharedHelper.shopWebData.ordersuccess.toString(),
                        HtmlCompat.FROM_HTML_MODE_LEGACY
                    )
                    val spanned1 = spanned.toString().replace(Constants.IntentKeys.VAR1, orderid)
                    val spanned2 = spanned1.replace(
                        Constants.IntentKeys.VAR2,
                        BaseUtils.nullCheckerStr(sharedHelper.shopWebData.name)
                    )
                    dcontent = spanned2
                    binding.msgSuccess.text = dcontent
                } else {
                    binding.msgSuccess.text = dcontent
                }
            } else {
                Glide.with(this).load(AppCompatResources.getDrawable(this, R.drawable.order_failed))
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.no_image)
                            .error(R.mipmap.ic_launcher)
                    ).into(binding.orderImg)
                binding.orderid.text = getString(R.string.order_failed_msg)
                // DialogUtils.shownotify(2,this)
                binding.msgSuccess.text = getString(R.string.oops_something_went_wrong)
            }

        }
    }

    private fun proceedClick() {
        if (backcount == 0) {
            if (deliverytype == 0) {
                UiUtils.showSnack(binding.root, getString(R.string.choose_delivery_option))
            } else {
                backcount = 1
                loadPage()
            }
        } else if (backcount == 1) {
            if (deliverytype == 1) {
                // pickup
                placeOrder()
            } else if (deliverytype == 2) {
                // home delivery
                if (spincode.equals("") || spincode!!.isEmpty()) {
                    UiUtils.showSnack(binding.root, getString(R.string.choose_pincode_first))
                } else {
                    if (isaddaddress) {
                        locationname = binding.edtAddressname.text.toString()
                        address = binding.edtAddressfull.text.toString()
                        landmark = binding.edtAddresslandmark.text.toString()
                        if (locationname.equals("") || locationname!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_address_name)
                            )
                        } else if (address!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_address)
                            )
                        } else if (landmark!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_landmark)
                            )
                        } else {
                            backcount = 2
                            loadPage()
                        }
                    } else if (isselectedaddress) {
                        if (deliveryid.equals("0")) {
                            UiUtils.showSnack(binding.root, getString(R.string.choose_address))
                        } else {
                            backcount = 2
                            loadPage()
                        }
                    } else {
                        UiUtils.showSnack(
                            binding.root,
                            getString(R.string.please_select_or_add_address)
                        )
                    }
                }
            }
        } else if (backcount == 2) {
            placeOrder()
        }
    }

    private fun cartSummary(
        getcartData: GetCartData?,
        pindata: Ispindata?,
        coupondata: CouponData?,
    ) {
        if (getcartData != null) {
            orderamount = getcartData.grand_total!!.toDouble()
            if(UiUtils.formattedValues(getcartData.total_save_value) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.saved.visibility = View.VISIBLE
                val text1 = "${getString(R.string.you_saved)} ${":"} ${Constants.Common.CURRENCY}${
                    UiUtils.formattedValues(getcartData.total_save_value)
                }"
                binding.saved.text = text1
            } else {
                binding.saved.visibility = View.GONE
            }

            val text4 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(getcartData.grand_total)}"
            binding.total2.text = text4

            if (getcartData.order!!.size == 1) {
                val text2 =
                    "${getString(R.string.subtotal)} ${"("}${getcartData.order!!.size} ${getString(R.string.item)} ${")"}"
                binding.subtotalTxt.text = text2
            } else {
                val text3 =
                    "${getString(R.string.subtotal)} ${"("}${getcartData.order!!.size} ${getString(R.string.items)} ${")"}"
                binding.subtotalTxt.text = text3
            }

            val text5 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(getcartData.order_amount)}"
            binding.subtotal.text = text5

            if(UiUtils.formattedValues(getcartData.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.linearGst.visibility = View.VISIBLE
                val text6 = "${getString(R.string.gst)} ${"("}${getcartData.gst.toString()} ${"%)"}"
                binding.gstPer.text = text6
                val text7 =
                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(getcartData.gst_amount)}"
                binding.gstAmount.text = text7
            } else {
                binding.linearGst.visibility = View.GONE
            }
        } else if (pindata != null) {
            orderamount = pindata.grand_total!!.toDouble()
            if(UiUtils.formattedValues(pindata.total_save_value) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.saved.visibility = View.VISIBLE
                val text1 = "${getString(R.string.you_saved)} ${":"} ${Constants.Common.CURRENCY}${
                    UiUtils.formattedValues(pindata.total_save_value)
                }"
                binding.saved.text = text1
            } else {
                binding.saved.visibility = View.GONE
            }

            val text4 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(pindata.grand_total)}"
            binding.total2.text = text4

            val text5 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(pindata.order_amount)}"
            binding.subtotal.text = text5

            if(UiUtils.formattedValues(pindata.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.linearGst.visibility = View.VISIBLE
                val text6 = "${getString(R.string.gst)} ${"("}${pindata.gst.toString()} ${"%)"}"
                binding.gstPer.text = text6
                val text7 =
                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(pindata.gst_amount)}"
                binding.gstAmount.text = text7
            } else {
                binding.linearGst.visibility = View.GONE
            }

            if(UiUtils.formattedValues(pindata.delivery_charge!!) != Constants.IntentKeys.AMOUNT_DUMMY) {
                val text8 =
                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(pindata.delivery_charge)}"
                binding.deliveryAmount.text = text8
                binding.linearDeliverycharge.visibility = View.VISIBLE
            } else {
                binding.linearDeliverycharge.visibility = View.GONE
            }
        } else if (coupondata != null) {
            orderamount = coupondata.grand_total!!.toDouble()
            couponid = coupondata.couppen_id.toString()
            if(UiUtils.formattedValues(coupondata.total_save_value) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.saved.visibility = View.VISIBLE
                val text1 = "${getString(R.string.you_saved)} ${":"} ${Constants.Common.CURRENCY}${
                    UiUtils.formattedValues(coupondata.total_save_value)
                }"
                binding.saved.text = text1
            } else {
                binding.saved.visibility = View.GONE
            }

            val text4 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(coupondata.grand_total)}"
            binding.total2.text = text4

            val text5 =
                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(coupondata.order_amount)}"
            binding.subtotal.text = text5

            if(UiUtils.formattedValues(coupondata.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
                binding.linearGst.visibility = View.VISIBLE
                val text6 = "${getString(R.string.gst)} ${"("}${coupondata.gst.toString()} ${"%)"}"
                binding.gstPer.text = text6
                val text7 =
                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(coupondata.gst_amount)}"
                binding.gstAmount.text = text7
            } else {
                binding.linearGst.visibility = View.GONE
            }

            if(UiUtils.formattedValues(coupondata.delivery_charge) != Constants.IntentKeys.AMOUNT_DUMMY) {
                val text8 =
                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(coupondata.delivery_charge)}"
                binding.deliveryAmount.text = text8
                binding.linearDeliverycharge.visibility = View.VISIBLE
            } else {
                binding.linearDeliverycharge.visibility = View.GONE
            }
        }
    }

    private fun getWallet() {
        DialogUtils.showLoader(this)
        viewModel.getWallet(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        binding.walletCheckbox.visibility = View.GONE
                        binding.walletCheckbox.isChecked = false
                        iswalletapproved = false
                        iswalletpartiallyapproved = false
                    } else {
                        val text =
                            "${getString(R.string.are_you_sure_to_use_wallet_amount)} ${"( "} ${Constants.Common.CURRENCY}${sharedHelper.walletAmount} ${" )"}"
                        binding.walletCheckbox.text = text
                        walletamount = it.data!![0].avail_credit.toString()
                        if (walletamount!!.toDouble() > 0) {
                            binding.walletCheckbox.visibility = View.VISIBLE
                            binding.walletCheckbox.isChecked = true
                            if (orderamount!! < walletamount!!.toDouble()) {
                                iswalletapproved = true
                                iswalletpartiallyapproved = false
                                balanceamount =
                                    (walletamount!!.toDouble() - orderamount!!.toDouble()).toString()
                            } else {
                                iswalletapproved = false
                                iswalletpartiallyapproved = true
                                balanceamount = "0"
                            }
                        } else {
                            binding.walletCheckbox.visibility = View.GONE
                            binding.walletCheckbox.isChecked = false
                            iswalletapproved = false
                            iswalletpartiallyapproved = false
                        }
                    }
                }
            }
        }
    }

    fun getCart() {
        DialogUtils.showLoader(this)
        viewModel.getCart(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        cartSummary(it.getcart_data!!, null, null)
                        getWallet()
                    }
                }
            }
        }
    }

    fun checkPinCode() {
        UiUtils.txtBtnLoadingStart(binding.btnCheckpincode)
        if (deliverytype != 0) {
            if (binding.edtPincode.text.isNotEmpty() && binding.edtPincode.text.length == 6) {
                isPinCode(binding.edtPincode.text.toString())
            } else {
                UiUtils.txtBtnLoadingEnd(
                    binding.btnCheckpincode,
                    getString(R.string.check_delivery)
                )
                UiUtils.showSnack(binding.root, getString(R.string.please_enter_valid_pincode))
            }
        } else {
            UiUtils.txtBtnLoadingEnd(binding.btnCheckpincode, getString(R.string.check_delivery))
            UiUtils.showSnack(binding.root, getString(R.string.choose_delivery_option))
        }
    }

    fun isPinCode(pincode: String) {
        DialogUtils.showLoader(this)
        viewModel.isPinCode(this, pincode).observe(this) {
            DialogUtils.dismissLoader()
            UiUtils.txtBtnLoadingEnd(binding.btnCheckpincode, getString(R.string.check_delivery))
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        binding.availablepincode.visibility = View.VISIBLE
                        val text1 = if (BaseUtils.nullCheckerStr(it.message).isNotEmpty()) {
                            it.message!! + " "
                        } else {
                            BaseUtils.nullCheckerStr(sharedHelper.shopWebData.pincode_error_message) + " "
                        }
                        val text2 = getString(R.string.check_available_pincode)
                        val text3: String = (text1 + text2)
                        val spannable: Spannable = SpannableString(text3)
                        spannable.setSpan(
                            ForegroundColorSpan(Color.parseColor(sharedHelper.primaryColor)),
                            text1.length,
                            (text1 + text2).length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        binding.availablepincode.setText(spannable, TextView.BufferType.SPANNABLE)
                    } else {
                        cartSummary(null, it.data, null)
                        pinCodeSelected(pincode)
                    }
                }
            }
        }
    }

    private fun getAvailablePinCode() {
        DialogUtils.showLoader(this)
        viewModel.getAvailablePinCode(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        binding.recyclerPincode.layoutManager = LinearLayoutManager(
                            this,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        binding.recyclerPincode.adapter = PincodeAdapter(
                            this,
                            this,
                            it.data!!
                        )
                    }
                }
            }
        }
    }

    private fun couponReload() {
        binding.couponMsg.visibility = View.GONE
        binding.coupon.setText("")
        binding.imgCoupon.isEnabled = true
        binding.coupon.isEnabled = true
    }

    private fun checkCoupon(pinCode: String, coupon: String) {
        binding.couponMsg.visibility = View.GONE
        DialogUtils.showLoader(this)
        viewModel.checkCoupon(this, coupon, pinCode).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        binding.couponMsg.visibility = View.VISIBLE
                        binding.couponMsg.text = it.message
                        binding.coupon.setText("")
                        UiUtils.textViewTextColor(binding.couponMsg, null, R.color.red)
                    } else {
                        if (it.data != null) {
                            //UiUtils.showSnack(binding.root, it.message!!)
                            cartSummary(null, null, it.data)
                            binding.imgCoupon.isEnabled = false
                            binding.coupon.isEnabled = false
                            binding.couponMsg.visibility = View.VISIBLE
                            binding.couponMsg.text = it.message
                            UiUtils.textViewTextColor(binding.couponMsg, null, R.color.green)
                            binding.cardSummary.visibility = View.VISIBLE
                        } else {
                            // UiUtils.showSnack(binding.root, it.message!!)
                            binding.couponMsg.visibility = View.VISIBLE
                            binding.couponMsg.text = it.message
                            binding.coupon.setText("")
                            UiUtils.textViewTextColor(binding.couponMsg, null, R.color.red)
                        }
                    }
                }
            }
        }
    }

    private fun getTimeSlot() {
        DialogUtils.showLoader(this)
        viewModel.getTimeSlot(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    timeData = if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        ArrayList()
                        //  showdeliverydata()
                    } else {
                        it.data
                        // showdeliverydata()
                    }
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun showDeliveryDate() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow: Date = calendar.time
        val date: String = SimpleDateFormat("dd-MMM-yyyy").format(tomorrow)
        val sday: String = SimpleDateFormat("EEEE").format(tomorrow)
        val text = "$sday $date"
        binding.ddate.text = text
        deliverydate = binding.ddate.text.toString()

        if (timeData!!.isNotEmpty()) {
            for (items in timeData!!) {
                if (items.day.equals(sday)) {
                    binding.cardTimeslot.visibility = View.VISIBLE
                    binding.recyclerTime.layoutManager = LinearLayoutManager(
                        this,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                    binding.recyclerTime.adapter = TimeAdapter(
                        this,
                        this,
                        items.timing
                    )
                    break
                } else {
                    binding.cardTimeslot.visibility = View.GONE
                    deliveryslot = ""
                }
            }
        } else {
            binding.cardTimeslot.visibility = View.GONE
            deliveryslot = ""
        }

    }

    private fun getAddress() {
        DialogUtils.showLoader(this)
        viewModel.getAddressList(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        binding.linearListadddress.visibility = View.GONE
                        isselectedaddress = false
                    } else {
                        it.addressdata!!.reverse()
                        nAddressData.clear()
                        for (items in it.addressdata!!) {
                            if (spincode.equals(items.pincode)) {
                                nAddressData.add(items)
                            }
                        }

                        if (nAddressData.size > 0) {
                            isselectedaddress = true
                            if (odeliverycount == 0 && selectedDeliveryPosition == 0) {
                                selectedDeliveryPosition = 0
                                loadAddressRecycler()
                            } else if ((odeliverycount != nAddressData.size)) {
                                selectedDeliveryPosition = 0
                                loadAddressRecycler()
                            }
                        } else {
                            isselectedaddress = false
                            binding.linearListadddress.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun loadAddressRecycler() {
        binding.linearListadddress.visibility = View.VISIBLE
        binding.recyclerAddress.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerAddress.adapter = AddressListAdapter(
            1,
            this, null,
            this,
            nAddressData
        )
        binding.swiperefresh.setOnRefreshListener {
            odeliverycount = nAddressData.size
            getAddress()
            // This line is important as it explicitly refreshes only once
            // If "true" it implicitly refreshes forever
            binding.swiperefresh.isRefreshing = false
        }
    }

    private fun getDeliveryOption() {
        when (BaseUtils.nullCheckerInt(sharedHelper.shopWebData.profile.delivery_option)) {
            0 -> {
                binding.linearHomedelivery.visibility = View.VISIBLE
                binding.linearPickup.visibility = View.VISIBLE
            }
            1 -> {
                binding.linearHomedelivery.visibility = View.VISIBLE
                binding.linearPickup.visibility = View.GONE
                homeDeliveryClick()
            }
            2 -> {
                binding.linearHomedelivery.visibility = View.GONE
                binding.linearPickup.visibility = View.VISIBLE
                pickupDeliveryClick()
            }
        }
    }

    private fun getPayOption() {
        val payOption = BaseUtils.nullCheckerStr(sharedHelper.shopWebData.profile.pay_option)
        if (payOption.contains("1") && payOption.contains("2") && payOption.contains("3")) {
            binding.linearPayCard.visibility = View.VISIBLE
            binding.linearPayGpay.visibility = View.VISIBLE
            binding.linearPayCod.visibility = View.VISIBLE
        } else if (payOption.contains("1") && payOption.contains("2")) {
            binding.linearPayCard.visibility = View.GONE
            binding.linearPayGpay.visibility = View.VISIBLE
            binding.linearPayCod.visibility = View.VISIBLE
        } else if (payOption.contains("1") && payOption.contains("3")) {
            binding.linearPayCard.visibility = View.VISIBLE
            binding.linearPayGpay.visibility = View.VISIBLE
            binding.linearPayCod.visibility = View.GONE
        } else if (payOption.contains("2") && payOption.contains("3")) {
            binding.linearPayCard.visibility = View.VISIBLE
            binding.linearPayGpay.visibility = View.GONE
            binding.linearPayCod.visibility = View.VISIBLE
        } else if (payOption.contains("1")) {
            binding.linearPayCard.visibility = View.GONE
            binding.linearPayGpay.visibility = View.VISIBLE
            binding.linearPayCod.visibility = View.GONE
        } else if (payOption.contains("2")) {
            binding.linearPayCard.visibility = View.GONE
            binding.linearPayGpay.visibility = View.GONE
            binding.linearPayCod.visibility = View.VISIBLE
        } else if (payOption.contains("3")) {
            binding.linearPayCard.visibility = View.VISIBLE
            binding.linearPayGpay.visibility = View.GONE
            binding.linearPayCod.visibility = View.GONE
        } else if (payOption.contains("0")) {
            binding.linearPayCard.visibility = View.GONE
            binding.linearPayGpay.visibility = View.GONE
            binding.linearPayCod.visibility = View.VISIBLE
        } else {
            binding.linearPayCard.visibility = View.GONE
            binding.linearPayGpay.visibility = View.GONE
            binding.linearPayCod.visibility = View.VISIBLE
        }
    }

    private fun upiLoad(isvisible : Boolean){
        if(isvisible && BaseUtils.nullCheckerStr(sharedHelper.shopWebData.profile.payment_mode).isNotEmpty()){
            binding.upiRelative.visibility = View.VISIBLE
            val txt = "${"UPI ID :"} ${sharedHelper.shopWebData.profile.payment_mode}"
            binding.upiId.text = txt
        }
        else{
            binding.upiRelative.visibility = View.GONE
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?, paymentData: PaymentData) {
        // this method is called on payment success.
        Toast.makeText(this, "Payment is successful : $razorpayPaymentId", Toast.LENGTH_SHORT)
            .show()
        /* paymentData.data.getString("razorpay_order_id")
         paymentData.data.getString("razorpay_signature")
         paymentData.data.getString("razorpay_payment_id")*/
        paymentid = razorpayPaymentId
        placeOrder()

    }

    override fun onPaymentError(errorCode: Int, response: String?, paymentData: PaymentData?) {
        // on payment failed.
        paymentid = ""
        Toast.makeText(this, "Payment Failed due to error : $response", Toast.LENGTH_SHORT).show()
    }

    private fun checkRazorPay() {
        if (BaseUtils.nullCheckerStr(sharedHelper.shopWebData.profile.razorpay_api) != "" && BaseUtils.nullCheckerStr(
                sharedHelper.shopWebData.profile.razorpay_name
            ) != ""
        ) {
            razorPay()
        } else {
            UiUtils.showSnack(binding.root, "Payment Gateway Key Not Updated")
        }
    }

    private fun razorPay() {
        // on below line we are getting
        // amount that is entered by user.
        // on below line we are getting
        // amount that is entered by user.
        val payAmount: String = payorderamount.toString()

        // rounding off the amount.

        // rounding off the amount.
        val amount = (payAmount.toFloat() * 100).roundToInt()

        // initialize Razorpay account.

        // initialize Razorpay account.
        val checkout = Checkout()

        // set your id as below

        // set your id as below
        checkout.setKeyID(sharedHelper.shopWebData.profile.razorpay_api)

        // set image

        // set image
        checkout.setImage(R.mipmap.ic_launcher)

        // initialize json object

        // initialize json object
        val jobject = JSONObject()
        try {
            // to put name
            jobject.put("name", sharedHelper.shopWebData.profile.razorpay_name)

            // put description
            jobject.put("description", "Order Payment")

            // to set theme color
            jobject.put("theme.color", sharedHelper.primaryColor)

            // put the currency
            jobject.put("currency", Constants.IntentKeys.INR)

            // put amount
            jobject.put("amount", amount)

            // put mobile number
            jobject.put(
                "prefill.contact",
                "+" + sharedHelper.countryCode.toString() + sharedHelper.mobileNumber
            )

            // put email
            jobject.put("prefill.email", sharedHelper.email)
            Log.d("ghdjhfg", "" + jobject)

            // open razorpay to checkout activity
            checkout.open(this@CheckoutActivity, jobject)
        } catch (e: JSONException) {
            Toast.makeText(this, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)
                val filePath = BaseUtils.getRealPathFromUriNew(this, uri)
                if (filePath != null) {
                    Log.d("filePath", java.lang.String.valueOf(filePath))
                    val bitmap1: Bitmap = BitmapFactory.decodeFile(filePath)
                    val imagename = System.currentTimeMillis()
                    if (isAttach1) {
                        binding.relativeImg1.visibility = View.VISIBLE
                        UiUtils.loadCustomImage(
                            binding.attachImg1,
                            BaseUtils.getRealPathFromUriNew(this, uri)
                        )
                        imageData1 = DataPart(
                            "$imagename.jpg",
                            getFileDataFromDrawable(bitmap1)
                        )
                    } else if (isAttach2) {
                        binding.relativeImg2.visibility = View.VISIBLE
                        UiUtils.loadCustomImage(
                            binding.attachImg2,
                            BaseUtils.getRealPathFromUriNew(this, uri)
                        )
                        imageData2 = DataPart("$imagename.jpg", getFileDataFromDrawable(bitmap1))
                    }
                }
                DialogUtils.dismissLoader()
                // uploadFiles(BaseUtils.getRealPathFromURI(this, uri),"")
            } else {
                // UiUtils.showSnack(binding.root, "null")
                DialogUtils.dismissLoader()
            }
        } else {
            DialogUtils.dismissLoader()
        }
    }

    private fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper.imageUploadPath)
        if (uploadFile!!.exists()) {
            val imagename = System.currentTimeMillis()
            val bitmap1 = BitmapFactory.decodeFile(sharedHelper.imageUploadPath)
            if (isAttach1) {
                binding.relativeImg1.visibility = View.VISIBLE
                UiUtils.loadCustomImage(
                    binding.attachImg1,
                    sharedHelper.imageUploadPath
                )
                imageData1 = DataPart("$imagename.jpg", getFileDataFromDrawable(bitmap1))

            } else if (isAttach2) {
                binding.relativeImg2.visibility = View.VISIBLE
                UiUtils.loadCustomImage(
                    binding.attachImg2,
                    sharedHelper.imageUploadPath
                )
                imageData2 = DataPart("$imagename.jpg", getFileDataFromDrawable(bitmap1))
            }
            DialogUtils.dismissLoader()
        } else {
            DialogUtils.dismissLoader()
            UiUtils.showSnack(binding.root, getString(0))
        }
    }

    private fun placeOrder() {
        description = binding.deliverynotes.text.toString()

        if (binding.selectedCod.visibility == View.VISIBLE && binding.selectedCard.visibility != View.VISIBLE && binding.selectedUpi.visibility != View.VISIBLE && !binding.walletCheckbox.isChecked) {
            ordertype = 2
            //cod only
        } else if (binding.selectedCod.visibility != View.VISIBLE && binding.selectedCard.visibility == View.VISIBLE && binding.selectedUpi.visibility != View.VISIBLE && !binding.walletCheckbox.isChecked) {
            ordertype = 3
            // card only
        } else if (binding.selectedCod.visibility != View.VISIBLE && binding.selectedCard.visibility != View.VISIBLE && binding.selectedUpi.visibility == View.VISIBLE && !binding.walletCheckbox.isChecked) {
            ordertype = 1
            // upi only
        } else if (binding.selectedCod.visibility != View.VISIBLE && binding.selectedCard.visibility != View.VISIBLE && binding.selectedUpi.visibility != View.VISIBLE && binding.walletCheckbox.isChecked) {
            ordertype = if (iswalletapproved == true) {
                4
            } else {
                0
            }
        } else if (binding.selectedCod.visibility != View.VISIBLE && binding.selectedCard.visibility != View.VISIBLE && binding.selectedUpi.visibility != View.VISIBLE && !binding.walletCheckbox.isChecked) {
            ordertype = 0
        } else {
            ordertype = 5
        }


        if (deliverytype == 0) {
            UiUtils.showSnack(binding.root, getString(R.string.choose_delivery_option))
        } else {
            if (deliverytype == 1) {
                // pickup
                if (ordertype == 0) {
                    UiUtils.showSnack(binding.root, getString(R.string.choose_payment_mode))
                } else if (ordertype == 3) {
                    if (!paymentid.equals("")) {
                        placeOrderNew()
                    } else {
                        checkRazorPay()
                    }
                } else {
                    placeOrderNew()
                }
            } else if (deliverytype == 2) {
                // home delivery
                if (spincode.equals("") || spincode!!.isEmpty()) {
                    UiUtils.showSnack(binding.root, getString(R.string.choose_pincode_first))
                } else {
                    if (isaddaddress) {
                        locationname = binding.edtAddressname.text.toString()
                        address = binding.edtAddressfull.text.toString()
                        landmark = binding.edtAddresslandmark.text.toString()
                        if (locationname.equals("") || locationname!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_address_name)
                            )
                        } else if (address!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_address)
                            )
                        } else if (landmark!!.isEmpty()) {
                            UiUtils.showSnack(
                                binding.root,
                                getString(R.string.please_enter_landmark)
                            )
                        } else if (ordertype == 0) {
                            UiUtils.showSnack(binding.root, getString(R.string.choose_payment_mode))
                        } else if (ordertype == 3) {
                            if (!paymentid.equals("")) {
                                placeOrderNew()
                            } else {
                                checkRazorPay()
                            }
                        } else {
                            placeOrderNew()
                        }
                    } else if (isselectedaddress) {
                        if (deliveryid.equals("0")) {
                            UiUtils.showSnack(binding.root, getString(R.string.choose_address))
                        } else {
                            if (ordertype == 0) {
                                UiUtils.showSnack(
                                    binding.root,
                                    getString(R.string.choose_payment_mode)
                                )
                            } else if (ordertype == 3) {
                                if (!paymentid.equals("")) {
                                    placeOrderNew()
                                } else {
                                    checkRazorPay()
                                }
                            } else {
                                placeOrderNew()
                            }
                        }
                    } else {
                        UiUtils.showSnack(
                            binding.root,
                            getString(R.string.please_select_or_add_address)
                        )
                    }
                }
            }
        }
    }

    private fun getBodyParams(): Map<String, String> {
        val params: MutableMap<String, String> = HashMap()
        params["shopper_id"] = sharedHelper.shopId.toString()
        params["order_type"] = ordertype.toString()
        params["coupon_id"] = couponid.toString()
        params["delivery_type"] = deliverytype.toString()
        params["pincode"] = spincode.toString()
        params["location_name"] = locationname.toString()
        params["address"] = address.toString()
        params["land_mark"] = landmark.toString()
        when {
            isselectedaddress -> {
                params["location_lat"] = flat.toString()
                params["location_long"] = flng.toString()
                params["delivery_id"] = deliveryid.toString()
            }
            isaddaddress -> {
                params["location_lat"] = flat.toString()
                params["location_long"] = flng.toString()
                params["delivery_id"] = ""
            }
            else -> {
                params["location_lat"] = ""
                params["location_long"] = ""
                params["delivery_id"] = ""
            }
        }
        params["description"] = description.toString()
        params["delivery_date"] = deliverydate.toString()
        params["delivery_slot"] = deliveryslot.toString()
        params["payment_id"] = paymentid.toString()
        params["wallet_amount"] = walletamount.toString()
        params["balance_amount"] = balanceamount.toString()
        return params
    }

    private fun getImgParams(): Map<String, DataPart> {
        val params: MutableMap<String, DataPart> = HashMap()
        // multi files
        /*val uploads = arrayOfNulls<File>(images.size())
        for (i in 0 until images.size()) {
            try {
                params["uploads[$i]"] = DataPart(FileName + i + ".jpg", b, "image/*")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }*/*/

        if (imageData1 != null && imageData2 != null) {
            // long imagename = System.currentTimeMillis();
            params["file"] = imageData2!!
            params["non_image"] = imageData1!!
        } else if (imageData1 != null) {
            params["non_image"] = imageData1!!
        } else if (imageData2 != null) {
            params["file"] = imageData2!!
        }
        return params
    }

    private fun placeOrderNew() {
        DialogUtils.showLoader(this)
        val bodyparams: Map<String, String> = getBodyParams()
        val imgparams: Map<String, DataPart> = getImgParams()
        viewModel.placeOrder(this, bodyparams, imgparams).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                        orderUnplaced()
                    } else {
                        orderPlaced(it.order_id.toString())
                    }
                }
            }
        }
    }

    private fun orderPlaced(order_id: String) {
        isorderplaced = true
        backcount = 3
        orderid = order_id
        loadPage()
    }

    private fun orderUnplaced() {
        isorderplaced = false
        backcount = 3
        loadPage()
    }
}

//private fun placeOrderOld() {
//        DialogUtils.showLoader(this)
//        val volleyMultipartRequest: VolleyMultipartRequest = object : VolleyMultipartRequest(
//            Method.POST, UrlHelper(this).PLACEORDER,
//            Response.Listener { response ->
//                DialogUtils.dismissLoader()
//                Log.d("orderresponse2", "" + String(response.data))ene
//                try {
//                    val obj = JSONObject(String(response.data))
//                    if (!obj.getBoolean("error")) {
//                        orderPlaced(obj.getString("order_id"))
//                    } else {
//                        UiUtils.showSnack(binding.root, obj.getString("status"))
//                        orderUnplaced()
//                    }
//                } catch (e: JSONException) {
//                    e.printStackTrace()
//                    UiUtils.showSnack(binding.root, "Backend Side Issue")
//                    orderUnplaced()
//                }
//            },
//            Response.ErrorListener { error ->
//                DialogUtils.dismissLoader()
//                orderUnplaced()
//                Log.d("hfhg", "" + error)
//                Log.d("hfhg2", "" + error.message)
//                if (error.networkResponse != null) {
//                    val response: NetworkResponse = error.networkResponse
//                    Log.d("hgvxdhf3", "" + String(response.data))
//                }
//            }) {
//            override fun getHeaders(): Map<String, String> {
//                val params: MutableMap<String, String> = HashMap()
//                params["Authorization"] = "Bearer " + sharedHelper.token
//                Log.d("param header", "" + params)
//                return params
//            }
//
//            @Throws(AuthFailureError::class)
//            override fun getParams(): Map<String, String> {
//                val params: MutableMap<String, String> = HashMap()
//                params["shopper_id"] = sharedHelper.shopId.toString()
//                params["order_type"] = ordertype.toString()
//                params["coupon_id"] = couponid.toString()
//                params["delivery_type"] = deliverytype.toString()
//                params["pincode"] = spincode.toString()
//                params["location_name"] = locationname.toString()
//                params["address"] = address.toString()
//                params["land_mark"] = landmark.toString()
//                if (isselectedaddress) {
//                    params["location_lat"] = flat.toString()
//                    params["location_long"] = flng.toString()
//                    params["delivery_id"] = deliveryid.toString()
//                } else if (isaddaddress) {
//                    params["location_lat"] = flat.toString()
//                    params["location_long"] = flng.toString()
//                    params["delivery_id"] = ""
//                } else {
//                    params["location_lat"] = ""
//                    params["location_long"] = ""
//                    params["delivery_id"] = ""
//                }
//                params["description"] = description.toString()
//                params["delivery_date"] = deliverydate.toString()
//                params["delivery_slot"] = deliveryslot.toString()
//                params["payment_id"] = paymentid.toString()
//                params["wallet_amount"] = walletamount.toString()
//                params["balance_amount"] = balanceamount.toString()
//                Log.d("param order body", "" + params)
//                return getBodyParams()
//            }
//
//            override fun getByteData(): Map<String, DataPart> {
//                val params: MutableMap<String, DataPart> = HashMap()
//                // multi files
//                //val uploads = arrayOfNulls<File>(images.size())
//                //for (i in 0 until images.size()) {
//                //   try {
//                //      params["uploads[$i]"] = DataPart(FileName + i + ".jpg", b, "image/*")
//                //  } catch (e: Exception) {
//                //      e.printStackTrace()
//                //  }
//                //}
//                if (imageData1 != null && imageData2 != null) {
//                    // long imagename = System.currentTimeMillis();
//                    params["file"] = imageData2!!
//                    params["non_image"] = imageData1!!
//                } else if (imageData1 != null) {
//                    params["non_image"] = imageData1!!
//                } else if (imageData2 != null) {
//                    params["file"] = imageData2!!
//                }
//                Log.d("param img", "" + params)
//                return getImgParams()
//            }
//        }
////adding the request to volley
////Volley.newRequestQueue(this).add(volleyMultipartRequest)
//        VolleySingleton.getInstance(this).addToRequestQueue(
//            volleyMultipartRequest.setRetryPolicy(
//                DefaultRetryPolicy(
//                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
//                    0,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
//                )
//            )
//        )
//    }
