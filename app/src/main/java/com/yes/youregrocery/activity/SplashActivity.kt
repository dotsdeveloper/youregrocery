package com.yes.youregrocery.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.yes.youregrocery.`interface`.AnimationCallBack
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.databinding.ActivitySplashBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.NetworkUtils

class SplashActivity : BaseActivity() {
    lateinit var binding: ActivitySplashBinding
    private var isEnd = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Youregrocery)
        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        sharedHelper.baseurl = "https://yourestore.in/yes-backend/api/"
        sharedHelper.baseId = 1
        if(sharedHelper.primaryColor.isNotEmpty()){
            UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        }

        UiUtils.animationWithCallback(this,view,R.anim.screen_in,object : AnimationCallBack{
            override fun onAnimationStart() {
            }

            override fun onAnimationEnd() {
                if(!sharedHelper.isInstallFirst){
                    BaseUtils.permissionsEnableRequest(this@SplashActivity,Constants.IntentKeys.ALL)
                }
                else{
                    proceed()
                }
            }
            override fun onAnimationRepeat() {
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("xcbnc",""+requestCode)
        when (requestCode) {
            Constants.RequestCode.ALL_PERMISSION_REQUEST ->
                if (grantResults.isNotEmpty()) {
                    if (BaseUtils.grantResults(grantResults)) {
                       gps()
                    }
                    else {
                        gps()
                    }
                }
        }
    }

    private fun gps(){
        if(!BaseUtils.isGpsEnabled(this@SplashActivity)){
            BaseUtils.gpsEnableRequest(this@SplashActivity)
        }
        else {
            proceed()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("xcbnc",""+requestCode)
        if(requestCode == Constants.RequestCode.GPS_REQUEST){
            if(!BaseUtils.isGpsEnabled(this)){
                proceed()
            }
            else {
                proceed()
            }
        }
    }

    fun proceed(){
        if(isValid()){
            if(sharedHelper.shopId == 0) {
                BaseUtils.startActivity(this,BranchActivity(),null,true)
            }
            else{
                BaseUtils.startDashboardActivity(this,false,"")
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            !NetworkUtils.isNetworkConnected(this) -> {
                NetworkUtils.noNetworkDialog(this)
                return false
            }
            !BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.ALL) -> {
               // BaseUtils.permissionsEnableRequest(this,"all")
                return true
            }
            !BaseUtils.isGpsEnabled(this) -> {
               // BaseUtils.gpsEnableRequest(this)
                return true
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        if(isEnd){
            isEnd = false
            proceed()
        }
    }
}
