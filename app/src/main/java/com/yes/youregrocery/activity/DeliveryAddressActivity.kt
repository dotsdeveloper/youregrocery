package com.yes.youregrocery.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.adapter.AddressListAdapter
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityDeliveryAddressBinding
import com.yes.youregrocery.utils.BaseUtils

class DeliveryAddressActivity : BaseActivity() {
    lateinit var binding: ActivityDeliveryAddressBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeliveryAddressBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        binding.back.setOnClickListener {
            onBackPressed()
        }

        binding.addaddress.setOnClickListener {
            BaseUtils.startActivity(this, AddAddressActivity(), null, false)
        }
    }

    override fun onStart() {
        super.onStart()
        load()
    }

    fun load(){
        DialogUtils.showLoader(this)
        viewModel.getAddressList(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        it.addressdata!!.reverse()
                        binding.recyclerAddress.layoutManager = LinearLayoutManager(this,
                            LinearLayoutManager.VERTICAL,
                            false)
                        binding.recyclerAddress.adapter = AddressListAdapter(
                            0,
                            null, this,
                            this,
                            it.addressdata
                        )
                    }
                }
            }
        }
    }
}