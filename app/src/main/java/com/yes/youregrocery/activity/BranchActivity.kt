package com.yes.youregrocery.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.R
import com.yes.youregrocery.adapter.BranchAdapter
import com.yes.youregrocery.network.UrlHelper
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityBranchBinding
import java.util.regex.Pattern
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import androidx.core.content.res.ResourcesCompat

class BranchActivity : BaseActivity() {
    lateinit var binding: ActivityBranchBinding
    val filter =
        InputFilter { source, start, end, _, _, _ ->
            for (i in start until end) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*")
                        .matcher(
                            source[i].toString()
                        ).matches()
                ) {
                    return@InputFilter ""
                }
            }
            null
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBranchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_branch)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
        load()
    }

    fun listener(){
        binding.swiperefresh.setOnRefreshListener {
            load()
            binding.swiperefresh.isRefreshing = false
        }
        binding.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (Pattern.matches("[0-9 ]+", s)) {
                    binding.close.visibility = View.VISIBLE
                    binding.search.filters = arrayOf(filter, LengthFilter(6))
                    if(s.length == 6){
                        search(s.toString())
                    }
                }
                else{
                    binding.search.filters = arrayOf<InputFilter>()
                    search(s.toString())
                }
            }
            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        binding.search.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                BaseUtils.closeKeyBoard(binding.search,this)
                //search()
                return@OnEditorActionListener true
            }
            false
        })
        binding.close.setOnClickListener {
            binding.search.setText("")
        }
    }

    fun load(){
        DialogUtils.showLoader(this)
        viewModel.getShopWebDetails(this,true).observe(this) {
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        DialogUtils.dismissLoader()
                        UiUtils.showSnack(binding.root, it.message!!)
                        binding.swiperefresh.background = ResourcesCompat.getDrawable(resources, R.drawable.splash_portrait, null)
                    } else {
                        it.shopWebData.let { data ->
                            binding.swiperefresh.background = null
                            sharedHelper.primaryColor = data.color_primary.toString()
                            //sharedHelper.secondarycolor = data.color_secondary.toString()
                            UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
                            UiUtils.relativeLayoutBgColor(binding.relative, SharedHelper(binding.relative.context).primaryColor, null)

                            if(BaseUtils.nullCheckerInt(data.is_welcome) != 0 && !sharedHelper.isInstallFirst){
                                DialogUtils.dismissLoader()
                                sharedHelper.isInstallFirst = true
                                BaseUtils.startActivity(this,WelcomeActivity(),null,true)
                            }
                            else{
                                sharedHelper.isInstallFirst = true
                                if (data.show_branch == 0) {
                                    DialogUtils.dismissLoader()
                                    sharedHelper.showBranch = false
                                    binding.top.text = getString(R.string.store)
                                    binding.search.requestFocus()
                                    binding.relative.visibility = View.VISIBLE
                                }
                                else if (data.show_branch == null || data.show_branch == 1) {
                                    sharedHelper.showBranch = true
                                    binding.top.text = getString(R.string.choose_store)
                                    loadBranch()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun loadBranch(){
        viewModel.getBranchList(this,false,"").observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        SharedHelper(this).shopId = UrlHelper(this).MASTERID
                        BaseUtils.startDashboardActivity(this, true, "")
                    } else {
                        it.branch_data?.let { data ->
                            if (data.size == 0) {
                                SharedHelper(this).shopId = UrlHelper(this).MASTERID
                                BaseUtils.startDashboardActivity(this, true, "")
                            } else {
                                binding.relative.visibility = View.VISIBLE
                                binding.recyclerBranch.layoutManager = LinearLayoutManager(this,
                                    LinearLayoutManager.VERTICAL,
                                    false)
                                binding.recyclerBranch.adapter = BranchAdapter(
                                    this,
                                    this,
                                    data
                                )

                                SharedHelper(this).shopList = data
                            }
                        }
                    }
                }
            }
        }
    }

    fun search(s: String) {
        if(s.isNotEmpty()){
            binding.close.visibility = View.VISIBLE
            binding.progress.visibility = View.VISIBLE
            binding.nobranch.visibility = View.GONE
            binding.recyclerBranch.visibility = View.GONE
            viewModel.getBranchList(this,true,binding.search.text.toString()).observe(this) {
                binding.progress.visibility = View.GONE
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            //UiUtils.showSnack(binding.root, it.message!!)
                            binding.nobranch.visibility = View.VISIBLE
                            binding.recyclerBranch.visibility = View.GONE
                            if (binding.search.text.isEmpty()) {
                                search("")
                            }
                        } else {
                            it.branch_data?.let { data ->
                                if (data.size == 0) {
                                    binding.nobranch.visibility = View.VISIBLE
                                    binding.recyclerBranch.visibility = View.GONE
                                    if (binding.search.text.isEmpty()) {
                                        search("")
                                    }
                                } else {
                                    binding.nobranch.visibility = View.GONE
                                    binding.recyclerBranch.visibility = View.VISIBLE
                                    binding.recyclerBranch.layoutManager = LinearLayoutManager(this,
                                        LinearLayoutManager.VERTICAL,
                                        false)
                                    binding.recyclerBranch.adapter = BranchAdapter(
                                        this,
                                        this,
                                        data
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            binding.close.visibility = View.GONE
            if(sharedHelper.showBranch){
                binding.progress.visibility = View.GONE
                binding.nobranch.visibility = View.GONE
                binding.recyclerBranch.visibility = View.VISIBLE
                binding.recyclerBranch.layoutManager = LinearLayoutManager(this,
                    LinearLayoutManager.VERTICAL,
                    false)
                binding.recyclerBranch.adapter = BranchAdapter(
                    this,
                    this,
                    sharedHelper.shopList
                )
            }
            else{
                binding.nobranch.visibility = View.VISIBLE
                binding.recyclerBranch.visibility = View.GONE
                binding.progress.visibility = View.GONE
            }
        }
    }
}