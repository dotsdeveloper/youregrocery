package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityForgetPasswordBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper

class ForgetPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityForgetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_forget_password)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
        if(intent.getStringExtra(Constants.IntentKeys.MOBILE)!!.isNotEmpty()){
            binding.mobile.setText(intent.getStringExtra(Constants.IntentKeys.MOBILE).toString())
        }
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.back, sharedHelper.primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding.back,sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.sendotp, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.editTextColor(binding.mobileTextField,sharedHelper.primaryColor,null)

        binding.back.setOnClickListener {
            finish()
        }

        binding.sendotp.setOnClickListener {
            if(binding.mobile.text!!.isNotEmpty() && binding.mobile.length() == 10){
                DialogUtils.showLoader(this)
                viewModel.forgetPassword(this,binding.mobile.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                startActivity(Intent(this, OtpActivity::class.java)
                                    .putExtra(Constants.IntentKeys.PAGE,
                                        Constants.IntentKeys.FORGET)
                                    .putExtra(Constants.IntentKeys.MOBILE,
                                        binding.mobile.text.toString()))
                            }
                        }
                    }
                }
            }
            else{
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_mobile_no))
            }
        }
    }
}