package com.yes.youregrocery.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityMyWalletBinding
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import com.yes.youregrocery.adapter.WalletAdapter
import com.yes.youregrocery.utils.BaseUtils
import org.json.JSONException
import org.json.JSONObject
import kotlin.math.roundToInt

class MyWalletActivity : BaseActivity(), PaymentResultWithDataListener {
    lateinit var binding: ActivityMyWalletBinding
    private var orderamount:Double? = null
    private var paymentid:String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyWalletBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        binding.root.visibility = View.INVISIBLE
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
        getWallet()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.add, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)

        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.add.setOnClickListener {
            if(binding.amount.text.toString().isNotEmpty()){
                orderamount = binding.amount.text.toString().toDouble()
                if(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.profile.razorpay_api) != "" && BaseUtils.nullCheckerStr(sharedHelper.shopWebData.profile.razorpay_name) != ""){
                    razorPay()
                }
                else{
                    UiUtils.showSnack(binding.root,"Payment Gateway Key Not Updated")
                }
            }
            else{
                UiUtils.showSnack(binding.root,"Enter Amount")
            }
        }
    }

    private fun getWallet(){
        DialogUtils.showLoader(this)
        viewModel.getWallet(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        binding.root.visibility = View.VISIBLE
                        binding.card.visibility = View.VISIBLE
                        binding.walletRecycler.visibility = View.GONE
                        binding.walletamount.visibility = View.GONE
                        sharedHelper.walletAmount = Constants.IntentKeys.AMOUNT_DUMMY
                        binding.amount.setText("")
                    } else {
                        binding.root.visibility = View.VISIBLE
                        binding.amount.setText("")
                        binding.card.visibility = View.GONE
                        binding.walletRecycler.visibility = View.VISIBLE
                        binding.walletamount.visibility = View.VISIBLE
                        sharedHelper.walletAmount = it.data!![0].avail_credit.toString()
                        sharedHelper.walletAmount =
                            UiUtils.formattedValues(sharedHelper.walletAmount)

                        val text =
                            "${getString(R.string.total_amount)} ${":"} ${Constants.Common.CURRENCY}${sharedHelper.walletAmount}"
                        binding.walletamount.text = text

                        it.data!![0].credit_audit!!.reverse()
                        binding.walletRecycler.layoutManager = LinearLayoutManager(this,
                            LinearLayoutManager.VERTICAL,
                            false)
                        binding.walletRecycler.adapter = WalletAdapter(
                            this,
                            this,
                            it.data!![0].credit_audit!!
                        )
                    }
                }
            }
        }
    }

    private fun addWallet(){
        DialogUtils.showLoader(this)
        viewModel.addWallet(this,orderamount.toString(),paymentid!!).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        if (BaseUtils.nullCheckerStr(sharedHelper.shopWebData.payment_added_message)
                                .isNotEmpty()
                        ) {
                            UiUtils.showSnack(binding.root,
                                sharedHelper.shopWebData.payment_added_message!!)
                        }
                        getWallet()
                    }
                }
            }
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?, paymentData: PaymentData) {
        // this method is called on payment success.
        Toast.makeText(this, "Payment is successful : $razorpayPaymentId", Toast.LENGTH_SHORT).show()
        /* paymentData.data.getString("razorpay_order_id")
         paymentData.data.getString("razorpay_signature")
         paymentData.data.getString("razorpay_payment_id")*/
        paymentid = razorpayPaymentId
        addWallet()
    }

    override fun onPaymentError(errorCode: Int, response: String?, paymentData: PaymentData?) {
        // on payment failed.
        Toast.makeText(this, "Payment Failed due to error : $response", Toast.LENGTH_SHORT).show()
    }

    private fun razorPay(){
        // on below line we are getting
        // amount that is entered by user.
        // on below line we are getting
        // amount that is entered by user.
        val payAmount: String = orderamount.toString()

        // rounding off the amount.

        // rounding off the amount.
        val amount = (payAmount.toFloat() * 100).roundToInt()

        // initialize Razorpay account.

        // initialize Razorpay account.
        val checkout = Checkout()

        // set your id as below

        // set your id as below
        checkout.setKeyID(sharedHelper.shopWebData.profile.razorpay_api)

        // set image

        // set image
        checkout.setImage(R.mipmap.ic_launcher)

        // initialize json object

        // initialize json object
        val jobject = JSONObject()
        try {
            // to put name
            jobject.put("name", sharedHelper.shopWebData.profile.razorpay_name)

            // put description
            jobject.put("description", getString(R.string.wallet_payment))

            // to set theme color
            jobject.put("theme.color", sharedHelper.primaryColor)

            // put the currency
            jobject.put("currency", Constants.IntentKeys.INR)

            // put amount
            jobject.put("amount", amount)

            // put mobile number
            jobject.put("prefill.contact", "+"+sharedHelper.countryCode.toString()+sharedHelper.mobileNumber)

            // put email
            jobject.put("prefill.email", sharedHelper.email)

            Log.d("ghdjhfg",""+jobject)
            // open razorpay to checkout activity
            checkout.open(this@MyWalletActivity, jobject)
        }
        catch (e: JSONException) {
            Toast.makeText(this,"Error in payment: "+ e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }
}