package com.yes.youregrocery.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.adapter.CouponListAdapter
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityCouponsBinding

class CouponsActivity : BaseActivity() {
    lateinit var binding: ActivityCouponsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCouponsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
        load()
    }

    fun load(){
        binding.referal.text = UiUtils.removeSpace(UiUtils.stringCon(arrayOf(Constants.Common.CURRENCY,sharedHelper.referralAmount)))
        binding.wallet.text = UiUtils.removeSpace(UiUtils.stringCon(arrayOf(Constants.Common.CURRENCY,sharedHelper.walletAmount)))
        DialogUtils.showLoader(this)
        viewModel.getCouponList(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        binding.recyclerCoupon.layoutManager = LinearLayoutManager(this,
                            LinearLayoutManager.VERTICAL,
                            false)
                        binding.recyclerCoupon.adapter = CouponListAdapter(
                            this,
                            this,
                            it.data
                        )
                    }
                }
            }
        }
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.linearLayoutBgColor(binding.linear, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        binding.back.setOnClickListener {
            onBackPressed()
        }

    }
}