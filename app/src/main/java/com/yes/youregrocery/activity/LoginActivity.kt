package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityLoginBinding
import com.yes.youregrocery.responses.LoginResponse
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.TempSingleton

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.back, SharedHelper(binding.back.context).primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding.back,SharedHelper(binding.back.context).primaryColor, null)
        UiUtils.textViewTextColor(binding.signup, SharedHelper(binding.signup.context).primaryColor, null)
        UiUtils.buttonBgColor(binding.login, SharedHelper(binding.login.context).primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.editTextColor(binding.mobileTextField,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.passwordTextField,sharedHelper.primaryColor,null)

        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.signup.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
        binding.login.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                viewModel.login(this,binding.mobile.text.toString(),binding.password.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                loginSuccess(it)
                            }
                        }
                    }
                }
            }
        }
        binding.signinotp.setOnClickListener {
            if(binding.mobile.text!!.isNotEmpty() && binding.mobile.length() == 10){
                DialogUtils.showLoader(this)
                viewModel.loginViaOTP(this,binding.mobile.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                startActivity(Intent(this, OtpActivity::class.java)
                                    .putExtra(Constants.IntentKeys.PAGE, Constants.IntentKeys.LOGIN)
                                    .putExtra(Constants.IntentKeys.MOBILE,
                                        binding.mobile.text.toString())
                                )
                            }
                        }
                    }
                }
            }
            else{
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_mobile_no))
            }
        }

        binding.forget.setOnClickListener {
            if(binding.mobile.text.toString().isNotEmpty()){
                startActivity(Intent(this, ForgetPasswordActivity::class.java).putExtra(Constants.IntentKeys.MOBILE,binding.mobile.text.toString()))
            }
            else{
                startActivity(Intent(this, ForgetPasswordActivity::class.java).putExtra(Constants.IntentKeys.MOBILE,""))
            }
        }
    }

    private fun loginSuccess(it1: LoginResponse){
        if(it1.login_data!!.user_type == 2){
            if(sharedHelper.cartIdTemp == "") {
                saveLoginData(it1)
                BaseUtils.startDashboardActivity(this,true,"")
            }
            else{
                DialogUtils.showLoader(this)
                viewModel.updateCart(this, it1.login_data!!.id!!).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                saveLoginData(it1)
                                sharedHelper.cartIdTemp = ""
                                BaseUtils.startDashboardActivity(this,
                                    true,
                                    Constants.IntentKeys.IS_CART)
                            }
                        }
                    }
                }
            }
        }
        else{
            UiUtils.showSnack(binding.root,getString(R.string.invalid_user))
        }
    }

    private fun saveLoginData(it: LoginResponse){
        TempSingleton.clearAllValues()
        DialogUtils.showNotify(Constants.NotificationKeys.LOGIN,this)
        sharedHelper.loggedIn = true
        sharedHelper.token = it.token.toString()
        sharedHelper.id = it.login_data!!.id!!.toInt()
        sharedHelper.name = it.login_data!!.name!!
        sharedHelper.mobileNumber = it.login_data!!.mobile!!
        if(it.login_data!!.country_code != null &&  !it.login_data!!.country_code.equals(Constants.IntentKeys.NULL)){
            sharedHelper.countryCode = it.login_data!!.country_code!!.toInt()
        }

        if(it.login_data!!.email != null && !it.login_data!!.email.equals(Constants.IntentKeys.NULL)){
            sharedHelper.email = it.login_data!!.email!!
        }

        if(it.referral != null && it.referral!!.isNotEmpty()){
            sharedHelper.referCode = it.referral!![0][0].referral_code.toString()
        }
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.mobile.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_mobile_no))
                return false
            }
            binding.mobile.length() != 10 -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_valid_mobile_no))
                return false
            }
            binding.password.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_password))
                return false
            }
        }
        return true
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}