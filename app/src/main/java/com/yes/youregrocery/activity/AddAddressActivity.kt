package com.yes.youregrocery.activity

import android.annotation.SuppressLint
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.GpsUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityAddAddressBinding
import com.yes.youregrocery.utils.BaseUtils
import java.io.IOException

class AddAddressActivity : BaseActivity() , OnMapReadyCallback {
    lateinit var binding: ActivityAddAddressBinding
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    var map: GoogleMap? = null
    private var flat: Double? = 0.0
    private var flng: Double? = 0.0
    private var spincode: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddAddressBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)

        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        if(intent != null && intent.extras != null){
            if(intent.extras!!.getBoolean(Constants.IntentKeys.IS_CHECKOUT)){
                spincode = intent.extras!!.getString(Constants.IntentKeys.PIN_CODE).toString()
            }
        }
        listener()
        loadMap()
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.add, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        binding.add.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                viewModel.addAddress(this,binding.edtAddressname.text.toString(),binding.edtAddressfull.text.toString(),binding.edtAddresslandmark.text.toString(),flat.toString(),flng.toString(),binding.edtPinAdd.text.toString()).observe(this
                ) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                finish()
                            }
                        }
                    }
                }
            }
        }
        binding.back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        if(BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.LOCATION)){
            loadMap()
        }
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.edtAddressname.text!!.isEmpty() -> {
                //UiUtils.showSnack(binding.root,"Please Enter Address Name")
                binding.edtAddressname.error = getString(R.string.please_enter_address_name)
                binding.edtAddressname.requestFocus()
                return false
            }
            binding.edtAddressfull.text!!.isEmpty() -> {
               // UiUtils.showSnack(binding.root,"Please Enter Address")
                binding.edtAddressfull.error = getString(R.string.please_enter_address)
                binding.edtAddressfull.requestFocus()
                return false
            }
            binding.edtAddresslandmark.text!!.isEmpty() -> {
               // UiUtils.showSnack(binding.root,"Please Enter Landmark")
                binding.edtAddresslandmark.error = getString(R.string.please_enter_landmark)
                binding.edtAddresslandmark.requestFocus()
                return false
            }
            binding.edtPinAdd.text!!.isEmpty() -> {
               // UiUtils.showSnack(binding.root,"Please Enter Pincode")
                binding.edtPinAdd.error = getString(R.string.please_enter_pincode)
                binding.edtPinAdd.requestFocus()
                return false
            }
        }
        return true
    }

    private fun loadMap(){
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationListner()
        askLocationPermission()
    }

   /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("xcbnc",""+requestCode)
        if(requestCode == Constants.RequestCode.GPS_REQUEST){
            if(!BaseUtils.isGpsEnabled(this)){
                BaseUtils.gpsEnableRequest(this)
            }
            else {
                getUserLocation()
            }
        }
    }*/

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constants.RequestCode.LOCATION_REQUEST ->
                if (grantResults.isNotEmpty()) {
                    if (BaseUtils.grantResults(grantResults)) {
                        getUserLocation()
                    }
                    else {
                        if (BaseUtils.isDeniedPermission(this,Constants.IntentKeys.LOCATION)) {
                            BaseUtils.permissionsEnableRequest(this,Constants.IntentKeys.LOCATION)
                            return
                        }
                        else {
                            BaseUtils.displayManuallyEnablePermissionsDialog(this,Constants.IntentKeys.LOCATION,null)
                        }
                    }
                }
        }
    }
    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                /*if (p0.locations == null) {
                    return
                }*/
                for (location in p0.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }
    private fun askLocationPermission() {
        if (!BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.LOCATION)) {
            BaseUtils.permissionsEnableRequest(this,Constants.IntentKeys.LOCATION)
            return
        } else {
            getUserLocation()
        }
    }
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        if (BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.LOCATION)) {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                // Got last known location. In some rare situations this can be null.

                myLat = location.latitude
                myLng = location.longitude

                map?.let {
                    flat = myLat
                    flng = myLng
                    locateMyLocation()
                }


            }
        }
    }
    private fun getUserLocation() {
        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }
    private fun locateMyLocation() {
        Log.d("flat",""+flat)
        Log.d("flng",""+flng)
        flat?.let { lat ->
            flng?.let { lng ->
                map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 12.0f))
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        map = p0
        if (!BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.LOCATION)) {
            return
        }
        else {
            map?.isMyLocationEnabled = true
            map?.uiSettings!!.isZoomGesturesEnabled = true
            map?.uiSettings!!.isZoomControlsEnabled = true
            map?.isTrafficEnabled = true
            // val sydney = LatLng(23.6611815, 42.9249046)
            // mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
            // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
            // map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9.0f))

            map?.setOnCameraIdleListener {

                val centerLatLng = map?.cameraPosition?.target
                flat = centerLatLng?.latitude
                flng = centerLatLng?.longitude

                val geocoder = Geocoder(this)
                try {
                    val addresses = flat?.let {
                        flng?.let { it1 ->
                            geocoder.getFromLocation(
                                it,
                                it1, 1
                            )
                        }
                    }
                    if (addresses != null && addresses.size > 0) {
                        val address =
                            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        /*  val city = addresses[0].locality
                      val state = addresses[0].adminArea
                      val countrycode = addresses[0].countryCode
                      val country = addresses[0].countryName
                      val knownName = addresses[0].featureName*/
                        val postalCode = addresses[0].postalCode
                        val landmark = addresses[0].thoroughfare

                        binding.edtAddressfull.setText(address)
                        binding.edtAddresslandmark.setText(landmark)
                        binding.edtPinAdd.setText(postalCode)

                        if (spincode.isNotEmpty()) {
                            if (postalCode.equals(spincode)) {
                                binding.edtPinAdd.setText(postalCode)
                            } else {
                                binding.edtPinAdd.setText(spincode)
                            }
                        }

                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

}