package com.yes.youregrocery.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityResetPasswordBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper

class ResetPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityResetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_reset_password)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        listener()
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.back, sharedHelper.primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding.back,sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.submit, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.editTextColor(binding.passwordTextField1,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.passwordTextField2,sharedHelper.primaryColor,null)

        binding.back.setOnClickListener {
            finish()
        }
        binding.submit.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                viewModel.resetPassword(this,intent.getStringExtra(Constants.IntentKeys.MOBILE).toString(),binding.pass.text.toString(),binding.cpass.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                finish()
                                // startActivity(Intent(this, LoginActivity::class.java))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.pass.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_password))
                return false
            }
            binding.cpass.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_confirm_password))
                return false
            }
            binding.pass.text.toString().trim() != binding.cpass.text.toString().trim() -> {
                UiUtils.showSnack(binding.root,getString(R.string.password_and_confirm_password_does_not_match))
                return false
            }
        }
        return true
    }

}