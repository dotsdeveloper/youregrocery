package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityInviteFriendBinding
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.BaseUtils

class InviteFriendActivity : BaseActivity() {
    lateinit var binding: ActivityInviteFriendBinding
    var sharecontent = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInviteFriendBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
        load()
    }

    fun load(){
        val shoppername:String? = sharedHelper.shopWebData.shopper.name
        val customername:String = sharedHelper.name
        val rcode:String = sharedHelper.referCode
        val weburl:String? = sharedHelper.shopWebData.profile.web_url
        val playstoreurl:String? = sharedHelper.shopWebData.playstore_url

        binding.rcode.text = rcode
        if(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.share_content) == ""){
            if(playstoreurl != null && weburl != null){
                sharecontent =
                    "${getString(R.string.refer1)} $shoppername ${getString(R.string.refer2)} $rcode ${getString(R.string.refer3)} $customername ${getString(R.string.refer4)} $weburl ${getString(R.string.and)} ${getString(R.string.refer5)} $playstoreurl"
            }
            else if(playstoreurl == null && weburl == null){
                sharecontent =
                    "${getString(R.string.refer1)} $shoppername ${getString(R.string.refer1)} $rcode ${getString(R.string.refer3)} $customername"
            }
            else if(playstoreurl == null && weburl != null){
                sharecontent =
                    "${getString(R.string.refer1)} $shoppername ${getString(R.string.refer1)} $rcode ${getString(R.string.refer3)} $customername ${getString(R.string.refer4)} $weburl"
            }
            else if(playstoreurl != null && weburl == null){
                sharecontent =
                "${getString(R.string.refer1)} $shoppername ${getString(R.string.refer1)} $rcode ${getString(R.string.refer3)} $customername ${getString(R.string.refer5)} $playstoreurl"
            }
        }
        else{
            val spanned = HtmlCompat.fromHtml(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.share_content), HtmlCompat.FROM_HTML_MODE_LEGACY)
            val spanned1 = spanned.toString().replace(Constants.IntentKeys.VAR1,shoppername!!)
            val spanned2 = spanned1.replace(Constants.IntentKeys.VAR2,rcode)
            val spanned3 = spanned2.replace(Constants.IntentKeys.VAR3,weburl!!)
            sharecontent = spanned3
        }
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.rcode, sharedHelper.primaryColor, null)
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)

        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.share.setOnClickListener {
            if(binding.rcode.text.isNotEmpty()){
                Log.d("cnvb",""+sharecontent)
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type="text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, ""+sharecontent)
                startActivity(Intent.createChooser(shareIntent,getString(R.string.share_with)))
            }
        }
    }
}