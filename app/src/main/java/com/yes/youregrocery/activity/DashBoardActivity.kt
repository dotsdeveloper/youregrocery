package com.yes.youregrocery.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.yes.youregrocery.R
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.databinding.ActivityDashboardBinding
import com.yes.youregrocery.databinding.TopHeader1Binding
import com.yes.youregrocery.fragment.*
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.responses.FragmentModel
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.responses.ShopWebData
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import java.util.*

@SuppressLint("ClickableViewAccessibility")
class DashBoardActivity : BaseActivity(), AdapterView.OnItemSelectedListener,View.OnTouchListener  {
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var binding: ActivityDashboardBinding
    private var isSpinnerTouched = false
    private var doubleBackToExitPressedOnce = false
    var fragArray:ArrayList<FragmentModel> = ArrayList()
    private lateinit var homefragment:HomeFragment
    lateinit var productListFragment:ProductListFragment
    lateinit var productViewFragment:ProductViewFragment
    lateinit var basketFragment:BasketFragment
    lateinit var quickOrderFragment:QuickOrderFragment
    lateinit var searchFragment:SearchFragment
    lateinit var wishlistFragment:WishlistFragment
    private var dX = 0f
    private var dY = 0f
    private var lastAction = 0
  //  private val MAX_CLICK_DURATION = 200
    private var startClickTime: Long = 0
    private var gestureDetector: GestureDetector? = null
    private var appUpdateManager: AppUpdateManager? = null
    private val appUpdateReqCode = 123
    private var installStateUpdatedListener: InstallStateUpdatedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_dashboard)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        homefragment = HomeFragment()
        binding.shimmerViewContainer.startShimmer()
        listener()
        load()
    }

    fun listener(){
        binding.bottomNavigationMain1.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.store -> {
                    if(!it.isCheckable) {
                        setAdapter(0)
                    }
                    true
                }
                R.id.basket -> {
                    if(!it.isCheckable) {
                        setAdapter(1)
                    }
                    true
                }
                R.id.about -> {
                    if(!it.isCheckable) {
                        setAdapter(2)
                    }
                    true
                }
                R.id.login -> {
                    if(!it.isCheckable) {
                        setAdapter(3)
                    }
                    true
                }
                R.id.more -> {
                    setAdapter(4)
                    false
                }
                else -> false
            }
        }
        binding.bottomNavigationMain2.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.privacy -> {
                    if(!it.isCheckable){
                        setAdapter(5)
                    }
                    true
                }
                R.id.contact -> {
                    if(!it.isCheckable) {
                        setAdapter(6)
                    }
                    true
                }
                R.id.refund -> {
                    if(!it.isCheckable) {
                        setAdapter(7)
                    }
                    //showComingSoon()
                    true
                }
                R.id.terms -> {
                    if(!it.isCheckable) {
                        setAdapter(8)
                    }
                    true
                }
                R.id.shipping -> {
                    if(!it.isCheckable) {
                        setAdapter(9)
                    }
                    true
                }
                else -> false
            }
        }
        gestureDetector = GestureDetector(this, SingleTapConfirm())
        binding.whats.setOnTouchListener(this)
        appUpdateManager = AppUpdateManagerFactory.create(this)
        installStateUpdatedListener = InstallStateUpdatedListener { state: InstallState ->
            when {
                state.installStatus() == InstallStatus.DOWNLOADED -> {
                    popupSnackBarForCompleteUpdate()
                }
                state.installStatus() == InstallStatus.INSTALLED -> {
                    removeInstallStateUpdateListener()
                }
                else -> {
                    Toast.makeText(
                        applicationContext,
                        "InstallStateUpdatedListener: state: " + state.installStatus(),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
        appUpdateManager!!.registerListener(installStateUpdatedListener!!)
        checkUpdate()
        bottomUI()
    }

    private fun checkUpdate() {
        val appUpdateInfoTask: Task<AppUpdateInfo> = appUpdateManager!!.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                startUpdateFlow(appUpdateInfo)
            }
            else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate()
            }
            else{
                Log.d("step3","step3--"+appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)+"--"+appUpdateInfo.updateAvailability()+"--"+appUpdateInfo.installStatus())
            }
        }
    }
    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager!!.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.FLEXIBLE,
                this,
                appUpdateReqCode
            )
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }
    private fun popupSnackBarForCompleteUpdate() {
        Snackbar.make(
            findViewById<View>(android.R.id.content).rootView,
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Install") {
                if (appUpdateManager != null) {
                    appUpdateManager!!.completeUpdate()
                }
            }
            .setActionTextColor(resources.getColor(R.color.black,null))
            .show()
    }
    private fun removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager!!.unregisterListener(installStateUpdatedListener!!)
        }
    }

    @Deprecated("Deprecated in Java")
    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.RequestCode.GPS_REQUEST ->
                if(BaseUtils.isGpsEnabled(this)){
                    if(homefragment.isAdded) {
                        homefragment.getUserLocation()
                    }
                }
            appUpdateReqCode ->
                when (resultCode) {
                    RESULT_CANCELED -> {
                        Toast.makeText(
                            applicationContext,
                            "Update canceled by user! Result Code: $resultCode", Toast.LENGTH_LONG
                        ).show()
                    }
                    RESULT_OK -> {
                        Toast.makeText(
                            applicationContext,
                            "Update success! Result Code: $resultCode", Toast.LENGTH_LONG
                        ).show()
                    }
                    else -> {
                        Toast.makeText(
                            applicationContext,
                            "Update Failed! Result Code: $resultCode",
                            Toast.LENGTH_LONG
                        ).show()
                        checkUpdate()
                    }
                }
        }
    }
    override fun onStop() {
        super.onStop()
        removeInstallStateUpdateListener()
    }
    override fun onBackPressed() {
        if(fragArray.isEmpty()){
            if (doubleBackToExitPressedOnce) {
                finish()
                return
            }
            doubleBackToExitPressedOnce = true
            Toast.makeText(this, getString(R.string.please_click_back_again_to_exit), Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
        else{
            removeFragment(fragArray.last().fragment)
            super.onBackPressed()
        }
    }
    override fun onResume() {
        super.onResume()
        resetBottomNav()
        if(homefragment.isAdded){
            updateBadges(homefragment.binding.header)
            if(BaseUtils.isGpsEnabled(this)){
                homefragment.getUserLocation()
            }
        }
        for (items in fragArray){
            if(items.fragment.tag.equals(Fragment().javaClass.name)){
                onBackPressed()
            }
        }
    }
    override fun onTouch(view: View, event: MotionEvent): Boolean {
        if (gestureDetector!!.onTouchEvent(event)) {
            setAdapter(10)
            return true
        }
        else {
            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> {
                    dX = view.x - event.rawX
                    dY = view.y - event.rawY
                    lastAction = MotionEvent.ACTION_DOWN
                    startClickTime = Calendar.getInstance().timeInMillis
                }
                MotionEvent.ACTION_MOVE -> {
                    view.y = event.rawY + dY
                    view.x = event.rawX + dX
                    lastAction = MotionEvent.ACTION_MOVE
                    startClickTime = Calendar.getInstance().timeInMillis
                }
                MotionEvent.ACTION_UP ->{
                   // val clickDuration: Long = Calendar.getInstance().timeInMillis - startClickTime
                    /*if (lastAction == MotionEvent.ACTION_DOWN) {
                        setAdapter(10)
                    }*/
                    /* else if(clickDuration < MAX_CLICK_DURATION){
                         setAdapter(10)
                     }*/
                }
                else -> return false
            }
            //view.invalidate()
            return true
        }

    }
    private class SingleTapConfirm : SimpleOnGestureListener() {
        override fun onSingleTapUp(event: MotionEvent): Boolean {
            return true
        }
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        // val item: String = parent!!.getItemAtPosition(position).toString()
        // Showing selected spinner item in toast
        if(isSpinnerTouched) {
            sharedHelper.shopId = sharedHelper.shopList[position].branch_id!!
            BaseUtils.startDashboardActivity(this,true,"")
        }

        /*Toast.makeText(
            parent.getContext(),
            "Selected Country: $item", Toast.LENGTH_LONG
        ).show()*/
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    /*  override fun onConfigurationChanged(newConfig: Configuration) {
          super.onConfigurationChanged(newConfig)
          // Checks the orientation of the screen
          if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
              //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show()
          }
          else{
              //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show()
          }
      }*/

    fun showBadge(count: Int) {
        val badge = binding.bottomNavigationMain1.getOrCreateBadge(R.id.basket)
        // badge.backgroundColor = getColor(R.color.colorRed)
        badge.maxCharacterCount = 3
        badge.isVisible = count > 0
        badge.number = count
        scrollDown()
    }
    fun updateBadges(topHeader1Binding: TopHeader1Binding) {
        if(sharedHelper.loggedIn){
            topHeader1Binding.wallet.root.visibility = View.INVISIBLE
            topHeader1Binding.notification.root.visibility = View.INVISIBLE
            topHeader1Binding.notification.notificationBadge.visibility = View.INVISIBLE
            topHeader1Binding.wallet.walletBadge.visibility = View.INVISIBLE
            if(sharedHelper.cartList.size > 0){
                showBadge(sharedHelper.cartList.size)
            }
            else{
                showBadge(0)
            }

            if(sharedHelper.walletAmount != "0.00" && sharedHelper.walletAmount != "0" && sharedHelper.walletAmount.isNotEmpty()){
                topHeader1Binding.wallet.walletBadge.visibility = View.VISIBLE
                sharedHelper.walletAmount = UiUtils.formattedValues(sharedHelper.walletAmount)
                val text = "${Constants.Common.CURRENCY}${sharedHelper.walletAmount}"
                topHeader1Binding.wallet.walletBadge.text = text
            }
            else{
                topHeader1Binding.wallet.walletBadge.visibility = View.INVISIBLE
            }

            if(sharedHelper.notifyCount != "0" && sharedHelper.notifyCount.isNotEmpty()){
                topHeader1Binding.notification.notificationBadge.visibility = View.VISIBLE
                topHeader1Binding.notification.notificationBadge.text = sharedHelper.notifyCount
            }
            else{
                topHeader1Binding.notification.notificationBadge.visibility = View.INVISIBLE
            }
        }
        else{
            topHeader1Binding.wallet.root.visibility = View.INVISIBLE
            topHeader1Binding.notification.root.visibility = View.INVISIBLE
            topHeader1Binding.notification.notificationBadge.visibility = View.INVISIBLE
            topHeader1Binding.wallet.walletBadge.visibility = View.INVISIBLE
            if(sharedHelper.cartList.size > 0){
                showBadge(sharedHelper.cartList.size)
            }
            else{
                showBadge(0)
            }
        }
    }
    fun topContentLoad(topHeader1Binding: TopHeader1Binding) {
        topHeader1Binding.spin.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
        topHeader1Binding.spin.onItemSelectedListener = this
        // Spinner Drop down elements
        val branch: ArrayList<String> = ArrayList()
        val branchid: ArrayList<Int> = ArrayList()

        if(sharedHelper.shopList.size > 0){
            topHeader1Binding.name.visibility = View.INVISIBLE
            topHeader1Binding.spin.visibility = View.VISIBLE
            topHeader1Binding.loc.visibility = View.VISIBLE
            topHeader1Binding.nameShop.visibility = View.VISIBLE
            topHeader1Binding.nameShop.text = BaseUtils.nullCheckerStr(sharedHelper.shopWebData.name)
            for(items in sharedHelper.shopList){
                branch.add(items.branchShop!!.location!!)
                //branch.add(items.branchShop!!.name!!)
                branchid.add(items.branchShop!!.id!!)
            }

            // Creating array adapter for spinner
            // val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, branch)
            val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this, R.layout.child_spinner, branch)

            // Drop down style will be listview with radio button
           // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            dataAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice)
            // attaching data adapter to spinner
            topHeader1Binding.spin.adapter = dataAdapter

            for ((index, value) in branchid.withIndex()) {
                if(value == sharedHelper.shopId){
                    topHeader1Binding.spin.setSelection(index)
                }
            }

            topHeader1Binding.spin.setOnTouchListener { _, _ ->
                isSpinnerTouched = true
                if (!sharedHelper.showBranch) {
                    sharedHelper.shopId = 0
                    startActivity(
                        Intent(this, BranchActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    )
                    finish()
                }
                false
            }
        }
        else{
            topHeader1Binding.spin.visibility = View.INVISIBLE
            topHeader1Binding.loc.visibility = View.INVISIBLE
            topHeader1Binding.nameShop.visibility = View.INVISIBLE
            topHeader1Binding.name.visibility = View.VISIBLE
            topHeader1Binding.name.text = BaseUtils.nullCheckerStr(sharedHelper.shopWebData.name)
           /* if(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.applogo).isNotEmpty()){
                topHeader1Binding.appLogo.visibility = View.VISIBLE
                UiUtils.loadImage(topHeader1Binding.appLogo,sharedHelper.shopWebData.applogo)
            }
            else if(BaseUtils.nullCheckerStr(sharedHelper.shopWebData.name).isNotEmpty()){
                topHeader1Binding.name.visibility = View.VISIBLE
                topHeader1Binding.name.text = BaseUtils.nullCheckerStr(sharedHelper.shopWebData.name)
            }*/
        }

        viewModel.isShopOpen(this).observe(this) {
            // DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    } else {
                        if (it.message.equals(Constants.ApiKeys.SHOP_IS_OPEN)) {
                            topHeader1Binding.ishhopopen.visibility = View.GONE
                            // textView.text = it.message!!
                        } else {
                            topHeader1Binding.ishhopopen.visibility = View.VISIBLE
                            if (sharedHelper.shopWebData.shop_close_message != null) {
                                topHeader1Binding.ishhopopen.text =
                                    sharedHelper.shopWebData.shop_close_message
                            } else {
                                if (it.message != null) {
                                    topHeader1Binding.ishhopopen.text = it.message!!
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    fun setAdapter(position: Int) {
        if(position != 4) {
            binding.bottomNavigationMain2.visibility = View.GONE
        }
        when (position) {
            0 -> {
                // navigationaddfragment(HomeFragment(),0,1)
                removeAllFragment()
                updateBadges(homefragment.binding.header)
            }
            1 -> {
                navigationAddFragment(BasketFragment(),1,1)
            }
            2 -> {
                if(sharedHelper.loggedIn){
                    navigationAddFragment(OrdersFragment(),2,1)
                }
                else{
                    navigationAddFragment(AboutFragment(),2,1)
                }
            }
            3 -> {
                if(sharedHelper.loggedIn){
                    navigationAddFragment(ProfileFragment(),3,1)
                }
                else{
                    //finish()
                    //clearbottomnav()
                    binding.bottomNavigationMain1.menu.getItem(3).isCheckable = true
                    binding.bottomNavigationMain1.menu.getItem(3).isChecked = true
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }
            4 -> {
                if(binding.bottomNavigationMain2.visibility == View.VISIBLE){
                    resetBottomNav()
                    binding.bottomNavigationMain2.visibility = View.GONE
                }
                else {
                    resetBottomNav()
                    binding.bottomNavigationMain2.visibility = View.VISIBLE
                    binding.bottomNavigationMain1.menu.getItem(4).isCheckable = true
                    binding.bottomNavigationMain1.menu.getItem(4).isChecked = true
                }
            }
            5 -> {
                if(sharedHelper.loggedIn){
                    navigationAddFragment(QuickOrderFragment(),0,2)
                }
                else{
                    navigationAddFragment(PrivacyPolicyFragment(),0,2)
                }
            }
            6 -> {
                if(sharedHelper.loggedIn){
                    moveWalletActivity(false)
                }
                else{
                    navigationAddFragment(ContactFragment(),1,2)
                }
            }
            7 -> {
                if(sharedHelper.loggedIn){
                    navigationAddFragment(WishlistFragment(),2,2)
                }
                else{
                    navigationAddFragment(RefundPolicyFragment(),2,2)
                }
            }
            8 -> {
                if(sharedHelper.loggedIn){
                    navigationAddFragment(FeedbackFragment(),3,2)
                }
                else{
                    navigationAddFragment(TermsFragment(),3,2)
                }
            }
            9 -> {
                navigationAddFragment(ShippingFragment(),4,2)
            }
            10 -> {
                if(isAppInstalled( "com.whatsapp.w4b") && isAppInstalled( "com.whatsapp")){
                    val url = "${Constants.IntentKeys.WHATSAPP_SHARE}${sharedHelper.shopWebData.whatsapp_code}"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
                else if(isAppInstalled( "com.whatsapp.w4b")){
                    val appPackage = "com.whatsapp.w4b"
                    val url = "${Constants.IntentKeys.WHATSAPP_SHARE}${sharedHelper.shopWebData.whatsapp_code}"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    i.setPackage(appPackage)
                    startActivity(i)
                }
                else if(isAppInstalled( "com.whatsapp")){
                    val appPackage = "com.whatsapp"
                    val url = "${Constants.IntentKeys.WHATSAPP_SHARE}${sharedHelper.shopWebData.whatsapp_code}"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    i.setPackage(appPackage)
                    startActivity(i)
                }
                else {
                    // anothor way
                    /*val appPackage = "com.whatsapp"
                    val i = Intent(Intent.ACTION_SEND)
                    i.putExtra(Intent.EXTRA_TEXT, "")
                    i.type = "text/plain"
                    i.setPackage(appPackage)
                    startActivity(i)*/
                    Toast.makeText(this, "WhatsApp is not installed", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    private fun isAppInstalled(packageName: String): Boolean {
        val pm: PackageManager = packageManager
        return try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
    fun moveWalletActivity(isProfile:Boolean){
        if(!isProfile) {
            navigationAddFragment(Fragment(), 1, 2)
        }
        startActivity(Intent(this, MyWalletActivity::class.java))
    }
    private fun resetBottomNav(){
        scrollDown()
        if(fragArray.isEmpty()){
            val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.addRule(RelativeLayout.ABOVE, 0)
            binding.container.layoutParams = params
            if(homefragment.isAdded){
                updateBadges(homefragment.binding.header)
            }
            clearBottomNav()
            binding.bottomNavigationMain1.menu.getItem(0).isCheckable = true
            binding.bottomNavigationMain1.menu.getItem(0).isChecked = true
        }
        else{
            val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.addRule(RelativeLayout.ABOVE, R.id.bottom_navigation_main1)
            binding.container.layoutParams = params

            /* val bnavid = getnavid(fragArray.last().fragment.tag)[0].navid
             val bnav = getnavid(fragArray.last().fragment.tag)[0].nav*/
            val bnavid = fragArray.last().navid

            when (fragArray.last().nav) {
                1 -> {
                    clearBottomNav()
                    binding.bottomNavigationMain1.menu.getItem(bnavid).isCheckable = true
                    binding.bottomNavigationMain1.menu.getItem(bnavid).isChecked = true
                }
                2 -> {
                    clearBottomNav()
                    binding.bottomNavigationMain2.menu.getItem(bnavid).isCheckable = true
                    binding.bottomNavigationMain2.menu.getItem(bnavid).isChecked = true
                }
                else -> {
                    clearBottomNav()
                }
            }
        }
    }
    fun clearBottomNav(){
        for (i in 0 until binding.bottomNavigationMain1.menu.size()) {
            binding.bottomNavigationMain1.menu.getItem(i).isCheckable = false
        }
        for (i in 0 until binding.bottomNavigationMain2.menu.size()) {
            binding.bottomNavigationMain2.menu.getItem(i).isCheckable = false
        }
    }
    private fun navigationAddFragment(fragment: Fragment, bnavid: Int, bnav: Int){
        removeAllFragment()
        addFragment(fragment,null,bnavid,bnav)
    }
    fun addFragment(fragment: Fragment, bundle: Bundle?, bnavid: Int, bnav: Int){
        fragArray.add(fragArray.size,FragmentModel(bnavid,bnav,fragment))
        fragment.arguments = bundle
        fragmentTransaction = supportFragmentManager.beginTransaction()
        //fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_out_right, R.anim.enter_from_right, R.anim.exit_out_right)
        fragmentTransaction.add(R.id.container, fragment,fragment.javaClass.name)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
        resetBottomNav()
        if(fragment.javaClass.name.equals(ProductListFragment().javaClass.name)){
            productListFragment = fragment as ProductListFragment
        }

        if(fragment.javaClass.name.equals(ProductViewFragment().javaClass.name)){
            productViewFragment = fragment as ProductViewFragment
        }

        if(fragment.javaClass.name.equals(BasketFragment().javaClass.name)){
            basketFragment = fragment as BasketFragment
        }

        if(fragment.javaClass.name.equals(QuickOrderFragment().javaClass.name)){
            quickOrderFragment = fragment as QuickOrderFragment
        }

        if(fragment.javaClass.name.equals(SearchFragment().javaClass.name)){
            searchFragment = fragment as SearchFragment
        }

        if(fragment.javaClass.name.equals(WishlistFragment().javaClass.name)){
            wishlistFragment = fragment as WishlistFragment
        }
    }
    fun removeFragment(fragment: Fragment){
        fragArray.removeLast()
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()
        resetBottomNav()
        ///onBackPressed()
    }
    fun removeAllFragment(){
        val fm: FragmentManager = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
        fragArray.clear()
        resetBottomNav()
    }
    private fun homeFragmentLoad(){
        if(sharedHelper.loggedIn){
            getBanners()
            getCart()
            updateToken()
            getPushNotify()
            getNotify()
            getWallet()
            getWishlist()
            getCustomerType()
        }
        else{
            getBanners()
            getCart()
            updateToken()
            getPushNotify()
        }

        binding.bottomNavigationMain2.visibility = View.GONE
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.add(R.id.container, homefragment,homefragment.javaClass.name)
        fragmentTransaction.commit()
        resetBottomNav()
    }
    fun scrollUp(){
        if(binding.whats.visibility == View.VISIBLE){
            UiUtils.animation(this,binding.whats,
                R.anim.screen_out,false)
        }
        if(binding.bottomNavigationMain1.visibility == View.VISIBLE){
            UiUtils.animation(this,binding.bottomNavigationMain1,
                R.anim.screen_out,false)
        }
        if(binding.bottomNavigationMain2.visibility == View.VISIBLE){
            UiUtils.animation(this,binding.bottomNavigationMain2,
                R.anim.screen_out,false)
        }
    }
    fun scrollDown(){
        if(binding.whats.visibility == View.GONE) {
            UiUtils.animation(this,binding.whats,
                R.anim.screen_in,true)
        }
        if(binding.bottomNavigationMain1.visibility == View.GONE){
            UiUtils.animation(this,binding.bottomNavigationMain1,
                R.anim.screen_in,true)
        }
    }
    fun load(){
        // DialogUtils.showLoader(this)
        viewModel.getShopWebDetails(this,false).observe(this) { it ->
            //  DialogUtils.dismissLoader()
            it?.let { it1 ->
                it1.error?.let { error ->
                    if (error) {
                        DialogUtils.dismissLoader()
                        sharedHelper.shopWebData = ShopWebData()
                        UiUtils.showSnack(binding.root, it1.message!!)
                    } else {
                        it1.shopWebData.let { data ->
                            sharedHelper.primaryColor = data.color_primary.toString()
                            if (data.color_secondary != null && !data.color_primary.equals(data.color_secondary)) {
                                sharedHelper.secondaryColor = data.color_secondary.toString()
                            }
                            if (data.outofstock_message == null) {
                                data.outofstock_message = getString(R.string.out_of_stock)
                            }
                            if (data.newarrival_count == null || data.newarrival_count == 0) {
                                data.newarrival_count = Constants.Common.LIMIT
                            }
                            sharedHelper.shopWebData = data

                            /*   sharedHelper.shopname = data.name!!
                               sharedHelper.about = data.about_content.toString()
                               sharedHelper.contact_content = data.contact_content.toString()
                               sharedHelper.help = data.help.toString()
                               sharedHelper.terms_condition = data.terms_condition.toString()
                               sharedHelper.privacy_policy = data.privacy_policy.toString()
                               sharedHelper.refund_policy = data.refund_policy.toString()
                               sharedHelper.shipping_policy = data.shipping_policy.toString()
                               sharedHelper.popularhead = data.category_heading.toString()
                               sharedHelper.productshead = data.product_heading.toString()
                               sharedHelper.newarrivalhead = data.newarrival_heading.toString()
                               sharedHelper.bestsellinghead = data.bestseller_heading.toString()
                               sharedHelper.bestsellingcount = data.bestselling_count!!.toInt()
                               sharedHelper.newarrivalcount = data.newarrival_count!!.toInt()
                               sharedHelper.whatsapp = data.whatsapp_code.toString()
                               sharedHelper.loader_cart = data.loader_cart.toString()
                               sharedHelper.loader_common = data.loader_common.toString()
                               if(data.share_content != null){
                                   sharedHelper.share_content = data.share_content!!.toString()
                               }
                               if(data.profile != null){
                                   if(data.profile!!.stock_option != null){
                                       sharedHelper.stock_option = data.profile!!.stock_option!!.toInt()
                                   }

                                   if(data.profile!!.max_quantity != null){
                                       sharedHelper.max_quantity = data.profile!!.max_quantity!!.toInt()
                                   }

                                   if(data.profile!!.currency != null){
                                       sharedHelper.currency = data.profile!!.currency!!.toString()
                                       if(data.profile!!.currency!!.toString() == Constants.IntentKeys.INR){
                                           Constants.Common.CURRENCY = "₹"
                                       }
                                   }

                                   if(data.profile!!.web_url != null){
                                       sharedHelper.weburl = data.profile!!.web_url.toString()
                                   }

                                   if(data.profile!!.pay_option != null){
                                       sharedHelper.payoption = data.profile!!.pay_option!!.toString()
                                   }

                                   if(data.profile!!.delivery_option != null){
                                       sharedHelper.deliveryoption = data.profile!!.delivery_option!!.toInt()
                                   }

                                   if(data.profile!!.razorpay_name != null){
                                       sharedHelper.razorpayname = data.profile!!.razorpay_name!!
                                   }

                                   if(data.profile!!.razorpay_api != null){
                                       sharedHelper.razorpayapi = data.profile!!.razorpay_api!!.toString()
                                   }
                               }
                               if(data.shopper != null){
                                   if(data.shopper!!.country_code != null){
                                       sharedHelper.shopperccode = data.shopper!!.country_code!!
                                   }
                                   if(data.shopper!!.mobile != null){
                                       sharedHelper.shoppermobile = data.shopper!!.mobile!!
                                   }
                                   if(data.shopper!!.email != null){
                                       sharedHelper.shoppermail = data.shopper!!.email!!
                                   }
                               }*/

                            UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
                            if (intent != null) {
                                when {
                                    intent.getBooleanExtra(Constants.IntentKeys.IS_CART, false) -> {
                                        homeFragmentLoad()
                                        setAdapter(1)
                                    }
                                    intent.getBooleanExtra(Constants.IntentKeys.IS_ORDER,
                                        false) -> {
                                        homeFragmentLoad()
                                        setAdapter(2)
                                    }
                                    intent.getBooleanExtra(Constants.IntentKeys.IS_NOTIFY,
                                        false) -> {
                                        when {
                                            intent.getStringExtra(Constants.IntentKeys.PAGE)
                                                .equals(Constants.IntentKeys.DASHBOARD) -> {
                                                homeFragmentLoad()
                                            }
                                            intent.getStringExtra(Constants.IntentKeys.PAGE)
                                                .equals(Constants.IntentKeys.CATEGORY) -> {
                                                homeFragmentLoad()
                                                val linkid =
                                                    intent.getIntExtra(Constants.IntentKeys.KEY, 0)
                                                DialogUtils.showLoader(this)
                                                viewModel.getSubCategory(this, linkid)
                                                    .observe(this) {
                                                        DialogUtils.dismissLoader()
                                                        it?.let {
                                                            it.error?.let { error ->
                                                                if (error) {
                                                                    val args = Bundle()
                                                                    args.putString(Constants.IntentKeys.CNAME,
                                                                        getString(R.string.products))
                                                                    args.putBoolean(Constants.IntentKeys.IS_COME,
                                                                        false)
                                                                    args.putString(Constants.IntentKeys.PAGE,
                                                                        Constants.IntentKeys.LOAD_CATEGORY_P)
                                                                    args.putInt(Constants.IntentKeys.CID,
                                                                        linkid)
                                                                    args.putInt(Constants.IntentKeys.S_C_ID,
                                                                        0)
                                                                    addFragment(
                                                                        ProductListFragment(),
                                                                        args,
                                                                        -1,
                                                                        -1
                                                                    )
                                                                } else {
                                                                    val args = Bundle()
                                                                    args.putString(Constants.IntentKeys.CNAME,
                                                                        getString(R.string.products))
                                                                    args.putBoolean(Constants.IntentKeys.IS_COME,
                                                                        false)
                                                                    args.putString(Constants.IntentKeys.PAGE,
                                                                        Constants.IntentKeys.LOAD_SUBCATEGORY_LIST)
                                                                    args.putInt(Constants.IntentKeys.CID,
                                                                        linkid)
                                                                    args.putInt(Constants.IntentKeys.S_C_ID,
                                                                        0)
                                                                    addFragment(
                                                                        ProductListFragment(),
                                                                        args,
                                                                        -1,
                                                                        -1
                                                                    )
                                                                }
                                                            }
                                                        }
                                                    }

                                            }
                                            intent.getStringExtra(Constants.IntentKeys.PAGE)
                                                .equals(Constants.IntentKeys.SUBCATEGORY) -> {
                                                homeFragmentLoad()
                                                val linkid =
                                                    intent.getIntExtra(Constants.IntentKeys.KEY, 0)
                                                val cid =
                                                    intent.getIntExtra(Constants.IntentKeys.KEY1, 0)
                                                val args = Bundle()
                                                args.putString(Constants.IntentKeys.CNAME,
                                                    getString(R.string.products))
                                                args.putBoolean(Constants.IntentKeys.IS_COME, false)
                                                args.putString(Constants.IntentKeys.PAGE,
                                                    Constants.IntentKeys.LOAD_CATEGORY_P)
                                                args.putInt(Constants.IntentKeys.CID, cid)
                                                args.putInt(Constants.IntentKeys.S_C_ID, linkid)
                                                addFragment(ProductListFragment(), args, -1, -1)
                                            }
                                            intent.getStringExtra(Constants.IntentKeys.PAGE)
                                                .equals(Constants.IntentKeys.PRODUCT) -> {
                                                homeFragmentLoad()
                                                val linkid =
                                                    intent.getIntExtra(Constants.IntentKeys.KEY, 0)
                                                DialogUtils.showLoader(this)
                                                viewModel.getProduct(this, linkid).observe(this) {
                                                    DialogUtils.dismissLoader()
                                                    it?.let {
                                                        it.error?.let { error ->
                                                            if (error) {
                                                                UiUtils.showSnack(binding.root,
                                                                    it.message!!)
                                                            } else {
                                                                it.product_data?.let { data ->
                                                                    val args = Bundle()
                                                                    args.putSerializable(Constants.IntentKeys.KEY,
                                                                        data[0])
                                                                    addFragment(
                                                                        ProductViewFragment(),
                                                                        args,
                                                                        -1,
                                                                        -1
                                                                    )
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            intent.getStringExtra(Constants.IntentKeys.PAGE)
                                                .equals(Constants.IntentKeys.COUPON) -> {
                                                homeFragmentLoad()
                                                startActivity(Intent(this,
                                                    CouponsActivity::class.java))
                                            }
                                            else -> {
                                                homeFragmentLoad()
                                            }
                                        }
                                    }
                                    else -> {
                                        homeFragmentLoad()
                                    }
                                }
                            } else {
                                homeFragmentLoad()
                            }
                        }
                    }
                }
            }
        }
    }
    private fun bottomUI(){
        if(sharedHelper.loggedIn){
            binding.bottomNavigationMain1.menu.getItem(2).title = getString(R.string.my_orders)
            binding.bottomNavigationMain1.menu.getItem(2).icon = AppCompatResources.getDrawable(this,R.drawable.ic_my_order)
            binding.bottomNavigationMain1.menu.getItem(3).title = getString(R.string.profile)
            binding.bottomNavigationMain1.menu.getItem(3).icon = AppCompatResources.getDrawable(this,R.drawable.ic_profile)

            binding.bottomNavigationMain2.menu.getItem(0).title = getString(R.string.quick_order)
            binding.bottomNavigationMain2.menu.getItem(0).icon = AppCompatResources.getDrawable(this,R.drawable.ic_quick_order)
            binding.bottomNavigationMain2.menu.getItem(1).title = getString(R.string.credit_book)
            binding.bottomNavigationMain2.menu.getItem(1).icon = AppCompatResources.getDrawable(this,R.drawable.ic_credit_book)
            binding.bottomNavigationMain2.menu.getItem(2).title = getString(R.string.wishlist)
            binding.bottomNavigationMain2.menu.getItem(2).icon = AppCompatResources.getDrawable(this,R.drawable.ic_wishlist)
            binding.bottomNavigationMain2.menu.getItem(3).title = getString(R.string.feed_back)
            binding.bottomNavigationMain2.menu.getItem(3).icon = AppCompatResources.getDrawable(this,R.drawable.ic_feedback)
            binding.bottomNavigationMain2.menu.getItem(4).isVisible = false
        }
    }
    private fun updateToken(){
        if(sharedHelper.fcmToken == ""){
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result

                // Log and toast
                sharedHelper.fcmToken = token.toString()
                Log.d("FcmToken : ",""+sharedHelper.fcmToken)
                // DialogUtils.showLoader(this)
                viewModel.updateToken(this).observe(this) {
                    // DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let {
                            /* if(error) {
                                 //UiUtils.showSnack(binding!!.root, it.message!!)
                             }*/
                        }
                    }
                }
            })
        }
        else{
            // DialogUtils.showLoader(this)
            viewModel.updateToken(this).observe(this) {
                // DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let {
                        /* if (error) {
                             //UiUtils.showSnack(binding!!.root, it.message!!)
                         }*/
                    }
                }
            }
        }
    }
    private fun getPushNotify(){
        // DialogUtils.showLoader(this)
        viewModel.getPushNotifications(this).observe(this) {
            // DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                        sharedHelper.pushNotifyList = ArrayList()
                    } else {
                        if (it.data!!.size > 0) {
                            sharedHelper.pushNotifyList = it.data!!
                            DialogUtils.initialNotify(this)
                        } else {
                            sharedHelper.pushNotifyList = ArrayList()
                        }
                    }
                }
            }
        }
    }
    private fun getNotify(){
        // DialogUtils.showLoader(this)
        viewModel.getNotifications(this,0).observe(this) {
            // DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        sharedHelper.notifyCount = "0"
                        updateBadges(homefragment.binding.header)
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                    } else {
                        var count = 0
                        for (items in it.data!!) {
                            if (items.status == 1) {
                                count++
                            }
                        }

                        if (count == 0) {
                            sharedHelper.notifyCount = "0"
                            updateBadges(homefragment.binding.header)
                        } else {
                            sharedHelper.notifyCount = count.toString()
                            updateBadges(homefragment.binding.header)
                        }
                    }
                }
            }
        }
    }
    private fun getWallet(){
        // DialogUtils.showLoader(this)
        viewModel.getWallet(this).observe(this) {
            //  DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        sharedHelper.walletAmount = "0"
                        updateBadges(homefragment.binding.header)
                    } else {
                        if (it.data != null && it.data!!.isNotEmpty() && it.data!![0].avail_credit != null) {
                            sharedHelper.walletAmount = it.data!![0].avail_credit.toString()
                            updateBadges(homefragment.binding.header)
                        } else {
                            sharedHelper.walletAmount = "0"
                            updateBadges(homefragment.binding.header)
                        }
                    }
                }
            }
        }
    }
    fun getWishlist(){
        // DialogUtils.showLoader(this)
        viewModel.getWishList(this).observe(this) {
            //  DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        sharedHelper.wishlist = ArrayList()
                    } else {
                        if (it.wishlistdata!!.size > 0) {
                            sharedHelper.wishlist = it.wishlistdata!!
                        } else {
                            sharedHelper.wishlist = ArrayList()
                        }
                    }
                }
            }
        }
    }
    private fun getCustomerType(){
        // DialogUtils.showLoader(this)
        viewModel.getCustomerType(this).observe(this) {
            //  DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding.root, it.message!!)
                        sharedHelper.customerType = 0
                    }
                    else {
                        sharedHelper.customerType = BaseUtils.nullCheckerInt(it.data.customer_type)
                    }
                }
            }
        }
    }
    fun getCart(){
        if(sharedHelper.cartIdTemp != "" || sharedHelper.loggedIn){
            viewModel.getCart(this).observe(this) {
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            //UiUtils.showSnack(binding!!.root, it.message!!)
                            emptyCart()
                        } else {
                            if (it.getcart_data!!.order!!.size > 0) {
                                sharedHelper.cartList = it.getcart_data!!.order!!
                                if (Constants.Common.IS_FIRST_OPEN) {
                                    Constants.Common.IS_FIRST_OPEN = false
                                    DialogUtils.showNotify(Constants.NotificationKeys.CART_NOTIFY,
                                        this)
                                }
                                showBadge(it.getcart_data!!.order!!.size)
                            } else {
                                emptyCart()
                            }
                        }
                    }
                }
            }
        }
        else{
            emptyCart()
        }
    }
    private fun emptyCart(){
        sharedHelper.cartIdTemp = ""
        sharedHelper.cartList = ArrayList()
        showBadge(0)
        if(Constants.Common.IS_FIRST_OPEN){
            Constants.Common.IS_FIRST_OPEN = false
        }
    }
    private fun getBanners(){
        viewModel.getBanners(this).observe(this) {
            // DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                        sharedHelper.banners = ArrayList()
                    } else {
                        it.banner_data?.let { data ->
                            if (data.size == 0) {
                                sharedHelper.banners = ArrayList()
                            } else {
                                sharedHelper.banners = data
                                if (homefragment.isAdded) {
                                    homefragment.loadBanner()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    fun isCartCheckData(data: ArrayList<Products>):ArrayList<Products> {
        val iterator = data.iterator()
        while(iterator.hasNext()){
            val items = iterator.next()
            if(items.quantities!!.size == 0){
                iterator.remove()
            }
            else{
                items.sqtyid = items.quantities!![0].id
                items.sqtyindex = 0
                val productid = items.id
                if(sharedHelper.cartList.size > 0){
                    for(items1 in sharedHelper.cartList) {
                        if (items1.product_id!!.toInt() == productid!!.toInt()) {
                            /* items.iscart = true
                             items.cartid = items1.id!!.toInt()
                             items.qty = items1.quantity!!.toInt()
                             items.sqtyid = items1.quantities!!.id*/
                            for((index, value) in items.quantities!!.withIndex()){
                                if(value.id == items1.quantities!!.id){
                                    items.quantities!![index].iscart = true
                                    items.quantities!![index].qty = items1.quantity!!.toInt()
                                    items.quantities!![index].cartid = items1.id!!.toInt()
                                }
                            }

                        }
                    }
                }
                else{
                    for((index, _) in items.quantities!!.withIndex()){
                        items.quantities!![index].iscart = false
                        items.quantities!![index].qty = 0
                        items.quantities!![index].cartid = 0
                    }
                }
            }
        }
        return data
    }
    fun shimmerViewClose(){
        binding.bottomNavigationMain1.setBackgroundColor(Color.parseColor(sharedHelper.primaryColor))
        binding.bottomNavigationMain2.setBackgroundColor(Color.parseColor(sharedHelper.primaryColor))
        binding.bottomNavigationMain1.itemIconTintList = UiUtils.getNavIconColorStates(sharedHelper.secondaryColor)
        binding.bottomNavigationMain1.itemTextColor = UiUtils.getNavIconColorStates(sharedHelper.secondaryColor)
        binding.bottomNavigationMain2.itemIconTintList = UiUtils.getNavIconColorStates(sharedHelper.secondaryColor)
        binding.bottomNavigationMain2.itemTextColor = UiUtils.getNavIconColorStates(sharedHelper.secondaryColor)
        binding.bottomNavigationMain1.visibility =  View.VISIBLE
        binding.whats.visibility =  View.VISIBLE

        binding.shimmerViewContainer.visibility = View.GONE
        binding.shimmerViewContainer.stopShimmer()
        binding.container.visibility = View.VISIBLE
    }
    fun logoutDialog(){
        DialogUtils.showAlertDialog(this,getString(R.string.are_you_sure_you_want_to_logout),"",getString(R.string.ok),getString(R.string.cancel), object :
            DialogCallBack {
            override fun onPositiveClick(value: String) {
                sharedHelper.loggedIn = false
                sharedHelper.token = ""
                sharedHelper.id = 0
                sharedHelper.name = ""
                sharedHelper.email = ""
                sharedHelper.mobileNumber = ""
                sharedHelper.countryCode = 0
                sharedHelper.referCode = ""
                sharedHelper.cartIdTemp = ""
                sharedHelper.customerType = 0
                BaseUtils.startDashboardActivity(this@DashBoardActivity,true,"")
            }

            override fun onNegativeClick() {

            }
        })
    }
    fun searchTopLoad(searchHome: EditText) {
        searchHome.setOnTouchListener { _, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                openSearchFragment()
            }
            false
        }
    }
    fun openSearchFragment(){
        val args = Bundle()
        args.putString(Constants.IntentKeys.SEARCH_TYPE, Constants.IntentKeys.ALL)
        addFragment(SearchFragment(), args, -1, -1)
    }


    /*
    fun getnavid(tag: String?):ArrayList<FragmentModel> {
        val arr:ArrayList<FragmentModel> = ArrayList()
        when {
            tag.equals(HomeFragment().javaClass.name) -> {
                arr.add(FragmentModel(0,1,HomeFragment()))
                return arr
            }
            tag.equals(BasketFragment().javaClass.name) -> {
                arr.add(FragmentModel(1,1,BasketFragment()))
                return arr
            }
            tag.equals(AboutFragment().javaClass.name) -> {
                arr.add(FragmentModel(2,1,AboutFragment()))
                return arr
            }
            tag.equals(OrdersFragment().javaClass.name) -> {
                arr.add(FragmentModel(2,1,AboutFragment()))
                return arr
            }
            tag.equals(ProfileFragment().javaClass.name) -> {
                arr.add(FragmentModel(3,1,OrdersFragment()))
                return arr
            }
            tag.equals(QuickOrderFragment().javaClass.name) -> {
                arr.add(FragmentModel(0,2,QuickOrderFragment()))
                return arr
            }
            tag.equals(PrivacyPolicyFragment().javaClass.name) -> {
                arr.add(FragmentModel(0,2,PrivacyPolicyFragment()))
                return arr
            }
            tag.equals(ContactFragment().javaClass.name) -> {
                arr.add(FragmentModel(1,2,ContactFragment()))
                return arr
            }
            tag.equals(RefundPolicyFragment().javaClass.name) -> {
                arr.add(FragmentModel(2,2,RefundPolicyFragment()))
                return arr
            }
            tag.equals(WishlistFragment().javaClass.name) -> {
                arr.add(FragmentModel(2,2,WishlistFragment()))
                return arr
            }
            tag.equals(TermsFragment().javaClass.name) -> {
                arr.add(FragmentModel(3,2,TermsFragment()))
                return arr
            }
            tag.equals(FeedbackFragment().javaClass.name) -> {
                arr.add(FragmentModel(3,2,FeedbackFragment()))
                return arr
            }
            tag.equals(ShippingFragment().javaClass.name) -> {
                arr.add(FragmentModel(4,2,ShippingFragment()))
                return arr
            }
            else -> {
                arr.add(FragmentModel(-1,-1, Fragment()))
                return arr
            }
        }
    }
*/
}