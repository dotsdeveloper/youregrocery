package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.R
import com.yes.youregrocery.adapter.ReferralAdapter
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityMyReferralsBinding

class MyReferralsActivity : BaseActivity() {
    lateinit var binding: ActivityMyReferralsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyReferralsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
        load()
    }

    fun load(){
        DialogUtils.showLoader(this)
        viewModel.getMyReferrals(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                        sharedHelper.referralAmount = Constants.IntentKeys.AMOUNT_DUMMY
                    } else {
                        sharedHelper.referralAmount = UiUtils.formattedValues(it.total)
                        if (it.data!!.size == 0) {
                            binding.card.visibility = View.VISIBLE
                            binding.main.visibility = View.GONE
                        } else {
                            binding.card.visibility = View.GONE
                            binding.main.visibility = View.VISIBLE
                            val text =
                                "${getString(R.string.total_earnings)} ${Constants.Common.CURRENCY} ${sharedHelper.referralAmount}"
                            binding.total.text = text
                            binding.referRecycler.layoutManager = LinearLayoutManager(this,
                                LinearLayoutManager.VERTICAL,
                                false)
                            binding.referRecycler.adapter = ReferralAdapter(
                                this,
                                this,
                                it.data!!
                            )
                        }
                    }
                }
            }
        }
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.textViewTextColor(binding.refer, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this,sharedHelper.primaryColor, null)

        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.refer.setOnClickListener {
            finish()
            startActivity(Intent(this, InviteFriendActivity::class.java))
        }
    }
}