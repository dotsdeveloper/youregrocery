package com.yes.youregrocery.activity

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.appcompat.app.AppCompatActivity
import com.yes.youregrocery.session.SharedHelper
import java.util.*
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.network.ViewModel

abstract class BaseActivity : AppCompatActivity(){
    lateinit var sharedHelper: SharedHelper
    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(this)
        setPhoneLanguage()
    }

/*
    override fun attachBaseContext(newBase: Context) {
        sharedHelper = SharedHelper(newBase)
        val currentLanguage = sharedHelper!!.language
        val locale = Locale(currentLanguage)
        Locale.setDefault(locale)
        val configuration: Configuration = newBase.getResources().getConfiguration()
        configuration.setLocale(locale)
        super.attachBaseContext(newBase.createConfigurationContext(configuration))
    }
*/

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (overrideConfiguration != null) {
            val uiMode: Int = overrideConfiguration.uiMode
            overrideConfiguration.setTo(baseContext.resources.configuration)
            overrideConfiguration.uiMode = uiMode
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }


    private fun setPhoneLanguage() {
        val res = resources
        val conf = res.configuration
        val locale = Locale(sharedHelper.language)
        Locale.setDefault(locale)
        conf.setLocale(locale)
        applicationContext.createConfigurationContext(conf)
        //val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))

        } else {
            conf.setLocale(locale)
        }
       // res.updateConfiguration(conf, dm)
       // res.configuration.updateFrom(conf)
    }
}