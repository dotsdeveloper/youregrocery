package com.yes.youregrocery.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityOtpBinding
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.LoginResponse
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.session.TempSingleton
import com.yes.youregrocery.utils.BaseUtils

class OtpActivity : BaseActivity() {
    lateinit var binding: ActivityOtpBinding
    var page : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_otp)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
        val text = "+91 ${intent.getStringExtra(Constants.IntentKeys.MOBILE)}"
        binding.mobile.text = text
        page = intent.getStringExtra(Constants.IntentKeys.PAGE).toString()
        timer()
        otp()
    }

    fun listener(){
        UiUtils.textViewTextColor(binding.back, sharedHelper.primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding.back,sharedHelper.primaryColor, null)
        UiUtils.textViewTextColor(binding.mobile, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.submit, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)

        binding.submit.setOnClickListener {
            if(binding.otp1.text!!.isNotEmpty() && binding.otp2.text!!.isNotEmpty()&& binding.otp3.text!!.isNotEmpty()&& binding.otp4.text!!.isNotEmpty()&& binding.otp5.text!!.isNotEmpty()&& binding.otp6.text!!.isNotEmpty()){
                val otp = binding.otp1.text.toString()+binding.otp2.text.toString()+binding.otp3.text.toString()+binding.otp4.text.toString()+binding.otp5.text.toString()+binding.otp6.text.toString()
                if(page == Constants.IntentKeys.FORGET || page == Constants.IntentKeys.SIGNUP){
                    DialogUtils.showLoader(this)
                    viewModel.verifyOTP(this, page,intent.getStringExtra(Constants.IntentKeys.MOBILE)!!,otp).observe(this) { response ->
                        DialogUtils.dismissLoader()
                        response?.let {
                            response.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding.root, response.message!!)
                                } else {
                                    if (page == Constants.IntentKeys.SIGNUP) {
                                        DialogUtils.showLoader(this)
                                        viewModel.login(this,
                                            TempSingleton.getInstance().mobile!!,
                                            TempSingleton.getInstance().password!!).observe(this) {
                                            DialogUtils.dismissLoader()
                                            it?.let {
                                                it.error?.let { error ->
                                                    if (error) {
                                                        UiUtils.showSnack(binding.root,
                                                            it.message!!)
                                                    } else {
                                                        loginSuccess(it)
                                                    }
                                                }
                                            }
                                        }
                                    } else if (page == Constants.IntentKeys.FORGET) {
                                        finish()
                                        startActivity(Intent(this,
                                            ResetPasswordActivity::class.java)
                                            .putExtra(Constants.IntentKeys.MOBILE,
                                                intent.getStringExtra(Constants.IntentKeys.MOBILE)))
                                    }
                                }
                            }
                        }
                    }
                }
                else if(page == Constants.IntentKeys.LOGIN){
                    DialogUtils.showLoader(this)
                    viewModel.login(this,intent.getStringExtra(Constants.IntentKeys.MOBILE)!!,otp).observe(this) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding.root, it.message!!)
                                } else {
                                    loginSuccess(it)
                                }
                            }
                        }
                    }

                }
            }
            else{
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_valid_otp))
            }
        }
        binding.back.setOnClickListener {
            finish()
        }
        binding.resend.setOnClickListener {
            DialogUtils.showLoader(this)
            viewModel.resendOTP(this,intent.getStringExtra(Constants.IntentKeys.MOBILE).toString()).observe(this) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.message!!)
                        } else {
                            timer()
                        }
                    }
                }
            }
        }
    }

    private fun loginSuccess(it1: LoginResponse){
        if(it1.login_data!!.user_type == 2){
            if(sharedHelper.cartIdTemp == "") {
                saveLoginData(it1)
                BaseUtils.startDashboardActivity(this,true,"")
            }
            else{
                DialogUtils.showLoader(this)
                viewModel.updateCart(this, it1.login_data!!.id!!).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                saveLoginData(it1)
                                sharedHelper.cartIdTemp = ""
                                BaseUtils.startDashboardActivity(this,
                                    true,
                                    Constants.IntentKeys.IS_CART)
                            }
                        }
                    }
                }
            }
        }
        else{
            UiUtils.showSnack(binding.root,getString(R.string.invalid_user))
        }
    }

    private fun saveLoginData(it: LoginResponse){
        TempSingleton.clearAllValues()
        DialogUtils.showNotify(Constants.NotificationKeys.LOGIN,this)
        sharedHelper.token = it.token.toString()
        sharedHelper.loggedIn = true
        sharedHelper.id = it.login_data!!.id!!.toInt()
        sharedHelper.name = it.login_data!!.name!!
        sharedHelper.mobileNumber = it.login_data!!.mobile!!
        if(it.login_data!!.country_code != null &&  !it.login_data!!.country_code.equals("null")){
            sharedHelper.countryCode = it.login_data!!.country_code!!.toInt()
        }

        if(it.login_data!!.email != null && !it.login_data!!.email.equals("null")){
            sharedHelper.email = it.login_data!!.email!!
        }

        if(it.referral != null && it.referral!!.isNotEmpty()){
            Log.d("dgfhdj1",""+ it.referral!![0][0].referral_code)
            sharedHelper.referCode = it.referral!![0][0].referral_code.toString()
        }
    }

    private fun timer(){
        binding.timer.visibility = View.VISIBLE
        binding.resend.visibility = View.GONE
        val timer = object: CountDownTimer(45000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val text = "${getString(R.string.resend_in)} ${":"} ${millisUntilFinished / 1000}${"s"}"
                binding.timer.text = text
            }

            override fun onFinish() {
                binding.timer.visibility = View.GONE
                binding.resend.visibility = View.VISIBLE
            }
        }
        timer.start()
    }

    private fun otp(){
        binding.otp1.requestFocus()

        binding.otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp2.isFocusable = true
                    binding.otp2.requestFocus()
                }
            }
        })

        binding.otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp3.isFocusable = true
                    binding.otp3.requestFocus()
                } else {
                    binding.otp1.isFocusable = true
                    binding.otp1.requestFocus()
                }
            }
        })

        binding.otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp4.isFocusable = true
                    binding.otp4.requestFocus()
                } else {
                    binding.otp2.isFocusable = true
                    binding.otp2.requestFocus()
                }
            }
        })

        binding.otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp5.isFocusable = true
                    binding.otp5.requestFocus()
                } else {
                    binding.otp3.isFocusable = true
                    binding.otp3.requestFocus()
                }
            }
        })

        binding.otp5.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp6.isFocusable = true
                    binding.otp6.requestFocus()
                } else {
                    binding.otp4.isFocusable = true
                    binding.otp4.requestFocus()
                }
            }
        })

        binding.otp6.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp6.isFocusable = true
                    binding.otp6.requestFocus()
                } else {
                    binding.otp5.isFocusable = true
                    binding.otp5.requestFocus()
                }
            }
        })

        binding.otp2.setOnKeyListener{ _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp2.text.toString().isEmpty()) {
                    binding.otp1.setText("")
                    binding.otp1.requestFocus()
                }
            }
            false
        }


        binding.otp3.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp3.text.toString().isEmpty()) {
                    binding.otp2.setText("")
                    binding.otp2.requestFocus()
                }
            }
            false
        }

        binding.otp4.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp4.text.toString().isEmpty()) {
                    binding.otp3.setText("")
                    binding.otp3.requestFocus()
                }
            }
            false
        }

        binding.otp5.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp5.text.toString().isEmpty()) {
                    binding.otp4.setText("")
                    binding.otp4.requestFocus()
                }
            }
            false
        }

        binding.otp6.setOnKeyListener{ _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp6.text.toString().isEmpty()) {
                    binding.otp5.setText("")
                    binding.otp5.requestFocus()
                }
            }
            false
        }

    }
}