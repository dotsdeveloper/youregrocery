package com.yes.youregrocery.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.ActivityChangePasswordBinding
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils

class ChangePasswordActivity : BaseActivity() {
    lateinit var binding: ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // setContentView(R.layout.activity_change_password)
        sharedHelper = SharedHelper(this)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        listener()
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding.top, sharedHelper.primaryColor, null)
        UiUtils.buttonBgColor(binding.confirm, sharedHelper.primaryColor, null)
        UiUtils.notificationBar(this, sharedHelper.primaryColor, null)
        UiUtils.editTextColor(binding.passwordTextField1,sharedHelper.primaryColor,null)
        UiUtils.editTextColor(binding.passwordTextField2,sharedHelper.primaryColor,null)
        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.confirm.setOnClickListener {
            if(binding.pass.text.toString().isNotEmpty() && binding.cpass.text.toString().isNotEmpty()){
                DialogUtils.showLoader(this)
                viewModel.resetPassword(this,sharedHelper.mobileNumber,binding.pass.text.toString(),binding.cpass.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            } else {
                                UiUtils.showSnack(binding.root, it.message!!)
                            }
                        }
                    }
                }
            }
        }
    }

}