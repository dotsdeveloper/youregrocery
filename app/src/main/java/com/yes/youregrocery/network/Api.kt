package com.yes.youregrocery.network

import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.yes.youregrocery.`interface`.ApiResponseCallback
import com.yes.youregrocery.utils.NetworkUtils.isNetworkConnected
import com.yes.youregrocery.utils.UiUtils
import org.json.JSONException
import org.json.JSONObject
import com.android.volley.NetworkResponse
import com.yes.youregrocery.R
import com.yes.youregrocery.network.local.ApiDataDialog
import com.yes.youregrocery.others.AppController
import com.yes.youregrocery.others.VolleyMultipartRequest
import com.yes.youregrocery.session.Constants

object Api {
    private var MY_SOCKET_TIMEOUT_MS = 50000
    var TAG: String = Api::class.java.simpleName

    fun postMethod(input: ApiInput, apiResponseCallback: ApiResponseCallback) {
        UiUtils.showLog("$TAG Request", "${input.url.toString()} ${input.jsonObject.toString()} ${input.headers.toString()}")
        if (isNetworkConnected(input.context)) {
            val jsonObjectRequest = object : JsonObjectRequest(Method.POST, input.url, input.jsonObject,
                {
                    UiUtils.showLog("$TAG response", "${input.url.toString()} $it")
                    apiResponseCallback.setResponseSuccess(it)
                    ApiDataDialog(input.context!!).load(
                        input.url,
                        Constants.IntentKeys.POST,
                        input.jsonObject,
                        input.headers,
                        "200",
                        it.toString()
                    )
                },
                {
                    if(it.networkResponse != null){
                        val response: NetworkResponse = it.networkResponse
                        Log.d("Server Error:"+input.url.toString(),""+String(response.data))
                        ApiDataDialog(input.context!!).load(
                            input.url,
                            Constants.IntentKeys.POST,
                            input.jsonObject,
                            input.headers,
                            response.statusCode.toString(),
                            String(response.data)
                        )
                    }
                    if (it is TimeoutError || it is NoConnectionError) {
                        input.context?.getString(R.string.no_internet_connection)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is AuthFailureError) {
                        //moveToLoginActivity(input.context)
                        input.context?.getString(R.string.session_expired)?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ServerError) {
                        input.context?.getString(R.string.server_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is NetworkError) {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ParseError) {
                        input.context?.getString(R.string.parsing_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    }
                })
            {
                override fun getHeaders(): MutableMap<String, String> {
                    return if (input.headers != null) {
                        val params: HashMap<String, String> = HashMap()

                        for ((key, value) in input.headers!!) {
                            params[key] = value
                        }
                        params
                    } else {
                        super.getHeaders()
                    }
                }
            }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            UiUtils.showLog("$TAG Request", input.toString())
            AppController.getInstance().addrequestToQueue(jsonObjectRequest)
        }
        else {
            apiResponseCallback.setErrorResponse(input.context!!.getString(R.string.no_internet_connection))
        }
    }

    fun getMethod(input: ApiInput, apiResponseCallback: ApiResponseCallback) {
        UiUtils.showLog("$TAG Request", "${input.url.toString()} ${input.jsonObject.toString()} ${input.headers.toString()}")
        if (isNetworkConnected(input.context)) {
            val jsonObjectRequest = object : JsonObjectRequest(Method.GET, input.url, input.jsonObject,
                {
                    apiResponseCallback.setResponseSuccess(it)
                    UiUtils.showLog("$TAG Response", "${input.url.toString()} $it")
                    ApiDataDialog(input.context!!).load(
                        input.url,
                        Constants.IntentKeys.GET,
                        input.jsonObject,
                        input.headers,
                        "200",
                        it.toString()
                    )
                },
                {

                    if(it.networkResponse != null){
                        val response: NetworkResponse = it.networkResponse
                        Log.d("Server Error:"+input.url.toString(),""+String(response.data))
                        ApiDataDialog(input.context!!).load(
                            input.url,
                            Constants.IntentKeys.GET,
                            input.jsonObject,
                            input.headers,
                            response.statusCode.toString(),
                            String(response.data)
                        )
                    }
                    if (it is TimeoutError || it is NoConnectionError) {
                        input.context?.getString(R.string.no_internet_connection)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is AuthFailureError) {
                        // moveToLoginActivity(input.context)
                        input.context?.getString(R.string.session_expired)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ServerError) {
                        input.context?.getString(R.string.server_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is NetworkError) {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ParseError) {
                        input.context?.getString(R.string.parsing_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    }
                })
            {
                override fun getHeaders(): MutableMap<String, String> {
                    return if (input.headers != null) {
                        val params: HashMap<String, String> = HashMap()

                        for ((key, value) in input.headers!!) {
                            params[key] = value
                        }
                        params
                    } else {
                        super.getHeaders()
                    }
                }
            }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            UiUtils.showLog("$TAG Request", input.toString())
            AppController.getInstance().addrequestToQueue(jsonObjectRequest)
        }
        else {
            apiResponseCallback.setErrorResponse(input.context!!.getString(R.string.no_internet_connection))
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun postDataPartMethod(input: ApiInput, apiResponseCallback: ApiResponseCallback) {
        UiUtils.showLog("$TAG Request", "${input.url.toString()} ${input.paramsData.toString()} ${input.paramsImg.toString()} ${input.headers.toString()}")
        if (isNetworkConnected(input.context)) {
            val volleyMultipartRequest: VolleyMultipartRequest = object : VolleyMultipartRequest(
                Method.POST, input.url,
                Response.Listener { response ->
                    try {
                        val obj = JSONObject(String(response.data))
                        UiUtils.showLog("$TAG response", "${input.url.toString()} $obj")
                        apiResponseCallback.setResponseSuccess(obj)
                        ApiDataDialog(input.context!!).load(
                            input.url,
                            Constants.IntentKeys.POST,
                            JSONObject(input.paramsData as MutableMap<Any?, Any?>),
                            input.headers,
                            "200",
                            obj.toString()
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener {
                    if(it.networkResponse != null){
                        val response: NetworkResponse = it.networkResponse
                        Log.d("Server Error:"+input.url.toString(),""+String(response.data))
                        ApiDataDialog(input.context!!).load(
                            input.url,
                            Constants.IntentKeys.POST,
                            JSONObject(input.paramsData as MutableMap<Any?, Any?>),
                            input.headers,
                            response.statusCode.toString(),
                            String(response.data)
                        )
                    }
                    if (it is TimeoutError || it is NoConnectionError) {
                        input.context?.getString(R.string.no_internet_connection)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is AuthFailureError) {
                        // moveToLoginActivity(input.context)
                        input.context?.getString(R.string.session_expired)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ServerError) {
                        input.context?.getString(R.string.server_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is NetworkError) {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else if (it is ParseError) {
                        input.context?.getString(R.string.parsing_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    } else {
                        input.context?.getString(R.string.network_error)
                            ?.let { it1 -> apiResponseCallback.setErrorResponse(it1) }
                    }
                })
            {
                override fun getHeaders(): Map<String, String> {
                    return if (input.headers != null) {
                        val params: HashMap<String, String> = HashMap()
                        for ((key, value) in input.headers!!) {
                            params[key] = value
                        }
                        params
                    } else {
                        super.getHeaders()
                    }
                }

                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String>? {
                    return input.paramsData
                }

                override fun getByteData(): Map<String, DataPart>? {
                    return input.paramsImg
                }
            }

            //adding the request to volley
            //Volley.newRequestQueue(this).add(volleyMultipartRequest)
            //VolleySingleton.getInstance(input.context).addToRequestQueue(volleyMultipartRequest.setRetryPolicy(DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)))
            volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            AppController.getInstance().addrequestToQueue(volleyMultipartRequest)
        }
        else {
            apiResponseCallback.setErrorResponse(input.context!!.getString(R.string.no_internet_connection))
        }
    }

   /* private fun moveToLoginActivity(context: Context?) {
        context?.let {
            SharedHelper(it).loggedIn = false
            SharedHelper(it).token = ""
            SharedHelper(it).id = 0
            SharedHelper(it).name = ""
            SharedHelper(it).email = ""
            SharedHelper(it).mobileNumber = ""
            SharedHelper(it).countryCode = 0
            val intent = Intent(it, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.startActivity(intent)
            (it as Activity).finish()
        }
    }

    fun uploadImage(file: File, url: String, context: Context, apiResponseCallback: ApiResponseCallback) {
        if (isNetworkConnected(context)) {
            val MEDIA_TYPE_PNG = "image/jpeg".toMediaTypeOrNull()
            val requestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "file",
                    "fileName",
                    File(file.path).asRequestBody(MEDIA_TYPE_PNG)
                )
                .build()
            val request = okhttp3.Request.Builder()
                .url(url)
                .post(requestBody).build()
            Log.e(TAG, "file: " + file.path)

            Coroutine.iOWorker {
                try {
                    val response = uploadRequest(request)
                    val jsonObject = JSONObject(response.body!!.toString())
                    apiResponseCallback.setResponseSuccess(jsonObject)
                } catch (e: IOException) {
                    Log.e(TAG, "IOException: $e")
                    apiResponseCallback.setErrorResponse(e.message.toString())
                } catch (e: JSONException) {
                    apiResponseCallback.setErrorResponse(e.message.toString())
                }
            }
        }
        else {
            apiResponseCallback.setErrorResponse(context.getString(R.string.no_internet_connection))
        }
    }

    private fun uploadRequest(request: okhttp3.Request): okhttp3.Response {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        val client = builder.build()
        val response = client.newCall(request).execute()
        return response
    }
*/
}