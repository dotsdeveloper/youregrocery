package com.yes.youregrocery.network.local

import org.json.JSONObject

class ApiDataStore(
    var isNew: Int?,
    var url: String?,
    var method: String?,
    var body: JSONObject?,
    var header: MutableMap<String, String>?,
    var responseCode: String?,
    var response: String?
)