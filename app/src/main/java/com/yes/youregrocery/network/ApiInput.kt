package com.yes.youregrocery.network

import android.content.Context
import com.yes.youregrocery.others.VolleyMultipartRequest.DataPart
import org.json.JSONObject
import java.io.File
import java.util.HashMap

class ApiInput{
    var jsonObject: JSONObject? = null
    var url: String? = null
    var headers : MutableMap<String,String>? = null
    var file: File? = null
    var context: Context? = null
    var contentType: String? = null
    var paramsImg: Map<String, DataPart>? =  HashMap()
    var paramsData: Map<String, String>? = HashMap()
}