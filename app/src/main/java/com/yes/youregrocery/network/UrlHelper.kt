package com.yes.youregrocery.network

import android.content.Context
import com.yes.youregrocery.others.AppController
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper

class UrlHelper(context: Context) {
 val sharedHelper: SharedHelper = SharedHelper(context)
 private val BASE_URL = sharedHelper.baseurl
 val MASTERID = sharedHelper.baseId

 val SHOPWEBDETAILS = BASE_URL + "shop_web_details/"
 val GETBRANCHLIST = BASE_URL + "get_branch_map/"+MASTERID
 val GETBANNERS = BASE_URL + "banner_customer/"
 val ISSHOPOPEN = BASE_URL
 val GETHOMENEWARRIVEL = BASE_URL + "offer_product/"
 val GETHOMEBESTSELLING = BASE_URL + "products/best_selling_product_without_token/"
 val GETPRODUCTLIST = BASE_URL + "home_product/"
 val GETHOTSALEPRODUCTLIST = BASE_URL + "products/hot_selling_product/"
 val GETCATAGORY = BASE_URL
 val GETBRANDLIST = BASE_URL + "brand_customer/"
 val GETSUBCATAGORY = BASE_URL + "customer_subcategory"
 val PRODUCTBYBRAND = BASE_URL+"brand_customer"
 val PRODUCTBYCATEGORY = BASE_URL
 val PRODUCTBYSUBCATEGORY = BASE_URL
 val SEARCHPRODUCTS = BASE_URL
 val SEARCHPRODUCTSBYBRAND = BASE_URL+"customer/productSearch_brand"
 val FEEDBACK = BASE_URL + "feedbacks/store"
 val GETINVOICE = BASE_URL + "invoice/"
 val GETPRODUCT = BASE_URL + "productlist/"
 val GETALLPRODUCT = BASE_URL + "customer/productbyshopper"
 val PRODUCTADDDON = BASE_URL + "products/addon/"
 val ENQUIRY = BASE_URL+"customer/"
 val ADDTOCART = BASE_URL
 val GETCART = BASE_URL + "cart/"
 val DELETECART = BASE_URL + "cart/"
 val UPDATECART = BASE_URL + "cart/update_cartStatus"
 val GETPROFILE = BASE_URL + "profile"
 val GETMYREFFERRALS = BASE_URL + "myreferral/"
 val GETREWARD = BASE_URL + "get_reward_by_shop/"
 val GETWALLET = BASE_URL + "customer/get_credit/"
 val ADDWALLET = BASE_URL + "add_credit"
 val PLACEORDER = BASE_URL + "order"
 val REORDER = BASE_URL + "re_order/"
 val CANCELORDER = BASE_URL + "order_status"
 val GETORDERS = BASE_URL + "order_by_shopper/"
 val SEARCHORDERS = BASE_URL + "order_by_shopper_search/"
 val GETCOUPONLIST = BASE_URL + "coupons_customer/"
 val GETNOTIFICATION = BASE_URL + "notification/"
 val GETPUSHNOTIFICATION = BASE_URL + "notificationcommon_customer/"
 val DELETENOTIFICATION = BASE_URL + "delete_notification"
 val READNOTIFICATION = BASE_URL + "read_notification"
 val GETWISHLIST = BASE_URL + "wishlist/"
 val ADDWISHLIST = BASE_URL + "wishlist"
 val DELETEWISHLIST = BASE_URL + "wishlist/delete"
 val AVAILABLEPINCODE = BASE_URL + "is_pincode/"
 val ISPINCODE = BASE_URL + "is_pincode"
 val CHECKCOUPON = BASE_URL + "cart/checkCouppen"
 val GETTIMESLOT = BASE_URL + "get_time_slate/"
 val GETADDRESS = BASE_URL + "location"
 val ADDADDRESS = BASE_URL + "add_location"
 val LOGIN = BASE_URL + "login"
 val UPDATEDEVICETOKEN = BASE_URL + "device_new"
 val LOGINVIAOTP = BASE_URL + "get_login_otp"
 val REGISTER = BASE_URL + "register"
 val FORGETPASSWORD = BASE_URL + "password/create"
 val VERIFYOTPFORGET = BASE_URL + "password/verify"
 val VERIFYOTPREGISTER = BASE_URL + "verifyotp"
 val RESENDOTP = BASE_URL + "password/resend"
 val RESETPASSWORD = BASE_URL + "password/reset"
 val GETCUSTOMERTYPE = BASE_URL + "customer/type"

 private val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
 /*val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
 val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
 val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
 val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"*/
 private val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"
 
 fun getAddress(latitude: Double, longitude: Double): String {
  val lat = latitude.toString()
  val lng = longitude.toString()
  return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
          + lng + "&sensor=true&key=" + AppController.getInstance().resources.getString(
   R.string.map_api_key
  ))
 }
}