package com.yes.youregrocery.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.yes.youregrocery.`interface`.ApiResponseCallback
import com.yes.youregrocery.responses.*
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.BaseUtils
import org.json.JSONArray
import org.json.JSONObject

class Repository private constructor() {
    companion object {
        private var repository: Repository? = null

        fun getInstance(): Repository {
            if (repository == null) {
                repository = Repository()
            }
            return repository as Repository
        }
    }
    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse> {

        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AutoCompleteResponse =
                    gson.fromJson(jsonObject.toString(), AutoCompleteResponse::class.java)
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = AutoCompleteResponse()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getBanners(input: ApiInput): LiveData<ListBannerResponse> {
        val apiResponse: MutableLiveData<ListBannerResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ListBannerResponse =
                    gson.fromJson(jsonObject.toString(), ListBannerResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ListBannerResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getNewArrivalsProducts(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getBestSellerProducts(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getCategoryList(input: ApiInput): LiveData<CategoryResponse> {
        val apiResponse: MutableLiveData<CategoryResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: CategoryResponse =
                        gson.fromJson(jsonObject.toString(), CategoryResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    /* val jsonArray: JSONArray = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                     for (i in 0 until jsonArray.length()) {
                         val jsonObject1 = jsonArray.getJSONObject(i)
                         jsonObject1.put("product",productScan(jsonObject1.getJSONArray("product")))
                     }
                     jsonObject.put(Constants.ApiKeys.DATA,jsonArray)*/
                    val response: CategoryResponse =
                        gson.fromJson(jsonObject.toString(), CategoryResponse::class.java)
                    apiResponse.value = response
                }

            }

            override fun setErrorResponse(error: String) {
                val response = CategoryResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getBrandList(input: ApiInput): LiveData<CategoryResponse> {
        val apiResponse: MutableLiveData<CategoryResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CategoryResponse =
                    gson.fromJson(jsonObject.toString(), CategoryResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CategoryResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getHomeProductList(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getHotsaleList(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getProduct(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getAllProducts(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getSubCategory(input: ApiInput): LiveData<CategoryResponse> {
        val apiResponse: MutableLiveData<CategoryResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CategoryResponse =
                    gson.fromJson(jsonObject.toString(), CategoryResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CategoryResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun productsByBrand(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun productsByCategory(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun productsBySubCategory(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getShopWebDetails(input: ApiInput): LiveData<ShopWebDetailsResponse> {
        val apiResponse: MutableLiveData<ShopWebDetailsResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ShopWebDetailsResponse =
                    gson.fromJson(jsonObject.toString(), ShopWebDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ShopWebDetailsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun isShopOpen(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getBranchList(input: ApiInput): LiveData<BranchlistResponse> {
        val apiResponse: MutableLiveData<BranchlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: BranchlistResponse =
                    gson.fromJson(jsonObject.toString(), BranchlistResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = BranchlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun login(input: ApiInput): LiveData<LoginResponse> {
        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: LoginResponse =
                    gson.fromJson(jsonObject.toString(), LoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = LoginResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun forgetPassword(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun verifyOTP(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun resendOTP(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun resetPassword(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun register(input: ApiInput): LiveData<RegisterResponse> {
        val apiResponse: MutableLiveData<RegisterResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val str = jsonObject.getJSONArray(Constants.ApiKeys.STATUS)
                    jsonObject.put(Constants.ApiKeys.STATUS,str.get(0).toString())
                    val response: RegisterResponse = gson.fromJson(jsonObject.toString(), RegisterResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    val response: RegisterResponse = gson.fromJson(jsonObject.toString(), RegisterResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = RegisterResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun productScan(jsonArray: JSONArray): JSONArray {
        for (i in 0 until jsonArray.length()) {
            if(jsonArray.getJSONObject(i).has(Constants.ApiKeys.FILES)){
                val files = jsonArray.getJSONObject(i).get(Constants.ApiKeys.FILES)
                if(!BaseUtils.isArray(files)){
                    jsonArray.getJSONObject(i).put(Constants.ApiKeys.FILES, JSONArray())
                }
            }
            if(jsonArray.getJSONObject(i).has(Constants.ApiKeys.ORIGINAL_FILES)){
                val oFiles = jsonArray.getJSONObject(i).get(Constants.ApiKeys.ORIGINAL_FILES)
                if(!BaseUtils.isArray(oFiles)){
                    jsonArray.getJSONObject(i).put(Constants.ApiKeys.ORIGINAL_FILES, JSONArray())
                }
            }
        }
        return jsonArray
    }
    fun searchProducts(input: ApiInput): LiveData<ProductlistResponse> {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put(Constants.ApiKeys.DATA,productScan(jsonObject.getJSONArray(Constants.ApiKeys.DATA)))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun addToCart(input: ApiInput): LiveData<AddCartResponse> {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = AddCartResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getCart(input: ApiInput): LiveData<GetCartResponse> {
        val apiResponse: MutableLiveData<GetCartResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: GetCartResponse =
                        gson.fromJson(jsonObject.toString(), GetCartResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    val data = jsonObject.getJSONObject(Constants.ApiKeys.DATA)
                    val items =  data.getJSONArray(Constants.ApiKeys.ORDER)
                    for(j in 0 until items.length()){
                        val product = items.getJSONObject(j).getJSONObject(Constants.ApiKeys.PRODUCT)
                        items.getJSONObject(j).put(Constants.ApiKeys.PRODUCT,productScan(JSONArray().put(0,product))[0])
                    }

                    val response: GetCartResponse =
                        gson.fromJson(jsonObject.toString(), GetCartResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = GetCartResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun deleteCart(input: ApiInput): LiveData<AddCartResponse> {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = AddCartResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getAvailablePinCode(input: ApiInput): LiveData<AvailablePincodeResponse> {
        val apiResponse: MutableLiveData<AvailablePincodeResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AvailablePincodeResponse =
                    gson.fromJson(jsonObject.toString(), AvailablePincodeResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = AvailablePincodeResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getTimeSlot(input: ApiInput): LiveData<TimeslotResponse> {
        val apiResponse: MutableLiveData<TimeslotResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: TimeslotResponse =
                    gson.fromJson(jsonObject.toString(), TimeslotResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = TimeslotResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun isPinCode(input: ApiInput): LiveData<IspincodeResponse> {
        val apiResponse: MutableLiveData<IspincodeResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: IspincodeResponse =
                    gson.fromJson(jsonObject.toString(), IspincodeResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = IspincodeResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun checkCoupon(input: ApiInput): LiveData<CheckCouponResponse> {
        val apiResponse: MutableLiveData<CheckCouponResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CheckCouponResponse =
                    gson.fromJson(jsonObject.toString(), CheckCouponResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CheckCouponResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getAddressList(input: ApiInput): LiveData<GetLocationResponse> {
        val apiResponse: MutableLiveData<GetLocationResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetLocationResponse =
                    gson.fromJson(jsonObject.toString(), GetLocationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetLocationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun addAddress(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateCart(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getProfile(input: ApiInput): LiveData<GetProfileResponse> {
        val apiResponse: MutableLiveData<GetProfileResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetProfileResponse =
                    gson.fromJson(jsonObject.toString(), GetProfileResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetProfileResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getMyReferrals(input: ApiInput): LiveData<GetReferralResponse> {
        val apiResponse: MutableLiveData<GetReferralResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetReferralResponse =
                    gson.fromJson(jsonObject.toString(), GetReferralResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetReferralResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getReward(input: ApiInput): LiveData<GetRewardResponse> {
        val apiResponse: MutableLiveData<GetRewardResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetRewardResponse =
                    gson.fromJson(jsonObject.toString(), GetRewardResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetRewardResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getWallet(input: ApiInput): LiveData<GetWalletResponse> {
        val apiResponse: MutableLiveData<GetWalletResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetWalletResponse =
                    gson.fromJson(jsonObject.toString(), GetWalletResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetWalletResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun addWallet(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun placeOrder(input: ApiInput): LiveData<PlaceOrderResponse> {
        val apiResponse: MutableLiveData<PlaceOrderResponse> = MutableLiveData()
        Api.postDataPartMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: PlaceOrderResponse =
                    gson.fromJson(jsonObject.toString(), PlaceOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = PlaceOrderResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getOrders(input: ApiInput): LiveData<GetOrdersResponse> {
        val apiResponse: MutableLiveData<GetOrdersResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: GetOrdersResponse =
                        gson.fromJson(jsonObject.toString(), GetOrdersResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    val data = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                    for(i in 0 until data.length()){
                        val items =  data.getJSONObject(i).getJSONArray(Constants.ApiKeys.ITEMS)
                        for(j in 0 until items.length()){
                            val product = items.getJSONObject(j).getJSONObject(Constants.ApiKeys.PRODUCT)
                            items.getJSONObject(j).put(Constants.ApiKeys.PRODUCT,productScan(JSONArray().put(0,product))[0])
                        }
                    }
                    val response: GetOrdersResponse =
                        gson.fromJson(jsonObject.toString(), GetOrdersResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = GetOrdersResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getCouponList(input: ApiInput): LiveData<GetCouponListResponse> {
        val apiResponse: MutableLiveData<GetCouponListResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetCouponListResponse =
                    gson.fromJson(jsonObject.toString(), GetCouponListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetCouponListResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getNotifications(input: ApiInput): LiveData<GetNotificationResponse> {
        val apiResponse: MutableLiveData<GetNotificationResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetNotificationResponse =
                    gson.fromJson(jsonObject.toString(), GetNotificationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetNotificationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getPushNotifications(input: ApiInput): LiveData<GetPushNotificationResponse> {
        val apiResponse: MutableLiveData<GetPushNotificationResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetPushNotificationResponse =
                    gson.fromJson(jsonObject.toString(), GetPushNotificationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetPushNotificationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun deleteNotifications(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun readNotifications(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun reOrder(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun cancelOrder(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun updateToken(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun feedback(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getWishList(input: ApiInput): LiveData<WishlistResponse> {
        val apiResponse: MutableLiveData<WishlistResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean(Constants.ApiKeys.ERROR)){
                    val response: WishlistResponse =
                        gson.fromJson(jsonObject.toString(), WishlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    val data = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                    for(i in 0 until data.length()){
                        val product = data.getJSONObject(i).getJSONObject(Constants.ApiKeys.PRODUCT)
                        data.getJSONObject(i).put(Constants.ApiKeys.PRODUCT,productScan(JSONArray().put(0,product))[0])
                    }
                    val response: WishlistResponse =
                        gson.fromJson(jsonObject.toString(), WishlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = WishlistResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun addWishList(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun deleteWishList(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun addEnquiry(input: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getInvoice(input: ApiInput): LiveData<InvoiceResponse> {
        val apiResponse: MutableLiveData<InvoiceResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: InvoiceResponse =
                    gson.fromJson(jsonObject.toString(), InvoiceResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = InvoiceResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getProductAddOn(input: ApiInput): LiveData<ProductAddonResponse> {
        val apiResponse: MutableLiveData<ProductAddonResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ProductAddonResponse = gson.fromJson(jsonObject.toString(), ProductAddonResponse::class.java)
                response.datanew = jsonObject.getJSONObject(Constants.ApiKeys.DATA)
                val str = jsonObject.getJSONObject(Constants.ApiKeys.DATA).get("product_how_to_use")
                if(str.toString().length > 2){
                    val response1: Htu = gson.fromJson(str.toString(), Htu::class.java)
                    response.product_how_to_use = response1
                    apiResponse.value = response
                }
                else{
                    response.product_how_to_use = Htu()
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                val response = ProductAddonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
    fun getCustomerType(input: ApiInput): LiveData<GetCustomerType> {
        val apiResponse: MutableLiveData<GetCustomerType> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetCustomerType =
                    gson.fromJson(jsonObject.toString(), GetCustomerType::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetCustomerType()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })
        return apiResponse
    }
}