package com.yes.youregrocery.network

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.yes.youregrocery.others.VolleyMultipartRequest.DataPart
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.responses.*
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import org.json.JSONArray
import org.json.JSONObject
import java.util.regex.Pattern

class ViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: Repository = Repository.getInstance()
    private var applicationIns: Application = application
    var sharedHelper: SharedHelper = SharedHelper(applicationIns.applicationContext)

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {
        val header: MutableMap<String, String> = HashMap()
        if(sharedHelper.loggedIn && sharedHelper.token.isNotEmpty()){
            sharedHelper.let { header[Constants.ApiKeys.AUTHORIZATION] = Constants.ApiKeys.BEARER+it.token }
        }
        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName
        return apiInputs
    }

    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse> {
        return repository.getAddress(input)
    }


    fun getBanners(context: Context): LiveData<ListBannerResponse> {
        return repository.getBanners(getApiParams(context, null, UrlHelper(context).GETBANNERS+sharedHelper.shopId))
    }

    fun getCategoryList(context: Context): LiveData<CategoryResponse> {
        return if(sharedHelper.loggedIn){
            repository.getCategoryList(getApiParams(context, null, UrlHelper(context).GETCATAGORY+"productslist_onlycategory_withlogin/"+sharedHelper.shopId))
        } else{
            repository.getCategoryList(getApiParams(context, null, UrlHelper(context).GETCATAGORY+"productslist_onlycategory/"+sharedHelper.shopId))
        }
    }

    fun getBrandList(context: Context): LiveData<CategoryResponse> {
        return repository.getBrandList(getApiParams(context, null, UrlHelper(context).GETBRANDLIST+sharedHelper.shopId))
    }

    fun getSubCategory(context: Context, cId: Int): LiveData<CategoryResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.CATEGORY_ID, cId)
        return repository.getSubCategory(getApiParams(context, jsonObject, UrlHelper(context).GETSUBCATAGORY))
    }

    fun productsByCategory(context: Context, cId: Int, offset:Int): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.CATEGORY_ID, cId)
        jsonObject.put(Constants.ApiKeys.LIMIT, Constants.Common.LIMIT)
        jsonObject.put(Constants.ApiKeys.OFFSET, offset)
        return if(sharedHelper.loggedIn){
            repository.productsByCategory(getApiParams(context, jsonObject, UrlHelper(context).PRODUCTBYCATEGORY+"customer/product"))
        } else{
            repository.productsByCategory(getApiParams(context, jsonObject, UrlHelper(context).PRODUCTBYCATEGORY+"customer_product"))
        }
    }

    fun productsByBrand(context: Context, cId: Int, offset:Int): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        val cid = JSONArray()
        jsonObject.put(Constants.ApiKeys.CATEGORY_ID, cid)
        val bid = JSONArray()
        bid.put(cId)
        jsonObject.putOpt(Constants.ApiKeys.BRAND_ID, bid)
        jsonObject.put(Constants.ApiKeys.LIMIT, Constants.Common.LIMIT)
        jsonObject.put(Constants.ApiKeys.OFFSET, offset)
        return repository.productsByBrand(getApiParams(context, jsonObject, UrlHelper(context).PRODUCTBYBRAND))

    }

    fun productsBySubCategory(context: Context, cId: Int, scId: Int, offset:Int): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.CATEGORY_ID, cId)
        jsonObject.put(Constants.ApiKeys.SUB_CATEGORY_ID, scId)
        jsonObject.put(Constants.ApiKeys.LIMIT, Constants.Common.LIMIT)
        jsonObject.put(Constants.ApiKeys.OFFSET, offset)
        return if(sharedHelper.loggedIn){
            repository.productsBySubCategory(getApiParams(context, jsonObject, UrlHelper(context).PRODUCTBYSUBCATEGORY+"customer/productbysub"))
        } else{
            repository.productsBySubCategory(getApiParams(context, jsonObject, UrlHelper(context).PRODUCTBYSUBCATEGORY+"customer_productbysubcat"))
        }
    }

    fun searchProducts(context: Context, key: String, cid: Int, scId:Int, page:Int): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.SEARCH, key)
        jsonObject.put(Constants.ApiKeys.LIMIT, Constants.Common.LIMIT)
        jsonObject.put(Constants.ApiKeys.OFFSET, page)
        if(cid != 0){
            jsonObject.put(Constants.ApiKeys.CATEGORY_ID, cid)
            if(scId != 0){
                jsonObject.put(Constants.ApiKeys.SUB_CATEGORY_ID, scId)
            }
        }

        return if(sharedHelper.loggedIn){
            repository.searchProducts(getApiParams(context, jsonObject, UrlHelper(context).SEARCHPRODUCTS+"customer/"+"productSearch"))
        } else{
            repository.searchProducts(getApiParams(context, jsonObject, UrlHelper(context).SEARCHPRODUCTS+"customer_productSearch"))
        }
    }

    fun searchProductsByBrand(context: Context, key: String, bid: Int, page:Int): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.BRAND_ID, bid)
        jsonObject.put(Constants.ApiKeys.SEARCH, key)
        jsonObject.put(Constants.ApiKeys.LIMIT, Constants.Common.LIMIT)
        jsonObject.put(Constants.ApiKeys.OFFSET, page)
        return repository.searchProducts(getApiParams(context, jsonObject, UrlHelper(context).SEARCHPRODUCTSBYBRAND))
    }

    fun getProduct(context: Context, pid: Int?): LiveData<ProductlistResponse> {
        return repository.getProduct(getApiParams(context, null, UrlHelper(context).GETPRODUCT+pid))
    }

    fun getAllProducts(context: Context): LiveData<ProductlistResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        return repository.getAllProducts(getApiParams(context, jsonObject, UrlHelper(context).GETALLPRODUCT))
    }

    fun getHomeProductList(context: Context): LiveData<ProductlistResponse> {
        return repository.getHomeProductList(getApiParams(context, null, UrlHelper(context).GETPRODUCTLIST+sharedHelper.shopId))
    }

    fun getHotsaleProductList(context: Context): LiveData<ProductlistResponse> {
        return repository.getHotsaleList(getApiParams(context, null, UrlHelper(context).GETHOTSALEPRODUCTLIST+sharedHelper.shopId))
    }

    fun getNewArrivalsProducts(context: Context): LiveData<ProductlistResponse> {
        return if(BaseUtils.nullCheckerInt(sharedHelper.shopWebData.newarrival_count) != 0){
            repository.getNewArrivalsProducts(getApiParams(context, null, UrlHelper(context).GETHOMENEWARRIVEL+sharedHelper.shopId+"/"+sharedHelper.shopWebData.newarrival_count))
        } else{
            repository.getNewArrivalsProducts(getApiParams(context, null, UrlHelper(context).GETHOMENEWARRIVEL+sharedHelper.shopId+"/"+Constants.Common.LIMIT))
        }
    }

    fun getBestSellerProducts(context: Context, page:Int): LiveData<ProductlistResponse> {
        return if(BaseUtils.nullCheckerInt(sharedHelper.shopWebData.bestselling_count) != 0){
            repository.getBestSellerProducts(getApiParams(context, null, UrlHelper(context).GETHOMEBESTSELLING+sharedHelper.shopId+"/"+sharedHelper.shopWebData.bestselling_count+"/"+(page)))
        } else{
            repository.getBestSellerProducts(getApiParams(context, null, UrlHelper(context).GETHOMEBESTSELLING+sharedHelper.shopId+"/"+Constants.Common.LIMIT+"/"+(page)))

        }
    }

    fun getShopWebDetails(context: Context, isBranch:Boolean): LiveData<ShopWebDetailsResponse> {
        return if(isBranch){
            repository.getShopWebDetails(getApiParams(context, null, UrlHelper(context).SHOPWEBDETAILS+UrlHelper(context).MASTERID))
        } else{
            repository.getShopWebDetails(getApiParams(context, null, UrlHelper(context).SHOPWEBDETAILS+sharedHelper.shopId))
        }
    }

    fun isShopOpen(context: Context): LiveData<CommonResponse> {
        return if(sharedHelper.loggedIn){
            repository.isShopOpen(getApiParams(context, null, UrlHelper(context).ISSHOPOPEN+"is_open/"+sharedHelper.shopId))
        } else{
            repository.isShopOpen(getApiParams(context, null, UrlHelper(context).ISSHOPOPEN+"is_open_wol/"+sharedHelper.shopId))
        }
    }

    fun getBranchList(context: Context, issearch : Boolean, svalue:String): LiveData<BranchlistResponse> {
        return if(issearch){
            when {
                Pattern.matches("[a-zA-Z]+", svalue) -> {
                    repository.getBranchList(getApiParams(context, null, UrlHelper(context).GETBRANCHLIST+"/1/"+svalue))
                }
                Pattern.matches("[0-9 ]+", svalue) -> {
                    repository.getBranchList(getApiParams(context, null, UrlHelper(context).GETBRANCHLIST+"/2/"+svalue))
                }
                else -> {
                    repository.getBranchList(getApiParams(context, null, UrlHelper(context).GETBRANCHLIST+"/1/"+svalue))
                }
            }
        } else{
            repository.getBranchList(getApiParams(context, null, UrlHelper(context).GETBRANCHLIST))
        }
    }

    fun login(context: Context, mobile: String, passsword: String): LiveData<LoginResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        jsonObject.put(Constants.ApiKeys.PASSWORD, passsword)
        return repository.login(getApiParams(context, jsonObject, UrlHelper(context).LOGIN))
    }


    fun loginViaOTP(context: Context, mobile: String): LiveData<LoginResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        return repository.login(getApiParams(context, jsonObject, UrlHelper(context).LOGINVIAOTP))
    }

    fun register(
        context: Context,
        name: String,
        mail: String,
        mobile: String,
        password: String,
        cpasssword: String,
        location: String,
        rcode: String
    ): LiveData<RegisterResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, mail)
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
        jsonObject.put(Constants.ApiKeys.PASSWORD_CONFIRMATION, cpasssword)
        jsonObject.put(Constants.ApiKeys.USER_TYPE, "2")
        jsonObject.put(Constants.ApiKeys.LOCATION, location)
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, "91")
        jsonObject.put(Constants.ApiKeys.REFERRAL_CODE, rcode)
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        return repository.register(getApiParams(context, jsonObject, UrlHelper(context).REGISTER))
    }

    fun forgetPassword(context: Context, mobile: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        return repository.forgetPassword(getApiParams(context, jsonObject, UrlHelper(context).FORGETPASSWORD))
    }

    fun verifyOTP(context: Context, page: String, mobile: String, otp: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        jsonObject.put(Constants.ApiKeys.OTP, otp)
        return if(page == Constants.IntentKeys.FORGET){
            repository.verifyOTP(getApiParams(context, jsonObject, UrlHelper(context).VERIFYOTPFORGET))
        } else{
            repository.verifyOTP(getApiParams(context, jsonObject, UrlHelper(context).VERIFYOTPREGISTER))
        }
    }

    fun resendOTP(context: Context, mobile: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        return repository.resendOTP(getApiParams(context, jsonObject, UrlHelper(context).RESENDOTP))
    }

    fun resetPassword(context: Context, mobile: String, pass: String, cpass: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
        jsonObject.put(Constants.ApiKeys.NEW_PASSWORD, pass)
        jsonObject.put(Constants.ApiKeys.CONFIRM_PASSWORD, cpass)
        return repository.resetPassword(getApiParams(context, jsonObject, UrlHelper(context).RESETPASSWORD))
    }

    fun addToCart(context: Context, qty: Int?, qtyid: Int?, productid: Int?): LiveData<AddCartResponse> {
        if(sharedHelper.loggedIn){
            var isnew = true
            val jsonObject1 = JSONObject()
            val jsonArray = JSONArray()

            if(sharedHelper.cartList.size != 0){
                for(items in sharedHelper.cartList){
                    if(items.product_id == productid) {
                        if(items.quantity_id == qtyid){
                            isnew = false
                            if(qty != 0){
                                val jsonObject = JSONObject()
                                jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                                jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                                jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                                jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                                jsonArray.put(jsonObject)
                            }
                        }
                        else{
                            val jsonObject = JSONObject()
                            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                            jsonObject.put(Constants.ApiKeys.PRODUCT_ID, items.product_id)
                            jsonObject.put(Constants.ApiKeys.QUANTITY, items.quantity)
                            jsonObject.put(Constants.ApiKeys.QUANTITY_ID, items.quantity_id)
                            jsonArray.put(jsonObject)
                        }
                    }
                    else{
                        val jsonObject = JSONObject()
                        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                        jsonObject.put(Constants.ApiKeys.PRODUCT_ID, items.product_id)
                        jsonObject.put(Constants.ApiKeys.QUANTITY, items.quantity)
                        jsonObject.put(Constants.ApiKeys.QUANTITY_ID, items.quantity_id)
                        jsonArray.put(jsonObject)
                    }
                }
                if(isnew){
                    if(qty != 0) {
                        val jsonObject = JSONObject()
                        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                        jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                        jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                        jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                        jsonArray.put(jsonObject)
                    }
                }
            }
            else{
                if(qty != 0) {
                    val jsonObject = JSONObject()
                    jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                    jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                    jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                    jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                    jsonArray.put(jsonObject)
                }
            }

            jsonObject1.put(Constants.ApiKeys.DATA,jsonArray)

            return if(jsonArray.length() == 0){
                val jsonObject = JSONObject()
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 1)
                jsonObject.put(Constants.ApiKeys.ID, "")
                repository.deleteCart(getApiParams(context, jsonObject, UrlHelper(context).DELETECART+"delete"))
            } else{
                repository.addToCart(getApiParams(context, jsonObject1, UrlHelper(context).ADDTOCART+"cart"))
            }

        }
        else{
            var isnew = true
            val jsonObject1 = JSONObject()
            val jsonArray = JSONArray()

            if(sharedHelper.cartList.size != 0){
                for(items in sharedHelper.cartList){
                    if(items.product_id == productid) {
                        if(items.quantity_id == qtyid){
                            isnew = false
                            if(qty != 0){
                                val jsonObject = JSONObject()
                                jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                                jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                                jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                                jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                                jsonObject.put(Constants.ApiKeys.CART_ID, sharedHelper.cartIdTemp)
                                jsonArray.put(jsonObject)
                            }
                        }
                        else{
                            val jsonObject = JSONObject()
                            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                            jsonObject.put(Constants.ApiKeys.PRODUCT_ID, items.product_id)
                            jsonObject.put(Constants.ApiKeys.QUANTITY, items.quantity)
                            jsonObject.put(Constants.ApiKeys.QUANTITY_ID, items.quantity_id)
                            jsonObject.put(Constants.ApiKeys.CART_ID, sharedHelper.cartIdTemp)
                            jsonArray.put(jsonObject)
                        }
                    }
                    else{
                        val jsonObject = JSONObject()
                        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                        jsonObject.put(Constants.ApiKeys.PRODUCT_ID, items.product_id)
                        jsonObject.put(Constants.ApiKeys.QUANTITY, items.quantity)
                        jsonObject.put(Constants.ApiKeys.QUANTITY_ID, items.quantity_id)
                        jsonObject.put(Constants.ApiKeys.CART_ID, sharedHelper.cartIdTemp)
                        jsonArray.put(jsonObject)
                    }
                }
                if(isnew){
                    if(qty != 0) {
                        val jsonObject = JSONObject()
                        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                        jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                        jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                        jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                        jsonObject.put(Constants.ApiKeys.CART_ID, sharedHelper.cartIdTemp)
                        jsonArray.put(jsonObject)
                    }
                }
            }
            else{
                if(qty != 0) {
                    val jsonObject = JSONObject()
                    jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
                    jsonObject.put(Constants.ApiKeys.PRODUCT_ID, productid)
                    jsonObject.put(Constants.ApiKeys.QUANTITY, qty)
                    jsonObject.put(Constants.ApiKeys.QUANTITY_ID, qtyid)
                    jsonObject.put(Constants.ApiKeys.CART_ID, sharedHelper.cartIdTemp)
                    jsonArray.put(jsonObject)
                }
            }

            jsonObject1.put(Constants.ApiKeys.DATA,jsonArray)

            return if(jsonArray.length() == 0){
                val jsonObject = JSONObject()
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 1)
                jsonObject.put(Constants.ApiKeys.ID, "")
                jsonObject.put(Constants.ApiKeys.CART_ID1, sharedHelper.cartIdTemp)
                repository.deleteCart(getApiParams(context, jsonObject, UrlHelper(context).DELETECART+"customer_delete_woc"))
            } else{
                repository.addToCart(getApiParams(context, jsonObject1, UrlHelper(context).ADDTOCART+"customer_cartAdd"))
            }


        }
    }

    fun getCart(context: Context): LiveData<GetCartResponse> {
        return if(sharedHelper.loggedIn){
            repository.getCart(getApiParams(context, null, UrlHelper(context).GETCART+sharedHelper.shopId))
        } else{
            repository.getCart(getApiParams(context, null, UrlHelper(context).GETCART+sharedHelper.shopId+"/"+sharedHelper.cartIdTemp))
        }
    }

    fun deleteCart(context: Context, cid: Int?): LiveData<AddCartResponse> {
        if(sharedHelper.loggedIn){
            val jsonObject = JSONObject()
            if(cid == -1){
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 1)
                jsonObject.put(Constants.ApiKeys.ID, "")
            }
            else{
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 0)
                jsonObject.put(Constants.ApiKeys.ID, cid)
            }
            // jsonObject.put(Constants.ApiKeys.CARTID, sharedHelper.cartid_temp)

            return repository.deleteCart(getApiParams(context, jsonObject, UrlHelper(context).DELETECART+"delete"))

        }
        else{
            val jsonObject = JSONObject()
            if(cid == -1){
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 1)
                jsonObject.put(Constants.ApiKeys.ID, "")
            }
            else{
                jsonObject.put(Constants.ApiKeys.IS_EMPTY, 0)
                jsonObject.put(Constants.ApiKeys.ID, cid)
            }
            jsonObject.put(Constants.ApiKeys.CART_ID1, sharedHelper.cartIdTemp)

            return repository.deleteCart(getApiParams(context, jsonObject, UrlHelper(context).DELETECART+"customer_delete_woc"))

        }
    }

    fun getAvailablePinCode(context: Context): LiveData<AvailablePincodeResponse> {
        return repository.getAvailablePinCode(getApiParams(context, null, UrlHelper(context).AVAILABLEPINCODE+sharedHelper.shopId))
    }

    fun getTimeSlot(context: Context): LiveData<TimeslotResponse> {
        return repository.getTimeSlot(getApiParams(context, null, UrlHelper(context).GETTIMESLOT+sharedHelper.shopId))
    }

    fun isPinCode(context: Context, pin:String): LiveData<IspincodeResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.PIN_CODE, pin)
        return repository.isPinCode(getApiParams(context, jsonObject, UrlHelper(context).ISPINCODE))
    }

    fun checkCoupon(context: Context, coupon:String, pin:String): LiveData<CheckCouponResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        if(pin.isEmpty()){
            jsonObject.put(Constants.ApiKeys.PIN_CODE, 0)
        }
        else{
            jsonObject.put(Constants.ApiKeys.PIN_CODE, pin)
        }
        jsonObject.put(Constants.ApiKeys.COUPON, coupon)
        return repository.checkCoupon(getApiParams(context, jsonObject, UrlHelper(context).CHECKCOUPON))
    }

    fun getAddressList(context: Context): LiveData<GetLocationResponse> {
        return repository.getAddressList(getApiParams(context, null, UrlHelper(context).GETADDRESS))
    }

    fun addAddress(context: Context, name:String, address:String, landmark:String, lat:String, lng:String, pin:String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.LOCATION_NAME,name )
        jsonObject.put(Constants.ApiKeys.ADDRESS, address)
        jsonObject.put(Constants.ApiKeys.LANDMARK, landmark)
        jsonObject.put(Constants.ApiKeys.LOCATION_LAT, lat)
        jsonObject.put(Constants.ApiKeys.LOCATION_LONG, lng)
        jsonObject.put(Constants.ApiKeys.PIN_CODE, pin)
        return repository.addAddress(getApiParams(context, jsonObject, UrlHelper(context).ADDADDRESS))
    }

    fun updateCart(context: Context, cid :Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CART_ID1,sharedHelper.cartIdTemp)
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.CUSTOMER_ID, cid)
        return repository.updateCart(getApiParams(context, jsonObject, UrlHelper(context).UPDATECART))
    }

    fun getProfile(context: Context): LiveData<GetProfileResponse> {
        return repository.getProfile(getApiParams(context, null, UrlHelper(context).GETPROFILE))
    }

    fun getMyReferrals(context: Context): LiveData<GetReferralResponse> {
        return repository.getMyReferrals(getApiParams(context, null, UrlHelper(context).GETMYREFFERRALS+sharedHelper.shopId))
    }

    fun getReward(context: Context): LiveData<GetRewardResponse> {
        return repository.getReward(getApiParams(context, null, UrlHelper(context).GETREWARD+sharedHelper.shopId))
    }

    fun getWallet(context: Context): LiveData<GetWalletResponse> {
        return repository.getWallet(getApiParams(context, null, UrlHelper(context).GETWALLET+sharedHelper.shopId))
    }

    fun addWallet(context: Context, amount: String, pid:String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CREDIT_AMOUNT,amount)
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.PAYMENT_ID, pid)
        return repository.addWallet(getApiParams(context, jsonObject, UrlHelper(context).ADDWALLET))
    }

    fun getOrders(context: Context, offset: Int): LiveData<GetOrdersResponse> {
        val limit = Constants.Common.LIMIT
        return repository.getOrders(getApiParams(context, null, UrlHelper(context).GETORDERS+sharedHelper.shopId+"/"+limit+"/"+offset))
    }

    fun searchOrder(context: Context, value: String): LiveData<GetOrdersResponse> {
        return repository.getOrders(getApiParams(context, null, UrlHelper(context).SEARCHORDERS+sharedHelper.shopId+"/"+value))
    }

    fun getOrder(context: Context, oid:Int): LiveData<GetOrdersResponse> {
        return repository.getOrders(getApiParams(context, null, UrlHelper(context).PLACEORDER+"/"+oid))
    }

    fun getCouponList(context: Context): LiveData<GetCouponListResponse> {
        return repository.getCouponList(getApiParams(context, null, UrlHelper(context).GETCOUPONLIST+sharedHelper.shopId))
    }

    fun getNotifications(context: Context, offset: Int): LiveData<GetNotificationResponse> {
        val limit = Constants.Common.LIMIT
        return repository.getNotifications(getApiParams(context, null, UrlHelper(context).GETNOTIFICATION+limit+"/"+offset))
    }

    fun getPushNotifications(context: Context): LiveData<GetPushNotificationResponse> {
        return repository.getPushNotifications(getApiParams(context, null, UrlHelper(context).GETPUSHNOTIFICATION+sharedHelper.shopId))
    }

    fun deleteNotifications(context: Context, id: Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        if(id == 0){
            jsonObject.put(Constants.ApiKeys.ID,"")
            jsonObject.put(Constants.ApiKeys.TYPE, 1)
        }
        else{
            jsonObject.put(Constants.ApiKeys.ID,id)
            jsonObject.put(Constants.ApiKeys.TYPE, 0)
        }
        return repository.deleteNotifications(getApiParams(context, jsonObject, UrlHelper(context).DELETENOTIFICATION))
    }

    fun readNotifications(context: Context, id: Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.ID, id)
        return repository.readNotifications(getApiParams(context, jsonObject, UrlHelper(context).READNOTIFICATION))
    }


    fun reOrder(context: Context, id:Int): LiveData<CommonResponse> {
        return repository.reOrder(getApiParams(context, null, UrlHelper(context).REORDER+""+id.toString()))
    }

    fun cancelOrder(context: Context, id:Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.STATUS, 2)
        jsonObject.put(Constants.ApiKeys.ORDER_ID, id)
        return repository.cancelOrder(getApiParams(context, jsonObject, UrlHelper(context).CANCELORDER))
    }

    fun updateToken(context: Context): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        return if(sharedHelper.loggedIn){
            jsonObject.put(Constants.ApiKeys.DEVICE, "Android")
            jsonObject.put(Constants.ApiKeys.DEVICE_ID, sharedHelper.fcmToken)
            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
            jsonObject.put(Constants.ApiKeys.CUSTOMER_ID, sharedHelper.id)
            repository.updateToken(getApiParams(context, jsonObject, UrlHelper(context).UPDATEDEVICETOKEN))
        } else{
            jsonObject.put(Constants.ApiKeys.DEVICE, "Android")
            jsonObject.put(Constants.ApiKeys.DEVICE_ID, sharedHelper.fcmToken)
            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
            repository.updateToken(getApiParams(context, jsonObject, UrlHelper(context).UPDATEDEVICETOKEN))
        }
    }

    fun feedback(context: Context,feedback: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.NAME, sharedHelper.name)
        jsonObject.put(Constants.ApiKeys.MOBILE, sharedHelper.mobileNumber)
        jsonObject.put(Constants.ApiKeys.FEEDBACK, feedback)
        return repository.feedback(getApiParams(context, jsonObject, UrlHelper(context).FEEDBACK))
    }

    fun getWishList(context: Context): LiveData<WishlistResponse> {
        return repository.getWishList(getApiParams(context, null, UrlHelper(context).GETWISHLIST+sharedHelper.shopId))
    }

    fun deleteWishList(context: Context, id: Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.ID, id)
        return repository.deleteWishList(getApiParams(context, jsonObject, UrlHelper(context).DELETEWISHLIST))
    }

    fun addWishList(context: Context, pid: Int, uid: Int): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOP_ID, sharedHelper.shopId)
        jsonObject.put(Constants.ApiKeys.PRODUCT_ID, pid)
        jsonObject.put(Constants.ApiKeys.UNIT_ID, uid)
        return repository.addWishList(getApiParams(context, jsonObject, UrlHelper(context).ADDWISHLIST))
    }


    fun addEnquiry(context: Context, pid: Int, desc: String, name:String, email:String, mobile: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        return if(sharedHelper.loggedIn){
            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
            jsonObject.put(Constants.ApiKeys.PRODUCT_ID, pid)
            jsonObject.put(Constants.ApiKeys.DESCRIPTION, desc)
            repository.addEnquiry(getApiParams(context, jsonObject, UrlHelper(context).ENQUIRY+"enquiry"))
        } else{
            jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
            jsonObject.put(Constants.ApiKeys.PRODUCT_ID, pid)
            jsonObject.put(Constants.ApiKeys.DESCRIPTION, desc)
            jsonObject.put(Constants.ApiKeys.NAME, name)
            jsonObject.put(Constants.ApiKeys.EMAIL, email)
            jsonObject.put(Constants.ApiKeys.MOBILE, mobile)
            repository.addEnquiry(getApiParams(context, jsonObject, UrlHelper(context).ENQUIRY+"enquiry_wol"))
        }
    }

    fun getInvoice(context: Context, id: Int): LiveData<InvoiceResponse> {
        return repository.getInvoice(getApiParams(context, null, UrlHelper(context).GETINVOICE+id))
    }

    fun getProductAddOn(context: Context, pid: Int): LiveData<ProductAddonResponse> {
        return repository.getProductAddOn(getApiParams(context, null, UrlHelper(context).PRODUCTADDDON+""+pid+"/"+sharedHelper.shopId))
    }

    fun placeOrder(
        context: Context,
        bodyParams: Map<String, String>,
        imgParams: Map<String, DataPart>
    ): LiveData<PlaceOrderResponse>{
        val header: MutableMap<String, String> = HashMap()
        if(sharedHelper.loggedIn && sharedHelper.token.isNotEmpty()){
            sharedHelper.let { header[Constants.ApiKeys.AUTHORIZATION] = Constants.ApiKeys.BEARER+it.token }
        }
        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = null
        apiInputs.headers = header
        apiInputs.url = UrlHelper(context).PLACEORDER
        apiInputs.paramsData = bodyParams
        apiInputs.paramsImg = imgParams
        return repository.placeOrder(apiInputs)
    }

    fun getCustomerType(context: Context): LiveData<GetCustomerType> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SHOPPER_ID, sharedHelper.shopId)
        return repository.getCustomerType(getApiParams(context, jsonObject, UrlHelper(context).GETCUSTOMERTYPE))
    }
}