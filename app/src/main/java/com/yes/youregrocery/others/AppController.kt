package com.yes.youregrocery.others

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.firebase.analytics.FirebaseAnalytics
import com.yes.youregrocery.R

class AppController : MultiDexApplication() {
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        instance = this
        MultiDex.install(this)
    }

    companion object {

        private val TAG: String = AppController::class.java.simpleName
        private var instance: AppController? = null
        private var requestQueue: RequestQueue? = null
        private var mFirebaseAnalytics: FirebaseAnalytics? = null
        private val callTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()

        @Synchronized
        fun getInstance(): AppController {
            return instance as AppController
        }

    }


    private fun getRequestQueue(): RequestQueue? {

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(instance)

        return requestQueue
    }

    fun <T> addrequestToQueue(request: Request<T>) {
        request.tag = TAG
        getRequestQueue()?.add(request)
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics?.setAnalyticsCollectionEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val attributesCall = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()


        val notificationManager =
            instance?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val message =
            NotificationChannel(getString(R.string.notification_channel_id), getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_HIGH)

        message.setSound(Uri.parse(callTone), attributesCall)
        notificationManager.createNotificationChannel(message)
    }

}
