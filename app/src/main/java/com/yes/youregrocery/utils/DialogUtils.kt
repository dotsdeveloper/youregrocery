package com.yes.youregrocery.utils

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.`interface`.OnClickListener
import com.yes.youregrocery.R
import com.yes.youregrocery.responses.Quantities
import com.yes.youregrocery.session.SharedHelper
import kotlin.math.roundToInt
import android.graphics.drawable.Drawable
import androidx.annotation.Nullable
import androidx.core.text.HtmlCompat
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.yes.youregrocery.adapter.DialogProductQuantityAdapter
import com.yes.youregrocery.activity.LoginActivity
import com.yes.youregrocery.activity.SignupActivity
import com.yes.youregrocery.activity.SplashActivity
import com.yes.youregrocery.session.Constants

@SuppressLint("UnspecifiedImmutableFlag")
object DialogUtils {
    private var loaderDialog: Dialog? = null
    fun initialNotify(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val currentNotify: Int = BaseUtils.getFormattedDate(java.time.LocalDate.now().toString(), "yyyy-MM-dd", "dd").toInt()
            if (SharedHelper(context).lastNotify != currentNotify) {
                SharedHelper(context).lastNotify = currentNotify
                showNotify(Constants.NotificationKeys.WELCOME, context)
            }
        }
        else {
            val currentNotify: Int = BaseUtils.getFormattedDate(BaseUtils.getCurrentDate().toString(), "yyyy-MM-dd", "dd").toInt()
            if (SharedHelper(context).lastNotify != currentNotify) {
                SharedHelper(context).lastNotify = currentNotify
                showNotify(Constants.NotificationKeys.WELCOME, context)
            }
        }
    }
    fun showNotify(type: Int, context: Context) {
        for (items in SharedHelper(context).pushNotifyList) {
            if (items.notification_type == type) {
                if(items.description != null && items.title != null){
                    val spanned = HtmlCompat.fromHtml(items.description.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
                    val body = if (SharedHelper(context).loggedIn) {
                        spanned.toString().replace("<first_name>", SharedHelper(context).name)
                    } else {
                        spanned.toString().replace("<first_name>", "")
                    }

                    if (items.link_type == 0) {
                        createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file,context)
                    }
                    else if (items.link_type == 1) {
                        if (SharedHelper(context).loggedIn) {
                            createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                        }
                        else {
                            createNotification(pendingIntent(context,LoginActivity()), items.title.toString(), body,items.file, context)
                        }
                    }
                    else if (items.link_type == 2) {
                        if (SharedHelper(context).loggedIn) {
                            createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                        }
                        else {
                            createNotification(pendingIntent(context,SignupActivity()), items.title.toString(), body,items.file, context)
                        }
                    }
                    else if (items.link_type == 3) {
                        createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                    }
                    else if (items.link_type == 4) {
                        createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                    }
                    else if (items.link_type == 5) {
                        createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                    }
                    else if (items.link_type == 6) {
                        createNotification(pendingIntent(context,DashBoardActivity()), items.title.toString(), body,items.file, context)
                    }
                    else {
                        createNotification(pendingIntent(context,SplashActivity()), items.title.toString(), body,items.file, context)
                    }
                }
            }
        }
    }
    private fun pendingIntent(context: Context,activity: Activity):PendingIntent{
        when {
            activity.javaClass.simpleName.equals(DashBoardActivity().javaClass.simpleName) -> {
                val intent = Intent(context, activity::class.java)
                intent.putExtra(Constants.IntentKeys.IS_ORDER, false)
                intent.putExtra(Constants.IntentKeys.IS_CART, false)
                intent.putExtra(Constants.IntentKeys.IS_NOTIFY, true)
                intent.putExtra(Constants.IntentKeys.PAGE, Constants.IntentKeys.DASHBOARD)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            }
            activity.javaClass.simpleName.equals(SplashActivity().javaClass.simpleName) -> {
                val intent = Intent(context, activity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            }
            else -> {
                val intent = Intent(context, activity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            }
        }
    }
    private fun createNotification(pendingIntent: PendingIntent, title: String, body: String, imageUrl: String?, context: Context){
        if(imageUrl != null && imageUrl != "" && imageUrl.isNotEmpty()){
            createImageNotification(pendingIntent, title, body, imageUrl,context)
        }
        else{
            simpleNotification(pendingIntent, title, body, context)
        }
    }
    private fun simpleNotification(pendingIntent: PendingIntent, notificationTitle: String, notificationContent: String, context: Context) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification: NotificationCompat.Builder =
            NotificationCompat.Builder(context, context.getString(R.string.notification_channel_id))
                .setContentText(notificationContent)
                .setContentTitle(notificationTitle)
                .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setStyle(
                    NotificationCompat.BigTextStyle().setBigContentTitle(notificationTitle)
                        .bigText(notificationContent)
                )
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.notification_icon)
                .setColor(Color.parseColor(SharedHelper(context).primaryColor))
                .setFullScreenIntent(pendingIntent, true)
        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())
    }
    private fun createImageNotification(pendingIntent: PendingIntent, title: String, body: String, imageUrl: String, context: Context) {
        val bitmap = arrayOf<Bitmap?>(null)
        Glide.with(context)
            .asBitmap()
            .load(imageUrl)
            .into(object : CustomTarget<Bitmap?>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap?>?
                ) {
                    bitmap[0] = resource
                    imageNotification(pendingIntent, context, title, body, bitmap[0]!!)
                }

                override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
            })
    }
    fun imageNotification(pendingIntent: PendingIntent, context: Context, notificationTitle: String, notificationContent: String, imageUrl: Bitmap?) {
        val builder =
            NotificationCompat.Builder(context, context.getString(R.string.notification_channel_id))
        val notificationManager =
            context.getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        builder.setContentTitle(notificationTitle)
            .setContentText(notificationContent)
            .setSmallIcon(R.drawable.notification_icon)
            .setColor(Color.parseColor(SharedHelper(context).primaryColor))
            .setSound(defaultSoundUri)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
        // Set the image for the notification
        if (imageUrl != null) {
            // val bitmap: Bitmap? = getBitmapFromUrl(imageUrl)
            builder.setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(imageUrl)
                    .bigLargeIcon(imageUrl)
            ).setLargeIcon(imageUrl)
        }
        notificationManager.notify(1, builder.build())
    }
    fun showPictureDialog(activity: Activity) {
        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle(activity.getString(R.string.choose_your_option))
        val items = arrayOf(activity.getString(R.string.gallery), activity.getString(R.string.camera))
        //val items = arrayOf("Gallery")

        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.GALLERY)) {
                    BaseUtils.openGallery(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.GALLERY)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.GALLERY)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.GALLERY, null)
                    }
                }
                1 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.CAMERA)) {
                    BaseUtils.openCamera(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.CAMERA)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.CAMERA)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.CAMERA, null)
                    }
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }
    fun showLoader(context: Context) {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

        if (BaseUtils.nullCheckerStr(SharedHelper(context).shopWebData.loader_common) == "") {
            Glide.with(context).load(R.drawable.loader_common)
                .into(loaderDialog!!.findViewById(R.id.image))
        } else {
            Glide.with(context).load(SharedHelper(context).shopWebData.loader_common)
                .into(loaderDialog!!.findViewById(R.id.image))
        }

        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }
    }
    fun showCartLoader(context: Context) {
            if (loaderDialog != null) {
                if (loaderDialog!!.isShowing) {
                    loaderDialog!!.dismiss()
                }
            }

            loaderDialog = Dialog(context)
            loaderDialog?.setCancelable(false)
            loaderDialog?.setCanceledOnTouchOutside(false)
            loaderDialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val inflater = LayoutInflater.from(context)
            val view = inflater?.inflate(R.layout.dialog_loader, null)
            if (view != null) {
                loaderDialog!!.setContentView(view)
            }

            if (BaseUtils.nullCheckerStr(SharedHelper(context).shopWebData.loader_cart) == "") {
                Glide.with(context).load(R.drawable.cart_loader)
                    .into(loaderDialog!!.findViewById(R.id.image))
            } else {
                Glide.with(context).load(SharedHelper(context).shopWebData.loader_cart)
                    .into(loaderDialog!!.findViewById(R.id.image))
            }

            if (!loaderDialog?.isShowing!!) {
                loaderDialog?.show()
            }

        }
    fun dismissLoader() {
            if (loaderDialog != null) {
                if (loaderDialog!!.isShowing) {
                    loaderDialog!!.dismiss()
                }
            }
        }
    fun showQuantityDialog(context: Context, content: String, list: ArrayList<Quantities>?, callBack: OnClickListener) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_quantity)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val headerTextView = dialog.findViewById<TextView>(R.id.name)
        headerTextView.text = content
        val close = dialog.findViewById<ImageView>(R.id.close)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val recycler = dialog.findViewById<RecyclerView>(R.id.qty_recycler)
        recycler.layoutManager = GridLayoutManager(
            context, 1,
            LinearLayoutManager.VERTICAL, false
        )
        recycler.adapter = DialogProductQuantityAdapter(
            dialog,
            callBack,
            context,
            list)
            /*  cancel.setOnClickListener {
              callBack.onNegativeClick()
              dialog.dismiss()
          }

          ok.setOnClickListener {
              callBack.onPositiveClick()
              dialog.dismiss()
          }*/
        dialog.show()

    }
    fun showAlertDialog(context: Context, title: String, content: String, positiveText: String, negativeText: String, callBack: DialogCallBack) {
            val dialog = Dialog(context)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_alert)
            dialog.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            /*var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
*/

            val headerTextView = dialog.findViewById<TextView>(R.id.header)
            val contentTextView = dialog.findViewById<TextView>(R.id.content)

            val cancel = dialog.findViewById<Button>(R.id.cancel)
            val ok = dialog.findViewById<Button>(R.id.ok)

            if (content == "") {
                contentTextView.visibility = View.GONE
            }

            headerTextView.text = title
            contentTextView.text = content

            cancel.text = negativeText
            ok.text = positiveText

            UiUtils.buttonBgColor(cancel, SharedHelper(cancel.context).primaryColor, null)
            UiUtils.buttonBgColor(ok, SharedHelper(ok.context).primaryColor, null)

            cancel.setOnClickListener {
                callBack.onNegativeClick()
                dialog.dismiss()
            }

            ok.setOnClickListener {
                callBack.onPositiveClick("")
                dialog.dismiss()
            }

            dialog.show()

        }
    fun showEnquiryDialog(pid: Int, dashBoardActivity: DashBoardActivity, context: Context, callBack: DialogCallBack) {
            val dialog = Dialog(context)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_enquiry)
            dialog.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            val width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
            // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

            dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val ok = dialog.findViewById<TextView>(R.id.ok)
            val desc = dialog.findViewById<EditText>(R.id.desc)
            val name = dialog.findViewById<EditText>(R.id.name)
            val email = dialog.findViewById<EditText>(R.id.email)
            val mobile = dialog.findViewById<EditText>(R.id.mobile)
            val withOutLogin = dialog.findViewById<LinearLayout>(R.id.withoutlogin)
            val close = dialog.findViewById<ImageView>(R.id.close)

            if (SharedHelper(context).loggedIn) {
                withOutLogin.visibility = View.GONE
            } else {
                withOutLogin.visibility = View.VISIBLE
            }

            close.setOnClickListener {
                dialog.dismiss()
            }

            cancel.setOnClickListener {
                callBack.onNegativeClick()
                dialog.dismiss()
            }

            ok.setOnClickListener {
                if (SharedHelper(context).loggedIn) {
                    if (desc.text.toString().isNotEmpty()) {
                        showLoader(context)
                        dashBoardActivity.viewModel.addEnquiry(
                            context,
                            pid,
                            desc.text.toString(),
                            name.text.toString(),
                            email.text.toString(),
                            mobile.text.toString()
                        ).observe(dashBoardActivity) {
                            dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        callBack.onPositiveClick(it.message!!)
                                        dialog.dismiss()
                                    } else {
                                        callBack.onPositiveClick(it.message!!)
                                        dialog.dismiss()
                                    }
                                }
                            }
                        }
                    } else {
                        desc.error = context.getString(R.string.please_fill)
                    }
                } else {
                    if (desc.text.toString().isNotEmpty() && name.text.toString()
                            .isNotEmpty() && email.text.toString()
                            .isNotEmpty() && mobile.text.toString().isNotEmpty()
                    ) {
                        showLoader(context)
                        dashBoardActivity.viewModel.addEnquiry(
                            context,
                            pid,
                            desc.text.toString(),
                            name.text.toString(),
                            email.text.toString(),
                            mobile.text.toString()
                        ).observe(dashBoardActivity) {
                            dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        callBack.onPositiveClick(it.message!!)
                                        dialog.dismiss()
                                    } else {
                                        callBack.onPositiveClick(it.message!!)
                                        dialog.dismiss()
                                    }
                                }
                            }
                        }
                    } else {
                        desc.error = context.getString(R.string.please_fill)
                    }
                }

            }
            dialog.show()
        }
    /*fun showAlert(context: Context, singleTapListener: SingleTapListener, content: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }
    fun showAlertWithHeader(context: Context, singleTapListener: SingleTapListener, content: String, headerVal: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val header = dialog.findViewById<TextView>(R.id.header)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerVal


        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        dialog.show()

    }
*/}