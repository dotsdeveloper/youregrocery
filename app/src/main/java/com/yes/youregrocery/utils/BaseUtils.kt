package com.yes.youregrocery.utils

import android.Manifest.permission
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.database.Cursor
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.yes.youregrocery.session.Constants
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import java.text.ParseException as ParseException1
import android.text.format.DateUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.app.ActivityCompat.requestPermissions
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.activity.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import com.yes.youregrocery.*
import com.yes.youregrocery.*
import com.yes.youregrocery.*
import com.yes.youregrocery.*

@SuppressLint("SimpleDateFormat")
object BaseUtils {
    fun isContainsChar(value: String):Boolean{
        val specialChars = "!@#$%&*()'+,-/:;<=>?[]^_`{|}"
        for (i in value) {
            if (specialChars.contains(i)) {
                return true
            }
        }
        return false
    }
    fun nullCheckerStr(value : String?):String{
        if(value == null || value == "" || value == Constants.IntentKeys.NULL || value == Constants.IntentKeys.UNDEFINED || value.isEmpty() || value == "0" || value == "0.00"){
            return ""
        }
        return value
    }

    fun nullCheckerInt(value : Int?):Int{
        if(value == null || value == 0 || value.toDouble() == 0.0){
            return 0
        }
        return value
    }

    fun closeKeyBoard(editText: EditText, context: Context){
        editText.clearFocus()
        val input = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        input!!.hideSoftInputFromWindow(editText.windowToken, 0)
    }
    fun getCurrentDate(): String? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val today = Calendar.getInstance().time
        return dateFormat.format(today)
    }
    fun startActivity(from: Activity, to :Activity, bundle: Bundle?, isfinish:Boolean){
        val send = Intent(from, to::class.java)
        if(bundle != null){
            send.putExtras(bundle)
            from.startActivity(send)
        }
        else{
            from.startActivity(send)
        }

        if(isfinish){
            from.finish()
        }
    }
    fun startDashboardActivity(activity: Activity,finish: Boolean,page:String){
        when {
            page.isEmpty() -> {
                activity.startActivity(Intent(activity, DashBoardActivity::class.java)
                    .putExtra(Constants.IntentKeys.IS_ORDER,false)
                    .putExtra(Constants.IntentKeys.IS_CART,false)
                    .putExtra(Constants.IntentKeys.IS_NOTIFY,false)
                    .putExtra(Constants.IntentKeys.KEY,"")
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }
            page == Constants.IntentKeys.IS_CART -> {
                activity.startActivity(Intent(activity, DashBoardActivity::class.java)
                    .putExtra(Constants.IntentKeys.IS_ORDER,false)
                    .putExtra(Constants.IntentKeys.IS_CART,true)
                    .putExtra(Constants.IntentKeys.IS_NOTIFY,false)
                    .putExtra(Constants.IntentKeys.KEY,"")
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }
            page == Constants.IntentKeys.IS_ORDER -> {
                activity.startActivity(Intent(activity, DashBoardActivity::class.java)
                    .putExtra(Constants.IntentKeys.IS_ORDER,true)
                    .putExtra(Constants.IntentKeys.IS_CART,false)
                    .putExtra(Constants.IntentKeys.IS_NOTIFY,false)
                    .putExtra(Constants.IntentKeys.KEY,"")
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }
            page == Constants.IntentKeys.IS_NOTIFY -> {
                activity.startActivity(Intent(activity, DashBoardActivity::class.java)
                    .putExtra(Constants.IntentKeys.IS_ORDER,false)
                    .putExtra(Constants.IntentKeys.IS_CART,false)
                    .putExtra(Constants.IntentKeys.IS_NOTIFY,true)
                    .putExtra(Constants.IntentKeys.KEY,"")
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }
        }
        if(finish){
            activity.finish()
        }
    }
    fun isArray(x: Any): Boolean {
        return x.toString().contains("[") && x.toString().contains("]")
    }
    fun getTimeAgo(date: String, inputFormat: String): String{
        val sdf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val time = sdf.parse(date)!!.time
            val now = System.currentTimeMillis()
            val ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS)
            return ago.toString()
        } catch (e: ParseException1) {
            e.printStackTrace()
        }
        return ""
    }
    fun getFormattedDate(date: String, inputFormat: String, outputFormat: String): String {

        var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException1) {
            e.printStackTrace()
        }


        spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return if (newDate != null)
            spf.format(newDate)
        else
            ""

    }
    fun getFormattedDateUtc(date: String, inputFormat: String?, outputFormat: String?): String? {
        var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        spf.timeZone = TimeZone.getTimeZone("UTC")
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }
        spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return spf.format(newDate!!)
    }
    fun openCamera(activity: Activity) {
        /* val sharedHelper = SharedHelper(activity)
         val file = getFileTostoreImage(activity)
         val uri: Uri*/
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    getFileToStoreImage(activity)
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        activity,
                        activity.packageName+".fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    if(activity.javaClass.simpleName.equals(CheckoutActivity().javaClass.simpleName)){
                        val checkoutActivity:CheckoutActivity = activity as CheckoutActivity
                        checkoutActivity.requestCode = Constants.RequestCode.CAMERA_INTENT
                        checkoutActivity.resultLauncher.launch(takePictureIntent)
                    }
                    else{
                        activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
                    }
                }
            }
        }

    }
    fun openGallery(activity: Activity) {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        if(activity.javaClass.simpleName.equals(CheckoutActivity().javaClass.simpleName)){
            val checkoutActivity:CheckoutActivity = activity as CheckoutActivity
            checkoutActivity.requestCode = Constants.RequestCode.GALLERY_INTENT
            checkoutActivity.resultLauncher.launch(i)
        }
        else{
            activity.startActivityForResult(i, Constants.RequestCode.GALLERY_INTENT)
        }
    }
    private fun getFileToStoreImage(activity: Activity): File {
        val sharedHelper = SharedHelper(activity)
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            sharedHelper.imageUploadPath = absolutePath
        }
    }
    @SuppressLint("NewApi")
    fun getRealPathFromUriNew(context: Context, uri: Uri): String? {
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return context.getExternalFilesDir(null)!!.toString() + "/" + split[1]
                }

                //  handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }
    private fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor =
                context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
    fun getRealPathFromURI(context: Context, contentURI: Uri): String? {
        val result: String?
        val filePathColumn = arrayOf(MediaStore.Images.Media._ID)
        val cursor = context.contentResolver.query(contentURI, filePathColumn, null, null, null)

        if (cursor == null) {
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor
                .getColumnIndex(filePathColumn[0])
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }
    fun isPermissionsEnabled(context: Context,name:String): Boolean {
        val result1 = ContextCompat.checkSelfPermission(context, permission.ACCESS_FINE_LOCATION)
        val result2 = ContextCompat.checkSelfPermission(context, permission.CAMERA)
        val result3 = ContextCompat.checkSelfPermission(context, permission.WRITE_EXTERNAL_STORAGE)
        val result4 = ContextCompat.checkSelfPermission(context, permission.READ_EXTERNAL_STORAGE)
        return when (name) {
            Constants.IntentKeys.LOCATION -> {
                result1 == PackageManager.PERMISSION_GRANTED
            }
            Constants.IntentKeys.CAMERA -> {
                result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED
            }
            Constants.IntentKeys.GALLERY -> {
                result4 == PackageManager.PERMISSION_GRANTED
            }
            Constants.IntentKeys.STORAGE -> {
                result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED
            }
            else -> {
                result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED
            }
        }
    }
    fun isDeniedPermission(activity: Activity, name: String):Boolean{
        val result1 = activity.shouldShowRequestPermissionRationale(permission.ACCESS_FINE_LOCATION)
        val result2 = activity.shouldShowRequestPermissionRationale(permission.CAMERA)
        val result3 = activity.shouldShowRequestPermissionRationale(permission.WRITE_EXTERNAL_STORAGE)
        val result4 = activity.shouldShowRequestPermissionRationale(permission.READ_EXTERNAL_STORAGE)
        return when (name) {
            Constants.IntentKeys.LOCATION -> {
                result1
            }
            Constants.IntentKeys.CAMERA -> {
                result2 && result3 && result4
            }
            Constants.IntentKeys.GALLERY -> {
                result4
            }
            Constants.IntentKeys.STORAGE -> {
                result3 && result4
            }
            else -> {
                result1 || result2 || result3
            }
        }
    }
    fun permissionsEnableRequest(activity: Activity,name: String) {
        when (name) {
            Constants.IntentKeys.LOCATION -> {
                requestPermissions(activity,
                    Constants.Permission.LOCATION_LIST,
                    Constants.RequestCode.LOCATION_REQUEST
                )
            }
            Constants.IntentKeys.CAMERA -> {
                requestPermissions(activity,
                    Constants.Permission.CAMERA_LIST,
                    Constants.RequestCode.CAMERA_REQUEST
                )
            }
            Constants.IntentKeys.GALLERY -> {
                requestPermissions(activity,
                    Constants.Permission.GALLERY_LIST,
                    Constants.RequestCode.GALLERY_REQUEST
                )
            }
            Constants.IntentKeys.STORAGE -> {
                requestPermissions(activity,
                    Constants.Permission.STORAGE_LIST,
                    Constants.RequestCode.STORAGE_REQUEST
                )
            }
            else -> {
                requestPermissions(activity,
                    arrayOf(
                        permission.ACCESS_COARSE_LOCATION,
                        permission.ACCESS_FINE_LOCATION,
                        permission.CAMERA,
                        permission.WRITE_EXTERNAL_STORAGE,
                        permission.READ_EXTERNAL_STORAGE
                    ),
                    Constants.RequestCode.ALL_PERMISSION_REQUEST
                )
            }
        }
    }
    fun grantResults(grantResults: IntArray):Boolean {
        for(items in grantResults){
            if(items != PackageManager.PERMISSION_GRANTED){
                return false
            }
        }
        return true
    }
    fun content(name: String):String{
        return   """
            We need to access $name for performing necessary task. Please permit the permission through Settings screen.
            
            Select App Permissions -> Enable permission($name)
            """.trimIndent()
    }
    fun displayManuallyEnablePermissionsDialog(activity: Activity,name: String,callBack: DialogCallBack?) {
        val builder = AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog_Alert)
        when (name) {
            Constants.IntentKeys.LOCATION -> {
                builder.setMessage(content(activity.getString(R.string.location)))
            }
            Constants.IntentKeys.CAMERA -> {
                val msg = "${activity.getString(R.string.camera)}${","}${activity.getString(R.string.storage)}"
                builder.setMessage(content(msg))
            }
            Constants.IntentKeys.GALLERY -> {
                builder.setMessage(content(activity.getString(R.string.storage)))
            }
            Constants.IntentKeys.STORAGE -> {
                builder.setMessage(content(activity.getString(R.string.storage)))
            }
            else -> {
                val msg = "${activity.getString(R.string.location)}${","}${activity.getString(R.string.storage)}${","}${activity.getString(R.string.camera)}"
                builder.setMessage(content(msg))
            }
        }
        builder.setCancelable(false)
        builder.setPositiveButton(activity.getString(R.string.ok)) { dialog, _ ->
            dialog.dismiss()
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", activity.packageName, null)
            intent.data = uri
            activity.startActivity(intent)
            callBack?.onPositiveClick("")
        }
        builder.setNegativeButton(activity.getString(R.string.cancel)) { dialog, _ ->
            dialog.dismiss()
            callBack?.onNegativeClick()
        }
        builder.show()
    }
    fun isGpsEnabled(activity: Activity): Boolean {
        val locationManager: LocationManager?
        locationManager = activity.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }
    fun gpsEnableRequest(activity: Activity) {
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingsClient = LocationServices.getSettingsClient(activity)
        val task = settingsClient.checkLocationSettings(builder.build())
        task.addOnSuccessListener(activity) {
            //setIsGps(true)
        }
        task.addOnFailureListener(activity) { e ->
            if (e is ResolvableApiException) {
                try {
                    e.startResolutionForResult(activity, Constants.RequestCode.GPS_REQUEST)
                } catch (e1: SendIntentException) {
                    e1.printStackTrace()
                }
            }
        }
/*
        task.addOnCompleteListener(activity, OnCompleteListener { task ->
            if(task.isSuccessful){

            }
            else{
                val e = task.exception
                if (e is ResolvableApiException) {
                    try {
                        e.startResolutionForResult(activity, Constants.RequestCode.GPS_REQUEST)
                    } catch (e1: SendIntentException) {
                        e1.printStackTrace()
                    }
                }
            }
        })
*/
    }
/*
    fun getBitmapFromUrl(imageUrl: String?): Bitmap? {
        return try {
            // val urlImage = "https://thumbs.dreamstime.com/z/hands-holding-blue-earth-cloud-sky" + "-elements-imag-background-image-furnished-nasa-61052787.jpg"
            val url = URL(imageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            Log.e("awesome", "Error in getting notification image: " + e.localizedMessage)
            null
        }
    }
*/

    /* fun getCurrentTime(): String? {
         val dateFormat = SimpleDateFormat("hh:mm a")
         dateFormat.timeZone = TimeZone.getTimeZone("UTC")
         val today = Calendar.getInstance().time
         return dateFormat.format(today)
     }
     fun navigateToFragment(id: Int, fragmentName: Fragment, fragmentManager: FragmentManager) {
         val fragmentTransaction =
             fragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(
             R.anim.enter_from_left,
             R.anim.exit_out_right,
             R.anim.enter_from_right,
             R.anim.exit_out_right
         )
         fragmentTransaction.replace(id, fragmentName)
         fragmentTransaction.addToBackStack(fragmentName.javaClass.simpleName)
         fragmentTransaction.commit()
     }
     fun getTimeFromTimeStamp(value: String): String {
         val date = Date(value.toLong())
         val simpleDateFormat = SimpleDateFormat("hh:mm aa", Locale.ENGLISH)
         return simpleDateFormat.format(date)
     }
     fun getUtcTime(date: String, dateFormat: String): String {
         val spf = SimpleDateFormat(dateFormat, Locale.ENGLISH)
         var newDate: Date? = null
         try {
             newDate = spf.parse(date)
         } catch (e: ParseException1) {
             e.printStackTrace()
         }

         newDate?.let {
             val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.ENGLISH)
             simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
             return simpleDateFormat.format(newDate)
         }

         return ""
     }
     fun navigateToFragmentFromBottom(id: Int, fragmentName: Fragment, fragmentManager: FragmentManager) {
         val fragmentTransaction =
             fragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(
             R.anim.slide_in_from_bottom,
             R.anim.slide_down_to_bottom
         )
         fragmentTransaction.replace(id, fragmentName)
         fragmentTransaction.addToBackStack(fragmentName.javaClass.simpleName)
         fragmentTransaction.commit()
     }
     fun numberFormat(value: Int): String {
         return String.format(Locale.ENGLISH,"%02d", value)
     }
     fun numberFormat(value: Double): String {
         return String.format(Locale.ENGLISH,"%.2f", value)
     }
     fun getAge(year: Int, month: Int, day: Int): String {
         val dob = Calendar.getInstance()
         val today = Calendar.getInstance()
         dob[year, month] = day
         var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
         if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
             age--
         }
         val ageInt = age
         return ageInt.toString()
     }
     fun addEndTime(date: String, intputFormat: String, outputFormat: String, minutes: Int): String? {

         var spf = SimpleDateFormat(intputFormat, Locale.ENGLISH)
         var newDate: Date? = null
         try {
             newDate = spf.parse(date)
         } catch (e: ParseException1) {
             e.printStackTrace()
         }

         newDate?.let {
             val calander = getCalanderInstance(newDate)
             calander.add(Calendar.MINUTE, minutes)
             spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
             return spf.format(calander.time)

         }

         return ""

     }
     private fun getCalanderInstance(date: Date): Calendar {
         val calander = Calendar.getInstance()
         calander.time = date
         return calander
     }
     fun isPastTime(givenTime: String): Boolean {
         val simpleDateFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
         val date1 = simpleDateFormat.parse(givenTime)
         val date2 = simpleDateFormat.parse(SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date()))

         if (date1 != null && date2 != null) {
             val difference = date1.time - date2.time
             // val days = difference / (1000 * 60 * 60 * 24)

             val hours = difference / (1000 * 60 * 60)
             val mins = difference / (1000 * 60) % 60
 //            var hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60))
 //            val min = (difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours) / (1000 * 60)

             if (hours < 0) {
                 return true
             } else {
                 if (hours > 0) {
                     return false
                 }
                 return mins <= 0
             }
         } else {
             return true
         }
     }
     fun stringToCalendering(dateString: String): Calendar {
         val date: Date = stringToDate(dateString)
         return dateToCalender(date)
     }
     fun dpToPx(dp: Float): Int {
         val density = Resources.getSystem().displayMetrics.density
         return ceil((dp * density).toDouble()).toInt()
     }
     fun differenceBetweenDates(appDate: String): Float {
         val currentDate: Date = Calendar.getInstance(Locale.ENGLISH).time
         val appoinmentdate: Date = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(appDate)!!
         val diff = appoinmentdate.time - currentDate.time
         return diff.toFloat() / (24 * 60 * 60 * 1000)
     }
     fun differenceBetweenDatesInInt(appDate: String): Int {
         val currentDate: Date = Calendar.getInstance(Locale.ENGLISH).time
         val appoinmentdate: Date = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(appDate)!!
         val diff = appoinmentdate.time - currentDate.time
         val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)
         return dayCount.toInt()
     }
     fun getDayFromDate(calenderInstance: Calendar): String {
         return getDay(calenderInstance.get(Calendar.DAY_OF_WEEK))
     }
     fun isValidPassword(context: Context, password: String): String {
         val specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
         val upperCasePatten = Pattern.compile("[A-Z ]")
         val lowerCasePatten = Pattern.compile("[a-z ]")
         val digitCasePatten = Pattern.compile("[0-9 ]")

         return if (password.length < 8) {
             context.resources.getString(0)
         } else if (!specailCharPatten.matcher(password).find()) {
             context.resources.getString(0)
         } else if (!upperCasePatten.matcher(password).find()) {
             context.resources.getString(0)
         } else if (!lowerCasePatten.matcher(password).find()) {
             context.resources.getString(0)
         } else if (!digitCasePatten.matcher(password).find()) {
             context.resources.getString(0)
         } else {
             "true"
         }
     }
     fun isValidMobile(phone: String): Boolean {
         var check = false
         if (!Pattern.matches("[a-zA-Z]+", phone)) {
             if (phone.length in 6..13) {
                 check = true
             }
         }
         return check
     }
     fun isValidEmail(email: String): Boolean {
         return Patterns.EMAIL_ADDRESS.matcher(email).matches()
     }
     private fun getFileTostoreImageold(context: Context): File {
         val filepath = context.getExternalFilesDir(null)!!
         val zoeFolder = File(
             filepath.absolutePath,
             context.getString(R.string.app_name)
         ).absoluteFile
         if (!zoeFolder.exists()) {
             zoeFolder.mkdir()
         }
         val newFolder = File(
             zoeFolder,
             context.getString(R.string.app_name) + " Image"
         ).absoluteFile
         if (!newFolder.exists()) {
             newFolder.mkdir()
         }

         val filename = System.currentTimeMillis()
         val cameracaptureFile = "IMG_" + filename + "_" + System.currentTimeMillis().toString() + "_"
         return File(newFolder, "$cameracaptureFile.jpg")
     }
     fun openCameraold(activity: Activity) {
         val sharedHelper = SharedHelper(activity)
         val file = getFileTostoreImage(activity)
         val uri: Uri = FileProvider.getUriForFile(activity, activity.packageName + ".fileprovider", file)
         sharedHelper.imageUploadPath = file.absolutePath
         val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
         takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
         takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
         activity.startActivityForResult(takePictureIntent, Constants.RequestCode.CAMERA_INTENT)
     }
     fun checkPermission(context: Context, permissionName: String): Boolean {
         val res = context.checkCallingPermission(permissionName)
         return res == PackageManager.PERMISSION_GRANTED
     }
     private fun stringToDate(date: String): Date {
         return SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(date)!!
     }
     private fun dateToCalender(date: Date): Calendar {
         val cal = Calendar.getInstance(Locale.ENGLISH)
         cal.time = date
         return cal
     }
     private fun getDay(day: Int): String {
         return when (day) {
             1 -> "Sun"
             2 -> "Mon"
             3 -> "Tue"
             4 -> "Wed"
             5 -> "Thu"
             6 -> "Fri"
             7 -> "Sat"
             else -> ""
         }
     }
     fun isValidTime(date: String, format: String, time: Int): Boolean {
         val spf = SimpleDateFormat(format, Locale.ENGLISH)
         var newDate: Date? = null
         try {
             newDate = spf.parse(date)
         } catch (e: ParseException1) {
             e.printStackTrace()
         }
         val secondsInMilli = 1000
         val minutesInMilli = secondsInMilli * 60
         val currentDate: Date = Calendar.getInstance().time
         val currentString = spf.format(currentDate)
         var curretDate: Date? = null
         try {
             curretDate = spf.parse(currentString)
         } catch (e: ParseException1) {
             e.printStackTrace()
         }
         newDate?.let { nDate ->
             curretDate?.let { cDate ->
                 val diff = nDate.time - cDate.time
                 val elapsedMinutes: Int = (diff / minutesInMilli).toInt()
                 Log.d("diff", elapsedMinutes.toString())
                 return elapsedMinutes >= time
             }
         }
         return false
     }
      fun convertDate(date: Date, format: String?): String? {
         val simpleDateFormat = SimpleDateFormat(format, Locale.ENGLISH)
         return simpleDateFormat.format(date)
     }
     fun convertDate(date: String, inputFormat: String?, outputFormat: String?): String? {
         val simpleDateFormat = SimpleDateFormat(inputFormat, Locale.ENGLISH)
         var date1: Date? = null
         try {
             date1 = simpleDateFormat.parse(date)
         } catch (e: ParseException) {
             e.printStackTrace()
         }
         val simpleOutputFormat = SimpleDateFormat(outputFormat, Locale.ENGLISH)
         return if (date1 != null) {
             simpleOutputFormat.format(date1)
         } else null
     }
     fun convertDateutc(date: String, inputFormat: String?, outputFormat: String?): String? {
         val simpleDateFormat = SimpleDateFormat(inputFormat, Locale.ENGLISH)
         simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
         var date1: Date? = null
         try {
             date1 = simpleDateFormat.parse(date)
         } catch (e: ParseException) {
             e.printStackTrace()
         }
         val simpleOutputFormat = SimpleDateFormat(outputFormat, Locale.ENGLISH)
         return if (date1 != null) {
             simpleOutputFormat.format(date1)
         } else null
     }
     fun getConvertedTime(createdTime: String): String {
         val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH)
         simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
         var targetdatevalue = ""
         try {
             val startDate: Date? = simpleDateFormat.parse(createdTime)
             val targetFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
             targetFormat.timeZone = Calendar.getInstance().timeZone
             targetdatevalue = targetFormat.format(startDate!!)
         } catch (e: ParseException) {
             e.printStackTrace()
         }
         return targetdatevalue
     }
     fun getFormatedString(createdTime: String, format: String?): String {
         val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH)
         var targetdatevalue = ""
         try {
             val startDate: Date? = simpleDateFormat.parse(createdTime)
             val targetFormat = SimpleDateFormat(format, Locale.ENGLISH)
             targetdatevalue = targetFormat.format(startDate!!)
         } catch (e: ParseException) {
             e.printStackTrace()
         }
         return targetdatevalue
     }
     */

}