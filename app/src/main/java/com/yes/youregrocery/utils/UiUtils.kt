package com.yes.youregrocery.utils

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.yes.youregrocery.R
import android.util.DisplayMetrics
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.yes.youregrocery.BuildConfig
import com.yes.youregrocery.session.SharedHelper
import java.util.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.github.razir.progressbutton.DrawableButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.yes.youregrocery.`interface`.AnimationCallBack
import com.yes.youregrocery.session.Constants

object UiUtils {
    @Suppress("DEPRECATION")
    fun convertHtml(text : String): String {
        return if(text.isNotEmpty()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val res = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
                res.toString()
            } else {
                val res = Html.fromHtml(text)
                res.toString()
            }
        } else{
            text
        }
    }
    fun animation(context: Context,view: View,type: Int,visible: Boolean){
        val anim: Animation = AnimationUtils.loadAnimation(context,type)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                if(visible){
                    view.alpha = 1F
                    view.visibility = View.VISIBLE
                }
                else{
                    view.alpha = 1F
                    view.visibility = View.GONE
                }
            }
            override fun onAnimationRepeat(p0: Animation?) {

            }
        })
        view.startAnimation(anim)
    }
    fun animationWithCallback(context: Context, view: View, type: Int, callBack: AnimationCallBack){
        val anim: Animation = AnimationUtils.loadAnimation(context,type)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
                callBack.onAnimationStart()
            }

            override fun onAnimationEnd(p0: Animation?) {
                callBack.onAnimationEnd()
            }
            override fun onAnimationRepeat(p0: Animation?) {
                callBack.onAnimationRepeat()
            }
        })
        view.startAnimation(anim)
    }
    fun showSnack(view: View, content: String) {
        Snackbar.make(view, content, Snackbar.LENGTH_SHORT).show()
    }
    fun showLog(TAG: String, content: String) {
        Log.d(TAG, content)
    }
    fun log(TAG: String?, content: String?) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, content!!)
        }
    }
    fun loadDefaultImage(imageView: ImageView?){
        if (imageView == null) {
            return
        }
        else{
            Glide.with(imageView.context)
                .load(AppCompatResources.getDrawable(imageView.context,R.drawable.no_image))
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.banner_loader1)
                        .error(R.mipmap.ic_launcher)
                )
                .into(imageView)
        }
    }
    fun loadImage(imageView: ImageView?, imageUrl: String?) {
        if (imageUrl == null || imageView == null || !imageUrl.contains("http")) {
            return
        }
        else{
            Glide.with(imageView.context)
                .load(imageUrl)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.banner_loader1)
                        .error(R.mipmap.ic_launcher)
                )
                .into(imageView)
        }
    }
    fun loadCustomImage(imageView: ImageView?, imageUrl: String?){
        if (imageUrl == null || imageView == null) {
            return
        }
        else{
            Glide.with(imageView.context)
                .load(imageUrl)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.banner_loader1)
                        .error(R.mipmap.ic_launcher)
                )
                .into(imageView)
        }
    }
    fun loadImageWithCenterCrop(imageView: ImageView?, imageUrl: String?){
        if (imageUrl == null || imageView == null || !imageUrl.contains("http")) {
            return
        }

        imageView.scaleType = ImageView.ScaleType.CENTER
        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(R.drawable.banner_loader1).error(R.drawable.no_image))
            .listener(object: RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    imageView.scaleType = ImageView.ScaleType.CENTER
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    imageView.scaleType = ImageView.ScaleType.FIT_XY
                    return false
                }

            })
            .into(imageView)

    }
    fun getNavIconColorStates(color: String): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_checked),
                intArrayOf(android.R.attr.state_checked)
            ),
            intArrayOf(Color.parseColor("#ffffff"), Color.parseColor(color))
        )
    }
    fun getAngleDrawable(context: Context, solidColor: Int, _radius: FloatArray?, strokeWidth: Int, strokeColor: String): GradientDrawable {
        val gradientDrawable = GradientDrawable()
        gradientDrawable.color = ColorStateList.valueOf(context.getColor(solidColor))
        gradientDrawable.shape = GradientDrawable.RECTANGLE
        gradientDrawable.cornerRadii = _radius
        gradientDrawable.setStroke(strokeWidth, ColorStateList.valueOf(Color.parseColor(strokeColor)))
        return gradientDrawable
    }
    fun editTextColor(edittext: TextInputLayout, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                edittext.boxStrokeColor = Color.parseColor(colorStr)
                edittext.hintTextColor = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                edittext.boxStrokeColor = edittext.context.getColor(colorInt)
                edittext.hintTextColor = ColorStateList.valueOf(edittext.context.getColor(colorInt))
            }
            else -> {
                edittext.boxStrokeColor = edittext.context.getColor(R.color.colorPrimary)
                edittext.hintTextColor = ColorStateList.valueOf(edittext.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun setTextViewDrawableColor(textView: TextView,colorStr: String?, colorInt: Int?) {
        for (drawable in textView.compoundDrawablesRelative) {
            if (drawable != null) {
                when {
                    colorStr != null -> {
                        drawable.colorFilter = PorterDuffColorFilter(Color.parseColor(colorStr), PorterDuff.Mode.SRC_IN)
                    }
                    colorInt != null -> {
                        drawable.colorFilter = PorterDuffColorFilter(textView.context.getColor(colorInt), PorterDuff.Mode.SRC_IN)
                    }
                    else -> {
                        drawable.colorFilter = PorterDuffColorFilter(textView.context.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN)
                    }
                }
            }
        }
    }
    fun radioButtonTint(radioButton: RadioButton, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                radioButton.buttonTintList = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                radioButton.buttonTintList = ColorStateList.valueOf(radioButton.context.getColor(colorInt))
            }
            else -> {
                radioButton.buttonTintList = ColorStateList.valueOf(radioButton.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun checkBoxTint(checkBox: CheckBox, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                checkBox.buttonTintList = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                checkBox.buttonTintList = ColorStateList.valueOf(checkBox.context.getColor(colorInt))
            }
            else -> {
                checkBox.buttonTintList = ColorStateList.valueOf(checkBox.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun imageViewTint(imageView: ImageView, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                imageView.imageTintList = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                imageView.imageTintList = ColorStateList.valueOf(imageView.context.getColor(colorInt))
            }
            else -> {
                imageView.imageTintList = ColorStateList.valueOf(imageView.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun cardViewBgColor(cardView: CardView, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                cardView.setCardBackgroundColor(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                cardView.setCardBackgroundColor(cardView.context.getColor(colorInt))
            }
            else -> {
                cardView.setCardBackgroundColor(cardView.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun buttonBgColor(button: Button, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                button.setBackgroundColor(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                button.setBackgroundColor(button.context.getColor(colorInt))
            }
            else -> {
                button.setBackgroundColor(button.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun relativeLayoutBgColor(relativeLayout: RelativeLayout, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                relativeLayout.setBackgroundColor(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                relativeLayout.setBackgroundColor(relativeLayout.context.getColor(colorInt))
            }
            else -> {
                relativeLayout.setBackgroundColor(relativeLayout.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun linearLayoutBgColor(linearLayout: LinearLayout, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                linearLayout.setBackgroundColor(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                linearLayout.setBackgroundColor(linearLayout.context.getColor(colorInt))
            }
            else -> {
                linearLayout.setBackgroundColor(linearLayout.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun textViewBgTint(textView: TextView, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                textView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(colorStr), BlendModeCompat.SRC_ATOP)
            }
            colorInt != null -> {
                textView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(textView.context.getColor(colorInt), BlendModeCompat.SRC_ATOP)
            }
            else -> {
                textView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(textView.context.getColor(R.color.colorPrimary), BlendModeCompat.SRC_ATOP)
            }
        }
    }
    fun buttonBgTint(button: Button, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                button.backgroundTintList = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                button.backgroundTintList = ColorStateList.valueOf(button.context.getColor(colorInt))
            }
            else -> {
                button.backgroundTintList = ColorStateList.valueOf(button.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun cardViewBgTint(cardView: CardView, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                cardView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(colorStr), BlendModeCompat.SRC_ATOP)
            }
            colorInt != null -> {
                cardView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(cardView.context.getColor(colorInt), BlendModeCompat.SRC_ATOP)
            }
            else -> {
                cardView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(cardView.context.getColor(R.color.colorPrimary), BlendModeCompat.SRC_ATOP)
            }
        }
    }
    fun linearLayoutBgTint(linearLayout: LinearLayout, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                linearLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(colorStr), BlendModeCompat.SRC_ATOP)
            }
            colorInt != null -> {
                linearLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(linearLayout.context.getColor(colorInt), BlendModeCompat.SRC_ATOP)
            }
            else -> {
                linearLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(linearLayout.context.getColor(R.color.colorPrimary), BlendModeCompat.SRC_ATOP)
            }
        }
    }
    fun relativeLayoutBgTint(relativeLayout: RelativeLayout, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                relativeLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(colorStr), BlendModeCompat.SRC_ATOP)
            }
            colorInt != null -> {
                relativeLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(relativeLayout.context.getColor(colorInt), BlendModeCompat.SRC_ATOP)
            }
            else -> {
                relativeLayout.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(relativeLayout.context.getColor(R.color.colorPrimary), BlendModeCompat.SRC_ATOP)
            }
        }
    }
    fun imageviewDrawable(imageView: ImageView, drawable: Int) {
        imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, drawable))
    }
    fun textviewCustomDrawable(textView: TextView, drawable: Int){
        textView.background = AppCompatResources.getDrawable(textView.context,drawable)
    }
    fun textviewImgDrawable(textView: TextView, drawable: Int?, position:String){
        if(drawable != null){
           if(position == Constants.IntentKeys.START){
            textView.setCompoundDrawablesWithIntrinsicBounds(
                AppCompatResources.getDrawable(textView.context,drawable),
                null,
                null,
                null)
        }
           else if(position == Constants.IntentKeys.END){
            textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                AppCompatResources.getDrawable(textView.context,drawable),
                null)
        }
        }
        else{
           // textView.setCompoundDrawables(null,null,null,null)
            textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                null,
                null)
        }
    }
    fun txtBtnLoadingStart(button: MaterialButton){
        button.showProgress {
            progressColor = Color.WHITE
            gravity = DrawableButton.GRAVITY_CENTER
        }
        button.isEnabled = false
        /*  Handler().postDelayed({
             button.isEnabled = true
             button.hideProgress("ADD")
         }, 3000)*/
    }
    fun txtBtnLoadingEnd(button: MaterialButton, text:String){
        button.isEnabled = true
        button.hideProgress(text)
    }
    fun imgBtnLoadingEnd(button: MaterialButton){
        button.icon = null
        button.showProgress {
            progressColor = Color.BLACK
            gravity = DrawableButton.GRAVITY_CENTER
        }
        button.isEnabled = false
    }
    fun imgBtnLoadingEnd(button: MaterialButton, icon:Int){
        button.isEnabled = true
        button.icon = AppCompatResources.getDrawable(button.context,icon)
        button.hideProgress("")
    }
    fun notificationBar(activity: Activity, colorStr: String?, colorInt: Int?){
        val window: Window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        when {
            colorStr != null -> {
                window.statusBarColor = Color.parseColor(colorStr)
            }
            colorInt != null -> {
                window.statusBarColor = activity.getColor(colorInt)
            }
            else -> {
                window.statusBarColor = activity.getColor(R.color.colorPrimary)
            }
        }
    }
    fun plusMinus(button: MaterialButton){
        button.iconTint = ColorStateList.valueOf(Color.parseColor(SharedHelper(button.context).primaryColor))
        button.strokeColor = ColorStateList.valueOf(Color.parseColor(SharedHelper(button.context).primaryColor))
    }
    fun textViewTextColor(textView: TextView, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                textView.setTextColor(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                textView.setTextColor(textView.context.getColor(colorInt))
            }
            else -> {
                textView.setTextColor(textView.context.getColor(R.color.colorPrimary))
            }
        }
    }
    fun viewBgTint(view: View, colorStr: String?, colorInt: Int?){
        when {
            colorStr != null -> {
                view.backgroundTintList = ColorStateList.valueOf(Color.parseColor(colorStr))
            }
            colorInt != null -> {
                view.backgroundTintList = ColorStateList.valueOf(view.context.resources.getColor(colorInt,null))
            }
            else -> {
                view.backgroundTintList = ColorStateList.valueOf(view.context.resources.getColor(R.color.grey,null))
            }
        }
    }
    fun formattedValues(value: String?): String {
        return if (value != null && !value.equals("", ignoreCase = true) && !BaseUtils.isContainsChar(value)) java.lang.String.format(
            Locale.ENGLISH,
            "%.2f",
            value.toDouble()
        ) else Constants.IntentKeys.AMOUNT_DUMMY
    }
    fun stringCon(arrayOf: Array<String>): String{
        when (arrayOf.size) {
            1 -> {
                return String.format(Locale.ENGLISH,"%s",arrayOf[0])
            }
            2 -> {
                return String.format(Locale.ENGLISH,"%s %s", arrayOf[0], arrayOf[1])
            }
            3 -> {
                return String.format(Locale.ENGLISH,"%s %s %s", arrayOf[0], arrayOf[1],arrayOf[2])
            }
            4 -> {
                return String.format(Locale.ENGLISH,"%s %s %s %s", arrayOf[0], arrayOf[1],arrayOf[2],arrayOf[3])
            }
            5 -> {
                return String.format(Locale.ENGLISH,"%s %s %s %s %s", arrayOf[0], arrayOf[1],arrayOf[2],arrayOf[3],arrayOf[4])
            }
            else -> {
                return ""
            }
        }
    }
    fun removeSpace(string: String):String{
        return string.replace(" ","").trim()
    }
    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        //  return dp * (context.getResources().getDisplayMetrics().densityDpi as Float / DisplayMetrics.DENSITY_DEFAULT)
    }
   /* fun loadImageWithFitXY(imageView: ImageView?, imageUrl: String?){
        if (imageUrl == null || imageView == null || !imageUrl.contains("http")) {
            return
        }

        imageView.scaleType = ImageView.ScaleType.CENTER
        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(R.drawable.banner_loader1).error(R.drawable.no_image))
            .listener(object: RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    imageView.scaleType = ImageView.ScaleType.FIT_XY
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    imageView.scaleType = ImageView.ScaleType.FIT_CENTER
                    return false
                }

            })
            .into(imageView)

    }
    fun getSelectorDrawable(color: String): StateListDrawable {
        val colorList = ColorStateList.valueOf(Color.parseColor(color))
        val out = StateListDrawable()
        out.addState(intArrayOf(android.R.attr.state_pressed), createNormalDrawable(colorList))
        out.addState(intArrayOf(), createStrokeDrawable(colorList))
        out.addState(StateSet.WILD_CARD, createStrokeDrawable(colorList))
        return out
    }
    private fun createNormalDrawable(color: ColorStateList): GradientDrawable {
        val out = GradientDrawable()
        out.color = color
        return out
    }
    private fun createStrokeDrawable(color: ColorStateList): GradientDrawable {
        val out = GradientDrawable()
        out.setStroke(1, color)
        return out
    }
    fun convertStringStrike(text : String): String {
        val string: Spannable = SpannableString(text)
        string.setSpan(StrikethroughSpan(), 0, string.length, 0)
        string.setSpan(ForegroundColorSpan(Color.RED), 0, string.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return string.toString()
    }
    fun formattedValues(value: Int): String? {
        return java.lang.String.format(Locale.ENGLISH, "%02d", value)
    }
    fun formattedValues(value: Double?): String? {
        return java.lang.String.format(Locale.ENGLISH, "%.2f", value)
    }
    fun getColoredAndBoldString(context: Context?, content: String): SpannableStringBuilder {
        val stringBuilder = SpannableStringBuilder()
        val spannableString = SpannableString(content)
        //val styleSpan = StyleSpan(Typeface.BOLD)
        val colorSpan = ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.red))
        //spannableString.setSpan(styleSpan, 0, content.length, 0)
        spannableString.setSpan(colorSpan, 0, content.length, 0)
        spannableString.setSpan(StrikethroughSpan(), 0, content.length, 0)
        stringBuilder.append(spannableString)
        return stringBuilder
    }
   */
}
