package com.yes.youregrocery.`interface`

interface AnimationCallBack {
    fun onAnimationStart()
    fun onAnimationEnd()
    fun onAnimationRepeat()
}