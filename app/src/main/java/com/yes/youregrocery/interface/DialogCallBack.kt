package com.yes.youregrocery.`interface`

interface DialogCallBack {
    fun onPositiveClick(value : String)
    fun onNegativeClick()
}