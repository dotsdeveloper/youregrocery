package com.yes.youregrocery.session

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yes.youregrocery.responses.*

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)

    var isInstallFirst: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.IS_INSTALL_FIRST)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.IS_INSTALL_FIRST, value)
        }

    var token: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.TOKEN, value)
        }

    var referCode: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.REFER_CODE)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.REFER_CODE, value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.ID, value)
        }

    var customerType: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.CUSTOMER_TYPE)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.CUSTOMER_TYPE, value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.FCM_TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.FCM_TOKEN, value)
        }

    var language: String
        get() : String {
            return if (sharedPreference.getKey(Constants.SessionKeys.LANGUAGE) == "") {
                "en"
            } else {
                sharedPreference.getKey(Constants.SessionKeys.LANGUAGE)
            }

        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LANGUAGE, value)
        }

    var shopId: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.SHOP_ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.SHOP_ID, value)
        }

    var baseurl: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.BASEURL)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.BASEURL, value)
        }

    var baseId: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.BASE_ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.BASE_ID, value)
        }

    var name: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.NAME)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.NAME, value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.EMAIL)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.EMAIL, value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.MOBILE_NUMBER)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.MOBILE_NUMBER, value)
        }

    var countryCode: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.COUNTRY_CODE)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.COUNTRY_CODE, value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.LOGGED_IN)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.LOGGED_IN, value)
        }

    var location1: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.LOCATION1)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LOCATION1, value)
        }

    var location2: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.LOCATION2)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LOCATION2, value)
        }

    var primaryColor: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.PRIMARY_COLOR,"#A3A3A3")
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.PRIMARY_COLOR, value)
        }

    var secondaryColor: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.SECONDARY_COLOR,"#ffffff")
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.SECONDARY_COLOR, value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH, value)
        }

    var lastNotify: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.LAST_NOTIFY)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.LAST_NOTIFY, value)
        }

    var showBranch: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.SHOW_BRANCH)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.SHOW_BRANCH, value)
        }

    var notifyCount: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.NOTIFY_COUNT)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.NOTIFY_COUNT, value)
        }

    var walletAmount: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.WALLET_AMOUNT)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.WALLET_AMOUNT, value)
        }

    var referralAmount: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.REFERRAL_AMOUNT)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.REFERRAL_AMOUNT, value)
        }

    var cartIdTemp: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.CART_ID_TEMP)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.CART_ID_TEMP, value)
        }

    var shopList: ArrayList<Branches>
        get() : ArrayList<Branches> {
            val myType = object : TypeToken<List<Branches>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.SHOP_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Branches> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<Branches>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.SHOP_LIST, jsonString)
        }

    var cartList: ArrayList<Order>
        get() : ArrayList<Order> {
            val myType = object : TypeToken<List<Order>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.CART_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Order> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<Order>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.CART_LIST, jsonString)
        }

    var banners: ArrayList<Banners>
        get() : ArrayList<Banners> {
            val myType = object : TypeToken<List<Banners>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.BANNERS)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Banners> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<Banners>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.BANNERS, jsonString)
        }

    var wishlist: ArrayList<Wishlist>
        get() : ArrayList<Wishlist> {
            val myType = object : TypeToken<List<Wishlist>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.WISHLIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Wishlist> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<Wishlist>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.WISHLIST, jsonString)
        }

    var pushNotifyList: ArrayList<PushNotificationData>
        get() : ArrayList<PushNotificationData> {
            val myType = object : TypeToken<List<PushNotificationData>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.NOTIFY_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<PushNotificationData> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<PushNotificationData>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.NOTIFY_LIST, jsonString)
        }

    var shopWebData: ShopWebData
        get() : ShopWebData {
            val myType = object : TypeToken<ShopWebData>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.SHOP_WEB_DATA)

            if (vsl == "") {
                return ShopWebData()
            }
            return Gson().fromJson(vsl, myType)
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.SHOP_WEB_DATA, jsonString)
        }
}
