package com.yes.youregrocery.session

class TempSingleton private constructor() {
    companion object {

        private var instance: TempSingleton? = null

        fun getInstance(): TempSingleton {
            if (instance == null) {
                instance = TempSingleton()
            }
            return instance!!
        }

        fun clearAllValues() {
            instance = TempSingleton()
        }
    }

    var mobile: String? = null
    var password: String? = null
}