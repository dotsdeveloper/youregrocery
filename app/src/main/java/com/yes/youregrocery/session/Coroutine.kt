package com.yes.youregrocery.session

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object Coroutine {
    fun iOWorker (work : suspend (() -> Unit)){
        CoroutineScope(Dispatchers.IO).launch {
            work()
        }
    }
}