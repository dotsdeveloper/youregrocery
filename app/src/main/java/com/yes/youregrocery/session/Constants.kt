package com.yes.youregrocery.session

import android.Manifest

object Constants {
    object ApiKeys {
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer "
        const val SHOP_IS_OPEN = "Shop is open"
        const val SMALL_HOME_BANNER = "smallhomebanner"
        const val PRODUCT_MIDDLE = "productmiddle"
        const val DD = "dd"
        const val YYYY = "yyyy"
        const val MMMM = "MMMM"
        const val HH_MM_AAA = "hh:mm aaa"
        const val TIME_INPUT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"
        const val ERROR = "error"
        const val STATUS = "status"
        const val DATA = "data"
        const val ORIGINAL_FILES = "original_files"
        const val FILES = "files"
        const val SHOPPER_ID = "shopper_id"
        const val PRODUCT_ID = "product_id"
        const val QUANTITY = "quantity"
        const val QUANTITY_ID = "quantity_id"
        const val CART_ID = "cart_id"
        const val CART_ID1 = "cartid"
        const val IS_EMPTY = "is_empty"
        const val ID = "id"
        const val PIN_CODE = "pincode"
        const val COUPON = "coupon"
        const val LOCATION_NAME = "location_name"
        const val ADDRESS = "address"
        const val LANDMARK = "landmark"
        const val LOCATION_LAT = "location_lat"
        const val LOCATION_LONG = "location_long"
        const val CUSTOMER_ID = "customer_id"
        const val CREDIT_AMOUNT = "credit_amount"
        const val PAYMENT_ID = "paymentid"
        const val TYPE = "type"
        const val DEVICE = "device"
        const val DEVICE_ID = "device_id"
        const val NAME = "name"
        const val MOBILE = "mobile"
        const val FEEDBACK = "feedback"
        const val UNIT_ID = "unit_id"
        const val SHOP_ID = "shop_id"
        const val DESCRIPTION = "description"
        const val EMAIL = "email"
        const val NEW_PASSWORD = "new_password"
        const val CONFIRM_PASSWORD = "confirm_password"
        const val OTP = "otp"
        const val PASSWORD = "password"
        const val PASSWORD_CONFIRMATION = "password_confirmation"
        const val USER_TYPE = "user_type"
        const val LOCATION = "location"
        const val COUNTRY_CODE = "country_code"
        const val REFERRAL_CODE = "referral_code"
        const val BRAND_ID = "brand_id"
        const val SEARCH = "search"
        const val LIMIT = "limit"
        const val OFFSET = "offset"
        const val CATEGORY_ID = "category_id"
        const val SUB_CATEGORY_ID = "sub_category_id"
        const val PRODUCT = "product"
        const val PRODUCTS = "products"
        const val ITEMS = "items"
        const val ORDER = "order"
        const val ORDER_ID = "order_id"
    }

    object NotificationKeys {
        const val WELCOME = 1
        const val ORDER_PLACED = 2
        const val CART_NOTIFY = 3
        const val LOGIN = 4
//        const val ORDER_PROCESSING = 5
//        const val ORDER_OUR_FOR_DELIVERY = 6
//        const val ORDER_DELIVERED = 7
//        const val RETURNING_CUSTOMER = 8
//        const val DORMANT_CUSTOMER = 9
    }

    object SessionKeys {
        const val IS_INSTALL_FIRST = "isinstallfirst"
        const val TOKEN = "token"
        const val REFER_CODE = "refercode"
        const val ID = "id"
        const val CUSTOMER_TYPE = "customertype"
        const val FCM_TOKEN = "fcmToken"
        const val LANGUAGE = "language"
        const val SHOP_ID = "shopid"
        const val BASEURL = "baseurl"
        const val BASE_ID = "baseid"
        const val NAME = "name"
        const val EMAIL = "email"
        const val MOBILE_NUMBER = "mobileNumber"
        const val COUNTRY_CODE = "countryCode"
        const val LOGGED_IN = "loggedIn"
        const val LOCATION1 = "location1"
        const val LOCATION2 = "location2"
        const val PRIMARY_COLOR = "primarycolor"
        const val SECONDARY_COLOR = "secondarycolor"
        const val IMAGE_UPLOAD_PATH = "imageUploadPath"
        const val LAST_NOTIFY = "lastnotify"
        const val SHOW_BRANCH = "showbranch"
        const val NOTIFY_COUNT = "notifycount"
        const val WALLET_AMOUNT = "walletamount"
        const val REFERRAL_AMOUNT = "referalamount"
        const val CART_ID_TEMP = "cartid_temp"
        const val SHOP_LIST = "shoplist"
        const val CART_LIST = "cartlist"
        const val BANNERS = "banners"
        const val WISHLIST = "wishlist"
        const val NOTIFY_LIST = "notifylist"
        const val SHOP_WEB_DATA = "shopwebdata"

    }

    object IntentKeys {
        const val IS_CHECKOUT = "is_checkout"
        const val PIN_CODE = "pin_code"
        const val LOCATION = "location"
        const val CAMERA = "camera"
        const val GALLERY = "gallery"
        const val STORAGE = "storage"
        const val INR = "INR"
        const val IS_CART = "is_cart"
        const val IS_ORDER = "is_order"
        const val IS_NOTIFY = "is_notify"
        const val PAGE = "page"
        const val DASHBOARD = "dashboard"
        const val CATEGORY = "category"
        const val CNAME = "cname"
        const val IS_COME = "is_come"
        const val KEY = "key"
        const val KEY1 = "key1"
        const val CID = "cid"
        const val S_C_ID = "s_c_id"
        const val LOAD_CATEGORY_P = "load_category_p"
        const val LOAD_SUBCATEGORY_P = "load_subcategory_p"
        const val LOAD_SUBCATEGORY_LIST = "load_subcategory_list"
        const val LOAD_BRAND_P = "load_brand_p"
        const val PRODUCT = "product"
        const val SUBCATEGORY = "subcategory"
        const val COUPON = "coupon"
        const val VAR1 = "{{var1}}"
        const val VAR2 = "{{var2}}"
        const val VAR3 = "{{var3}}"
        const val MOBILE = "mobile"
        const val LOGIN = "login"
        const val NULL = "null"
        const val AMOUNT_DUMMY = "0.00"
        const val FORGET = "forget"
        const val SIGNUP = "signup"
        const val ALL = "all"
        const val LABEL = "label"
        const val UNDEFINED = "undefined"
        const val START = "start"
        const val END = "end"
        const val SEARCH_TYPE = "search_type"
        const val TITLE = "title"
        const val DATA = "data"
        const val IS_CAT = "is_cat"
        const val HOME = "home"
        const val NEW_ARRIVALS = "new_arrivals"
        const val BESTSELLER = "bestseller"
        const val POST = "POST"
        const val GET = "GET"
        const val BODY = "body"
        const val WHATSAPP_SHARE = "https://api.whatsapp.com/send?phone="
        // const val REQUEST_CODE = "RequestCode"
    }

    object Common {
        var CURRENCY = "₹"
        var IS_FIRST_OPEN = true
        var IS_FIRST_OPEN_1 = 0
        var LIMIT = 50
    }

    object Permission {
        //  val AUDIO_CALL_LIST = arrayOf(Manifest.permission.RECORD_AUDIO)
        //  val VIDEO_CALL_LIST = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA)
        //  val COURSE_LOCATION_LIST = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)
        val LOCATION_LIST = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        val CAMERA_LIST = arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val GALLERY_LIST = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        val STORAGE_LIST = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    object RequestCode {
        const val CAMERA_INTENT = 101
        const val GALLERY_INTENT = 102
        const val ALL_PERMISSION_REQUEST = 200
        const val GPS_REQUEST = 106
        const val LOCATION_REQUEST = 107
        const val CAMERA_REQUEST = 201
        const val GALLERY_REQUEST = 202
        const val STORAGE_REQUEST = 203
        //  const val COURSE_LOCATION_REQUEST = 205
        //  const val VIDEO_CALL_REQUEST = 206
        //  const val AUDIO_CALL_REQUEST = 207
    }
}