package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetWalletResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<WalletData>? = null
}

class WalletData : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("credit_amount")
    var credit_amount: String? = null

    @SerializedName("used_amount")
    var used_amount: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("credit_audit")
    var credit_audit: ArrayList<Criditaudit>? = null

    @SerializedName("avail_credit")
    var avail_credit: String? = null
}

class Criditaudit : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("order_id")
    var order_id: Int? = null

    @SerializedName("order_customer_id")
    var order_customer_id: Int? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("balance")
    var balance: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("created_at")
    var created_at: String? = null
}