package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetReferralResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<ReferData>? = null

    @SerializedName("total")
    var total: String? = ""

}

class ReferData : Serializable {
    @SerializedName("id")
    var id: String? = ""

    @SerializedName("customer_id")
    var customer_id: String? = ""

    @SerializedName("shopper_id")
    var shopper_id: String? = ""

    @SerializedName("customer_type")
    var customer_type: String? = ""

    @SerializedName("referral_userid")
    var referral_userid: String? = ""

    @SerializedName("referral_code")
    var referral_code: String? = ""

    @SerializedName("android_device_id")
    var android_device_id: String? = ""

    @SerializedName("status")
    var status: String? = ""

    @SerializedName("draft")
    var draft: String? = ""

    @SerializedName("created_at")
    var created_at: String? = ""

    @SerializedName("updated_at")
    var updated_at: String? = ""

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("user")
    var user: User? = null

}