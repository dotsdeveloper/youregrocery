package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BranchlistResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var branch_data: ArrayList<Branches>? = null

}

class Branches : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("branch_id")
    var branch_id: Int? = null

    @SerializedName("logo")
    var logo: String? = null

    @SerializedName("main_shop")
    var mainshop: Mainshop? = null

    @SerializedName("branch_shop")
    var branchShop: Branchshop? = null

}

class Mainshop : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("location")
    var location: String? = null


}

class Branchshop : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("location")
    var location: String? = null
}