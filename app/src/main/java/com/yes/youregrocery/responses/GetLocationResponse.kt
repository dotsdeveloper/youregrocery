package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetLocationResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var addressdata: ArrayList<AddressData>? = null

}

class AddressData : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("location_name")
    var location_name: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("land_mark")
    var land_mark: String? = null

    @SerializedName("location_lat")
    var location_lat: String? = null

    @SerializedName("location_long")
    var location_long: String? = null

    @SerializedName("pincode")
    var pincode: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("status")
    var status: String? = null
}
