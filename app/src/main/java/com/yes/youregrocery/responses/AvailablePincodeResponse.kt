package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AvailablePincodeResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<PinData>? = null
}

class PinData : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}
