package com.yes.youregrocery.responses
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CategoryResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var category_data: ArrayList<CategoryList> = ArrayList()

}

class CategoryList : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_type")
    var customer_type: Int? = null

    @SerializedName("information")
    var information: String? = null

    @SerializedName("file")
    var file: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("offer")
    var offer: String? = null

    @SerializedName("subcategory_count")
    var subcategory_count: Int? = null

    @SerializedName("isthissubcat")
    var isthissubcat: Boolean? = false

    @SerializedName("isthiscat")
    var isthiscat: Boolean? = false

    @SerializedName("ordering")
    var ordering: Int? = null

   /* @SerializedName("product")
    var product_data: ArrayList<Products>? = ArrayList<Products>()*/

}
