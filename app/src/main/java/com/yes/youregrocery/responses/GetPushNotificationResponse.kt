package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetPushNotificationResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String = ""

    @SerializedName("data")
    var data: ArrayList<PushNotificationData>? = null

}

class PushNotificationData : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("notification_type")
    var notification_type: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("subtitle")
    var subtitle: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("link_type")
    var link_type: Int = 0

    @SerializedName("link_id")
    var link_id: Int = 0

    @SerializedName("category_id")
    var category_id: Int = 0

    @SerializedName("file")
    var file: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("created_at")
    var created_at: String? = null

    @SerializedName("updated_at")
    var updated_at: String? = null

}