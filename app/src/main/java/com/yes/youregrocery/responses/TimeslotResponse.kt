package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TimeslotResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<TimeData>? = null

}

class TimeData : Serializable {
    @SerializedName("day")
    var day: String? = null

    @SerializedName("timing")
    var timing: Array<String>? = null
}
