package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RegisterResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

   /* @SerializedName("status")
    var message1: Array<String>? = null*/

    @SerializedName("status")
    var message: String? = null

    @SerializedName("countrycode")
    var countrycode: String? = null

}

