package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class InvoiceResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: String? = ""

}