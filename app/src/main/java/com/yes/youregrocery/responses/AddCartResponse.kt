package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddCartResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var addcart_data: AddCartData? = null
}

class AddCartData : Serializable {
    @SerializedName("cart_id")
    var cart_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("query")
    var query: Query? = null

    @SerializedName("query1")
    var query1: ArrayList<Query1>? = null
}

class Query : Serializable {
    @SerializedName("customer_id")
    var customer_id: String? = null

    @SerializedName("cart_id")
    var cart_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("product_id")
    var product_id: String? = null

    @SerializedName("quantity_id")
    var quantity_id: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("total")
    var total: String? = null

    @SerializedName("save_value")
    var save_value: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

}

class Query1 : Serializable {
    @SerializedName("customer_id")
    var customer_id: String? = null

    @SerializedName("cart_id")
    var cart_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("product_id")
    var product_id: String? = null

    @SerializedName("quantity_id")
    var quantity_id: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("total")
    var total: String? = null

    @SerializedName("save_value")
    var save_value: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

}