package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListBannerResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var banner_data: ArrayList<Banners>? = null

}

class Banners : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("file")
    var file: String = ""

    @SerializedName("title")
    var title: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("category_id")
    var category_id: Int? = null


    @SerializedName("link_type")
    var link_type: Int? = null

    @SerializedName("link_id")
    var link_id: Int? = null

    @SerializedName("page_name")
    var page_name: String? = null

    @SerializedName("ordering")
    var ordering: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("original_file")
    var original_file: String? = null

}