package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetRewardResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<RewardData>? = null

}

class RewardData : Serializable {
    @SerializedName("total_avail")
    var total_avail: String? = ""

    @SerializedName("audit_trial")
    var audit_trial: ArrayList<Audittrial>? = null

}

class Audittrial: Serializable{
    @SerializedName("id")
    var id: String? = ""

    @SerializedName("user_id")
    var user_id: String? = ""

    @SerializedName("shopper_id")
    var shopper_id: String? = ""

    @SerializedName("order_id")
    var order_id: String? = ""

    @SerializedName("point")
    var point: String? = ""

    @SerializedName("type")
    var type: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("status")
    var status: String? = ""

    @SerializedName("draft")
    var draft: String? = ""

    @SerializedName("created_at")
    var created_at: String? = ""

    @SerializedName("updated_at")
    var updated_at: String? = ""
}