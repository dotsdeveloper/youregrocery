package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetOrdersResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<OrderData>? = null

}

class OrderData : Serializable {

    @SerializedName("shopper")
    var shopper: Shopper? = null

    @SerializedName("customer")
    var customer: User? = null

    @SerializedName("items")
    var items: ArrayList<Order>? = null

    @SerializedName("order_track")
    var order_track: ArrayList<OrderTrack>? = null

    @SerializedName("order_stage")
    var order_stage: ArrayList<OrderStage>? = null

    @SerializedName("items_new")
    var items_new: ItemsNew? = null

    @SerializedName("customerprofile")
    var customerprofile: CustomerProfile? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("invoice")
    var invoice: String? = null

    @SerializedName("delivery_id")
    var delivery_id: String? = null

    @SerializedName("customer_id")
    var customer_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("agent_id")
    var agent_id: String? = null

    @SerializedName("coupon_id")
    var coupon_id: String? = null

    @SerializedName("coupon_value")
    var coupon_value: String? = null

    @SerializedName("charge_amount")
    var charge_amount: String? = null

    @SerializedName("gst")
    var gst: String? = null

    @SerializedName("gst_value")
    var gst_value: String? = null

    @SerializedName("total")
    var total: String? = null

    @SerializedName("grand_total")
    var grand_total: String? = null

    @SerializedName("paid_amount")
    var paid_amount: String? = null

    @SerializedName("wallet_amount")
    var wallet_amount: String? = null

    @SerializedName("balance_amount")
    var balance_amount: String? = null

    @SerializedName("is_pay_cod")
    var is_pay_cod: String? = null

    @SerializedName("payment_id")
    var payment_id: String? = null

    @SerializedName("save_value")
    var save_value: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("order_type")
    var order_type: String? = null

    @SerializedName("delivery_type")
    var delivery_type: String? = null

    @SerializedName("is_customer")
    var is_customer: String? = null

    @SerializedName("file")
    var file: String? = null

    @SerializedName("non_image")
    var non_image: String? = null

    @SerializedName("delivered_person")
    var delivered_person: String? = null

    @SerializedName("signature")
    var signature: String? = null

    @SerializedName("comments")
    var comments: String? = null

    @SerializedName("delivered_lat")
    var delivered_lat: String? = null

    @SerializedName("delivered_long")
    var delivered_long: String? = null

    @SerializedName("delivered_at")
    var delivered_at: String? = ""

    @SerializedName("delivery_date")
    var delivery_date: String? = ""

    @SerializedName("delivery_slot")
    var delivery_slot: String? = null

    @SerializedName("courier_details")
    var courier_details: String? = null

    @SerializedName("sms_details")
    var sms_details: String? = null

    @SerializedName("reason")
    var reason: String? = null

    @SerializedName("manual")
    var manual: String? = null

    @SerializedName("time_taken_to_order")
    var time_taken_to_order: String? = null

    @SerializedName("is_master_order")
    var is_master_order: String? = null

    @SerializedName("is_web")
    var is_web: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("created_by")
    var created_by: String? = null

    @SerializedName("updated_by")
    var updated_by: String? = null

    @SerializedName("created_at")
    var created_at: String? = null

    @SerializedName("updated_at")
    var updated_at: String? = null


    @SerializedName("location")
    var location: Locationdata? = null

    @SerializedName("geotag")
    var geotag: Geotag? = null

    @SerializedName("coupon")
    var coupon: Coupon? = null

    @SerializedName("track")
    var track: String? = null

    @SerializedName("shopprofile")
    var shopprofile: ShopProfile? = null

}

class ShopProfile : Serializable {

}

class Coupon : Serializable {

}

class Locationdata : Serializable {

}


class Geotag : Serializable {

}

class Items : Serializable {

}

class OrderTrack : Serializable {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("order_id")
    var order_id: String? = null

    @SerializedName("reason")
    var reason: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("created_by")
    var created_by: String? = null
}

class OrderStage : Serializable {

}

class ItemsNew : Serializable {

}

class CustomerProfile : Serializable {

}


