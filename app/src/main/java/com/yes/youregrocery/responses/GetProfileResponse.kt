package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetProfileResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<Profiledata>? = null
}

class Profiledata : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("domain_id")
    var domain_id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("is_master")
    var is_master: String? = null

    @SerializedName("user_type")
    var user_type: String? = null

    @SerializedName("company_name")
    var company_name: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("dob")
    var dob: String? = null

    @SerializedName("is_gst")
    var is_gst: String? = null

    @SerializedName("gst_no")
    var gst_no: String? = null

    @SerializedName("gst_bill")
    var gst_bill: String? = null

    @SerializedName("gst_percent")
    var gst_percent: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("state")
    var state: String? = null

    @SerializedName("pincode")
    var pincode: String? = null

    @SerializedName("payment_mode")
    var payment_mode: String? = null

    @SerializedName("delivery_option")
    var delivery_option: String? = null

    @SerializedName("pay_option")
    var pay_option: String? = null

    @SerializedName("additional_sms")
    var additional_sms: String? = null

    @SerializedName("cancel_order")
    var cancel_order: String? = null

    @SerializedName("max_quantity")
    var max_quantity: String? = null

    @SerializedName("min_order")
    var min_order: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("commission")
    var commission: String? = null

    @SerializedName("agreement")
    var agreement: String? = null

    @SerializedName("photos")
    var photos: String? = null

    @SerializedName("proof")
    var proof: String? = null

    @SerializedName("no_of_executive")
    var no_of_executive: String? = null

    @SerializedName("payment_qr_code")
    var payment_qr_code: String? = null

    @SerializedName("payment_qr_text")
    var payment_qr_text: String? = null

    @SerializedName("disclaimer")
    var disclaimer: String? = null

    @SerializedName("currency")
    var currency: String? = null

    @SerializedName("web_url")
    var web_url: String? = null

    @SerializedName("app_url")
    var app_url: String? = null

    @SerializedName("package_name")
    var package_name: String? = null

    @SerializedName("stock_reduce")
    var stock_reduce: String? = null

    @SerializedName("app_preview")
    var app_preview: String? = null

    @SerializedName("middle_banner")
    var middle_banner: String? = null

    @SerializedName("copyrights")
    var copyrights: String? = null

    @SerializedName("razorpay_api")
    var razorpay_api: String? = null

    @SerializedName("razor_secret")
    var razor_secret: String? = null

    @SerializedName("razorpay_name")
    var razorpay_name: String? = null

    @SerializedName("stock_option")
    var stock_option: String? = null

    @SerializedName("referral_percentage")
    var referral_percentage: String? = null

    @SerializedName("referral_minorder")
    var referral_minorder: String? = null

    @SerializedName("reward_amount")
    var reward_amount: String? = null

    @SerializedName("reward_point")
    var reward_point: String? = null

    @SerializedName("delivery_cutofftime")
    var delivery_cutofftime: String? = null

    @SerializedName("prefix")
    var prefix: String? = null

    @SerializedName("delivery_fees_type")
    var delivery_fees_type: String? = null

    @SerializedName("gateway_charge")
    var gateway_charge: String? = null

    @SerializedName("gateway_charge_type")
    var gateway_charge_type: String? = null

    @SerializedName("gateway_charge_value")
    var gateway_charge_value: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("user")
    var user: User? = null
}

class User : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("parent_id")
    var parent_id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("channel_partner_id")
    var channel_partner_id: String? = null

    @SerializedName("channel_partner_lead_id")
    var channel_partner_lead_id: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("email_verified_at")
    var email_verified_at: String? = null

    @SerializedName("user_type")
    var user_type: String? = null

    @SerializedName("role_id")
    var role_id: String? = null

    @SerializedName("customer_type")
    var customer_type: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("domain")
    var domain: String? = null

    @SerializedName("language")
    var language: String? = null

    @SerializedName("country_code")
    var country_code: String? = null

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("is_first")
    var is_first: String? = null

    @SerializedName("is_verified")
    var is_verified: String? = null

    @SerializedName("is_on")
    var is_on: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("is_master")
    var is_master: String? = null

}