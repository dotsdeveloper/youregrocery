package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName

class PlaceOrderResponse {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("order_id")
    var order_id: String? = null

    @SerializedName("paymentid")
    var paymentid: String? = null

}