package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommonResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: Data? = null
}

class Data : Serializable {

}