package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductlistResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var product_data: ArrayList<Products>? = ArrayList()

    @SerializedName("count")
    var pcount: Int? = null
}

class Products : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("category_id")
    var category_id: Int? = null

    @SerializedName("sub_category_id")
    var sub_category_id: Int? = null

    @SerializedName("unit_id")
    var unit_id: Int? = null

    @SerializedName("file")
    var file: String? = null

    @SerializedName("youtube_url")
    var youtube_url: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("brand_id")
    var brand_id: Int?= null

    @SerializedName("brand")
    var brand: Brand = Brand()

    @SerializedName("description")
    var description: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("offer_price")
    var offer_price: String? = null

    @SerializedName("home_display")
    var home_display: String? = null

    @SerializedName("enquiry")
    var enquiry: Int? = null

    @SerializedName("stock_available")
    var stock_available: Int? = null

    @SerializedName("display")
    var display: Int? = null

    @SerializedName("hotsale")
    var hotsale: String? = null

    @SerializedName("available_from_time")
    var available_from_time: String? = null

    @SerializedName("available_to_time")
    var available_to_time: String? = null

    @SerializedName("is_recommend")
    var is_recommend: String? = null

    @SerializedName("low_stock")
    var low_stock: String? = null

    @SerializedName("ordering")
    var ordering: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("original_files")
    var original_files: Array<String>? = null

    @SerializedName("savedpercentage")
    var savedpercentage: String? = null

    @SerializedName("files")
    var files: Array<String>? = null

    @SerializedName("sqtyid")
    var sqtyid: Int? = 0

    @SerializedName("sqtyindex")
    var sqtyindex: Int? = 0

    @SerializedName("iscart")
    var iscart: Boolean? = false

    @SerializedName("cartid")
    var cartid: Int? = null

    @SerializedName("qty")
    var qty: Int? = 0

    @SerializedName("quantities")
    var quantities: ArrayList<Quantities>? = ArrayList()

    @SerializedName("category")
    var category: Category? = Category()
}

class Quantities : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("unit_id")
    var unit_id: Int? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("offer_price")
    var offer_price: String? = null

    @SerializedName("stock")
    var stock: String? = null

    @SerializedName("minimum_stock")
    var minimum_stock: String? = null

    @SerializedName("is_loose")
    var is_loose: String? = null

    @SerializedName("is_visible")
    var is_visible: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("unit_name")
    var unit_name: String? = null

    @SerializedName("savedpercentage")
    var savedpercentage: String? = "0"

    @SerializedName("product_unit")
    var product_unit: Productunit? = null

    @SerializedName("iscart")
    var iscart: Boolean? = false

    @SerializedName("cartid")
    var cartid: Int? = null

    @SerializedName("qty")
    var qty: Int? = null
}

class Productunit : Serializable{
    @SerializedName("id")
    var id: String? = null

    @SerializedName("name")
    var name: String? = null
}

class Category : Serializable{
    @SerializedName("name")
    var name: String = ""

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("file")
    var file: String? = ""

    @SerializedName("ccount")
    var ccount: Int? = null
}

class Brand : Serializable{
    @SerializedName("name")
    var name: String = ""

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("file")
    var file: String? = ""

}