package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetNotificationResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<NotificationData>? = null

    @SerializedName("count")
    var count: Int = 0

}

class NotificationData : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("user_id")
    var user_id: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("order_id")
    var order_id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("subtitle")
    var subtitle: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("send_time")
    var send_time: String? = null

    @SerializedName("created_at")
    var created_at: String? = null

    @SerializedName("updated_at")
    var updated_at: String? = null

    @SerializedName("order")
    var order: OrderData? = null

}