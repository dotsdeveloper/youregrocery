package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class IspincodeResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: Ispindata? = null
}

class Ispindata : Serializable {
    @SerializedName("order_amount")
    var order_amount: String? = null

    @SerializedName("gst")
    var gst: String? = null

    @SerializedName("gst_amount")
    var gst_amount: String? = null

    @SerializedName("delivery_charge")
    var delivery_charge: String? = null

    @SerializedName("grand_total")
    var grand_total: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null
}
