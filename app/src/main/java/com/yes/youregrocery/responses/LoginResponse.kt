package com.yes.youregrocery.responses
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var login_data: LoginData? = null

    @SerializedName("token")
    var token: String? = null

    @SerializedName("referral")
    var referral: Array<Array<Referrals>>? = null

}

class LoginData {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("country_code")
    var country_code: String? = null

    @SerializedName("otp")
    var otp: String? = null

    @SerializedName("login_otp")
    var login_otp: String? = null

    @SerializedName("user_type")
    var user_type: Int? = 0
}

class Referrals : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("customer_type")
    var customer_type: Int? = null

    @SerializedName("referral_userid")
    var referral_userid: Int? = null

    @SerializedName("referral_code")
    var referral_code: String? = null

}
