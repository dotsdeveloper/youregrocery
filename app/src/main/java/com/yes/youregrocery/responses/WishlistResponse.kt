package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WishlistResponse: Serializable {
    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var wishlistdata: ArrayList<Wishlist>? = null
}

class Wishlist: Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("shop_id")
    var shop_id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("user")
    var user: User? = null

    @SerializedName("shopper")
    var shopper: Shopper? = null

    @SerializedName("product")
    var product: Products? = null

}