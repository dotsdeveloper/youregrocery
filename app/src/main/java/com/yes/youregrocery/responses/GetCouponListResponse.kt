package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetCouponListResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ListCouponData>? = null

}

class ListCouponData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("category_id")
    var category_id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("value")
    var value: String? = null

    @SerializedName("min_order_value")
    var min_order_value: String? = null

    @SerializedName("type")
    var type: Int? = null

    @SerializedName("max_discount")
    var max_discount: String? = null

    @SerializedName("expires_on")
    var expires_on: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("display")
    var display: String? = null

    @SerializedName("image")
    var image: String? = null

}