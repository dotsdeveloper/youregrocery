package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import java.io.Serializable

class GetCartResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var getcart_data: GetCartData? = null

}

class GetCartData : Serializable {

    @SerializedName("order_new")
    var ordernew: JSONObject? = JSONObject()

    @SerializedName("order")
    var order: ArrayList<Order>? = null

    @SerializedName("order_amount")
    var order_amount: String? = null

    @SerializedName("gst")
    var gst: String? = null

    @SerializedName("gst_amount")
    var gst_amount: String? = null

    @SerializedName("delivery_charge")
    var delivery_charge: String? = null

    @SerializedName("grand_total")
    var grand_total: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

}

class Order : Serializable {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("cart_id")
    var cart_id: String? = null

    @SerializedName("customer_id")
    var customer_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("quantity_id")
    var quantity_id: Int? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("total")
    var total: String? = null

    @SerializedName("save_value")
    var save_value: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

    @SerializedName("pincode")
    var pincode: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("is_save")
    var is_save: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("product")
    var product: Products? = null

    @SerializedName("quantities")
    var quantities: Quantities? = null

    @SerializedName("shopper")
    var shopper: Shopper? = null

}