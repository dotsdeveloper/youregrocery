package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import java.io.Serializable

class ProductAddonResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: Addondata? = null

    var datanew: JSONObject = JSONObject()
    var product_how_to_use: Htu = Htu()
}

class Addondata : Serializable {
    @SerializedName("product_faq")
    var product_faq: ArrayList<Faq>? = null

    @SerializedName("product_ingredient")
    var product_ingredient: ArrayList<Ingredient>? = null

    @SerializedName("product_why_buy")
    var product_why_buy: ArrayList<Pwb>? = null
}

class Faq : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("question")
    var question: String? = ""

    @SerializedName("answer")
    var answer: String? = ""
}

class Htu {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("file")
    var file: String? = null

    @SerializedName("description")
    var description: String? = ""
}

class Ingredient : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("heading")
    var heading: String? = ""

    @SerializedName("description")
    var description: String? = ""
}

class Pwb : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("heading")
    var heading: String? = ""

    @SerializedName("description")
    var description: String? = ""

    @SerializedName("file")
    var file: String? = null
}

