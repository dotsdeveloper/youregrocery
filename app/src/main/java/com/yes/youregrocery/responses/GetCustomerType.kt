package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetCustomerType {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: Customer = Customer()
}

class Customer : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("shopper_id")
    var shopper_id: Int? = null

    @SerializedName("customer_type")
    var customer_type: Int? = null

    @SerializedName("referral_userid")
    var referral_userid: Int? = null

    @SerializedName("referral_code")
    var referral_code: String? = null

    @SerializedName("android_device_id")
    var android_device_id: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("draft")
    var draft: Int? = null

    @SerializedName("created_at")
    var created_at: String? = null

    @SerializedName("updated_at")
    var updated_at: String? = null
}