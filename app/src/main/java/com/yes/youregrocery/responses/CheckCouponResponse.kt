package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CheckCouponResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var data: CouponData? = null

}

class CouponData : Serializable {

    @SerializedName("couppen_id")
    var couppen_id: Int? = null

    @SerializedName("order_amount")
    var order_amount: String? = null

    @SerializedName("gst")
    var gst: String? = null

    @SerializedName("gst_amount")
    var gst_amount: String? = null

    @SerializedName("delivery_charge")
    var delivery_charge: String? = null

    @SerializedName("coupon_amount")
    var coupon_amount: String? = null

    @SerializedName("grand_total")
    var grand_total: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

}