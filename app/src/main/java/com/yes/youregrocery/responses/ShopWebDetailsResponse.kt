package com.yes.youregrocery.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ShopWebDetailsResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var shopWebData: ShopWebData = ShopWebData()

}

class ShopWebData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("color_primary")
    var color_primary: String? = null

    @SerializedName("color_secondary")
    var color_secondary: String? = null

    @SerializedName("logo")
    var applogo: String? = null

    @SerializedName("banner")
    var banner: String? = null

    @SerializedName("call_us")
    var call_us: String? = null

    @SerializedName("facebook")
    var facebook: String? = null

    @SerializedName("instagram")
    var instagram: String? = null

    @SerializedName("twitter")
    var twitter: String? = null

    @SerializedName("youtube")
    var youtube: String? = null

    @SerializedName("about_content")
    var about_content: String? = null

    @SerializedName("contact_content")
    var contact_content: String? = null

    @SerializedName("help")
    var help: String? = null

    @SerializedName("terms_condition")
    var terms_condition: String? = null

    @SerializedName("privacy_policy")
    var privacy_policy: String? = null

    @SerializedName("refund_policy")
    var refund_policy: String? = null

    @SerializedName("store_information")
    var store_information: String? = null

    @SerializedName("shipping_policy")
    var shipping_policy: String? = null

    @SerializedName("playstore_url")
    var playstore_url: String? = null

    @SerializedName("afteraddcart")
    var afteraddcart: String? = null

    @SerializedName("ordersuccess")
    var ordersuccess: String? = null

    @SerializedName("preferred_delivery_message")
    var preferred_delivery_message: String? = null

    @SerializedName("outofstock_message")
    var outofstock_message: String? = null

    @SerializedName("no_productfound_message")
    var no_productfound_message: String? = null

    @SerializedName("shop_close_message")
    var shop_close_message: String? = null

    @SerializedName("min_order_message")
    var min_order_message: String? = null

    @SerializedName("minimumorderleft")
    var minimumorderleft: String? = null

    @SerializedName("category_view")
    var category_view: Int? = null

    @SerializedName("bestseller_heading")
    var bestseller_heading: String? = null

    @SerializedName("category_heading")
    var category_heading: String? = null

    @SerializedName("brand_heading")
    var brand_heading: String? = null

    @SerializedName("hotdeal_heading")
    var hotdeal_heading: String? = null

    @SerializedName("newarrival_heading")
    var newarrival_heading: String? = null

    @SerializedName("product_heading")
    var product_heading: String? = null

    @SerializedName("newarrival_count")
    var newarrival_count: Int? = null

    @SerializedName("bestselling_count")
    var bestselling_count: Int? = null

    @SerializedName("offer_left")
    var offer_left: String? = null

    @SerializedName("offer_middle")
    var offer_middle: String? = null

    @SerializedName("offer_right")
    var offer_right: String? = null

    @SerializedName("is_offer_left")
    var is_offer_left: Int? = null

    @SerializedName("is_offer_middle")
    var is_offer_middle: Int? = null

    @SerializedName("is_offer_right")
    var is_offer_right: Int? = null

    @SerializedName("whatsapp_code")
    var whatsapp_code: String? = null

    @SerializedName("loader_common")
    var loader_common: String? = null

    @SerializedName("loader_cart")
    var loader_cart: String? = null

    @SerializedName("share_content")
    var share_content: String? = null

    @SerializedName("pincode_error_message")
    var pincode_error_message: String? = null

    @SerializedName("payment_added_message")
    var payment_added_message: String? = null

    @SerializedName("nonlisted_text")
    var nonlisted_text: String? = null

    @SerializedName("show_branch")
    var show_branch: Int? = null

    @SerializedName("is_welcome")
    var is_welcome: Int? = null

    @SerializedName("is_brand")
    var is_brand: Int? = null

    @SerializedName("is_product")
    var is_product: Int? = null

    @SerializedName("bestseller")
    var bestseller: Int? = null

    @SerializedName("category")
    var category: Int? = null

    @SerializedName("newarrival")
    var newarrival: Int? = null

    @SerializedName("hotsale")
    var hotsale: Int? = null

    @SerializedName("shopper")
    var shopper: Shopper = Shopper()

    @SerializedName("profile")
    var profile: Profile = Profile()
}

class Shopper : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("parent_id")
    var parent_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("channel_partner_id")
    var channel_partner_id: String? = null

    @SerializedName("channel_partner_lead_id")
    var channel_partner_lead_id: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("email_verified_at")
    var email_verified_at: String? = null

    @SerializedName("user_type")
    var user_type: String? = null

    @SerializedName("role_id")
    var role_id: String? = null

    @SerializedName("customer_type")
    var customer_type: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("domain")
    var domain: String? = null

    @SerializedName("language")
    var language: String? = null

    @SerializedName("country_code")
    var country_code: String? = null

    @SerializedName("otp")
    var otp: String? = null

    @SerializedName("login_otp")
    var login_otp: String? = null

    @SerializedName("otp_valid")
    var otp_valid: String? = null

    @SerializedName("is_verified")
    var is_verified: String? = null

    @SerializedName("is_first")
    var is_first: String? = null

    @SerializedName("qr_code")
    var qr_code: String? = null
}

class Profile : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("domain_id")
    var domain_id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("is_master")
    var is_master: String? = null

    @SerializedName("user_type")
    var user_type: String? = null

    @SerializedName("company_name")
    var company_name: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("dob")
    var dob: String? = null

    @SerializedName("is_gst")
    var is_gst: String? = null

    @SerializedName("gst_no")
    var gst_no: String? = null

    @SerializedName("gst_bill")
    var gst_bill: String? = null

    @SerializedName("gst_percent")
    var gst_percent: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("state")
    var state: String? = null

    @SerializedName("pincode")
    var pincode: String? = null

    @SerializedName("payment_mode")
    var payment_mode: String? = null

    @SerializedName("delivery_option")
    var delivery_option: Int? = null

    @SerializedName("pay_option")
    var pay_option: String? = null

    @SerializedName("additional_sms")
    var additional_sms: String? = null

    @SerializedName("cancel_order")
    var cancel_order: String? = null

    @SerializedName("max_quantity")
    var max_quantity: Int? = null

    @SerializedName("min_order")
    var min_order: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("commission")
    var commission: String? = null

    @SerializedName("agreement")
    var agreement: String? = null

    @SerializedName("photos")
    var photos: String? = null

    @SerializedName("proof")
    var proof: String? = null

    @SerializedName("no_of_executive")
    var no_of_executive: String? = null

    @SerializedName("payment_qr_code")
    var payment_qr_code: String? = null

    @SerializedName("payment_qr_text")
    var payment_qr_text: String? = null

    @SerializedName("disclaimer")
    var disclaimer: String? = null

    @SerializedName("currency")
    var currency: String? = null

    @SerializedName("web_url")
    var web_url: String? = null

    @SerializedName("app_url")
    var app_url: String? = null

    @SerializedName("package_name")
    var package_name: String? = null

    @SerializedName("stock_reduce")
    var stock_reduce: String? = null

    @SerializedName("app_preview")
    var app_preview: String? = null

    @SerializedName("middle_banner")
    var middle_banner: String? = null

    @SerializedName("copyrights")
    var copyrights: String? = null

    @SerializedName("razorpay_api")
    var razorpay_api: String? = ""

    @SerializedName("razor_secret")
    var razor_secret: String? = ""

    @SerializedName("razorpay_name")
    var razorpay_name: String? = ""

    @SerializedName("stock_option")
    var stock_option: Int? = null

    @SerializedName("referral_percentage")
    var referral_percentage: String? = null

    @SerializedName("referral_minorder")
    var referral_minorder: String? = null

    @SerializedName("reward_amount")
    var reward_amount: String? = null

    @SerializedName("reward_point")
    var reward_point: String? = null

    @SerializedName("delivery_cutofftime")
    var delivery_cutofftime: String? = null

    @SerializedName("prefix")
    var prefix: String? = null

    @SerializedName("delivery_fees_type")
    var delivery_fees_type: String? = null

    @SerializedName("gateway_charge")
    var gateway_charge: String? = null

    @SerializedName("gateway_charge_type")
    var gateway_charge_type: String? = null

    @SerializedName("gateway_charge_value")
    var gateway_charge_value: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

}