package com.yes.youregrocery.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.activity.MyWalletActivity
import com.yes.youregrocery.adapter.*
import com.yes.youregrocery.databinding.FragmentHomeBinding
import com.yes.youregrocery.network.ApiInput
import com.yes.youregrocery.network.UrlHelper
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.GpsUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.smarteist.autoimageslider.SliderView
import com.yes.youregrocery.responses.*
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.utils.BaseUtils
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    private var myLng = 0.0
    private var myLat = 0.0
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    var viewModel: ViewModel? = null
    lateinit var sharedHelper: SharedHelper
    lateinit var dashBoardActivity: DashBoardActivity
    private var categorydata: ArrayList<CategoryList> = ArrayList()
    private var branddata: ArrayList<CategoryList> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        listener()
        load()
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    fun listener(){
        binding.viewPopularcategory.setOnClickListener {
            val args = Bundle()
            args.putString(Constants.IntentKeys.TITLE,binding.popularcategoryTxt.text.toString())
            args.putBoolean(Constants.IntentKeys.IS_CAT, true)
            args.putSerializable(Constants.IntentKeys.DATA, categorydata)
            dashBoardActivity.addFragment(CategoryListFragment(), args, -1, -1)
        }
        binding.viewBrand.setOnClickListener {
            val args = Bundle()
            args.putString(Constants.IntentKeys.TITLE,binding.brandTxt.text.toString())
            args.putBoolean(Constants.IntentKeys.IS_CAT, false)
            args.putSerializable(Constants.IntentKeys.DATA, branddata)
            dashBoardActivity.addFragment(CategoryListFragment(), args, -1, -1)
        }
        binding.viewProduct.setOnClickListener {
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,binding.productsTxt.text.toString())
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,Constants.IntentKeys.PRODUCT)
            args.putInt(Constants.IntentKeys.CID,0)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        binding.viewNewarrivals.setOnClickListener {
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,binding.newarrivalsTxt.text.toString())
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,Constants.IntentKeys.NEW_ARRIVALS)
            args.putInt(Constants.IntentKeys.CID,0)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        binding.viewBestseller.setOnClickListener {
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,binding.bestselllerTxt.text.toString())
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,Constants.IntentKeys.BESTSELLER)
            args.putInt(Constants.IntentKeys.CID,0)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        binding.header.wallet.walletframe.setOnClickListener {
            startActivity(Intent(requireContext(), MyWalletActivity::class.java))
        }
        binding.header.notification.notificationframe.setOnClickListener {
            dashBoardActivity.addFragment(NotificationFragment(), null, -1, -1)
        }
        binding.scroll.setOnScrollChangeListener { _, _, scrollY, _, oldScrollY ->
            if (scrollY <= oldScrollY) {
                dashBoardActivity.scrollDown()
            }
            else {
                dashBoardActivity.scrollUp()
            }
        }
        binding.brandRecycler.setOnTouchListener { _, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                dashBoardActivity.scrollUp()
            }
            else if(MotionEvent.ACTION_DOWN == event.action){
                dashBoardActivity.scrollUp()
            }
            false
        }
        binding.swiperefresh.setOnRefreshListener {
            load()
            binding.swiperefresh.isRefreshing = false
        }
    }

    fun load(){
        locationListener()
        askLocationPermission()
        UiUtils.relativeLayoutBgTint(
            binding.categoryLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.linearLayoutBgTint(
            binding.productsLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.linearLayoutBgTint(
            binding.hotsaleLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.linearLayoutBgTint(
            binding.bestsellerLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.linearLayoutBgTint(
            binding.newarrivalLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.linearLayoutBgTint(
            binding.brandLinear,
            sharedHelper.primaryColor,
            null
        )
        UiUtils.relativeLayoutBgColor(
            binding.header.linear,
            sharedHelper.primaryColor,
            null
        )
        dashBoardActivity.topContentLoad(binding.header)
        dashBoardActivity.searchTopLoad(binding.header.searchHome)
        dashBoardActivity.updateBadges(binding.header)
        loadDetails(sharedHelper.shopWebData)
    }
    private fun loadDetails(loadData: ShopWebData) {
        binding.header.name.text = loadData.name
        if(BaseUtils.nullCheckerStr(loadData.category_heading).isNotEmpty()){
            binding.popularcategoryTxt.text = loadData.category_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.newarrival_heading).isNotEmpty()){
            binding.newarrivalsTxt.text = loadData.newarrival_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.bestseller_heading).isNotEmpty()){
            binding.bestselllerTxt.text = loadData.bestseller_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.product_heading).isNotEmpty()){
            binding.productsTxt.text = loadData.product_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.hotdeal_heading).isNotEmpty()){
            binding.hotsaleTxt.text = loadData.hotdeal_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.brand_heading).isNotEmpty()){
            binding.brandTxt.text = loadData.brand_heading
        }
        if(BaseUtils.nullCheckerStr(loadData.hotdeal_heading).isNotEmpty()){
            binding.hotsaleTxt.text = loadData.hotdeal_heading
        }

        loadBanner()
        if(BaseUtils.nullCheckerInt(loadData.is_offer_left) == 0 || BaseUtils.nullCheckerInt(loadData.is_offer_middle) == 0 || BaseUtils.nullCheckerInt(loadData.is_offer_right) == 0){
            binding.offerLinear.visibility = View.GONE
        }
        else {
            binding.offerLinear.visibility = View.VISIBLE
            if(loadData.offer_left != null) {
                UiUtils.loadImageWithCenterCrop(binding.offer1, loadData.offer_left!!)
            }
            if(loadData.offer_middle != null){
                UiUtils.loadImageWithCenterCrop(binding.offer2, loadData.offer_middle!!)
            }
            if(loadData.offer_right != null){
                UiUtils.loadImageWithCenterCrop(binding.offer3, loadData.offer_right!!)
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.is_brand) == 0){
            binding.brandLinear.visibility = View.GONE
        }
        else{
            //DialogUtils.showLoader(requireContext())
            viewModel?.getBrandList(requireContext())?.observe(viewLifecycleOwner) {
                // DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            binding.brandLinear.visibility = View.GONE
                            // UiUtils.showSnack(binding.root1, it.message!!)
                        } else {
                            it.category_data.let { data ->
                                if (data.size == 0) {
                                    binding.brandLinear.visibility = View.GONE
                                } else {
                                    branddata = data
                                    binding.brandLinear.visibility = View.VISIBLE
                                    binding.brandRecycler.layoutManager = GridLayoutManager(
                                        requireContext(),
                                        3,
                                        LinearLayoutManager.VERTICAL,
                                        false)
                                    binding.brandRecycler.adapter = CategoryListAdapter(
                                        dashBoardActivity,
                                        5,
                                        this,
                                        null,
                                        requireContext(),
                                        data
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.is_product) == 0){
            binding.productsLinear.visibility = View.GONE
        }
        else{
            //DialogUtils.showLoader(requireContext())
            viewModel?.getHomeProductList(requireContext())?.observe(viewLifecycleOwner) {
                //DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root1, it.message!!)
                            binding.productsLinear.visibility = View.GONE
                        } else {
                            it.product_data?.let { data ->
                                if (data.size == 0) {
                                    binding.productsLinear.visibility = View.GONE
                                } else {
                                    binding.productsLinear.visibility = View.VISIBLE
                                    loadProductsRecycler(data,
                                        isbestseller = false,
                                        isnewarrival = false,
                                        isproduct = true
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.hotsale) == 0){
            binding.hotsaleLinear.visibility = View.GONE
        }
        else{
            //DialogUtils.showLoader(requireContext())
            viewModel?.getHotsaleProductList(requireContext())?.observe(viewLifecycleOwner) {
                //DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root1, it.message!!)
                            binding.hotsaleLinear.visibility = View.GONE
                        } else {
                            it.product_data?.let { data ->
                                if (data.size == 0) {
                                    binding.hotsaleLinear.visibility = View.GONE
                                } else {
                                    binding.hotsaleLinear.visibility = View.VISIBLE
                                    val productListAdapter = ProductListAdapter(
                                        dashBoardActivity,
                                        0,
                                        this,
                                        null,null,
                                        requireContext(),
                                        data,false
                                    )
                                    binding.hotsale.layoutManager = GridLayoutManager(
                                        requireContext(),
                                        2,
                                        LinearLayoutManager.VERTICAL,
                                        false)
                                    binding.hotsale.adapter = productListAdapter
                                }
                            }
                        }
                    }
                }
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.newarrival) == 0){
            binding.newarrivalLinear.visibility = View.GONE
        }
        else{
            //DialogUtils.showLoader(requireContext())
            viewModel?.getNewArrivalsProducts(requireContext())?.observe(viewLifecycleOwner) {
                // DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root1, it.message!!)
                            binding.newarrivalLinear.visibility = View.GONE
                        } else {
                            it.product_data?.let { data ->
                                if (data.size == 0) {
                                    binding.newarrivalLinear.visibility = View.GONE
                                } else {
                                    binding.newarrivalLinear.visibility = View.VISIBLE
                                    loadProductsRecycler(data,
                                        isbestseller = false,
                                        isnewarrival = true,
                                        isproduct = false
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.bestseller) == 0){
            binding.bestsellerLinear.visibility = View.GONE
        }
        else{
            // DialogUtils.showLoader(requireContext())
            viewModel?.getBestSellerProducts(requireContext(),0)?.observe(viewLifecycleOwner) {
                // DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root1, it.message!!)
                            binding.bestsellerLinear.visibility = View.GONE
                        } else {
                            it.product_data?.let { data ->
                                if (data.size == 0) {
                                    binding.bestsellerLinear.visibility = View.GONE
                                } else {
                                    binding.bestsellerLinear.visibility = View.VISIBLE
                                    loadProductsRecycler(data,
                                        isbestseller = true,
                                        isnewarrival = false,
                                        isproduct = false
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        if(BaseUtils.nullCheckerInt(loadData.category) == 0){
            binding.categoryLinear.visibility = View.GONE
            dashBoardActivity.shimmerViewClose()
        }
        else{
            viewModel?.getCategoryList(requireContext())?.observe(viewLifecycleOwner) {
                binding.shimmerViewContainer.startShimmer()
                binding.shimmerViewContainer.visibility = View.VISIBLE
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            dashBoardActivity.shimmerViewClose()
                            UiUtils.showSnack(binding.root1, it.message!!)
                            binding.categoryLinear.visibility = View.GONE
                        } else {
                            it.category_data.let { data ->
                                if (data.size == 0) {
                                    binding.categoryLinear.visibility = View.GONE
                                    dashBoardActivity.shimmerViewClose()
                                } else {
                                    binding.categoryLinear.visibility = View.VISIBLE
                                    binding.shimmerViewContainer.stopShimmer()
                                    binding.shimmerViewContainer.visibility = View.GONE
                                    binding.popular.visibility = View.VISIBLE
                                    dashBoardActivity.shimmerViewClose()
                                    if(sharedHelper.customerType == 1){
                                        for(items in data){
                                            if(BaseUtils.nullCheckerInt(items.customer_type) == 1){
                                                categorydata.add(items)
                                            }
                                        }
                                    }
                                    else{
                                        for(items in data){
                                            if(BaseUtils.nullCheckerInt(items.customer_type) != 1){
                                                categorydata.add(items)
                                            }
                                        }
                                    }
                                    if (loadData.category_view == 0) {
                                        categorydata.sortedBy { it1 ->
                                            it1.ordering
                                        }
                                        binding.popular.layoutManager =
                                            LinearLayoutManager(requireContext(),
                                                LinearLayoutManager.HORIZONTAL,
                                                false)
                                        binding.popular.adapter = CategoryListAdapter(
                                            dashBoardActivity,
                                            1,
                                            this,
                                            null,
                                            requireContext(),
                                            categorydata
                                        )
                                    }
                                    else {
                                        categorydata.sortedBy { it1 ->
                                            it1.ordering
                                        }
                                        binding.popular.layoutManager = GridLayoutManager(
                                            requireContext(),
                                            3,
                                            LinearLayoutManager.VERTICAL,
                                            false)
                                        binding.popular.adapter = CategoryListAdapter(
                                            dashBoardActivity,
                                            2,
                                            this,
                                            null,
                                            requireContext(),
                                            categorydata
                                        )
                                    }

                                    if(categorydata.size == 0){
                                        binding.categoryLinear.visibility = View.GONE
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private fun loadProductsRecycler(data: ArrayList<Products>, isbestseller: Boolean, isnewarrival: Boolean, isproduct: Boolean) {
        val productListAdapter = ProductListAdapter(
            dashBoardActivity,
            1,
            this,
            null,null,
            requireContext(),
            data,false
        )

        when {
            isbestseller -> {
                binding.bestseller.layoutManager = LinearLayoutManager(requireContext(),
                    LinearLayoutManager.HORIZONTAL,
                    false)
                binding.bestseller.adapter = productListAdapter
            }
            isnewarrival -> {
                binding.newarrivals.layoutManager = GridLayoutManager(requireContext(),1,LinearLayoutManager.HORIZONTAL,false)
                binding.newarrivals.adapter = productListAdapter
            }
            isproduct -> {
                binding.products.layoutManager = GridLayoutManager(requireContext(),1,LinearLayoutManager.HORIZONTAL,false)
                binding.products.adapter = productListAdapter
            }
        }
    }
    fun loadBanner() {
        val bannerlist2 = ArrayList<Banners>()
        for(item in sharedHelper.banners){
            if(item.title.equals(Constants.ApiKeys.SMALL_HOME_BANNER)){
                bannerlist2.add(item)
            }
        }
        if(bannerlist2.size > 0){
            binding.banners2.visibility = View.VISIBLE
            binding.banners2.layoutManager = LinearLayoutManager(requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false)
            binding.banners2.adapter = HomeSmallBannerAdaper(
                this,
                requireContext(),
                bannerlist2
            )
        }
        else{
            binding.banners2.visibility = View.GONE
        }

        val bannerlist1 = ArrayList<Banners>()
        for(item in sharedHelper.banners){
            if(item.title.equals(Constants.IntentKeys.HOME,true)){
                bannerlist1.add(item)
            }
        }
        if(bannerlist1.size > 0){
            binding.slider.visibility = View.VISIBLE
            val adapter = HomeMainBannerAdapter(this,requireContext(), bannerlist1)

            // below method is used to
            // setadapter to sliderview.
            binding.slider.setSliderAdapter(adapter)

            // below method is used to set auto cycle direction in left to
            // right direction you can change according to requirement.
            binding.slider.autoCycleDirection = SliderView.LAYOUT_DIRECTION_LTR

            // below method is use to set
            // scroll time in seconds.
            binding.slider.scrollTimeInSec = 5

            // to set it scrollable automatically
            // we use below method.
            binding.slider.isAutoCycle = true

            binding.slider.indicatorSelectedColor = Color.parseColor(sharedHelper.primaryColor)

            // to start autocycle below method is used.
            binding.slider.startAutoCycle()
        }
        else{
            binding.slider.visibility = View.GONE
        }
    }

    private fun askLocationPermission() {
        if (!BaseUtils.isPermissionsEnabled(requireContext(),Constants.IntentKeys.LOCATION)) {
           // BaseUtils.permissionsEnableRequest(this,"location")
            return
        }
        else {
            getUserLocation()
        }
    }
    fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        if (BaseUtils.isPermissionsEnabled(requireContext(),Constants.IntentKeys.LOCATION)) {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    myLat = location.latitude
                    myLng = location.longitude
                    searchPlaces()
                }
                else{
                    binding.addr1.text = sharedHelper.location1
                    binding.addr2.text = sharedHelper.location2
                    storeLocation()
                }
            }
        }
    }
    private fun searchPlaces() {
        val url = UrlHelper(requireContext()).getAddress(myLat, myLng)
        val inputForAPI = ApiInput()
        inputForAPI.context = requireContext()
        inputForAPI.url = url
        // DialogUtils.showLoader(requireContext())
        viewModel?.getAddress(inputForAPI)
            ?.observe(this) {
                //  DialogUtils.dismissLoader()
                it.let { responses ->
                    responses.error?.let { errorValue ->
                        if (!errorValue) {
                            responses.predictions?.let { value ->
                                getAddressFromResponse(value)
                            }
                        }
                    }

                }

            }
    }
    private fun getAddressFromResponse(value: List<Prediction>) {
        if (value.isNotEmpty()) {
            value[0].addressComponents?.let {
                // Log.d("tyfyg",""+it)
                when {
                    it.size >= 10 -> {
                        binding.addr1.visibility = View.VISIBLE
                        binding.addr2.visibility = View.VISIBLE
                        val text = "${it[4].long_name}${","}${it[5].long_name} ${"-"} ${it[9].long_name}"
                        binding.addr1.text = text
                        binding.addr2.text = it[1].long_name
                    }
                    it.size >= 5 -> {
                        binding.addr1.visibility = View.VISIBLE
                        binding.addr2.visibility = View.VISIBLE
                        binding.addr1.text = it[4].long_name
                        binding.addr2.text = it[1].long_name
                    }
                    it.size >= 3 -> {
                        binding.addr1.visibility = View.VISIBLE
                        binding.addr2.visibility = View.VISIBLE
                        binding.addr1.text = it[2].long_name
                        binding.addr2.text = it[1].long_name
                    }
                    it.size >= 2 -> {
                        binding.addr1.visibility = View.VISIBLE
                        binding.addr2.visibility = View.VISIBLE
                        binding.addr1.text = it[1].long_name
                        binding.addr2.text = it[0].long_name
                    }
                }
                storeLocation()
            }

//            if (value[0].addressComponents?.size != 0) {
//                value[0].addressComponents?.get(0)?.long_name?.let { address ->
//                    location.text = address
//                }
//            }

        }
    }
    private fun locationListener() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                /* if (p0.locations == null) {
                     return
                 }*/
                for (location in p0.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }
    private fun storeLocation(){
        sharedHelper.location1 = binding.addr1.text.toString()
        sharedHelper.location2 = binding.addr2.text.toString()
    }

}
