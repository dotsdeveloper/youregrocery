package com.yes.youregrocery.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.CheckoutActivity
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.activity.MyWalletActivity
import com.yes.youregrocery.adapter.CartlistAdapter
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.responses.GetCartData
import com.yes.youregrocery.responses.Order
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.responses.Quantities
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.others.SwipeHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentQuickOrderBinding
import com.yes.youregrocery.utils.BaseUtils
import java.util.ArrayList

class QuickOrderFragment : Fragment() {
    var binding: FragmentQuickOrderBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity
    var products: ArrayList<Products> = ArrayList()
    var quantities: ArrayList<Quantities> = ArrayList()
    private var isfirst:Boolean = true
    lateinit var adapter:CartlistAdapter
    private var productname: ArrayList<String> = ArrayList()
    var quantity: ArrayList<String> = ArrayList()
    var sqtystock:Int = 0
    var productid:Int = 0
    var qtyid:Int = 0
    var qty:Int = 0
    var lpos:Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentQuickOrderBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        dashBoardActivity.updateBadges(binding!!.header)
        dashBoardActivity.searchTopLoad(binding!!.header.searchHome)
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.quick_order)
        reloadCart()
        getAllProducts()
        return view
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.add, SharedHelper(binding!!.add.context).primaryColor, null)
        UiUtils.buttonBgColor(binding!!.proceed, sharedHelper!!.primaryColor, null)
        dashBoardActivity.searchTopLoad(binding!!.header.searchHome)
        binding!!.header.wallet.walletframe.setOnClickListener {
            startActivity(Intent(requireContext(), MyWalletActivity::class.java))
        }
        binding!!.header.notification.notificationframe.setOnClickListener {
            dashBoardActivity.addFragment(NotificationFragment(), null, -1, -1)
        }
        binding!!.add.setOnClickListener {
            if(productid != 0 && qtyid != 0 && qty != 0){
                if(!isCart()){
                    addCart()
                }
                else{
                    UiUtils.showSnack(binding!!.root,getString(R.string.product_already_in_cart))
                }
            }
            else{
                UiUtils.showSnack(binding!!.root,getString(R.string.please_choose_details))
            }
        }
        binding!!.proceed.setOnClickListener {
            startActivity(Intent(requireContext(), CheckoutActivity::class.java))
        }

        binding!!.qty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if(binding!!.qty.text!!.isNotEmpty() && qtyid!=0){
                    qty = binding!!.qty.text!!.toString().toInt()
                    if(quantities.size != 0){
                        for(items in products){
                            if(items.id == productid){
                                for(items1 in items.quantities!!){
                                    if(items1.id == qtyid){
                                        val total = "${Constants.Common.CURRENCY}${(items1.amount!!.toDouble()*qty)}"
                                        binding!!.total.setText(total)
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    qty = 0
                    binding!!.total.setText("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
    }
    fun reloadCart(){
        getCart(isGet = true, isAdd = false, isModify = false, isDelete = false)
    }
    private fun emptyCart(){
        sharedHelper!!.cartList = ArrayList()
        dashBoardActivity.showBadge(sharedHelper!!.cartList.size)
        binding!!.proceed.visibility = View.GONE
        binding!!.cartlist.visibility = View.GONE
    }
    private fun viewCart(){
        binding!!.cartlist.visibility = View.VISIBLE
        binding!!.proceed.visibility = View.VISIBLE
        dashBoardActivity.showBadge(sharedHelper!!.cartList.size)
    }
    fun getCart(isGet:Boolean, isAdd:Boolean, isModify:Boolean, isDelete:Boolean){
        // DialogUtils.showCartLoader(requireContext())
        viewModel?.getCart(requireContext())?.observe(viewLifecycleOwner) { response ->
            // DialogUtils.dismissLoader()
            response?.let {
                response.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                        emptyCart()
                    } else {
                        if (response.getcart_data!!.order!!.size > 0) {
                            for ((index, value) in response.getcart_data!!.order!!.withIndex()) {
                                value.product!!.category!!.ccount =
                                    getCatCount(response.getcart_data!!,
                                        response.getcart_data!!.order!![index].product!!.category!!.name)
                                //  it.getcart_data!!.order!![index].product!!.category!!.ccount = getcatcount(it.getcart_data!!,it.getcart_data!!.order!![index].product!!.category!!.name.toString())
                            }
                            response.getcart_data!!.order!!.sortBy { order: Order -> order.product!!.category!!.name }
                            sharedHelper!!.cartList = response.getcart_data!!.order!!
                            viewCart()
                            Log.d("cmvbnc", "" + isDelete + isModify)
                            if (isGet || isAdd) {
                                binding!!.cartlist.layoutManager =
                                    GridLayoutManager(requireContext(),
                                        1,
                                        LinearLayoutManager.VERTICAL,
                                        false)
                                adapter = CartlistAdapter(dashBoardActivity,
                                    null,
                                    this,
                                    requireContext(),
                                    sharedHelper!!.cartList)
                                binding!!.cartlist.adapter = adapter
                            }

                            if (isfirst) {
                                isfirst = false
                                val swipeHelper: SwipeHelper =
                                    object : SwipeHelper(requireContext(), binding!!.cartlist) {
                                        @SuppressLint("NotifyDataSetChanged")
                                        override fun instantiateUnderlayButton(
                                            viewHolder: RecyclerView.ViewHolder,
                                            underlayButtons: ArrayList<UnderlayButton>
                                        ) {
                                            underlayButtons.add(UnderlayButton(
                                                getString(R.string.delete),
                                                0,
                                                Color.parseColor(sharedHelper!!.primaryColor)
                                            ) { pos: Int ->
                                                DialogUtils.showCartLoader(requireContext())
                                                viewModel?.deleteCart(
                                                    requireContext(),
                                                    sharedHelper!!.cartList[pos].id!!.toInt()
                                                )
                                                    ?.observe(viewLifecycleOwner) {
                                                        DialogUtils.dismissLoader()
                                                        it?.let {
                                                            it.error?.let { error ->
                                                                if (error) {
                                                                    UiUtils.showSnack(
                                                                        binding!!.root,
                                                                        it.message!!
                                                                    )
                                                                } else {
                                                                    val scatname =
                                                                        adapter.list!![pos].product!!.category!!.name
                                                                    val scatcount =
                                                                        adapter.list!![pos].product!!.category!!.ccount!!.toInt()
                                                                    for (items in adapter.list!!) {
                                                                        if (items.product!!.category!!.name == scatname) {
                                                                            items.product!!.category!!.ccount =
                                                                                scatcount - 1
                                                                        }
                                                                    }
                                                                    adapter.list!!.removeAt(pos)
                                                                    adapter.notifyItemRemoved(pos)
                                                                    adapter.notifyDataSetChanged()
                                                                    getCart(
                                                                        isGet = false,
                                                                        isAdd = false,
                                                                        isModify = false,
                                                                        isDelete = true
                                                                    )
                                                                }
                                                            }
                                                        }
                                                    }
                                            })
                                        }
                                    }
                                swipeHelper.attachSwipe()
                            }
                        } else {
                            emptyCart()
                        }

                        productSpinner(products)
                    }
                }
            }
        }
    }
    private fun getCatCount(getcartData: GetCartData, c: String): Int {
        var count = 0
        for(items in getcartData.order!!){
            if(items.product!!.category!!.name == c){
                count++
            }
        }
        return count
    }
    private fun getAllProducts(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getAllProducts(requireContext())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                        productSpinner(it.product_data)
                    } else {
                        it.product_data?.let { data ->
                            products = data
                            productSpinner(it.product_data)
                        }
                    }
                }
            }
        }
    }
    private fun productSpinner(productData: ArrayList<Products>?) {
        productname = ArrayList()
        if(productData!!.size != 0){
            productname.add(getString(R.string.select_product))
            for(items in productData){
                productname.add(items.name!!)
            }
        }
        else{
            productname.add(getString(R.string.select_product))
        }

        val searchMethod = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,productname)
        searchMethod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerProduct.adapter = searchMethod

        // spinner on item selected listener
        binding!!.spinnerProduct.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // textView.text = "Selected: "
                // get selected item text
                //textView.append(parent.getItemAtPosition(position).toString())
                if(position != 0){
                    for((index, value) in products.withIndex()){
                        if((position-1) == index){
                            productid = value.id!!
                            quantities = value.quantities!!
                            qtySpinner(value.quantities!!)
                        }
                    }
                }
                else{
                    productid = 0
                    qtyid = 0
                    qtySpinner(ArrayList())
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }
    }
    fun qtySpinner(quantities1: ArrayList<Quantities>) {
        quantity = ArrayList()
        if(quantities1.size != 0){
            quantity.add(getString(R.string.select_quantity))
            for(items in quantities){
                quantity.add(items.quantity!!+" "+items.product_unit!!.name)
            }
        }
        else{
            quantity.add(getString(R.string.select_quantity))
        }

        val searchMethod = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,quantity)
        searchMethod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerQty.adapter = searchMethod

        // spinner on item selected listener
        binding!!.spinnerQty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // textView.text = "Selected: "
                // get selected item text
                //textView.append(parent.getItemAtPosition(position).toString())

                /*Log.d("dhvgdhfvh","hcbjx"+parent.getItemAtPosition(position).toString())
                Log.d("dhvgdhfvh","hcbjx"+position)*/
                if(position != 0){
                    qtyid = this@QuickOrderFragment.quantities[position-1].id!!
                    sqtystock = this@QuickOrderFragment.quantities[position-1].stock!!.toInt()
                    binding!!.qty.isEnabled = true
                }
                else{
                    qtyid = 0
                    binding!!.qty.isEnabled = false
                    binding!!.qty.setText("")
                    binding!!.total.setText("")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }
    private fun addCart(){
        val stockOption = BaseUtils.nullCheckerInt(sharedHelper!!.shopWebData.profile.stock_option)
        val maxQuantity = BaseUtils.nullCheckerInt(sharedHelper!!.shopWebData.profile.max_quantity)
        if(stockOption == 1){
            if(qty > maxQuantity && maxQuantity != 0) {
                UiUtils.showSnack(binding!!.root,getString(R.string.sorry_you_cannot_add_more_quantity))
            }
            else{
                if((maxQuantity == 0 || maxQuantity > qty) && qty < sqtystock){
                    DialogUtils.showCartLoader(requireContext())
                    viewModel?.addToCart(requireContext(),qty,qtyid,productid)
                        ?.observe(viewLifecycleOwner) {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding!!.root, it.message!!)
                                    } else {
                                        if (!sharedHelper!!.loggedIn) {
                                            sharedHelper!!.cartIdTemp =
                                                it.addcart_data!!.cart_id.toString()
                                        }
                                        getCart(
                                            isGet = false,
                                            isAdd = true,
                                            isModify = false,
                                            isDelete = false
                                        )
                                    }
                                }
                            }
                        }
                }
                else{
                    UiUtils.showSnack(binding!!.root,getString(R.string.out_of_stock))
                }
            }
        }
        else if(stockOption == 0){
            if(qty > maxQuantity && maxQuantity != 0){
                UiUtils.showSnack(binding!!.root,getString(R.string.sorry_you_cannot_add_more_quantity))
            }
            else{
                DialogUtils.showCartLoader(requireContext())
                viewModel?.addToCart(requireContext(),qty,qtyid,productid)
                    ?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding!!.root, it.message!!)
                                } else {
                                    if (!sharedHelper!!.loggedIn) {
                                        sharedHelper!!.cartIdTemp =
                                            it.addcart_data!!.cart_id.toString()
                                    }
                                    getCart(
                                        isGet = false,
                                        isAdd = true,
                                        isModify = false,
                                        isDelete = false
                                    )
                                }
                            }
                        }
                    }
            }
        }
    }
    fun isCart() : Boolean {
        if(sharedHelper!!.cartList.size != 0){
            for(items in sharedHelper!!.cartList){
                if(items.product_id == productid){
                    if(items.quantity_id == qtyid){
                        return true
                    }
                }
            }
        }
        else{
            return false
        }
        return false
    }
}