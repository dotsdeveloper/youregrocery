package com.yes.youregrocery.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentPrivacyPolicyBinding
import com.yes.youregrocery.utils.BaseUtils

class PrivacyPolicyFragment : Fragment() {
    var binding: FragmentPrivacyPolicyBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentPrivacyPolicyBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        dashBoardActivity!!.searchTopLoad(binding!!.header.searchHome)
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.privacy_policy)
        binding!!.privacy.text = UiUtils.convertHtml(BaseUtils.nullCheckerStr(sharedHelper!!.shopWebData.privacy_policy))
        return view
    }

}