package com.yes.youregrocery.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.WishlistAdapter
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentWishlistBinding
import com.yes.youregrocery.others.SwipeHelper
import kotlin.collections.ArrayList

class WishlistFragment : Fragment(){
    var binding: FragmentWishlistBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null
    private lateinit var wishlistAdapter: WishlistAdapter
    var ischange: Boolean = false
    private var isfirst:Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentWishlistBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(ischange){
                    isEnabled = if(dashBoardActivity!!.fragArray.isNotEmpty()){
                        if (dashBoardActivity!!.fragArray.last().fragment.tag == ProductViewFragment().javaClass.name) {
                            dashBoardActivity!!.productViewFragment.load()
                            //  dashBoardActivity!!.removefragment(this@ProductViewFragment)
                            false
                            // activity?.onBackPressed()
                        } else{
                            false
                            //activity?.onBackPressed()
                        }
                    } else{
                        false
                        //activity?.onBackPressed()
                    }
                }
                else{
                    isEnabled = false
                    //activity?.onBackPressed()
                }
            }
        })
        listener()
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.wishlist)
        dashBoardActivity!!.updateBadges(binding!!.header)
        reload()
        return view
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.retry, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.startshopping, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.emprtTxt1, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.emptyTxt, sharedHelper!!.primaryColor, null)
        dashBoardActivity!!.searchTopLoad(binding!!.header.searchHome)

        binding!!.back.setOnClickListener {
            backAction()
        }
        binding!!.retry.setOnClickListener {
            getWishlist()
        }

        binding!!.startshopping.setOnClickListener {
            backAction()
        }
    }

    private fun backAction(){
        if(ischange){
            if(dashBoardActivity!!.fragArray.isNotEmpty()){
                if (dashBoardActivity!!.fragArray.last().fragment.tag == ProductViewFragment().javaClass.name) {
                    dashBoardActivity!!.productViewFragment.load()
                    dashBoardActivity!!.removeFragment(this)
                }
                else{
                    dashBoardActivity!!.removeFragment(this)
                }
            }
            else{
                dashBoardActivity!!.removeFragment(this)
            }
        }
        else{
            dashBoardActivity!!.removeFragment(this)
        }
    }

    fun reload(){
        getWishlist()
    }

    private fun getWishlist(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getWishList(requireContext())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding!!.root, it.message!!)
                        binding!!.emptywishlist.visibility = View.VISIBLE
                        binding!!.recyclerWishlist.visibility = View.GONE
                        sharedHelper!!.wishlist = ArrayList()
                    } else {
                        if (it.wishlistdata!!.size > 0) {
                            sharedHelper!!.wishlist = it.wishlistdata!!
                            binding!!.emptywishlist.visibility = View.GONE
                            binding!!.recyclerWishlist.visibility = View.VISIBLE

                            binding!!.recyclerWishlist.layoutManager =
                                GridLayoutManager(requireContext(), 1,
                                    LinearLayoutManager.VERTICAL, false)
                            wishlistAdapter = WishlistAdapter(
                                this,
                                requireContext(),
                                sharedHelper!!.wishlist)
                            binding!!.recyclerWishlist.adapter = wishlistAdapter

                            if (isfirst) {
                                isfirst = false
                                val swipeHelper: SwipeHelper =
                                    object :
                                        SwipeHelper(requireContext(), binding!!.recyclerWishlist) {
                                        @SuppressLint("NotifyDataSetChanged")
                                        override fun instantiateUnderlayButton(
                                            viewHolder: RecyclerView.ViewHolder,
                                            underlayButtons: ArrayList<UnderlayButton>
                                        ) {
                                            underlayButtons.add(UnderlayButton(
                                                "Delete",
                                                R.drawable.ic_baseline_delete_24,
                                                Color.parseColor(sharedHelper!!.primaryColor)
                                            ) { pos: Int ->
                                                deleteWishlist(pos)
                                            })
                                        }
                                    }

                                swipeHelper.attachSwipe()
                            }

                        } else {
                            binding!!.emptywishlist.visibility = View.VISIBLE
                            binding!!.recyclerWishlist.visibility = View.GONE
                            sharedHelper!!.wishlist = ArrayList()
                        }

                    }
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteWishlist(pos: Int){
        DialogUtils.showLoader(requireContext())
        viewModel?.deleteWishList(requireContext(), wishlistAdapter.list!![pos].id!!)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                    } else {
                        ischange = true
                        wishlistAdapter.list!!.removeAt(pos)
                        sharedHelper!!.wishlist = wishlistAdapter.list!!
                        wishlistAdapter.notifyItemRemoved(pos)
                        wishlistAdapter.notifyDataSetChanged()
                        if (sharedHelper!!.wishlist.size == 0) {
                            binding!!.emptywishlist.visibility = View.VISIBLE
                            binding!!.recyclerWishlist.visibility = View.GONE
                            sharedHelper!!.wishlist = ArrayList()
                        }
                    }
                }
            }
        }
    }
}