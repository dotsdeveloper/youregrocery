package com.yes.youregrocery.fragment
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentSearchBinding
import android.view.inputmethod.EditorInfo

import android.widget.TextView.OnEditorActionListener
import androidx.activity.OnBackPressedCallback
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.ProductListAdapter
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils

class SearchFragment : Fragment() {
    var binding: FragmentSearchBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var productlist: ArrayList<Products> = ArrayList()
    lateinit var dashBoardActivity: DashBoardActivity
    private lateinit var productListAdapter: ProductListAdapter
    var lpos:Int = 0
    private var offset:Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        dashBoardActivity.clearBottomNav()
        listener()
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                isEnabled = if(dashBoardActivity.fragArray.isNotEmpty()){
                    if(dashBoardActivity.fragArray.last().fragment.tag == BasketFragment().javaClass.name){
                        dashBoardActivity.basketFragment.reloadCart()
                        //  dashBoardActivity.removefragment(this@ProductViewFragment)
                        false
                        // activity?.onBackPressed()
                    } else {
                        false
                    }
                } else{
                    false
                    // activity?.onBackPressed()
                }

            }
        })
        return view
    }

    fun listener(){
        UiUtils.linearLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        binding!!.search.requestFocus()
        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                search()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        binding!!.search.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                BaseUtils.closeKeyBoard(binding!!.search,requireContext())
                // search()
                return@OnEditorActionListener true
            }
            false
        })
        binding!!.back.setOnClickListener {
            if(dashBoardActivity.fragArray.isNotEmpty()) {
                if(dashBoardActivity.fragArray.last().fragment.tag == BasketFragment().javaClass.name){
                    dashBoardActivity.basketFragment.reloadCart()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else{
                dashBoardActivity.removeFragment(this)
            }
        }
        binding!!.close.setOnClickListener {
            binding!!.search.setText("")
        }
    }

    fun reload(){
        productListAdapter.list = dashBoardActivity.isCartCheckData(productlist)
        binding!!.recylerSearchProduct.adapter!!.notifyItemChanged(lpos)
    }

    fun search(){
        if(requireArguments().getString(Constants.IntentKeys.SEARCH_TYPE).equals(Constants.IntentKeys.ALL)){
            if(binding!!.search.text.toString().isNotEmpty()){
                binding!!.close.visibility = View.VISIBLE
                binding!!.progress.visibility = View.VISIBLE
                binding!!.notfound.visibility = View.GONE
                binding!!.recylerSearchProduct.visibility = View.GONE
                offset = 0
                viewModel?.searchProducts(requireContext(),binding!!.search.text.toString(),0,0,offset)?.observe(viewLifecycleOwner) {
                    //  DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                // UiUtils.showSnack(binding!!.root, it.message!!)
                                noProduct()
                            } else {
                                it.product_data?.let { data ->
                                    loadRecycler(data)
                                }
                            }
                        }
                    }
                }
            }
            else{
                binding!!.close.visibility = View.GONE
                binding!!.recylerSearchProduct.visibility = View.GONE
                binding!!.notfound.visibility = View.VISIBLE
            }
        }
    }

    private fun noProduct(){
        binding!!.recylerSearchProduct.visibility = View.GONE
        binding!!.progress.visibility = View.GONE
        binding!!.notfound.visibility = View.VISIBLE
    }

    private fun loadRecycler(data: ArrayList<Products>) {
        if(data.size != 0){
            binding!!.progress.visibility = View.GONE
            binding!!.notfound.visibility = View.GONE
            binding!!.recylerSearchProduct.visibility = View.VISIBLE
            productlist.clear()
            productlist = dashBoardActivity.isCartCheckData(data)
            val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false)
            binding!!.recylerSearchProduct.layoutManager = layoutManager
            productListAdapter = ProductListAdapter(dashBoardActivity, 2, null, null, this, requireContext(), productlist,true)
            productListAdapter.isscroolcomplete = false
            binding!!.recylerSearchProduct.adapter = productListAdapter
            binding!!.recylerSearchProduct.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if(!recyclerView.canScrollVertically(1)) {
                        // LOAD MORE
                        Log.d("Stepppp1","ghdfghd")
                    }
                    if(layoutManager.findLastCompletelyVisibleItemPosition() == productlist.size -1){
                        //bottom of list!
                        Log.d("Stepppp2","ghdfghd")
                        loadMoreData()
                    }
                }
            })
        }
        else{
            noProduct()
        }
    }

    fun loadMoreData(){
        offset += 1
        DialogUtils.showLoader(requireContext())
        viewModel?.searchProducts(requireContext(),binding!!.search.text.toString(),0,0,offset)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding!!.root, it.message!!)
                        scrollComplete()
                    } else {
                        it.product_data?.let { data ->
                            updateRecyclerScrool(data)
                        }
                    }
                }
            }
        }
    }

    private fun updateRecyclerScrool(data: ArrayList<Products>) {
        if(data.size != 0){
            productListAdapter.isscroolcomplete = false
            val ndata : ArrayList<Products> = offsetDublicate(data)
            if(ndata.size > 0){
                productlist.addAll(dashBoardActivity.isCartCheckData(ndata))
                binding!!.recylerSearchProduct.adapter!!.notifyItemInserted(productlist.size - 1)
                // binding!!.recylerProduct.adapter!!.notifyItemRangeInserted(totalItemsCount,productlist.size - 1)
            }
            else{
                loadMoreData()
            }
        }
        else{
            scrollComplete()
        }
    }

    private fun scrollComplete(){
        offset -= 1
        productListAdapter.isscroolcomplete = true
        binding!!.recylerSearchProduct.adapter!!.notifyItemChanged(productlist.size - 1)
    }

    private fun offsetDublicate(data: ArrayList<Products>): ArrayList<Products> {
        val ndata:ArrayList<Products> = ArrayList()
        for(i in 0 until data.size){
            if(!isProduct(data[i])){
                ndata.add(data[i])
            }
        }
        return ndata
    }

    private fun isProduct(products: Products):Boolean{
        for(items in productlist){
            if(items.id == products.id){
                return true
            }
        }
        return false
    }
}