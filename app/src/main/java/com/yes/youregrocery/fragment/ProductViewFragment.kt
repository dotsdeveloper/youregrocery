package com.yes.youregrocery.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.yes.youregrocery.R
import com.yes.youregrocery.`interface`.OnClickListener
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.*
import com.yes.youregrocery.databinding.FragmentProductViewBinding
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.responses.Quantities
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import org.json.JSONObject
import java.util.*
import kotlin.math.roundToInt

class ProductViewFragment : Fragment() {
    var binding: FragmentProductViewBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var products = Products()
    var productid : Int? = null
    var dashBoardActivity: DashBoardActivity = DashBoardActivity()
    var iscart : Boolean? = null
    var qty : Int? = null
    var iscartadd: Boolean = false
    var iswishlistchange: Boolean = false
    var isoutofstock: Boolean = false
    private var wishlistid: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentProductViewBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(dashBoardActivity.fragArray.isNotEmpty()){
                    when (dashBoardActivity.fragArray.last().fragment.tag) {
                        ProductListFragment().javaClass.name -> {
                            isEnabled = if(iscartadd){
                                dashBoardActivity.productListFragment.reload()
                                false
                            } else{
                                false
                            }
                        }
                        SearchFragment().javaClass.name -> {
                            isEnabled = if(iscartadd){
                                dashBoardActivity.searchFragment.reload()
                                false
                            } else{
                                false
                            }
                        }
                        BasketFragment().javaClass.name -> {
                            isEnabled = if(iscartadd){
                                dashBoardActivity.basketFragment.reloadCart()
                                false
                            } else{
                                false
                            }
                        }
                        QuickOrderFragment().javaClass.name -> {
                            isEnabled = if(iscartadd){
                                dashBoardActivity.quickOrderFragment.reloadCart()
                                false
                            } else{
                                false
                            }
                        }
                        WishlistFragment().javaClass.name -> {
                            isEnabled = if(iswishlistchange){
                                dashBoardActivity.wishlistFragment.reload()
                                false
                            } else{
                                false
                            }
                        }
                        else -> {
                            isEnabled = false
                        }
                    }
                }
                else{
                    isEnabled = false
                    //activity?.onBackPressed()
                }
            }
        })
        listener()
        load()
        return view
    }

    fun listener(){
        UiUtils.linearLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.buton, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.youtube, sharedHelper!!.primaryColor, null)

        dashBoardActivity.searchTopLoad(binding!!.searchHome)
        binding!!.youtube.setOnClickListener {
            /*val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/embed/h7g5YsyRwwA"))
            startActivity(intent)*/
            val url = products.youtube_url
            showWebviewDialog("<iframe width=\"100%\" height=\"100%\" src=\"$url\" frameborder=\"0\" allowfullscreen></iframe>")
        }
        binding!!.back.setOnClickListener {
            backAction()
        }
        binding!!.buton.setOnClickListener {
            backAction()
        }
        binding!!.share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            val name = products.name.toString().trim()
            val name1 = name.replace(" ", "")
            val string = BaseUtils.nullCheckerStr(sharedHelper!!.shopWebData.profile.web_url)+"/#/productinfo/"+name1+"/"+productid
            shareIntent.putExtra(Intent.EXTRA_TEXT, string)
            startActivity(Intent.createChooser(shareIntent,getString(R.string.share_with)))
        }
        binding!!.brand.setOnClickListener {
            dashBoardActivity.removeAllFragment()
            val args = Bundle()
            args.putString(Constants.IntentKeys.CNAME,products.brand.name)
            args.putBoolean(Constants.IntentKeys.IS_COME, false)
            args.putString(Constants.IntentKeys.PAGE,Constants.IntentKeys.LOAD_BRAND_P)
            args.putInt(Constants.IntentKeys.CID,products.brand_id!!)
            args.putInt(Constants.IntentKeys.S_C_ID,0)
            dashBoardActivity.addFragment(ProductListFragment(), args, -1, -1)
        }
        binding!!.howtouse.setOnClickListener {
            if(binding!!.howtouseRelative.visibility == View.VISIBLE){
                UiUtils.textviewImgDrawable(binding!!.howtouse,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
                binding!!.howtouseRelative.visibility = View.GONE
            }
            else{
                UiUtils.textviewImgDrawable(binding!!.howtouse,R.drawable.ic_baseline_arrow_drop_up_24,Constants.IntentKeys.END)
                binding!!.howtouseRelative.visibility = View.VISIBLE
            }
        }
        binding!!.ingredient.setOnClickListener {
            if(binding!!.ingredentRecycler.visibility == View.VISIBLE){
                UiUtils.textviewImgDrawable(binding!!.ingredient,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
                binding!!.ingredentRecycler.visibility = View.GONE
            }
            else{
                UiUtils.textviewImgDrawable(binding!!.ingredient,R.drawable.ic_baseline_arrow_drop_up_24,Constants.IntentKeys.END)
                binding!!.ingredentRecycler.visibility = View.VISIBLE
            }
        }
        binding!!.faq.setOnClickListener {
            if(binding!!.faqRecycler.visibility == View.VISIBLE){
                UiUtils.textviewImgDrawable(binding!!.faq,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
                binding!!.faqRecycler.visibility = View.GONE
            }
            else{
                UiUtils.textviewImgDrawable(binding!!.faq,R.drawable.ic_baseline_arrow_drop_up_24,Constants.IntentKeys.END)
                binding!!.faqRecycler.visibility = View.VISIBLE
            }
        }
        binding!!.whybuyTxt.setOnClickListener {
            if(binding!!.whybuyRecycler.visibility == View.VISIBLE){
                UiUtils.textviewImgDrawable(binding!!.whybuyTxt,R.drawable.ic_baseline_arrow_drop_down_24,Constants.IntentKeys.END)
                binding!!.whybuyRecycler.visibility = View.GONE
            }
            else{
                UiUtils.textviewImgDrawable(binding!!.whybuyTxt,R.drawable.ic_baseline_arrow_drop_up_24,Constants.IntentKeys.END)
                binding!!.whybuyRecycler.visibility = View.VISIBLE
            }
        }
        binding!!.wishlist.setOnClickListener {
            if(isWishlist()){
                DialogUtils.showLoader(requireContext())
                viewModel?.deleteWishList(requireContext(),wishlistid)?.observe(viewLifecycleOwner) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding!!.root, it.message!!)
                            } else {
                                iswishlistchange = true
                                binding!!.wishlist.visibility = View.VISIBLE
                                UiUtils.imageviewDrawable(binding!!.wishlist,
                                    R.drawable.ic_baseline_favorite_border_24)
                                UiUtils.imageViewTint(binding!!.wishlist,
                                    sharedHelper!!.primaryColor,
                                    null)
                                dashBoardActivity.getWishlist()
                            }
                        }
                    }
                }
            }
            else{
                DialogUtils.showLoader(requireContext())
                viewModel?.addWishList(requireContext(),productid!!.toInt(),0)?.observe(viewLifecycleOwner) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding!!.root, it.message!!)
                            } else {
                                iswishlistchange = true
                                binding!!.wishlist.visibility = View.VISIBLE
                                UiUtils.imageviewDrawable(binding!!.wishlist,
                                    R.drawable.ic_baseline_favorite_24)
                                UiUtils.imageViewTint(binding!!.wishlist,
                                    sharedHelper!!.primaryColor,
                                    null)
                                dashBoardActivity.getWishlist()
                            }
                        }
                    }
                }
            }
        }
    }
    private fun showWebviewDialog(url: String) {
        val dialog = Dialog(requireContext())
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_webview)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(requireContext(), R.color.transparent)))
        val width: Int = (requireContext().resources.displayMetrics.widthPixels * 0.9).roundToInt()
        var height: Int = (requireContext().resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, height)
        val close = dialog.findViewById<ImageView>(R.id.close)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val webview = dialog.findViewById<WebView>(R.id.webview)
        webview.settings.javaScriptEnabled = true
        webview.webChromeClient = object : WebChromeClient() {

        }
        webview.loadData(url, "text/html", "utf-8")
        dialog.show()
    }

    private fun backAction(){
        if(dashBoardActivity.fragArray.isNotEmpty()){
            if (dashBoardActivity.fragArray.last().fragment.tag == ProductListFragment().javaClass.name) {
                if(iscartadd){
                    dashBoardActivity.productListFragment.reload()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else if(dashBoardActivity.fragArray.last().fragment.tag == SearchFragment().javaClass.name){
                if(iscartadd){
                    dashBoardActivity.searchFragment.reload()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else if(dashBoardActivity.fragArray.last().fragment.tag == BasketFragment().javaClass.name){
                if(iscartadd){
                    dashBoardActivity.basketFragment.reloadCart()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else if(dashBoardActivity.fragArray.last().fragment.tag == QuickOrderFragment().javaClass.name){
                if(iscartadd){
                    dashBoardActivity.quickOrderFragment.reloadCart()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else if(dashBoardActivity.fragArray.last().fragment.tag == WishlistFragment().javaClass.name){
                if(iswishlistchange){
                    dashBoardActivity.wishlistFragment.reload()
                    dashBoardActivity.removeFragment(this)
                }
                else{
                    dashBoardActivity.removeFragment(this)
                }
            }
            else{
                dashBoardActivity.removeFragment(this)
            }
        }
        else{
            dashBoardActivity.removeFragment(this)
        }
    }
    fun load(){
        products = requireArguments().getSerializable(Constants.IntentKeys.KEY) as Products

        isoutofstock = BaseUtils.nullCheckerInt(products.stock_available) == 0
        binding!!.productName.text = BaseUtils.nullCheckerStr(products.name)
        productid = products.id!!.toInt()

        //products.youtube_url = "https://www.youtube.com/embed/h7g5YsyRwwA"
        if(BaseUtils.nullCheckerStr(products.youtube_url).isNotEmpty()){
            binding!!.youtube.visibility = View.VISIBLE
        }
        else{
            binding!!.youtube.visibility = View.GONE
        }

        if(products.files != null && products.files!!.isNotEmpty()){
            UiUtils.loadImageWithCenterCrop(binding!!.productImage,  products.files!![0])
        }
        else{
            UiUtils.loadDefaultImage(binding!!.productImage)
        }

        if(BaseUtils.nullCheckerStr(products.description).isEmpty()){
            binding!!.desc.visibility = View.GONE
            binding!!.txtDesc.visibility = View.GONE
        }
        else {
            binding!!.desc.visibility = View.VISIBLE
            binding!!.txtDesc.visibility = View.VISIBLE
            binding!!.desc.text = products.description
        }

        if(BaseUtils.nullCheckerInt(products.brand_id) != 0){
            binding!!.brand.visibility = View.VISIBLE
            val brand = "${getString(R.string.see_more)} ${products.brand.name} ${getString(R.string.products)}"
            binding!!.brand.text = brand
            val category = "${products.category!!.name} ${"-"} ${products.brand.name}"
            binding!!.productCategory.text = category
        }
        else{
            binding!!.brand.visibility = View.GONE
            binding!!.productCategory.text = products.category!!.name
        }

        if(sharedHelper!!.cartList.size > 0){
            for(items in sharedHelper!!.cartList) {
                if (items.product_id!!.toInt() == productid) {
                    for ((index, value) in products.quantities!!.withIndex()) {
                        if(value.id == items.quantities!!.id){
                            products.quantities!![index].iscart = true
                            products.quantities!![index].qty = items.quantity!!.toInt()
                            products.quantities!![index].cartid = items.id!!.toInt()
                        }
                    }
                }
            }
        }
        else{
            for ((index, value) in products.quantities!!.withIndex()) {
                products.quantities!![index].iscart = false
                products.quantities!![index].qty = 0
               // products.quantities!![index].cartid = 0
                value.cartid = 0
            }
        }

        if(sharedHelper!!.loggedIn){
            if(isWishlist()){
                binding!!.wishlist.visibility = View.VISIBLE
                UiUtils.imageviewDrawable(binding!!.wishlist,R.drawable.ic_baseline_favorite_24)
                UiUtils.imageViewTint(binding!!.wishlist, sharedHelper!!.primaryColor, null)
            }
            else{
                binding!!.wishlist.visibility = View.VISIBLE
                UiUtils.imageviewDrawable(binding!!.wishlist,R.drawable.ic_baseline_favorite_border_24)
                UiUtils.imageViewTint(binding!!.wishlist, sharedHelper!!.primaryColor, null)
            }
        }
        else{
            binding!!.wishlist.visibility = View.GONE
        }

        binding!!.packRecycler.layoutManager = GridLayoutManager(requireContext(),1, LinearLayoutManager.VERTICAL,false)
        binding!!.packRecycler.adapter = ProductQuantityAdapter(
            dashBoardActivity,
            this,
            requireContext(),
            products.quantities!!,
            products
        )
        addon()
        multyImage()
    }
    private fun addon(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getProductAddOn(requireContext(),productid!!.toInt())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding!!.root, it.message!!)
                        binding!!.howtouse.visibility = View.GONE
                        binding!!.howtouseRelative.visibility = View.GONE
                        binding!!.ingredentRecycler.visibility = View.GONE
                        binding!!.ingredient.visibility = View.GONE
                        binding!!.faq.visibility = View.GONE
                        binding!!.faqRecycler.visibility = View.GONE
                        binding!!.whybuyRecycler.visibility = View.GONE
                        binding!!.whybuyTxt.visibility = View.GONE
                    } else {
                        binding!!.howtouse.visibility = View.GONE
                        binding!!.howtouseRelative.visibility = View.GONE
                        binding!!.ingredentRecycler.visibility = View.GONE
                        binding!!.ingredient.visibility = View.GONE
                        binding!!.faq.visibility = View.GONE
                        binding!!.faqRecycler.visibility = View.GONE
                        binding!!.whybuyRecycler.visibility = View.GONE
                        binding!!.whybuyTxt.visibility = View.GONE

                        UiUtils.cardViewBgColor(
                            binding!!.howtouseRelative,
                            sharedHelper!!.primaryColor,
                            null
                        )
                        val json = Gson().toJson(it.product_how_to_use)
                        if (JSONObject(json).length() > 2) {
                            binding!!.howtouse.visibility = View.VISIBLE
                            binding!!.body.text = it.product_how_to_use.description
                            if (it.product_how_to_use.file != null && !it.product_how_to_use.file.equals(
                                    "")
                            ) {
                                UiUtils.loadImage(binding!!.img, it.product_how_to_use.file)
                            }
                        }
                        if (it.data!!.product_faq!!.size > 0) {
                            binding!!.faq.visibility = View.VISIBLE
                            binding!!.faqRecycler.layoutManager =
                                GridLayoutManager(requireContext(), 1,
                                    LinearLayoutManager.VERTICAL, false)
                            binding!!.faqRecycler.adapter = FaqAdapter(
                                this,
                                requireContext(),
                                it.data!!.product_faq!!
                            )
                        }
                        if (it.data!!.product_ingredient!!.size > 0) {
                            binding!!.ingredient.visibility = View.VISIBLE
                            binding!!.ingredentRecycler.layoutManager =
                                GridLayoutManager(requireContext(), 1,
                                    LinearLayoutManager.VERTICAL, false)
                            binding!!.ingredentRecycler.adapter = IngredientAdaper(
                                this,
                                requireContext(),
                                it.data!!.product_ingredient!!
                            )
                        }
                        if (it.data!!.product_why_buy!!.size > 0) {
                            binding!!.whybuyTxt.visibility = View.VISIBLE
                            binding!!.whybuyRecycler.layoutManager =
                                GridLayoutManager(requireContext(), 1,
                                    LinearLayoutManager.VERTICAL, false)
                            binding!!.whybuyRecycler.adapter = WhybuyAdapter(
                                this,
                                requireContext(),
                                it.data!!.product_why_buy!!
                            )
                        }


                    }
                }
            }
        }
    }
    private fun multyImage(){
        if(products.files!!.size > 1){
            binding!!.imageRecycler.visibility = View.VISIBLE
            binding!!.imageRecycler.layoutManager = LinearLayoutManager(requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false)
            binding!!.imageRecycler.adapter = ImageAdapter(
                this,
                requireContext(),
                products.files!!
            )
        }
        else{
            binding!!.imageRecycler.visibility = View.GONE
        }
    }
    private fun isWishlist(): Boolean {
        for(items in sharedHelper!!.wishlist){
            if(productid == items.product_id){
                wishlistid = items.id!!
                return true
            }
        }
        wishlistid = 0
        return false
    }
}