package com.yes.youregrocery.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.activity.CheckoutActivity
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.CartlistAdapter
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.responses.Order
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentBasketBinding
import java.util.ArrayList
import com.yes.youregrocery.others.SwipeHelper
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.LoginActivity
import com.yes.youregrocery.responses.GetCartData
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils

class BasketFragment : Fragment() {
    var binding: FragmentBasketBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity
    private var isfirst:Boolean = true
    var lpos:Int = 0
    private var swipeHelper: SwipeHelper? = null
    lateinit var adapter:CartlistAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentBasketBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        DialogUtils.showCartLoader(requireContext())
        reloadCart()
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    fun listener(){
        UiUtils.buttonBgColor(binding!!.retry, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.startshopping, sharedHelper!!.primaryColor, null)
        UiUtils.relativeLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgTint(binding!!.checkout, sharedHelper!!.primaryColor, null)
        UiUtils.textViewBgTint(binding!!.itemCountTxt, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.total2, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.txtTotal2, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.emptyTxt, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.minimumRequired, sharedHelper!!.primaryColor, null)

        binding!!.back.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
        binding!!.retry.setOnClickListener {
            reloadCart()
        }
        binding!!.more.setOnClickListener {
            if(binding!!.emptycartBtn.visibility == 0){
                binding!!.emptycartBtn.visibility = View.GONE
            }
            else{
                binding!!.emptycartBtn.visibility = View.VISIBLE
            }
        }
        binding!!.emptycartBtn.setOnClickListener {
            DialogUtils.showCartLoader(requireContext())
            viewModel?.deleteCart(requireContext(),-1)?.observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        } else {
                            reloadCart()
                        }
                    }
                }
            }

        }
        binding!!.startshopping.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
        binding!!.bottom.setOnClickListener {
            if(binding!!.details.visibility == View.VISIBLE){
                binding!!.details.visibility = View.GONE
                dashBoardActivity.binding.whats.visibility = View.VISIBLE
            }
            else{
                binding!!.details.visibility = View.VISIBLE
                dashBoardActivity.binding.whats.visibility = View.GONE
            }
        }
        binding!!.checkout.setOnClickListener {
            UiUtils.txtBtnLoadingStart(binding!!.checkout)
            if(sharedHelper!!.loggedIn){
                if(binding!!.minimumRequired.visibility != View.VISIBLE){
                    startActivity(Intent(requireContext(), CheckoutActivity::class.java))
                }
                else{
                    UiUtils.txtBtnLoadingEnd(binding!!.checkout,getString(R.string.checkout))
                    UiUtils.showSnack(binding!!.root,getString(R.string.minimum_order_value_required))
                }
            }
            else{
                BaseUtils.startActivity(requireActivity(),LoginActivity(),null,false)
            }
        }
        binding!!.searchHome.setOnTouchListener{ _, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                dashBoardActivity.openSearchFragment()
            }
            false
        }

        // Refresh function for the layout
        binding!!.swiperefresh.setOnRefreshListener {
            // Your code goes here
            // In this code, we are just changing the text in the
            // textbox
            // dashBoardActivity.refreshfragment(this)
            reloadCart()
            // This line is important as it explicitly refreshes only once
            // If "true" it implicitly refreshes forever
            binding!!.swiperefresh.isRefreshing = false
        }
    }

    override fun onResume() {
        super.onResume()
        UiUtils.txtBtnLoadingEnd(binding!!.checkout,getString(R.string.checkout))
    }

    fun reloadCart(){
        getCart(isGet = true, isAdd = false, isModify = false, isDelete = false)
    }


    private fun emptyCart(){
        sharedHelper!!.cartList = ArrayList()
        dashBoardActivity.showBadge(sharedHelper!!.cartList.size)
        binding!!.main.visibility = View.VISIBLE
        binding!!.emptycart.visibility = View.VISIBLE
        binding!!.more.visibility = View.GONE
        binding!!.emptycartBtn.visibility = View.GONE
        binding!!.bottom.visibility = View.GONE
        binding!!.cartlist.visibility = View.GONE
        binding!!.topp.visibility = View.GONE
    }

    private fun viewCart(grandTotal: String) {
        binding!!.main.visibility = View.VISIBLE
        binding!!.bottom.visibility = View.VISIBLE
        binding!!.emptycart.visibility = View.GONE
        binding!!.cartlist.visibility = View.VISIBLE
        binding!!.topp.visibility = View.VISIBLE
        binding!!.more.visibility = View.VISIBLE
        binding!!.emptycartBtn.visibility = View.GONE
        dashBoardActivity.showBadge(sharedHelper!!.cartList.size)

        if(sharedHelper!!.shopWebData.profile.min_order != null && sharedHelper!!.loggedIn){
            val min = UiUtils.formattedValues(sharedHelper!!.shopWebData.profile.min_order!!)
            val total = UiUtils.formattedValues(grandTotal)
            val bal = min.toDouble() - total.toDouble()
            if(total.toDouble() < min.toDouble()){
                if(BaseUtils.nullCheckerStr(sharedHelper!!.shopWebData.preferred_delivery_message).isNotEmpty()){
                    val spanned = HtmlCompat.fromHtml(sharedHelper!!.shopWebData.preferred_delivery_message.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
                    val spanned1 = spanned.toString().replace(Constants.IntentKeys.VAR1,total)
                    val spanned2 = spanned1.replace(Constants.IntentKeys.VAR2,bal.toString())
                    binding!!.minimumRequired.text = spanned2
                    binding!!.minimumRequired.visibility = View.VISIBLE
                }
                else{
                    val str = getString(R.string.minimum_order_content)
                    val spanned = HtmlCompat.fromHtml(str, HtmlCompat.FROM_HTML_MODE_LEGACY)
                    val spanned1 = spanned.toString().replace(Constants.IntentKeys.VAR1,total)
                    val spanned2 = spanned1.replace(Constants.IntentKeys.VAR2,bal.toString())
                    binding!!.minimumRequired.text = spanned2
                    binding!!.minimumRequired.visibility = View.VISIBLE
                }
            }
            else{
                binding!!.minimumRequired.visibility = View.GONE
            }
        }
    }

    fun getCart(isGet:Boolean, isAdd:Boolean, isModify:Boolean, isDelete:Boolean){
        viewModel?.getCart(requireContext())?.observe(viewLifecycleOwner) { response ->
            DialogUtils.dismissLoader()
            response?.let { it ->
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                        emptyCart()
                    } else {
                        if (it.getcart_data!!.order!!.size > 0) {
                            val total1 =
                                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(it.getcart_data!!.grand_total)}"
                            val subtotal =
                                "${Constants.Common.CURRENCY}${UiUtils.formattedValues(it.getcart_data!!.order_amount)}"
                            binding!!.total1.text = total1
                            binding!!.total2.text = total1
                            binding!!.subtotal.text = subtotal
                            if (UiUtils.formattedValues(it.getcart_data!!.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
                                binding!!.linearGst.visibility = View.VISIBLE
                                val gst1 =
                                    "${getString(R.string.gst)} ${"("}${it.getcart_data!!.gst.toString()}${"%)"}"
                                val gst2 =
                                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(it.getcart_data!!.gst_amount)}"
                                binding!!.gstPer.text = gst1
                                binding!!.gstAmount.text = gst2
                            } else {
                                binding!!.linearGst.visibility = View.GONE
                            }

                            if (UiUtils.formattedValues(it.getcart_data!!.delivery_charge) != Constants.IntentKeys.AMOUNT_DUMMY) {
                                binding!!.linearDeliverycharge.visibility = View.VISIBLE
                                val damount =
                                    "${Constants.Common.CURRENCY}${UiUtils.formattedValues(it.getcart_data!!.delivery_charge)}"
                                binding!!.deliveryAmount.text = damount
                            } else {
                                binding!!.linearDeliverycharge.visibility = View.GONE
                            }

                            if (UiUtils.formattedValues(it.getcart_data!!.total_save_value) != Constants.IntentKeys.AMOUNT_DUMMY) {
                                binding!!.saved.visibility = View.VISIBLE
                                val saved =
                                    "${getString(R.string.you_saved)} ${":"} ${Constants.Common.CURRENCY}${
                                        UiUtils.formattedValues(it.getcart_data!!.total_save_value)
                                    }"
                                binding!!.saved.text = saved
                            } else {
                                binding!!.saved.visibility = View.GONE
                            }

                            if (it.getcart_data!!.order!!.size == 1) {
                                val text1 =
                                    "${it.getcart_data!!.order!!.size} ${getString(R.string.item)}"
                                val text2 =
                                    "${getString(R.string.subtotal)} ${"("}${it.getcart_data!!.order!!.size} ${
                                        getString(R.string.item)
                                    }${")"}"
                                binding!!.itemCountTxt.text = text1
                                binding!!.subtotalTxt.text = text2
                            } else {
                                val text1 =
                                    "${it.getcart_data!!.order!!.size} ${getString(R.string.items)}"
                                val text2 =
                                    "${getString(R.string.subtotal)} ${"("}${it.getcart_data!!.order!!.size} ${
                                        getString(R.string.items)
                                    }${")"}"
                                binding!!.itemCountTxt.text = text1
                                binding!!.subtotalTxt.text = text2
                            }

                            for ((index, value) in it.getcart_data!!.order!!.withIndex()) {
                                value.product!!.category!!.ccount = getCatCount(it.getcart_data!!,
                                    it.getcart_data!!.order!![index].product!!.category!!.name)
                                // it.getcart_data!!.order!![index].product!!.category!!.ccount = getcatcount(it.getcart_data!!,it.getcart_data!!.order!![index].product!!.category!!.name.toString())
                            }

                            it.getcart_data!!.order!!.sortBy { order: Order -> order.product!!.category!!.name }
                            sharedHelper!!.cartList = it.getcart_data!!.order!!
                            viewCart(it.getcart_data!!.grand_total!!)

                            Log.d("cmvbnc", "" + isDelete + isModify)
                            if (isGet || isAdd) {
                                binding!!.cartlist.layoutManager =
                                    GridLayoutManager(requireContext(),
                                        1,
                                        LinearLayoutManager.VERTICAL,
                                        false)
                                adapter = CartlistAdapter(dashBoardActivity,
                                    this,
                                    null,
                                    requireContext(),
                                    sharedHelper!!.cartList)
                                binding!!.cartlist.adapter = adapter
                            }

                            if (isfirst) {
                                isfirst = false
                                swipeHelper =
                                    object : SwipeHelper(requireContext(), binding!!.cartlist) {
                                        @SuppressLint("NotifyDataSetChanged")
                                        override fun instantiateUnderlayButton(
                                            viewHolder: RecyclerView.ViewHolder,
                                            underlayButtons: ArrayList<UnderlayButton>
                                        ) {
                                            underlayButtons.add(UnderlayButton(
                                                getString(R.string.delete),
                                                0,
                                                Color.parseColor(sharedHelper!!.primaryColor)
                                            ) { pos: Int ->
                                                DialogUtils.showCartLoader(requireContext())
                                                viewModel?.deleteCart(
                                                    requireContext(),
                                                    sharedHelper!!.cartList[pos].id!!.toInt()
                                                )
                                                    ?.observe(viewLifecycleOwner) {
                                                        DialogUtils.dismissLoader()
                                                        it?.let {
                                                            it.error?.let { error ->
                                                                if (error) {
                                                                    UiUtils.showSnack(
                                                                        binding!!.root,
                                                                        it.message!!
                                                                    )
                                                                } else {
                                                                    val scatname =
                                                                        adapter.list!![pos].product!!.category!!.name
                                                                    val scatcount =
                                                                        adapter.list!![pos].product!!.category!!.ccount!!.toInt()
                                                                    for (items in adapter.list!!) {
                                                                        if (items.product!!.category!!.name == scatname) {
                                                                            items.product!!.category!!.ccount =
                                                                                scatcount - 1
                                                                        }
                                                                    }
                                                                    adapter.list!!.removeAt(pos)
                                                                    adapter.notifyItemRemoved(pos)
                                                                    adapter.notifyDataSetChanged()
                                                                    getCart(
                                                                        isGet = false,
                                                                        isAdd = false,
                                                                        isModify = false,
                                                                        isDelete = true
                                                                    )
                                                                }
                                                            }
                                                        }
                                                    }
                                            })
                                        }
                                    }
                                (swipeHelper as SwipeHelper).attachSwipe()
                            }
                        } else {
                            emptyCart()
                        }
                    }
                }
            }
        }
    }

    private fun getCatCount(getcartData: GetCartData, c: String): Int {
        var count = 0
        for(items in getcartData.order!!){
            if(items.product!!.category!!.name == c){
                count++
            }
        }
        return count
    }


}