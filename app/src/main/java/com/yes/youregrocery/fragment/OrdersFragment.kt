package com.yes.youregrocery.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.OrderListAdapter
import com.yes.youregrocery.responses.OrderData
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentOrdersBinding
import com.yes.youregrocery.others.EndlessRecyclerViewScrollListener
import com.yes.youregrocery.utils.BaseUtils

class OrdersFragment : Fragment() {
    var binding: FragmentOrdersBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null
    var orderlist: ArrayList<OrderData> = ArrayList()
    private var scrollListener: EndlessRecyclerViewScrollListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOrdersBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        listener()
        load()
        return view
    }

    fun listener(){
        UiUtils.linearLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        binding!!.back.setOnClickListener {
            dashBoardActivity!!.removeFragment(this)
        }
        binding!!.search.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                BaseUtils.closeKeyBoard(binding!!.search,requireContext())
                search(true)
                return@OnEditorActionListener true
            }
            false
        })
        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                search(false)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        binding!!.close.setOnClickListener {
            binding!!.search.setText("")
        }

        binding!!.imgSearch.setOnClickListener {
            search(true)
        }
    }

    fun search(search_finish: Boolean) {
        if(binding!!.search.text!!.isNotEmpty()){
            if(search_finish){
                DialogUtils.showLoader(requireContext())
                viewModel?.searchOrder(requireContext(),binding!!.search.text.toString())?.observe(viewLifecycleOwner) { ordersResponse ->
                    DialogUtils.dismissLoader()
                    ordersResponse?.let { response ->
                        response.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding!!.root, response.message!!)
                                binding!!.recylerOrder.visibility = View.GONE
                            } else {
                                //response.data!!.reverse()
                                if (response.data!!.size != 0) {
                                    loadRecycler(response.data!!, true)
                                } else {
                                    UiUtils.showSnack(binding!!.root, response.message!!)
                                    binding!!.recylerOrder.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            loadRecycler(orderlist,false)
        }
    }

    fun load(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getOrders(requireContext(),0)?.observe(viewLifecycleOwner) { ordersResponse ->
            DialogUtils.dismissLoader()
            ordersResponse?.let { response ->
                response.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, response.message!!)
                        binding!!.recylerOrder.visibility = View.GONE
                    } else {
                        //response.data!!.reverse()
                        if (response.data!!.size != 0) {
                            orderlist = response.data!!
                            loadRecycler(orderlist, false)
                        } else {
                            binding!!.recylerOrder.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun loadRecycler(data: ArrayList<OrderData>,isSearch:Boolean){
        binding!!.recylerOrder.visibility = View.VISIBLE
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false)
        binding!!.recylerOrder.layoutManager = layoutManager
        binding!!.recylerOrder.adapter = OrderListAdapter(
            this,
            requireContext(),
            data)
        if(!isSearch){
            scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                override fun onLoadMore(
                    page: Int,
                    totalItemsCount: Int,
                    view: RecyclerView?
                ) {
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list
                    //loadNextDataFromApi(page)
                    DialogUtils.showLoader(requireContext())
                    viewModel?.getOrders(requireContext(),page)?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    // UiUtils.showSnack(binding!!.root, response.message!!)
                                } else {
                                    it.data!!.reverse()
                                    if (it.data!!.size != 0) {
                                        orderlist.addAll(it.data!!)
                                        binding!!.recylerOrder.adapter!!.notifyItemInserted(
                                            orderlist.size - 1)
                                        // binding!!.recylerProduct.adapter!!.notifyItemRangeInserted(totalItemsCount,productlist.size - 1)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            binding!!.recylerOrder.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)
        }
    }
}