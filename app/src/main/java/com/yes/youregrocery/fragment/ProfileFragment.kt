package com.yes.youregrocery.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.*
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {
    var binding: FragmentProfileBinding? = null
    var viewModel: ViewModel? = null
    var dashBoardActivity: DashBoardActivity? = null
    var sharedHelper: SharedHelper? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        listener()
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.profile)
        dashBoardActivity!!.updateBadges(binding!!.header)
        getProfile()
        getMyReferrals()
        getReward()
        walletUpdate()
        return view
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        dashBoardActivity!!.searchTopLoad(binding!!.header.searchHome)
        binding!!.changepassword.setOnClickListener {
            startActivity(Intent(requireContext(), ChangePasswordActivity::class.java))
        }
        binding!!.deliveryaddress.setOnClickListener {
            startActivity(Intent(requireContext(), DeliveryAddressActivity::class.java))
        }
        binding!!.invitefriend.setOnClickListener {
            startActivity(Intent(requireContext(), InviteFriendActivity::class.java))
        }
        binding!!.myreferrals.setOnClickListener {
            startActivity(Intent(requireContext(), MyReferralsActivity::class.java))
        }
        binding!!.mywallet.setOnClickListener {
            dashBoardActivity!!.moveWalletActivity(true)
        }
        binding!!.header.wallet.walletframe.setOnClickListener {
            startActivity(Intent(requireContext(), MyWalletActivity::class.java))
        }
        binding!!.header.notification.notificationframe.setOnClickListener {
            dashBoardActivity!!.addFragment(NotificationFragment(), null, -1, -1)
        }
        binding!!.notificationTxt.setOnClickListener {
            dashBoardActivity!!.addFragment(NotificationFragment(), null, -1, -1)
        }
        binding!!.coupons.setOnClickListener {
            startActivity(Intent(requireContext(), CouponsActivity::class.java))
        }
        binding!!.logout.setOnClickListener {
            dashBoardActivity!!.logoutDialog()
        }
    }

    override fun onResume() {
        super.onResume()
        walletUpdate()
    }

    private fun walletUpdate(){
        if(sharedHelper!!.walletAmount == "" || sharedHelper!!.walletAmount == "0" || sharedHelper!!.walletAmount == "0.0"){
            val wallet = "${getString(R.string.wallet)} ${"("}${Constants.Common.CURRENCY}${"0)"}"
            binding!!.mywallet.text = wallet
        }
        else{
            val wallet = "${getString(R.string.wallet)} ${"("}${Constants.Common.CURRENCY}${sharedHelper!!.walletAmount}${")"}"
            binding!!.mywallet.text = wallet
        }
    }
    private fun getMyReferrals(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getMyReferrals(requireContext())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                        sharedHelper!!.referralAmount = Constants.IntentKeys.AMOUNT_DUMMY
                    } else {
                        sharedHelper!!.referralAmount = UiUtils.formattedValues(it.total)
                    }
                }
            }
        }
    }
    private fun getReward(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getReward(requireContext())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                    } else {
                        val point =
                            "${getString(R.string.reward_points)} ${":"} ${it.data!![0].total_avail.toString()}"
                        binding!!.rpoint.text = point
                    }
                }
            }
        }
    }
    private fun getProfile(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getProfile(requireContext())?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                    } else {
                        sharedHelper?.id = it.data!![0].user!!.id!!.toInt()
                        sharedHelper?.name = it.data!![0].user!!.name.toString()
                        sharedHelper?.mobileNumber = it.data!![0].user!!.mobile.toString()

                        binding!!.uname.text = it.data!![0].user!!.name.toString()
                        binding!!.umobile.text = it.data!![0].user!!.mobile.toString()
                        if (it.data!![0].user!!.country_code != null && !it.data!![0].user!!.country_code.equals(
                                Constants.IntentKeys.NULL)
                        ) {
                            sharedHelper?.countryCode = it.data!![0].user!!.country_code!!.toInt()
                            val code =
                                "${"+"}${sharedHelper!!.countryCode} ${it.data!![0].user!!.mobile.toString()}"
                            binding!!.umobile.text = code
                        } else {
                            sharedHelper?.countryCode = 0
                        }

                        if (it.data!![0].user!!.email != null && !it.data!![0].user!!.email.equals(
                                Constants.IntentKeys.NULL)
                        ) {
                            binding!!.umail.text = it.data!![0].user!!.email
                            sharedHelper?.email = it.data!![0].user!!.email!!
                        } else {
                            binding!!.umail.visibility = View.GONE
                            sharedHelper?.email = ""
                        }
                    }
                }
            }
        }
    }

}