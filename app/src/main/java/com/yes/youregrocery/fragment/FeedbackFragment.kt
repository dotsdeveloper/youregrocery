package com.yes.youregrocery.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.activity.MyWalletActivity
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentFeedbackBinding

class FeedbackFragment : Fragment(){
    var binding: FragmentFeedbackBinding? = null
    var viewModel: ViewModel? = null
    var dashBoardActivity: DashBoardActivity? = null
    var sharedHelper: SharedHelper? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFeedbackBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        listener()
        load()
        return view
    }

    fun load(){
        dashBoardActivity!!.searchTopLoad(binding!!.header.searchHome)
        dashBoardActivity!!.updateBadges(binding!!.header)
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.feed_back)
        binding!!.uname.setText(sharedHelper!!.name)
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.submit, sharedHelper!!.primaryColor, null)

        binding!!.submit.setOnClickListener {
            if(binding!!.feedback.text.toString().isNotEmpty()){
                DialogUtils.showLoader(requireContext())
                viewModel?.feedback(requireContext(),binding!!.feedback.text.toString())?.observe(viewLifecycleOwner) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding!!.root, it.message!!)
                            } else {
                                UiUtils.showSnack(binding!!.root, it.message!!)
                                binding!!.feedback.setText("")
                            }
                        }
                    }
                }
            }
        }
        binding!!.header.wallet.walletframe.setOnClickListener {
            startActivity(Intent(requireContext(), MyWalletActivity::class.java))
        }
        binding!!.header.notification.notificationframe.setOnClickListener {
            dashBoardActivity!!.addFragment(NotificationFragment(), null, -1, -1)
        }
    }

}