package com.yes.youregrocery.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentAboutBinding
import com.yes.youregrocery.utils.BaseUtils

class AboutFragment : Fragment() {
    var binding: FragmentAboutBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAboutBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        load()
        return view
    }

    fun load(){
        dashBoardActivity!!.searchTopLoad(binding!!.header.searchHome)
        UiUtils.relativeLayoutBgColor(binding!!.header.linear, sharedHelper!!.primaryColor, null)
        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = getString(R.string.about)
        binding!!.about.text = UiUtils.convertHtml(BaseUtils.nullCheckerStr(sharedHelper!!.shopWebData.about_content))
    }

}