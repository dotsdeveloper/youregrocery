package com.yes.youregrocery.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.OrderProductDetailsAdapter
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.R
import com.yes.youregrocery.`interface`.DialogCallBack
import com.yes.youregrocery.responses.Order
import com.yes.youregrocery.responses.OrderData
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentOrderDetailBinding

class OrderDetailFragment : Fragment() {
    var binding: FragmentOrderDetailBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    private var orderdata = OrderData()
    var dashBoardActivity: DashBoardActivity? = null

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentOrderDetailBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        listener()
        load()
        return view
    }

    fun load(){
        orderdata = requireArguments().getSerializable("key") as OrderData
        loadRecycler()
        track(BaseUtils.nullCheckerInt(orderdata.status))
        binding!!.orderid.text = orderdata.invoice
        binding!!.shop.text = orderdata.shopper!!.name
        binding!!.shopmobile.text = orderdata.shopper!!.mobile
        val subtotal = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.total)}"
        val total2 = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.grand_total)}"
        val total3 = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.paid_amount)}"
        binding!!.subtotal.text = subtotal
        binding!!.total2.text = total2
        binding!!.total3.text = total3
        val placedon = "${getString(R.string.placed_on)} ${":"} ${BaseUtils.getFormattedDateUtc(orderdata.created_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,"MMMM dd yyyy hh:mm aaa")}"
        binding!!.placedOn.text = placedon

        if(orderdata.delivered_at != null) {
            val deliveryon = "${getString(R.string.delivery_date_time)} ${":"} ${BaseUtils.getFormattedDateUtc(orderdata.delivered_at!!, Constants.ApiKeys.TIME_INPUT_FORMAT,"MMMM dd yyyy hh:mm aaa")}"
            binding!!.deliveryOn.text = deliveryon
        }
        else{
            binding!!.deliveryOn.visibility = View.GONE
        }

        if(UiUtils.formattedValues(orderdata.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
            val gst = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.gst_value)}"
            val gst1 = "${getString(R.string.gst)} ${"("}${orderdata.gst}${"%)"}"
            binding!!.linearGst.visibility = View.VISIBLE
            binding!!.gstAmount.text = gst
            binding!!.gstPer.text = gst1
        }
        else{
            binding!!.linearGst.visibility = View.GONE
        }

        if(orderdata.file != null){
            binding!!.file.visibility = View.VISIBLE
            UiUtils.loadImage(binding!!.file, orderdata.file)
        }
        else{
            binding!!.file.visibility = View.GONE
        }

        if(UiUtils.formattedValues(orderdata.charge_amount) != Constants.IntentKeys.AMOUNT_DUMMY) {
            val camount = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.charge_amount)}"
            binding!!.linearDeliverycharge.visibility = View.VISIBLE
            binding!!.deliveryAmount.text = camount
        }

        when {
            orderdata.order_type.toString().toInt() == 1 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.upi)}"
                binding!!.payMode.text = text
            }
            orderdata.order_type.toString().toInt() == 2 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.cash_on_delivery)}"
                binding!!.payMode.text = text
            }
            orderdata.order_type.toString().toInt() == 3 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.card)}"
                binding!!.payMode.text = text
            }
            orderdata.order_type.toString().toInt() == 4 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.wallet)}"
                binding!!.payMode.text = text
            }
            orderdata.order_type.toString().toInt() == 5 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.wallet)}${"+"}"
                binding!!.payMode.text = text
            }
        }
    }

    private fun loadRecycler(){
        for((index, value) in orderdata.items!!.withIndex()){
            value.product!!.category!!.ccount = getCatCount(orderdata,orderdata.items!![index].product!!.category!!.name)
            // orderdata.items!![index].product!!.category!!.ccount = getcatcount(orderdata,orderdata.items!![index].product!!.category!!.name.toString())
        }
        orderdata.items!!.sortBy { order: Order -> order.product!!.category!!.name }
        binding!!.recyclerProducts.layoutManager = GridLayoutManager(requireContext(),1, LinearLayoutManager.VERTICAL,false)
        binding!!.recyclerProducts.adapter = OrderProductDetailsAdapter(this, requireContext(), orderdata.items)
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.reorder, sharedHelper!!.primaryColor, null)
        UiUtils.buttonBgColor(binding!!.cancelOrder, sharedHelper!!.primaryColor, null)
        UiUtils.textViewTextColor(binding!!.optxt, sharedHelper!!.primaryColor, null)
        UiUtils.setTextViewDrawableColor(binding!!.optxt, sharedHelper!!.primaryColor,null)

        binding!!.back.setOnClickListener {
            dashBoardActivity!!.removeFragment(this)
        }
        binding!!.cancelOrder.setOnClickListener {
            DialogUtils.showAlertDialog(requireContext(),getString(R.string.are_you_sure_you_want_to_cancel_order),"",getString(R.string.ok),getString(R.string.cancel), object :
                DialogCallBack {
                override fun onPositiveClick(value: String) {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.cancelOrder(requireContext(),orderdata.id!!)?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding!!.root, it.message!!)
                                }
                                else {
                                    UiUtils.showSnack(binding!!.root, it.message!!)
                                    dashBoardActivity!!.removeAllFragment()
                                }
                            }
                        }
                    }
                }

                override fun onNegativeClick() {

                }
            })
        }
        binding!!.share.setOnClickListener {
            DialogUtils.showLoader(requireContext())
            viewModel?.getInvoice(requireContext(),orderdata.id!!)?.observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        } else {
                            val shareIntent = Intent()
                            shareIntent.action = Intent.ACTION_SEND
                            shareIntent.type = "text/plain"
                            val string = it.data
                            shareIntent.putExtra(Intent.EXTRA_TEXT, string)
                            startActivity(Intent.createChooser(shareIntent,
                                getString(R.string.share_with)))
                        }
                    }
                }
            }
        }
        binding!!.reorder.setOnClickListener {
            DialogUtils.showLoader(requireContext())
            viewModel?.reOrder(requireContext(),orderdata.id!!)?.observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        } else {
                            dashBoardActivity!!.addFragment(BasketFragment(), null, -1, -1)
                        }
                    }
                }
            }
        }
    }

    private fun getCatCount(orderdata: OrderData, c: String): Int {
        var count = 0
        for(items in orderdata.items!!){
            if(items.product!!.category!!.name == c){
                count++
            }
        }
        return count
    }

    private fun track(status: Int){
        when (status) {
            1 -> {
                binding!!.cancelOrder.visibility = View.VISIBLE
                UiUtils.textViewTextColor(binding!!.txtOrderplaced,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderplaced,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderaccepted,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line1,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderaccepted,null,R.color.grey)

                UiUtils.textViewTextColor(binding!!.txtOrderassigned,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line2,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderassigned,null,R.color.grey)

                UiUtils.textViewTextColor(binding!!.txtOrderdelivered,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line3,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderdelivered,null,R.color.grey)
            }
            2 -> {
                binding!!.cancelOrder.visibility = View.GONE
                UiUtils.textViewTextColor(binding!!.txtOrderplaced,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderplaced,sharedHelper!!.primaryColor,null)

                binding!!.viewOrdercanceled.visibility = View.VISIBLE
                binding!!.line0.visibility = View.VISIBLE
                binding!!.txtOrdercanceled.visibility = View.VISIBLE
                UiUtils.textViewTextColor(binding!!.txtOrdercanceled,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrdercanceled,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line1,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderaccepted,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line0,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderaccepted,null,R.color.grey)

                UiUtils.textViewTextColor(binding!!.txtOrderassigned,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line2,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderassigned,null,R.color.grey)

                UiUtils.textViewTextColor(binding!!.txtOrderdelivered,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line3,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderdelivered,null,R.color.grey)
            }
            3 -> {
                binding!!.cancelOrder.visibility = View.GONE
            }
            4 -> {
                binding!!.cancelOrder.visibility = View.GONE
                UiUtils.textViewTextColor(binding!!.txtOrderplaced,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderplaced,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderaccepted,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line1,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderaccepted,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderassigned,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line2,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderassigned,null,R.color.grey)

                UiUtils.textViewTextColor(binding!!.txtOrderdelivered,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line3,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderdelivered,null,R.color.grey)
            }
            5 -> {
                binding!!.cancelOrder.visibility = View.GONE
                binding!!.txtOrderplaced.setTextColor(Color.parseColor(sharedHelper!!.primaryColor))
                UiUtils.viewBgTint(binding!!.viewOrderplaced,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderaccepted,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line1,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderaccepted,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderassigned,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line2,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderassigned,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderdelivered,null,R.color.black)
                UiUtils.viewBgTint(binding!!.line3,null,R.color.grey)
                UiUtils.viewBgTint(binding!!.viewOrderdelivered,null,R.color.grey)
            }
            6 -> {
                binding!!.cancelOrder.visibility = View.GONE

                UiUtils.textViewTextColor(binding!!.txtOrderplaced,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderplaced,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderaccepted,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line1,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderaccepted,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderassigned,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line2,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderassigned,sharedHelper!!.primaryColor,null)

                UiUtils.textViewTextColor(binding!!.txtOrderdelivered,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.line3,sharedHelper!!.primaryColor,null)
                UiUtils.viewBgTint(binding!!.viewOrderdelivered,sharedHelper!!.primaryColor,null)
            }
        }
    }

}