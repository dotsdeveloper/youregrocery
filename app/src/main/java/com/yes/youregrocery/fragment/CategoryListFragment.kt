package com.yes.youregrocery.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.CategoryListAdapter
import com.yes.youregrocery.responses.CategoryList
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentPopularCategoryBinding
import com.yes.youregrocery.session.Constants

class CategoryListFragment : Fragment() {
    var binding: FragmentPopularCategoryBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity
    var products = ArrayList<Products>()
    private var categorydata: ArrayList<CategoryList> = ArrayList()
    var type = 4
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentPopularCategoryBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        loadCategory()
        //return inflater.inflate(R.layout.fragment_popular_category, container, false)
        return view
    }

    fun listener(){
        UiUtils.relativeLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        binding!!.back.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                search()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        binding!!.search.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId,_ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search()
                return@OnEditorActionListener true
            }
            false
        })
        binding!!.close.setOnClickListener {
            binding!!.search.setText("")
        }
    }

    private fun loadCategory(){
        binding!!.top.text = requireArguments().getString(Constants.IntentKeys.TITLE)
        @Suppress("UNCHECKED_CAST")
        categorydata = requireArguments().getSerializable(Constants.IntentKeys.DATA) as ArrayList<CategoryList>
        binding!!.cardSearch.visibility = View.VISIBLE
        for (i in 0 until categorydata.size) {
            categorydata[i].isthissubcat = false
            categorydata[i].isthiscat = true
        }

        if(requireArguments().getBoolean(Constants.IntentKeys.IS_CAT)){
            binding!!.recylerPopular.layoutManager = GridLayoutManager(
                requireContext(), 2,
                LinearLayoutManager.VERTICAL, false
            )
            binding!!.recylerPopular.adapter = CategoryListAdapter(
                dashBoardActivity,
                type,
                null,
                this,
                requireContext(),
                categorydata
            )
        }
        else{
            type = 6
            binding!!.recylerPopular.layoutManager = GridLayoutManager(
                requireContext(), 2,
                LinearLayoutManager.VERTICAL, false
            )
            binding!!.recylerPopular.adapter = CategoryListAdapter(
                dashBoardActivity,
                type,
                null,
                this,
                requireContext(),
                categorydata
            )
        }
    }

    fun search(){
        if(binding!!.search.text.isNotEmpty()){
            val searchcategorylist: ArrayList<CategoryList> = ArrayList()
            for(items in categorydata){
                if(items.name!!.toString().contains(binding!!.search.text.toString(),true)){
                    searchcategorylist.add(items)
                }
            }

            if(searchcategorylist.size == 0){
                binding!!.notfound.visibility = View.VISIBLE
                // UiUtils.showSnack(binding!!.root,"No Products Available")
            }
            else{
                binding!!.notfound.visibility = View.GONE
            }

            binding!!.recylerPopular.layoutManager = GridLayoutManager(
                requireContext(), 2,
                LinearLayoutManager.VERTICAL, false
            )
            binding!!.recylerPopular.adapter = CategoryListAdapter(
                dashBoardActivity,
                type,
                null,
                this,
                requireContext(),
                searchcategorylist
            )
        }
        else{
            binding!!.recylerPopular.layoutManager = GridLayoutManager(
                requireContext(), 2,
                LinearLayoutManager.VERTICAL, false
            )
            binding!!.recylerPopular.adapter = CategoryListAdapter(
                dashBoardActivity,
                type,
                null,
                this,
                requireContext(),
                categorydata
            )
        }
    }

}