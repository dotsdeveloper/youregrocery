package com.yes.youregrocery.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.R
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.ProductListAdapter
import com.yes.youregrocery.adapter.SubCategoryAdapter
import com.yes.youregrocery.databinding.FragmentHomeProductsBinding
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.others.EndlessRecyclerViewScrollListener
import com.yes.youregrocery.responses.Products
import com.yes.youregrocery.session.Constants
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.utils.BaseUtils
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils

class ProductListFragment : Fragment() {
    var binding: FragmentHomeProductsBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity
    private lateinit var productListAdapter: ProductListAdapter
    private var oldproductlist: ArrayList<Products> = ArrayList()
    private var productlist: ArrayList<Products> = ArrayList()
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    var categoryid:Int = 0
    var subcategoryid:Int = 0
    private var categoryname: String = ""
    var iscategory = false
    var issubcategory = false
    private var issearch = false
    var lpos:Int = 0
    private var pagestr:String = ""
    private var noffset:Int = 0
    private var soffset:Int = 0
    private var pcount = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentHomeProductsBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        load()
        return view
    }

    fun listener(){
        UiUtils.linearLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor, null)
        binding!!.back.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                searchNew()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        binding!!.search.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                BaseUtils.closeKeyBoard(binding!!.search,requireContext())
                //search()
                return@OnEditorActionListener true
            }
            false
        })

        binding!!.close.setOnClickListener {
            binding!!.search.setText("")
        }
    }

    fun search(){
        if(binding!!.search.text.isNotEmpty()){
            binding!!.close.visibility = View.VISIBLE
            val sproductlist: ArrayList<Products> = ArrayList()
            for(items in oldproductlist){
                if(items.name!!.toString().contains(binding!!.search.text.toString(),true)){
                    sproductlist.add(items)
                }
            }
            loadRecycler(sproductlist)
        }
        else{
            binding!!.close.visibility = View.GONE
            loadRecycler(oldproductlist)
        }
    }

    private fun loadSearchStart(){
        soffset = 0
        issearch = true
        binding!!.close.visibility = View.VISIBLE
        binding!!.txtCount.visibility = View.GONE
        binding!!.progress.visibility = View.VISIBLE
        binding!!.recylerProduct.visibility = View.GONE
        binding!!.notfound.visibility = View.GONE
    }
    private fun loadSearchEnd(){
        issearch = false
        binding!!.progress.visibility = View.GONE
        binding!!.close.visibility = View.GONE
        loadRecycler(oldproductlist)
    }
    fun searchNew(){
        if(binding!!.search.text.isNotEmpty()){
            loadSearchStart()
            if(pagestr == Constants.IntentKeys.LOAD_BRAND_P){
                viewModel?.searchProductsByBrand(requireContext(),binding!!.search.text.toString(),categoryid,soffset)?.observe(viewLifecycleOwner) {
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                noProducts()
                                //UiUtils.showSnack(binding!!.root, it.message!!)
                            } else {
                                it.product_data?.let { data ->
                                    loadRecycler(data)
                                }
                            }
                        }
                    }
                }
            }
            else{
                viewModel?.searchProducts(requireContext(),binding!!.search.text.toString(),categoryid,subcategoryid,soffset)?.observe(viewLifecycleOwner) {
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                noProducts()
                                //UiUtils.showSnack(binding!!.root, it.message!!)
                            } else {
                                it.product_data?.let { data ->
                                    loadRecycler(data)
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            loadSearchEnd()
        }
    }
    fun reload(){
        productListAdapter.list = dashBoardActivity.isCartCheckData(productlist)
        binding!!.recylerProduct.adapter!!.notifyItemChanged(lpos)
    }
    fun load(){
        pagestr = requireArguments().getString(Constants.IntentKeys.PAGE).toString()
        if(requireArguments().getString(Constants.IntentKeys.CNAME)!!.isNotEmpty()){
            binding!!.top.text = requireArguments().getString(Constants.IntentKeys.CNAME).toString()
        }

        if(!requireArguments().getBoolean(Constants.IntentKeys.IS_COME)){
            when (pagestr) {
                Constants.IntentKeys.PRODUCT -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.getHomeProductList(requireContext())?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    noProducts()
                                    //UiUtils.showSnack(binding!!.root, it.message!!)
                                } else {
                                    it.product_data?.let { data ->
                                        loadRecycler(data)
                                    }
                                }
                            }
                        }
                    }
                }
                Constants.IntentKeys.NEW_ARRIVALS -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.getNewArrivalsProducts(requireContext())?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    noProducts()
                                    //UiUtils.showSnack(binding!!.root, it.message!!)
                                } else {
                                    it.product_data?.let { data ->
                                        loadRecycler(data)
                                    }
                                }
                            }
                        }
                    }
                }
                Constants.IntentKeys.BESTSELLER -> {
                    noffset = 0
                    DialogUtils.showLoader(requireContext())
                    viewModel?.getBestSellerProducts(requireContext(),noffset)?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    //UiUtils.showSnack(binding!!.root, it.message!!)
                                    noProducts()
                                } else {
                                    it.product_data?.let { data ->
                                        loadRecycler(data)
                                    }
                                }
                            }
                        }
                    }
                }
                Constants.IntentKeys.LOAD_CATEGORY_P -> {
                    loadCategoryProducts(requireArguments().getInt(Constants.IntentKeys.CID))
                }
                Constants.IntentKeys.LOAD_SUBCATEGORY_P -> {
                    loadSubCategoryProducts(requireArguments().getInt(Constants.IntentKeys.CID), requireArguments().getInt(Constants.IntentKeys.S_C_ID))
                }
                Constants.IntentKeys.LOAD_SUBCATEGORY_LIST -> {
                    loadSubCategoryList(requireArguments().getInt(Constants.IntentKeys.CID),requireArguments().getString(Constants.IntentKeys.CNAME).toString())
                }
                Constants.IntentKeys.LOAD_BRAND_P -> {
                    loadBrandProducts(requireArguments().getInt(Constants.IntentKeys.CID))
                }
            }
        }
    }

    private fun loadSubCategoryList(cid: Int, cname: String){
        categoryid = cid
        categoryname = cname
        binding!!.top.text = cname
        DialogUtils.showLoader(requireContext())
        viewModel?.getSubCategory(requireContext(),cid)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                    } else {
                        it.category_data.let { data ->
                            if (data.size == 0) {
                                binding!!.recylerCategory.visibility = View.GONE
                            } else {
                                binding!!.recylerCategory.visibility = View.VISIBLE
                                for (i in 0 until data.size) {
                                    data[i].subcategory_count = 0
                                    data[i].isthissubcat = true
                                    data[i].isthiscat = false
                                    // data[i].product_data = products
                                }
                                data.sortedBy { it1 ->
                                    it1.ordering
                                }
                                binding!!.recylerCategory.layoutManager = GridLayoutManager(
                                    requireContext(), 1,
                                    LinearLayoutManager.HORIZONTAL, false
                                )
                                binding!!.recylerCategory.adapter = SubCategoryAdapter(
                                    this,
                                    requireContext(),
                                    data
                                )

                                loadSubCategoryProducts(cid, data[0].id!!)
                            }
                        }
                    }
                }
            }
        }
    }
    private fun showPCount(pcount : Int){
        if(pagestr == Constants.IntentKeys.LOAD_SUBCATEGORY_P){
            if(pcount > 0){
                binding!!.txtCount.visibility = View.VISIBLE
                if(pcount == 1){
                    val text = "$pcount ${getString(R.string.item)}"
                    binding!!.txtCount.text = text
                }
                else{
                    val text = "$pcount ${getString(R.string.items)}"
                    binding!!.txtCount.text = text
                }
            }
            else{
                binding!!.txtCount.visibility = View.GONE
            }
        }
        else{
            binding!!.txtCount.visibility = View.GONE
        }
    }
    private fun loadCategoryProducts(cid: Int){
        iscategory = true
        issubcategory = false
        categoryid = cid
        noffset = 0
        DialogUtils.showLoader(requireContext())
        viewModel?.productsByCategory(requireContext(), cid,noffset)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //  UiUtils.showSnack(binding!!.root, it.message!!)
                        noProducts()
                    } else {
                        it.product_data?.let { data ->
                            data.reverse()
                            loadRecycler(data)
                        }
                    }
                }
            }
        }
    }
    private fun loadBrandProducts(bid: Int){
        categoryid = bid
        noffset = 0
        DialogUtils.showLoader(requireContext())
        viewModel?.productsByBrand(requireContext(), categoryid,noffset)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        // UiUtils.showSnack(binding!!.root, it.message!!)
                        noProducts()
                    } else {
                        it.product_data?.let { data ->
                            data.reverse()
                            loadRecycler(data)
                        }
                    }
                }
            }
        }
    }
    fun loadSubCategoryProducts(cid: Int, scid: Int) {
        iscategory = true
        issubcategory = true
        categoryid = cid
        subcategoryid = scid
        noffset = 0
        DialogUtils.showLoader(requireContext())
        viewModel?.productsBySubCategory(requireContext(), cid,scid,noffset)?.observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding!!.root, it.message!!)
                        noProducts()
                    } else {
                        it.product_data?.let { data ->
                            pagestr = Constants.IntentKeys.LOAD_SUBCATEGORY_P
                            data.reverse()
                            loadRecycler(data)
                           /* if (it.pcount != null && it.pcount!! > 0) {
                                pcount = it.pcount!!
                                showPCount(it.pcount!!)
                            } else {
                                showPCount(0)
                            }*/
                        }
                    }
                }
            }
        }
    }
    private fun noProducts(){
        binding!!.progress.visibility = View.GONE
        binding!!.notfound.visibility = View.VISIBLE
        binding!!.recylerProduct.visibility = View.GONE
        binding!!.txtCount.visibility = View.GONE
    }
    private fun loadRecycler(data: ArrayList<Products>) {
        if(data.size != 0){
            binding!!.progress.visibility = View.GONE
            binding!!.notfound.visibility = View.GONE
            binding!!.recylerProduct.visibility = View.VISIBLE
            productlist = dashBoardActivity.isCartCheckData(data)
            pcount = productlist.size
            val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false)
            binding!!.recylerProduct.layoutManager = layoutManager
            productListAdapter = ProductListAdapter(dashBoardActivity, 2, null, this,null, requireContext(), productlist,false)
            productListAdapter.isscroolcomplete = false
            binding!!.recylerProduct.adapter = productListAdapter
            scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                override fun onLoadMore(
                    page: Int,
                    totalItemsCount: Int,
                    view: RecyclerView?
                ) {
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list
                    //loadNextDataFromApi(page)
                    Log.d("vgb",""+page)
                    loadMoreData()
                }
            }
            binding!!.recylerProduct.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)
            if(!issearch){
                oldproductlist = ArrayList()
                for(items in productlist){
                    oldproductlist.add(items)
                }
                showPCount(pcount)
            }
            else{
                showPCount(productlist.size)
            }
        }
        else{
            noProducts()
        }
    }
    fun loadMoreData(){
        if(issearch){
            soffset+=1
            if(pagestr == Constants.IntentKeys.LOAD_BRAND_P){
                DialogUtils.showLoader(requireContext())
                viewModel?.searchProductsByBrand(requireContext(),binding!!.search.text.toString(),categoryid,soffset)?.observe(viewLifecycleOwner) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                //UiUtils.showSnack(binding!!.root, it.message!!)
                                scrollComplete()
                            } else {
                                it.product_data?.let { data ->
                                    updateRecyclerScrool(data)
                                }
                            }
                        }
                    }
                }
            }
            else{
                DialogUtils.showLoader(requireContext())
                viewModel?.searchProducts(requireContext(),binding!!.search.text.toString(),categoryid,subcategoryid,soffset)?.observe(viewLifecycleOwner) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                //UiUtils.showSnack(binding!!.root, it.message!!)
                                scrollComplete()
                            } else {
                                it.product_data?.let { data ->
                                    updateRecyclerScrool(data)
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            noffset+=1
            when (pagestr) {
                Constants.IntentKeys.PRODUCT -> {
                    scrollComplete()
                }
                Constants.IntentKeys.NEW_ARRIVALS -> {
                    scrollComplete()
                }
                Constants.IntentKeys.BESTSELLER -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.getBestSellerProducts(requireContext(), noffset)
                        ?.observe(viewLifecycleOwner) {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        //UiUtils.showSnack(binding!!.root, it.message!!)
                                        scrollComplete()
                                    } else {
                                        it.product_data?.let { data ->
                                            updateRecyclerScrool(data)
                                        }
                                    }
                                }
                            }
                        }
                }
                Constants.IntentKeys.LOAD_CATEGORY_P -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.productsByCategory(requireContext(), categoryid, noffset)
                        ?.observe(viewLifecycleOwner) {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        scrollComplete()
                                        // UiUtils.showSnack(binding!!.root, it.message!!)
                                    } else {
                                        it.product_data?.let { data ->
                                            data.reverse()
                                            updateRecyclerScrool(data)
                                        }
                                    }
                                }
                            }
                        }
                }
                Constants.IntentKeys.LOAD_SUBCATEGORY_P -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.productsBySubCategory(
                        requireContext(),
                        categoryid,
                        subcategoryid,
                        noffset
                    )?.observe(viewLifecycleOwner) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    // UiUtils.showSnack(binding!!.root, it.message!!)
                                    scrollComplete()
                                } else {
                                    it.product_data?.let { data ->
                                        data.reverse()
                                        updateRecyclerScrool(data)
                                    }
                                }
                            }
                        }
                    }
                }
                Constants.IntentKeys.LOAD_SUBCATEGORY_LIST -> {

                }
                Constants.IntentKeys.LOAD_BRAND_P -> {
                    DialogUtils.showLoader(requireContext())
                    viewModel?.productsByBrand(requireContext(), categoryid, noffset)
                        ?.observe(viewLifecycleOwner) {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        scrollComplete()
                                        // UiUtils.showSnack(binding!!.root, it.message!!)
                                    } else {
                                        it.product_data?.let { data ->
                                            data.reverse()
                                            updateRecyclerScrool(data)
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }
    }
    private fun scrollComplete(){
        productListAdapter.isscroolcomplete = true
        binding!!.recylerProduct.adapter!!.notifyItemChanged(productlist.size - 1)
    }
    private fun updateRecyclerScrool(data: ArrayList<Products>) {
        if(data.size != 0){
            productListAdapter.isscroolcomplete = false
            val ndata : ArrayList<Products> = offsetDublicate(data)
            if(ndata.size > 0){
                if(issearch){
                    productlist.addAll(dashBoardActivity.isCartCheckData(ndata))
                    binding!!.recylerProduct.adapter!!.notifyItemInserted(productlist.size - 1)
                    showPCount(productlist.size)
                    // binding!!.recylerProduct.adapter!!.notifyItemRangeInserted(totalItemsCount,productlist.size - 1)
                }
                else{
                    oldproductlist.addAll(dashBoardActivity.isCartCheckData(ndata))
                    productlist.addAll(dashBoardActivity.isCartCheckData(ndata))
                    binding!!.recylerProduct.adapter!!.notifyItemInserted(productlist.size - 1)
                    showPCount(productlist.size)
                    // binding!!.recylerProduct.adapter!!.notifyItemRangeInserted(totalItemsCount,productlist.size - 1)
                }
            }
            else{
                loadMoreData()
            }
        }
        else{
            scrollComplete()
        }
    }
    private fun offsetDublicate(data: ArrayList<Products>): ArrayList<Products> {
        val ndata:ArrayList<Products> = ArrayList()
        for(i in 0 until data.size){
           if(!isProduct(data[i])){
               ndata.add(data[i])
           }
        }
        return ndata
    }
    private fun isProduct(products: Products):Boolean{
        for(items in productlist){
            if(items.id == products.id){
                return true
            }
        }
        return false
    }

}