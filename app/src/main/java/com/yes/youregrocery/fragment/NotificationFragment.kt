package com.yes.youregrocery.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.youregrocery.activity.DashBoardActivity
import com.yes.youregrocery.adapter.NotificationListAdapter
import com.yes.youregrocery.R
import com.yes.youregrocery.session.SharedHelper
import com.yes.youregrocery.others.SwipeHelper
import com.yes.youregrocery.utils.DialogUtils
import com.yes.youregrocery.utils.UiUtils
import com.yes.youregrocery.network.ViewModel
import com.yes.youregrocery.databinding.FragmentNotificationBinding
import com.yes.youregrocery.others.EndlessRecyclerViewScrollListener
import com.yes.youregrocery.responses.NotificationData

class NotificationFragment : Fragment() {
    var binding: FragmentNotificationBinding? = null
    var viewModel: ViewModel? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    private var notificationlist: ArrayList<NotificationData> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentNotificationBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        getNotify()
        return view
    }

    fun listener(){
        dashBoardActivity.clearBottomNav()
        UiUtils.relativeLayoutBgColor(binding!!.linear, sharedHelper!!.primaryColor,null)
        binding!!.back.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
        binding!!.delete.setOnClickListener {
            DialogUtils.showLoader(requireContext())
            viewModel?.deleteNotifications(requireContext(),0)?.observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        } else {
                            getNotify()
                        }
                    }
                }
            }
        }
    }

    private fun getNotify(){
        DialogUtils.showLoader(requireContext())
        viewModel?.getNotifications(requireContext(),0)?.observe(viewLifecycleOwner) { response ->
            DialogUtils.dismissLoader()
            response?.let {
                response.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, response.message!!)
                        binding!!.nonotify.visibility = View.VISIBLE
                        binding!!.recyclerNotification.visibility = View.GONE
                        sharedHelper!!.notifyCount = "0"
                    } else {
                        binding!!.nonotify.visibility = View.GONE
                        binding!!.recyclerNotification.visibility = View.VISIBLE
                        val layoutManager = GridLayoutManager(requireContext(), 1, LinearLayoutManager.VERTICAL, false)
                        binding!!.recyclerNotification.layoutManager = layoutManager
                        notificationlist = response.data!!
                        val adapter = NotificationListAdapter(this, requireContext(), notificationlist)
                        binding!!.recyclerNotification.adapter = adapter
                        var count = 0
                        for (items in response.data!!) {
                            if (items.status == 1) {
                                count++
                            }
                        }

                        sharedHelper!!.notifyCount = count.toString()
                        val swipeHelper: SwipeHelper = object : SwipeHelper(requireContext(), binding!!.recyclerNotification) {
                                @SuppressLint("NotifyDataSetChanged")
                                override fun instantiateUnderlayButton(
                                    viewHolder: RecyclerView.ViewHolder,
                                    underlayButtons: ArrayList<UnderlayButton>
                                ) {
                                    underlayButtons.add(UnderlayButton(
                                        getString(R.string.delete),
                                        R.drawable.ic_baseline_delete_24,
                                        Color.parseColor(sharedHelper!!.primaryColor)
                                    ) { pos: Int ->
                                        DialogUtils.showLoader(requireContext())
                                        viewModel?.deleteNotifications(requireContext(),
                                            response.data!![pos].id!!)
                                            ?.observe(viewLifecycleOwner) {
                                                DialogUtils.dismissLoader()
                                                it?.let {
                                                    it.error?.let { error ->
                                                        if (error) {
                                                            UiUtils.showSnack(binding!!.root,
                                                                it.message!!)
                                                        } else {
                                                            adapter.list!!.removeAt(pos)
                                                            adapter.notifyItemRemoved(pos)
                                                        }
                                                    }
                                                }
                                            }
                                    })
                                }
                            }
                        swipeHelper.attachSwipe()

                        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                            override fun onLoadMore(
                                page: Int,
                                totalItemsCount: Int,
                                view: RecyclerView?
                            ) {
                                // Triggered only when new data needs to be appended to the list
                                // Add whatever code is needed to append new items to the bottom of the list
                                //loadNextDataFromApi(page)
                                Log.d("vgb", "" + page)
                                loadMoreData(page)
                            }
                        }
                        binding!!.recyclerNotification.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)
                    }
                }
            }
        }
    }

    fun loadMoreData(page: Int) {
        DialogUtils.showLoader(requireContext())
        viewModel?.getNotifications(requireContext(),page)?.observe(viewLifecycleOwner) { response ->
            DialogUtils.dismissLoader()
            response?.let {
                response.error?.let { error ->
                    if (error) {
                       // UiUtils.showSnack(binding!!.root, response.message!!)
                    }
                    else {
                        if(response.data!!.isNotEmpty()){
                            var count = 0
                            for (items in response.data!!) {
                                if (items.status == 1) {
                                    count++
                                }
                            }
                            sharedHelper!!.notifyCount = sharedHelper!!.notifyCount+count
                            notificationlist.addAll(response.data!!)
                            binding!!.recyclerNotification.adapter!!.notifyItemInserted(notificationlist.size - 1)
                        }
                    }
                }
            }
        }
    }

    fun readNotify(id:Int) {
        DialogUtils.showLoader(requireContext())
            viewModel?.readNotifications(requireContext(),id)?.observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        } else {
                            getNotify()
                        }
                    }
                }
            }

    }

}